#! /usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'fileutils'
require 'digest/md5'
require 'date'
require 'fileutils'
require 'cgi'
require 'timeout'
require 'socket'

require 'mysql'

Thread.abort_on_exception = true

begin
	load 'conf/mailmagic.conf'
rescue LoadError
	dir = File.expand_path(File.dirname(__FILE__))
	begin
		load File.join(dir, 'conf/mailmagic.conf')
	rescue LoadError
		STDERR.puts 'Cannot find conf/mailmagic.conf, please copy and edit conf/mailmagic.conf.example'
		exit
	end
rescue SyntaxError => e
	STDERR.puts 'Error loading conf/mailmagic.conf config file: '+e.message
	exit
end

begin
	require 'contrib/mailparser'
rescue LoadError
	dir = File.expand_path(File.dirname(__FILE__))
	begin
		require File.join(dir, 'contrib/mailparser')
	rescue LoadError
		STDERR.puts 'Could not locate mailparser library'
		exit
	end
end

begin
	require 'contrib/hpricot_scrub'
rescue LoadError
	dir = File.expand_path(File.dirname(__FILE__))
	begin
		require File.join(dir, 'contrib/hpricot_scrub')
	rescue LoadError
		STDERR.puts 'Could not locate html scrubber'
		exit
	end
end

begin
	sock = UNIXSocket.new(MailMagicConf::SOCKET_FILE)
rescue Errno::ENOENT, Errno::ECONNREFUSED => e
	File.delete(MailMagicConf::SOCKET_FILE) if File.socket?(MailMagicConf::SOCKET_FILE)
	if pid = fork()
		Process.detach(pid)
		sleep 0.5
		retry
	else
		begin
			require 'contrib/ami'
		rescue LoadError
			dir = File.expand_path(File.dirname(__FILE__))
			begin
				require File.join(dir, 'contrib/ami')
			rescue LoadError
				STDERR.puts 'Could not locate AMI library'
				exit
			end
		end
		server = UNIXServer.new(MailMagicConf::SOCKET_FILE)
		ami = AMIConnection.new(MailMagicConf::AMI_HOST, MailMagicConf::AMI_PORT)
		ami.login(MailMagicConf::AMI_USERNAME, MailMagicConf::AMI_PASSWORD)
		loop do
			sock = server.accept
			cmd = sock.gets
			unless cmd.nil?
				begin
					sock.puts 'OK'
				rescue Errno::EPIPE
					STDERR.puts "Client process disconnected before ACK"
				end

				if cmd.chomp == 'discard'
					STDERR.puts 'client discarded email'
					next
				end

				Thread.new do

					from, time, mailid, brandid, queue, caseid = cmd.split(" ")

					begin
						ret = ami.action('Originate', 'Channel'=>'Local/qinject@queue-inject',
										'Timeout'=>604800000, 'Application'=>'Echo',
										'Variable'=>"TYPE=email\r\nVariable: FROM=#{from}\r\nVariable: DATE=#{time}\r\nVariable: MAILID=#{mailid}\r\nVariable: BRANDID=#{brandid}\r\nVariable: QUEUE=#{queue}\r\nVariable: QTIMEOUT=604800\r\nVariable: CASEID=#{caseid}", 'Async'=>1)
					rescue Errno::EPIPE
						ami.reconnect
						retry
					end

					unless ret.to_s.include? 'Success'
						STDERR.p ret
					end
				end

			else
				STDERR.puts "Client process sent nil! MAIL NOT QUEUED!"
			end
		end
	end
end

begin
	dbh = Mysql.real_connect(MailMagicConf::DB_HOST, MailMagicConf::DB_USERNAME, MailMagicConf::DB_PASSWORD, MailMagicConf::DATABASE)
rescue MysqlError => e
	STDERR.print "Error code: ", e.errno, "\n"
	STDERR.print "Error message: ", e.error, "\n"
	exit
end

class DateTime #little conversion method
	def to_time
		Time.local(year,month,day,hour,min,sec)
	end
end 

def generate_unique
	Digest::MD5.hexdigest(Time.now.to_s)+'-'+rand(9999).to_s
end

def wordwrap(string)
	res = ''
	string.each do |line|
		while line.length > 78
			if i = line.rindex(' ', 77)
				res << line[0..i]+"\n"
				line = line[i+1..-1]
			else
				res << line[0..78]+"\n"
				line = line[79..-1]
			end
		end
		res << line
	end
	return res.gsub(/\n\s+\n/, "\n\n").gsub(/[\n]{3,}/, "\n\n")
end

stdin = ARGV.delete '-'

unless stdin
	STDERR.puts "only accepts piped input following a '-'; YOU'RE DOING SOMETHING WRONG!"
	exit(-42)
end

mail = MailParser.parse_message STDIN

mail['return-path'] = mail[:from][0] unless mail['return-path']

xoriginalto = mail[:header]['delivered-to']

#STDERR.puts "mailmagic original: #{xoriginalto}"
#STDERR.puts "mailmagic to: #{mail[:to]}"

mailfirstpart = mail[:to][0].split('@', 2)[0]

mailto = xoriginalto.to_s.split('@', 2)[0]

if mailto.include? '-'
	toaddr = mailto.split('-', 2)[1]
else
	STDERR.puts "To address '#{mailto}' does not contain a '-' and therefore cannot be correctly parsed. Discarding email."
	exit(1)
end

body = mail[:body]

if mail[:multipart]
	#p mail[:parts].map{|x| "#{x[:type]}/#{x[:subtype]} - #{x[:multipart]}"}
	if text = mail[:parts].detect{|y| y[:type] == 'text' and y[:subtype] == 'plain'}
		# Multipart with text/plain (that's fine)
		body = text[:body]
	elsif text = mail[:parts].detect{|y| y[:type] == 'text' and y[:subtype] == 'html'}
		# Multipart with a text/html component but no text/plain, sucks
		body = wordwrap(Hpricot(text[:body].gsub("\n", '').gsub('&nbsp;', ' ')).scrub.inner_html)
		body << "\n\nHTML content has been scrubbed"
	elsif submail = mail[:parts].detect{|y| y[:multipart]}
		if text = submail[:parts].detect{|y| y[:type] == 'text' and y[:subtype] == 'plain'}
			# Multipart with text/plain (that's fine)
			body = text[:body]
		elsif text = submail[:parts].detect{|y| y[:type] == 'text' and y[:subtype] == 'html'}
			# Multipart with a text/html component but no text/plain, sucks
			body = wordwrap(Hpricot(text[:body].gsub("\n", '').gsub('&nbsp;', ' ')).scrub.inner_html)
			body << "\n\nHTML content has been scrubbed"
		elsif submail[:multipart] and submail2 = submail[:parts].detect{|y| y[:multipart]}
			if text = submail2[:parts].detect{|y| y[:type] == 'text' and y[:subtype] == 'plain'}
				# Multipart with text/plain (that's fine)
				body = text[:body]
			elsif text = submail2[:parts].detect{|y| y[:type] == 'text' and y[:subtype] == 'html'}
				# Multipart with a text/html component but no text/plain, sucks
				body = wordwrap(Hpricot(text[:body].gsub("\n", '').gsub('&nbsp;', ' ')).scrub.inner_html)
				body << "\n\nHTML content has been scrubbed"
			elsif submail2[:multipart] and submail3 = submail[:parts].detect{|y| y[:multipart]}
				if text = submail3[:parts].detect{|y| y[:type] == 'text' and y[:subtype] == 'plain'}
					# Multipart with text/plain (that's fine)
					body = text[:body]
				elsif text = submail3[:parts].detect{|y| y[:type] == 'text' and y[:subtype] == 'html'}
					# Multipart with a text/html component but no text/plain, sucks
					body = wordwrap(Hpricot(text[:body].gsub("\n", '').gsub('&nbsp;', ' ')).scrub.inner_html)
					body << "\n\nHTML content has been scrubbed"
				end
			end
		end
	else
		# WTF
		STDERR.puts 'Received multipart email without a text/plain or a text/html component!'
		#exit
	end
elsif mail[:type] == 'text' and mail[:subtype] == 'html'
	# pure HTML awfulness
	body = wordwrap(Hpricot(mail[:body].gsub("\n", '').gsub('&nbsp;', ' ')).scrub.inner_html)
	body << "\n\nHTML content has been scrubbed"
else
	# Assume plaintext
	body = mail[:body]
end

puts body

begin
	tenantid = toaddr[0..3].to_i
	brandid = toaddr[4..7].to_i
	raise ArgumentError if tenantid.zero? or brandid.zero?
rescue ArgumentError
	STDERR.puts "Mail address #{mailto} (parsed to #{toaddr}) does not have a numeric 8 character address; bailing."
	exit
end

if md = /\[Case:(\d+)\]/.match(mail[:subject])
	caseid = md[1].to_i
	STDERR.puts "Case ID: #{caseid}"
end

dbh.query("INSERT INTO #{MailMagicConf::EMAIL_TABLE}(msgto, msgfrom, msgreply, date, subject, body, rawheaders, tenantid, brandid)
					VALUES('#{dbh.quote(mail[:to].join(','))}',
						'#{dbh.quote(mail[:from].join(','))}',
						'#{dbh.quote(mail['return-path'])}',
						#{mail[:date].to_i},
						'#{dbh.quote(mail[:subject])}',
						'#{dbh.quote(body.to_s)}',
						'#{dbh.quote(mail[:rawheader])}',
						#{tenantid}, #{brandid})")

mailid =  dbh.insert_id

STDERR.puts "      To: #{dbh.quote(mail[:to].join(','))}"
STDERR.puts "Email ID: #{mailid}"

File.umask(0022)

Dir.mkdir(MailMagicConf::MAIL_FOLDER) unless File.directory? MailMagicConf::MAIL_FOLDER

if mail[:multipart]

	Dir.mkdir(File.join(MailMagicConf::MAIL_FOLDER, mailid.to_s)) unless File.directory?(File.join(MailMagicConf::MAIL_FOLDER, mailid.to_s))

	File.umask(0133)

	mail[:parts].each_with_index do |part, i|
		if part[:multipart]
			part[:parts].each_with_index do |subpart, i|
				name = subpart[:filename] || "unknown-#{i}"
				name.gsub!('/', '_')
				File.open(File.join(MailMagicConf::MAIL_FOLDER, mailid.to_s, CGI.unescape(name)), 'w') do |f|
					f.puts(subpart[:body] || subpart[:message])
				end

			dbh.query("INSERT INTO email_attachments SET emailid=#{mailid}, mimetype='#{dbh.quote(subpart[:type]+'/'+subpart[:subtype])}', filename='#{dbh.quote(name)}'")

			end
		else
			name = part[:filename] || "unknown-#{i}"
			name.gsub!('/', '_')
			File.open(File.join(MailMagicConf::MAIL_FOLDER, mailid.to_s, CGI.unescape(name)), 'w') do |f|
				f.puts(part[:body] || part[:message])
			end

			dbh.query("INSERT INTO email_attachments SET emailid=#{mailid}, mimetype='#{dbh.quote(part[:type]+'/'+part[:subtype])}', filename='#{dbh.quote(name)}'")
		end
	end
elsif mail[:type] == 'text' and mail[:subtype] == 'html'
	Dir.mkdir(File.join(MailMagicConf::MAIL_FOLDER, mailid.to_s)) unless File.directory?(File.join(MailMagicConf::MAIL_FOLDER, mailid.to_s))

	File.umask(0133)

	name = mail[:filename] || "unknown-0"
	name.gsub!('/', '_')
	File.open(File.join(MailMagicConf::MAIL_FOLDER, mailid.to_s, CGI.unescape(name)), 'w') do |f|
		f.puts(mail[:body] || mail[:message])
	end

	dbh.query("INSERT INTO email_attachments SET emailid=#{mailid}, mimetype='#{dbh.quote(mail[:type]+'/'+mail[:subtype])}', filename='#{dbh.quote(name)}'")
end

result = dbh.query("SELECT name FROM #{MailMagicConf::QUEUE_TABLE} WHERE name='#{dbh.quote(mailto)}'")
if result.num_rows != 1
	STDERR.puts "Invalid result when looking for queue #{mailto}"
	exit
end

result = dbh.query("SELECT keyname, value FROM #{MailMagicConf::BRAND_SETTINGS_TABLE} WHERE tenant=#{tenantid} AND brand=#{brandid} AND keyname IN ('emailstart', 'emailend', 'emailoverride')")
if result.num_rows > 0
	while (res = result.fetch_row)
		key, value = res
		case key
		when 'emailstart'
			emailstart = value.to_i
		when 'emailend'
			emailend = value.to_i
		when 'emailoverride'
			emailoverride = true
		end
	end
	if Time.now.wday > 5 # check for weekends
		STDERR.puts "Today is a weekend, queueing mail"
	elsif emailoverride
		STDERR.puts "Email override in effect, queueing mail"
	elsif emailstart and emailend
		if emailstart == emailend
			STDERR.puts "Times to start and stop receiving email are identical #{emailstart}, queueing mail"
		else
			#STDERR.puts "Email interval is #{emailstart} to #{emailend}"
			now = Time.now.strftime("%H%M").to_i
			if (now > emailstart and ((emailstart < emailend and now < emailend) or (emailend < emailstart))) or
				(emailstart > emailend and now < emailend and now < emailstart)
				STDERR.puts "#{now} is within interval of #{emailstart} to #{emailend}, queueing mail"
			else
				STDERR.puts "#{now} is NOT within interval of #{emailstart} to #{emailend}, discarding mail"
				sock.puts 'discard'
				sock.gets
				exit
			end
		end
	end
end

result.free

begin
	time = DateTime.parse(mail[:date].to_s).to_time
rescue ArgumentError
	STDERR.puts "Failed to parse date: '#{mail[:date]}', using Time.now"
	time = Time.now
end

sock.puts([mail[:from][0], time.to_i, mailid, toaddr, mailto, caseid].join(" "))

begin
	Timeout::timeout(60) do
		unless line = sock.gets and line.include? 'OK'
			STDERR.puts "FAILED TO SUBMIT EMAIL FOR QUEUEING!"
		end
	end
rescue Timeout::Error
	STDERR.puts "Timed out trying to queue the email after 60 seconds!"
end

