
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

begin
	require 'hpricot'
rescue LoadError
	require 'rubygems'
	require 'hpricot'
end

module Hpricot
  class Elements
    def strip(callbacks={})
      each { |x| x.strip(callbacks) }
    end
    
  end

  class Elem
    def remove
      parent.children.delete(self)
    end

    def strip(callbacks={})
      children.each do |x|
				if x.comment? or x.doctype? or x.procins?
					x.parent.replace_child(x, Text.new(''))
				elsif /<[Bb][Rr]\s*\/*>/.match(x.to_s)
					x.before("\n")
					x.strip(callbacks)
				elsif !x.text? and !x.bogusetag? and x.respond_to?(:strip)
					x.strip(callbacks)
				end
			end

      if strip_removes?
        remove
			elsif attributes and ((attributes['href'] and attributes['href'][0] != '#'[0]))# or attributes['src'])
				parent.replace_child(self, [Text.new(' [ '+(attributes['href']||attributes['src'])+' ] ')]+ Hpricot.make(inner_html))
			else
        parent.replace_child self, Hpricot.make(inner_html) unless parent.nil?
      end
    end
    
    def strip_removes?
      # I'm sure there are others that shuould be ripped instead of stripped
      attributes && attributes['type'] =~ /script|css/
    end
  end

  class Doc
    def scrub(callbacks={})
      children.reverse.each {|e|
				if e.comment? or e.doctype?
					e.parent.replace_child(e, Text.new(''))
				else
					e.strip unless e.text?
				end
      }
			self.search('meta').each do |meta|
				meta.parent.replace_child(meta, Text.new(''))
			end
      self
    end
  end
end
