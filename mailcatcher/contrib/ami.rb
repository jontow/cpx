
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'thread'
require 'socket'

class AMIConnection
	def initialize(host, port)
		@host = host
		@port = port
		connect
		@id = 0
		@uniq = rand(10000)
		@replies = Queue.new
		listen
	end

	def connect
		@connected = false
		@socket = TCPSocket.new(@host, @port)
		@socket.gets # read fusking stupid banner that isn't protocol compliant
		@connected = true
	end

	def reconnect
		begin
			STDERR.puts "Reconnecting"
			connect
			login(@username, @password)
		rescue Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::EPIPE, Errno::EPROTONOSUPPORT => e
			STDERR.puts "got #{e.class} while reconnecting, sleeping for 5 seconds"
			sleep 5
			retry
		end
		nil
	end

	def gen_id
		ret = "#{@uniq}-#{@id}"
		@id += 1
		ret
	end

	def login(username, password)
		ret = action('Login', 'Username'=>username, 'Password'=>password)
		if ret['Response'] == 'Success'
			STDERR.puts "Logged in OK"
			@username = username
			@password = password
		end
		ret
	end

	def action(type, arguments={})
		until @connected
			STDERR.puts "not connected, waiting"
			sleep 0.5
		end
		aid = gen_id
		arguments['ActionID'] = aid
		@socket.print("Action: #{type}\r\n"+arguments.to_a.map{|x| x.join(': ')}.join("\r\n")+"\r\n\r\n")
		sleep 0.5 until ent = scan_queue(aid)
		return ent
	end

	private

	def listen
		Thread.new do
			p 'listening'
			loop do
				unless @connected and (event = parse(@socket.gets("\r\n\r\n")))
					sleep 0.5
					next
				end
				@replies << event if event
			end
		end
	end

	def parse(event)
		if event.nil?
			Thread.new{reconnect}
			return
		end
		lines = event.split("\r\n")
		eventtype, eventname = lines[0].split(': ', 2)
		if eventtype == 'Response'
			return Hash[*lines.map{|x| x.split(': ')}.flatten]
		end
	end

	def scan_queue(id)
		return nil if @replies.empty?
		length = @replies.length
		length.times do
			ent = @replies.pop
			if ent["ActionID"] == id
				return ent
			else
				@replies << ent
			end
		end
		return nil
	end
end
