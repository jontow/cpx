#!/usr/bin/env ruby -w

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 }* modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'find'

unless ARGV.length >= 2
	STDERR.puts "Usage: #{$0} <branch|trunk> <export path> [release name]"
	exit(-1)
end

MODULES = %w{auiserver agentui cbx constants cpxmon deploy mailcatcher}

if ARGV[0] == 'trunk'
	branch = 'trunk'
else
	if File.directory? "branches/#{ARGV[0]}"
		branch = "branches/#{ARGV[0]}"
	elsif File.directory? "tags/#{ARGV[0]}"
		branch = "tags/#{ARGV[0]}"
	else
		STDERR.puts "Cannot find a branch or tag named #{ARGV[0]}"
		exit -1
	end
end
#branch = ARGV[0] == 'trunk' ? 'trunk' : "branches/#{ARGV[0]}"

target = File.expand_path(ARGV[1])

Dir.mkdir(target) unless File.directory?(target)

Dir.chdir(File.dirname(__FILE__))

url = ''
svninfo = `svn info`

if md = /^URL: (.*)$/.match(svninfo)
	url = md[1]
else
	STDERR.puts "Could not determine the SVN URL for this repository"
	exit(-1)
end

revision = 'Development Copy'
if md = /^Revision: (.*)$/.match(svninfo)
	revision = "SVN r#{md[1]}"
end

if ARGV[2]
	revision = ARGV[2]
end

MODULES.each do |mod|
	puts "exporting #{mod}"
	`svn export #{mod}/#{branch} #{target}/#{mod}`
	
	Find.find(File.join(mod, branch)) do |x|
		if File.directory? x and !x.include? '.svn'
			if `svn proplist #{x}`.include?('svn:externals')
				trimmed = x.sub(/#{branch}\/*/, '')

				puts 'found externals'
				externals =  `svn propget svn:externals #{x}`
				externals.split("\n").each do |external|
					directory, url = external.split(" ", 2)
					puts "exporting external from #{url} to #{target}/#{trimmed}/#{directory}"
					`svn export #{url} #{target}/#{trimmed}/#{directory}`
				end
			end
		end
	end
end

puts target
Dir.chdir(target)

MODULES.each do |mod|
	Dir.chdir(File.join(target, mod))
	if File.exists?('.filter')
		puts "Executing filter rules for #{mod}"
		File.read('.filter').split("\n").each do |x|
			puts "Removing #{x}"
			if x[-1].chr == '/'
				`rm -rf #{x}`
			else
				`rm #{x}`
			end
		end
		`rm .filter`
	end

	if File.exists?('version.rb')
		puts "Updating version info for #{mod}"
		File.open('version.rb', 'w') {|f| f.puts "VERSIONSTRING = '#{revision}'"}
	end
end
