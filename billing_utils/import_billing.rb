#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


# Script to import billing data into the MasterACD

require 'stringio'

require File.join(File.dirname(__FILE__), 'cdr2acd')

cpxdbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)
acddbh = DBI.connect("DBI:ODBC:#{UtilsConf::MSDBNAME}", UtilsConf::MSDBUSER, UtilsConf::MSDBPASS)

if ARGV.include? '--monthly'
	now = Time.now
	starttime = Time.mktime(now.year, now.month).to_i
	endtime = Time.mktime(now.year, now.month, now.day).to_i

elsif ARGV.include? '--lastmonthly'
	STDERR.puts "inferring the previous month is buggy, be CAREFUL!"
	now = Time.now
	month = now.month - 1
	year = now.year
	if month < 1
		month = 12
		year = now.year - 1
	end

	starttime = Time.mktime(year, month).to_i
	endtime = Time.mktime(now.year, now.month).to_i

else
	now = Time.now
	endtime = Time.mktime(now.year, now.month, now.day).to_i # last midnight
	starttime = endtime - (86400 * 7) # midnight a day ago
end

puts " #{Time.at(starttime)} - #{Time.at(endtime)}"

if ARGV.include? '--batch'
	log = StringIO.new('')
else
	log = STDOUT
end

added = updated = 0
result = cpxdbh.query("SELECT UniqueID FROM billing_summaries WHERE Start > #{starttime} AND End < #{endtime}")
result.each do |row|
	res = cdr2acd(cpxdbh, acddbh, row[0], log)
	case res
	when :add
		added += 1
	when :update
		updated += 1
	end
end

puts "\nAdded #{added}, Updated #{updated}"
if log != STDOUT
	puts "\n\n#{log.string}\nEOF"
end
