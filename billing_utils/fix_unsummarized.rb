#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


require 'rubygems'
require 'mysql'

#require 'constants/cdrconstants'
require 'write_summary'

begin
	require File.join(File.dirname(__FILE__), 'conf', 'utilsconf.rb')
rescue LoadError, SyntaxError
	STDERR.puts "Missing or invalid config file: conf/utilsconf.rb"
	exit(-1)
end

dbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)

if ARGV.include? '--monthly'
	now = Time.now
	starttime = Time.mktime(now.year, now.month).to_i
	endtime = Time.mktime(now.year, now.month, now.day).to_i

elsif ARGV.include? '--lastmonthly'
	STDERR.puts "inferring the previous month is buggy, be CAREFUL!"
	now = Time.now
	month = now.month - 1
	year = now.year
	if month < 1
		month = 12
		year = now.year - 1
	end

	starttime = Time.mktime(year, month).to_i
	endtime = Time.mktime(now.year, now.month).to_i

else
	starttime = (Time.now.to_i - 86400).to_i # 1 day ago
	endtime = (Time.now.to_i - 18000).to_i # 5 hours ago
end

result = dbh.query("SELECT DISTINCT billing_transactions.UniqueID, billing_transactions.Data FROM billing_transactions LEFT JOIN billing_summaries ON billing_transactions.UniqueID=billing_summaries.UniqueID WHERE billing_transactions.Transaction = 0 AND billing_summaries.UniqueID IS NULL and billing_transactions.Start > #{starttime} AND billing_transactions.End < #{endtime} AND billing_transactions.UniqueID LIKE '%.0%' AND billing_transactions.UniqueID NOT LIKE '%.0'")

result.each do |row|
	puts "deleting unsummarized #{row[1]} with uniqueid #{row[0]} (#{Time.at(row[0].to_f)})"
	dbh.query("DELETE FROM billing_transactions WHERE UniqueID='#{row[0]}'")
	dbh.query("DELETE FROM call_info WHERE UniqueID='#{row[0]}'")
end

result = dbh.query("SELECT DISTINCT billing_transactions.UniqueID, billing_transactions.Data FROM billing_transactions LEFT JOIN billing_summaries ON billing_transactions.UniqueID=billing_summaries.UniqueID WHERE billing_transactions.Transaction = 0 AND billing_summaries.UniqueID IS NULL and billing_transactions.Start > #{starttime} AND billing_transactions.End < #{endtime}")

count = result.num_rows
result.each do |row|
	puts "summarizing #{row[1]} with uniqueid #{row[0]} (#{Time.at(row[0].to_f)})"
	write_summary(dbh, row[0])
end

