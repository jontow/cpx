#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

begin
	require 'dbi'
	require 'mysql'
rescue LoadError
	require 'rubygems'
	require 'dbi'
	require 'mysql'
end

require File.join(File.dirname(__FILE__), 'constants/cdrconstants')

begin
	require File.join(File.dirname(__FILE__), 'conf', 'utilsconf.rb')
rescue LoadError, SyntaxError
	STDERR.puts "Missing or invalid config file: conf/utilsconf.rb"
end

def construct_brand_lookup(cpxdbh, acddbh)
	result = cpxdbh.query("SELECT tenant, brand, dnis FROM brandlist")
	stuff = []
	result.each_hash do |hsh|
		stuff << hsh
	end

	acddbh.select_all('SELECT DNIS,ClientID,ClientName FROM rClients') do |row|
		if ent = stuff.detect{|x| x['dnis'] == row[0]}
			ent['clientid'] = row[1]
			ent['clientname'] = row[2]
		end
	end
	stuff
end

def cdr2acd(cpxdbh, acddbh, uniqueid, logbuffer = STDOUT)
	# cache this stupid lookup table
	$brandlookup ||= construct_brand_lookup(cpxdbh, acddbh)

	abandonqueue = 0
	abandonivr = 0
	calldirection = 1

	result = cpxdbh.query("SELECT UniqueID,TenantID,BrandID,Start,End,
					InCall,WrapUp,AgentID,
						CallType,LastState,InQueue
					FROM billing_summaries WHERE
						UniqueID='#{uniqueid}'")
	if result.num_rows == 0
		logbuffer.puts "No summary for #{uniqueid} found"
		return
	end

	row = result.fetch_hash

	unless details = $brandlookup.detect{|x| x['tenant'] == row['TenantID'] and x['brand'] == row['BrandID'] }
		logbuffer.puts "Cannot find brand for tenant: #{row['TenantID']} and brand: #{row['BrandID']} {#{uniqueid})"
		return
	end

	unless details['clientid'] and details['clientid'] != ""
		logbuffer.puts "No ClientID for #{details['clientname']} Tenant: #{row['TenantID']} and brand: #{row['BrandID']} {#{uniqueid})"
		return
	end

	if row['AgentID']
		result = cpxdbh.query("SELECT LocationID from tblAgent WHERE AgentID=#{row['AgentID'].to_i - 1000}")
		if result.num_rows != 0
			locationid = result.fetch_row[0].to_i
		else
			locationid = 0
		end
		return if locationid == 3 # pete hates the nicholville agents and
								# won't let them in his precious MasterACD
	end

	case row['LastState'].to_i
	when CDRConstants::ABANDONQUEUE
		handled = 0
		abandonqueue = 1
		statusstring = "Abandoned In Queue"
	when CDRConstants::ABANDONIVR
		handled = 0
		abandonivr=1
		statusstring = "Disconnected In IVR"
	else
		handled = 1
		statusstring = "Handled"
	end

	case row['CallType']
	when 'voicemail'
		calltype = 9
	when 'email', 'tempemail'
		calltype = 8
	when 'outgoing', 'tempout'
		calltype = 6 # voice
		calldirection = 0 # outbound
	else
		calltype = 6 # default to voice
	end

	starttime = Time.at(row['Start'].to_i).strftime("%Y-%m-%d %I:%M:%S %p")
	endtime = Time.at(row['End'].to_i).strftime("%Y-%m-%d %I:%M:%S %p")

	query = "INSERT INTO tblCallData (
					CallType,Client,
					CallDirection,TalkTime,
					WrapUpTime,OnHoldCount,OnHoldTime,QueueTime,
					ArrivalTime,Agent,
					PickUp,Terminated,
					WrapUpTermination,NumberDialed,DNIS,DisconnectedInIVR,
					PresentCount,AbandonedInQueue,Source,
					StringID,Handled,TransferCount,LocationID
				) VALUES (
					#{calltype},#{details['clientid']},
					#{calldirection},#{row['InCall']},
					#{row['WrapUp']},0,0,#{row['InQueue']},
					'#{starttime}',#{row['AgentID']},
					'#{starttime}','#{endtime}', 
					'#{endtime}',NULL,'',#{abandonivr},
					0,#{abandonqueue},4,
					'#{row['UniqueID']}',#{handled},0,#{locationid}
				)"
	
	new = [calltype, details['clientid'].to_i,
		calldirection, row['InCall'].to_i,
		row['WrapUp'].to_i, row['InQueue'].to_i,
		starttime, row['AgentID'].to_i,
		starttime, endtime,
		endtime, abandonivr,
		abandonqueue, row['UniqueID'], handled, locationid]

	if old = acddbh.select_one("SELECT CallType, Client, CallDirection, TalkTime, WrapUpTime, QueueTime, ArrivalTime, Agent, PickUp, Terminated, WrapUpTermination, DisconnectedInIVR, AbandonedInQueue, StringID, Handled, LocationID FROM tblCallData WHERE StringID='#{uniqueid}'")
		old = old.map{|x| x.kind_of?(DBI::Timestamp) ? Time.mktime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%Y-%m-%d %I:%M:%S %p") : x }
		if old == new
			return :nothing
		end
		logbuffer.puts "Changed record for #{uniqueid}"
		logbuffer.puts old.inspect
		logbuffer.puts new.inspect
		#p old-new
		diff = []
		new.each_with_index do |x, i|
			diff[i] = x unless x == old[i]
		end
		logbuffer.puts diff.inspect
		query = "UPDATE tblCallData
					SET CallType=#{calltype},
					Client=#{details['clientid']},
					CallDirection=#{calldirection},
					TalkTime=#{row['InCall']},
					WrapUpTime=#{row['WrapUp']},
					QueueTime=#{row['InQueue']},
					ArrivalTime='#{starttime}',
					Agent=#{row['AgentID']},
					PickUp='#{starttime}',
					Terminated='#{endtime}',
					WrapUpTermination='#{endtime}',
					DisconnectedInIVR=#{abandonivr},
					AbandonedInQueue=#{abandonqueue},
					LocationID=#{locationid}
				WHERE StringID='#{row['UniqueID']}'"
		action = :update
	else
		logbuffer.puts "Added record for #{uniqueid}"
		action = :add
	end

	st = acddbh.execute(query)
	st.finish
	action
end

if $0 == __FILE__
	acddbh = DBI.connect("DBI:ODBC:#{UtilsConf::MSDBNAME}", UtilsConf::MSDBUSER, UtilsConf::MSDBPASS)
	cpxdbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)

	ARGV.each do |arg|
		cdr2acd(cpxdbh, acddbh, arg)
	end
end
