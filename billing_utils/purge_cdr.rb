#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'rubygems'                                    
require 'mysql'

if $0 == __FILE__
	begin
		require File.join(File.dirname(__FILE__), 'conf', 'utilsconf.rb')
	rescue LoadError, SyntaxError
		STDERR.puts "Missing or invalid config file: conf/utilsconf.rb"
		exit(-1)
	end

	input = ARGV

	if ARGV.empty?
		puts "Usage: #{$0} <uniqueid> [uniqueid [...]]"
		exit
	elsif ARGV[-1] == '-'
		puts 'reading list of uniqueids from stdin'
		input = STDIN
	end

	dbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)

	input.each do |arg|
		puts "Purging #{arg}"
		dbh.query("DELETE FROM billing_summaries WHERE UniqueID='#{arg.chomp}'")
		puts dbh.affected_rows()
		dbh.query("DELETE FROM billing_transactions WHERE UniqueID='#{arg.chomp}'")
		puts dbh.affected_rows()
		dbh.query("DELETE FROM call_info WHERE UniqueID='#{arg.chomp}'")
		puts dbh.affected_rows()
	end
end
