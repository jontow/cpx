#!/usr/bin/env ruby -w

require 'rubygems'
require 'mysql'

require File.join(File.dirname(__FILE__), 'constants/cdrconstants')

begin
	require File.join(File.dirname(__FILE__), 'conf', 'utilsconf.rb')
rescue LoadError, SyntaxError
	STDERR.puts "Missing or invalid config file: conf/utilsconf.rb"
	exit(-1)
end

if ARGV.length != 2
	STDERR.puts "Usage: #{$0} <tenant> <brand>"
	exit(-1)
end

@tenant = ARGV[0].to_i
@brand = ARGV[1].to_i

if @tenant.zero? or @brand.zero?
	STDERR.puts "Invalid tenant or brand"
	exit(-1)
end

@dbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)

def is_billable_agent(agentid)
	return false if agentid.zero?
	
	result = @dbh.query("SELECT LocationID FROM tblAgent WHERE AgentID=#{agentid - 1000}")
	
	if result.num_rows == 1
		location = result.fetch_row[0].to_i
		if location == 1
			return true
		end
	end
	return false
end

now = Time.now
starttime = Time.mktime(now.year, now.month).to_i

@total = 0;

# total up all the summarized calls

result = @dbh.query("SELECT UniqueID, InCall, WrapUp, CallType from billing_summaries WHERE (InCall > 0 OR WrapUp > 0) AND TenantID=#{@tenant} AND BrandID=#{@brand} AND Start >= #{starttime}")

result.each_hash do |row|
	result2 = @dbh.query("SELECT DISTINCT(Data) FROM billing_transactions WHERE UniqueID='#{row['UniqueID']}' AND Transaction IN (#{CDRConstants::INCALL}, #{CDRConstants::INWRAPUP}, #{CDRConstants::ENDWRAPUP})")
	if result2.num_rows == 1 # only one agent handled the call
		if is_billable_agent(result2.fetch_row[0].to_i)
			@total += (row['InCall'].to_i + row['WrapUp'].to_i)
		end
	else
		if row['CallType'] == 'outgoing'
			# An outgoing call that has more than one agent involved.
			# Unfortunately, the transaction for this doesn't contain the
			# agent ID in the data field, but instead the dialed number.
			# Therefore, we need to find the first agent to go to wrapup
			# who, by definition, is the one who first transferred the call
			# and check that agent's billable status to determine whether to
			# count the minutes for that transaction.
			result3 = @dbh.query("SELECT Data FROM billing_transactions WHERE UniqueID='#{row['UniqueID']}' AND Transaction=#{CDRConstants::INWRAPUP} ORDER BY Start ASC LIMIT 1")
			if result3.num_rows == 1
				if is_billable_agent(result3.fetch_row[0].to_i)
					result4 = @dbh.query("SELECT End-Start AS Time FROM billing_transactions WHERE UniqueID='#{row['UniqueID']}' AND Transaction=#{CDRConstants::INOUTGOING}")
					result5.each do |time|
						@total += time[0].to_i
					end
				end
			else
				STDERR.puts "couldn't determine agent to associate with initial outbound calltime for #{row['UniqueID']}"
			end
		end
		result2.each do |row2|
			if is_billable_agent(row2[0].to_i)
				result5 = @dbh.query("SELECT End-Start AS Time FROM billing_transactions WHERE UniqueID='#{row['UniqueID']}' AND Data='#{row2[0]}' AND Transaction IN (#{CDRConstants::INCALL}, #{CDRConstants::INWRAPUP})")
				result5.each do |time|
					@total += time[0].to_i
				end
			end
		end
	end
end


# total up any in-progress calls, as much as possible.

result = @dbh.query("SELECT DISTINCT billing_transactions.UniqueID FROM billing_transactions LEFT JOIN billing_summaries ON billing_transactions.UniqueID=billing_summaries.UniqueID WHERE billing_transactions.Transaction = 0 AND billing_summaries.UniqueID IS NULL AND billing_transactions.Start > #{starttime}")

result.each do |row|
	result2 = @dbh.query("SELECT * FROM call_info WHERE UniqueID='#{row[0]}' AND TenantID=#{@tenant} AND BrandID=#{@brand}")
	if result2.num_rows == 1
		# okay, so we have call_info for this call
		callinfo = result2.fetch_hash
		next if callinfo['CallType'] =~ /^temp/

		# add any already recorded minutes to the total
		result3 = @dbh.query("SELECT DISTINCT(Data) FROM billing_transactions WHERE UniqueID='#{row[0]}' AND Transaction IN (#{CDRConstants::INCALL}, #{CDRConstants::INWRAPUP}, #{CDRConstants::ENDWRAPUP})")
		if result3.num_rows == 1
			agent = result3.fetch_row[0].to_i
			if is_billable_agent(agent)
				result4 = @dbh.query("SELECT End-Start AS Time FROM billing_transactions WHERE UniqueID='#{row[0]}' AND Data='#{agent}' AND Transaction IN (#{CDRConstants::INCALL}, #{CDRConstants::INOUTGOING}, #{CDRConstants::INWRAPUP})")
				result4.each do |time|
					@total += time[0].to_i
				end
			end
		elsif result3.num_rows.zero?
			next 
		else
			#if callinfo['CallType'] == 'outgoing'
			#end
			result2.each do |row2|
				if is_billable_agent(row2[0].to_i)
					result5 = @dbh.query("SELECT End-Start AS Time FROM billing_transactions WHERE UniqueID='#{row['UniqueID']}' AND Data='#{row2[0]}' AND Transaction IN (#{CDRConstants::INCALL}, #{CDRConstants::INWRAPUP})")
					result5.each do |time|
						@total += time[0].to_i
					end
				end
			end
		end

		# find the last transaction
		#last = @dbh.query("SELECT Start, End, Data FROM billing_transactions WHERE UniqueID='#{row[0]}' ORDER BY Start DESC LIMIT 1").fetch_hash

		# guess wildly
		#case last['Transaction'].to_i
		#when CDRConstants::RINGING
		#when CDRConstants::ENDCALL
		#end
	end
end

puts "#{@total/60}"

