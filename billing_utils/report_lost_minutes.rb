#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


require 'rubygems'
require 'mysql'

require File.join(File.dirname(__FILE__), 'constants/cdrconstants')

begin
	require File.join(File.dirname(__FILE__), 'conf', 'utilsconf.rb')
rescue LoadError, SyntaxError
	STDERR.puts "Missing or invalid config file: conf/utilsconf.rb"
	exit(-1)
end

dbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)

if ARGV.include? '--monthly'
	now = Time.now
	starttime = Time.mktime(now.year, now.month).to_i
	endtime = Time.mktime(now.year, now.month, now.day).to_i
elsif ARGV.include? '--lastmonthly'
	STDERR.puts "inferring the previous month is buggy, be CAREFUL!"
	now = Time.now
	month = now.month - 1
	year = now.year
	if month < 1
		month = 12
		year = now.year - 1
	end

	starttime = Time.mktime(year, month).to_i
	endtime = Time.mktime(now.year, now.month).to_i

else
	starttime = (Time.now.to_i - 86400).to_i # 1 day ago
	endtime = (Time.now.to_i - 18000).to_i # 5 hours ago
end

result = dbh.query("SELECT DISTINCT billing_transactions.UniqueID, billing_transactions.Data FROM billing_transactions LEFT JOIN billing_summaries ON billing_transactions.UniqueID=billing_summaries.UniqueID WHERE billing_transactions.Transaction = 0 AND billing_summaries.UniqueID IS NULL and billing_transactions.Start > #{starttime} AND billing_transactions.End < #{endtime}")

count = result.num_rows
result.each do |row|
	laststate, lastdata = dbh.query("SELECT Transaction, Data FROM billing_transactions WHERE UniqueID='#{row[0]}' ORDER BY END DESC LIMIT 1").fetch_row
	puts "#{row[0]} - #{row[1]} with last transaction: #{CDRConstants::TRANSACTIONNAMES[laststate.to_i]} (#{lastdata}) - (#{Time.at(row[0].to_f)})"
end

puts "\n\n#{count} unsummarized calls between #{Time.at(starttime)} and #{Time.at(endtime)}\n\n\n" if count > 0 

result = dbh.query("SELECT UniqueID FROM billing_transactions WHERE Transaction=18 AND Start > #{starttime} and End < #{endtime} GROUP BY UniqueID HAVING count(*) > 1")

count = result.num_rows
result.each do |row|
	result2 = dbh.query("SELECT Data from billing_transactions WHERE UniqueID='#{row[0]}' AND Transaction=0")
	type = result2.fetch_row[0]
	puts "#{row[0]} - #{type} - (#{Time.at(row[0].to_f)})"
end

puts "\n\n#{count} double summarized calls between #{Time.at(starttime)} and #{Time.at(endtime)}\n\n\n" if count > 0

result = dbh.query("SELECT UniqueID FROM billing_transactions WHERE Transaction=0 AND ( Data IS NULL OR Data = '' ) AND Start > #{starttime} AND End < #{endtime}")

count = result.num_rows
result.each do |row|
	result2 = dbh.query("SELECT CallType FROM billing_summaries where UniqueID='#{row[0]}'")
	if result2.num_rows > 0
		type = result2.fetch_row[0]
		if type.nil? or type.empty?
			puts "#{row[0]} - (#{Time.at(row[0].to_f)})"
		else
			count -= 1
		end
	else
		puts "#{row[0]} - (#{Time.at(row[0].to_f)}) UNSUMMARIZED"
	end
end

puts "\n\n#{count} calls with no type between #{Time.at(starttime)} and #{Time.at(endtime)}\n\n\n" if count > 0

result = dbh.query("SELECT UniqueID, CallType, InQueue FROM billing_summaries WHERE InQueue > 32766 AND LastQueue NOT LIKE 'CSR%' AND Start > #{starttime} AND End < #{endtime}")

count = result.num_rows
result.each do |row|
	puts "#{row[0]} - #{row[1]} - #{row[2]} - (#{Time.at(row[0].to_f)})"
end

puts "\n\n#{count} calls with excessive queue time between #{Time.at(starttime)} and #{Time.at(endtime)}\n\n\n" if count > 0

result = dbh.query("SELECT UniqueID, CallType, InCall FROM billing_summaries WHERE InCall > 32766 AND Start > #{starttime} AND End < #{endtime}")

count = result.num_rows
result.each do |row|
	puts "#{row[0]} - #{row[1]} - #{row[2]} - (#{Time.at(row[0].to_f)})"
end

puts "\n\n#{count} calls with excessive in call time between #{Time.at(starttime)} and #{Time.at(endtime)}\n\n\n" if count > 0

result = dbh.query("SELECT UniqueID, CallType, WrapUp FROM billing_summaries WHERE WrapUp > 32766 AND Start > #{starttime} AND End < #{endtime}")

count = result.num_rows
result.each do |row|
	puts "#{row[0]} - #{row[1]} - #{row[2]} - (#{Time.at(row[0].to_f)})"
end

puts "\n\n#{count} calls with excessive wrapup time between #{Time.at(starttime)} and #{Time.at(endtime)}" if count > 0
