#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'rubygems'                                    
require 'mysql'

require File.join(File.dirname(__FILE__), 'constants/cdrconstants')

include CDRConstants

def write_summary(dbh, uniqueid, operation=:insert)
	result = dbh.query("SELECT * FROM billing_transactions WHERE UniqueID='#{uniqueid}'")

	unless result.num_rows > 0
		STDERR.puts "No transactions found for #{uniqueid}"
		return false
	end

	@transactions = []

	result.each_hash do |row|
		@transactions << {:transaction=>row['Transaction'].to_i, :start=>row['Start'].to_i, :end=>row['End'].to_i, :data=>row['Data']}
	end

	start = @transactions[0][:start]
	stop = @transactions[-1][:end]
	agentid = 0
	incall = 0
	wrapup = 0
	inqueue = 0
	laststate = 0
	lastqueue = ''
	tbrand = nil
	type = nil

	@transactions.each do |t|
		laststate = t[:transaction] unless t[:transaction] == CDREND #last state
		case t[:transaction]
		when CDRINIT
			type = t[:data]
		when INIVR
		when DIALOUTGOING
			tbrand = t[:data]
		when INQUEUE
			inqueue += (t[:end] - t[:start])
			lastqueue = t[:data]
		when RINGING
			inqueue += (t[:end].to_i - t[:start].to_i)
		when INCALL
			agentid = t[:data] if t[:data]
			incall += (t[:end] - t[:start])
		when INOUTGOING
			incall += (t[:end] - t[:start])
		when TRANSFER
		when FAILEDOUTGOING
			#failurecode
		when WARMXFERFAILED
		when WARMXFER, WARMXFERCOMPLETE, WARMXFERLEG
			incall += (t[:end] - t[:start])
		when INWRAPUP
			agentid = t[:data] if t[:data]
			wrapup += (t[:end] - t[:start])
		when ABANDONQUEUE, ABANDONIVR, LEFTVOICEMAIL, ENDCALL, ENDWRAPUP, UNKNOWNTERMINATION
		when CDREND
			break
		else
			p t
			puts "unknown transaction state #{t[:transaction]}"
		end
	end

	unless tbrand
		result = dbh.query("SELECT accountcode FROM cdr WHERE uniqueid = '#{uniqueid}'")
		if result.num_rows == 1
			tbrand = result.fetch_row[0]
		elsif result.num_rows == 0
			puts "Asterisk CDR does not contain accountcode!"
			#			return
		else
			puts "OH GOD: more than one result from accountcode-retrieval-screwup: #{uniqueid}"
			tbrand = result.fetch_row[0]
		end
	end

	if tbrand
		tenantid = tbrand[0..3].to_i
		brandid = tbrand[4..7].to_i
	end

	if agentid == 0
		puts "****** DEAR GOD WHAT THE HELL NO AGENT ID: #{uniqueid} ******"

		result = dbh.query("SELECT channel FROM cdr WHERE uniqueid = '#{uniqueid}' AND channel LIKE 'Agent/%'")
		if result.num_rows == 1
			#puts "AgentID Channel: #{result.fetch_row[0]}"
			agentid = result.fetch_row[0].split('/', 2)[1].to_i
		end
	end

	result = dbh.query("SELECT * FROM call_info WHERE UniqueID = '#{uniqueid}'")

	if result.num_rows == 1
		callinfo = result.fetch_hash
		tenantid = callinfo['TenantID'] if callinfo['TenantID'] and !callinfo['TenantID'].empty?
		brandid = callinfo['BrandID'] if callinfo['BrandID'] and !callinfo['BrandID'].empty?
		dnis = callinfo['DNIS'] if callinfo['DNIS'] and !callinfo['DNIS'].empty? 
		type = callinfo['CallType'] if callinfo['CallType'] and !callinfo['CallType'].empty? 
	else
		puts "****** #{result.num_rows} call_info records: #{uniqueid} *******"
	end

	if !(tenantid and brandid and type)
		puts "******** #{uniqueid}: Checked billing_transactions, cdr, and call_info; still don't have tenant, brand and type."
		return false
	end

	if type.nil? or type.empty?
		puts "No calltype defined, defaulting to call"
		type = 'call'
	end

	case operation
	when :insert
		qrystr = "INSERT INTO billing_summaries(UniqueID, DNIS, TenantID, BrandID, Start, End, InQueue, InCall, WrapUp, AgentID, CallType, LastState, LastQueue) VALUES('#{dbh.quote(uniqueid)}', '#{dbh.quote(dnis.to_s)}', '#{tenantid}', '#{brandid}', #{start}, #{stop}, #{inqueue}, #{incall}, #{wrapup}, #{agentid}, '#{dbh.quote(type.to_s)}', #{laststate}, '#{dbh.quote(lastqueue)}')"
		puts qrystr
		dbh.query(qrystr)
	when :update
		qrystr = "UPDATE billing_summaries SET TenantID=#{tenantid}, BrandID=#{brandid}, Start=#{start}, End=#{stop}, InQueue=#{inqueue}, InCall=#{incall}, WrapUp=#{wrapup}, AgentID=#{agentid}, CallType='#{dbh.quote(type.to_s)}', LastState=#{laststate}, LastQueue='#{dbh.quote(lastqueue)}', DNIS='#{dbh.quote(dnis.to_s)}' WHERE UniqueID='#{dbh.quote(uniqueid)}'"
		puts qrystr
		dbh.query(qrystr)
	else
		raise ArgumentError, "Invalid parameter passed (should be: insert, update)"
	end

	return true
end

if $0 == __FILE__
	begin
		require File.join(File.dirname(__FILE__), 'conf', 'utilsconf.rb')
	rescue LoadError, SyntaxError
		STDERR.puts "Missing or invalid config file: conf/utilsconf.rb"
		exit(-1)
	end

	dbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)

	if ARGV.include?('--update')
		action = :update
		ARGV.delete('--update')
	else
		action = :insert
	end

	ARGV.each do |arg|
		write_summary(dbh, arg, action)
	end
end
