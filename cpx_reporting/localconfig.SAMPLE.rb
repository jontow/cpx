module WebConfig
	#
	# This is the config file for various parameters used
	# specifically in the web scripts for the archival/spooling
	# process.
	#
	# Most notably: the call lookup/detail scripts.
	#

	DBHOST = 'localhost'
	DBUSER = 'username'
	DBPASS = 'password'
	DBNAME = 'db_name'

	WEBROOT = '/var/www/vhost/local/path'
	SPOOLROOT = '/path/to/spooler/recordings'
	EMAILROOTURL = 'http://example.com/email-script.php'

	SUMEDITURL = 'http://example.com/path/to/calledit.cgi'
end
