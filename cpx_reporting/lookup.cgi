#!/usr/local/bin/ruby

$LOAD_PATH << File.dirname(__FILE__)

require 'time'
require 'cgi'
begin
  require 'cgi/session'
rescue Exception => e
  puts "Content-type: text/html\r\n\r\n"
  puts "<br>#{e.classname}<br>#{e.message}"
  exit
end

begin
  require 'rubygems'
rescue LoadError
  # NOOP
end


begin
  require 'json'
  require 'mysql'
rescue LoadError => e
  puts "Content-type: text/html\r\n\r\n"
  puts "Please install the json and mysql ruby libraries. <br/>"
  puts e.message
  exit
end

require 'localconfig'
include WebConfig
require 'constants/cdrconstants'

@webroot = WebConfig::WEBROOT
@spoolroot = WebConfig::SPOOLROOT

@abandoned = 0
@ivrdisco = 0
@callsmatch = 0

###### Local Methods ######
def printheader(errorfield=nil)
  return if @headerprinted
  @headerprinted = true
  puts "Content-type: text/html\r\n\r\n"
  puts "<html><head><title>Call Lookup</title>"
  puts "<style type=\"text/css\">

	tr.email td {
		background-color: #b1f1f9;
	}

	tr.call td {
		background-color: #b8f9b1;
	}

	tr.queueabandon td {
		background-color: #f68080;
	}

	tr.voicemail td {
		background-color: #afbcbc;
	}

	tr.outgoing td {
		background-color: #f9e1a8;
	}

	tr.chat td {
	    background-color: #f1f1b9
	}

	tr.tempmetrics td {
		background-color: #f6b1f9;
	}

	tr.header td {
		font-weight: bold;
		text-align: center;
	}

	label {
		width: 7em;
		float: left;
		text-align: right;
		margin-right: 0.5em;
		display: block;
	}

	div.or {
		text-align: center;
		margin-right: auto;
		margin-left: auto;
		font-weight: bold;
		clear: both;
	}

	.submit input {
		margin-left: 9em;
	}

	fieldset {
		width: 25em;
		margin-left: auto;
		margin-right: auto;
	}

	legend {
		border: 1px solid black;
		padding: 0.3em;
	}

	div.content {
		width: 80%;
	}

	div#errormsg {
	    text-align: center;
		font-weight: bold;
		color: red;
		border: 2px solid red;
	}

	label##{errorfield} {
	    color: red;
	}

	</style>
	</head><body><div id='maincontent' class='content'>"
end

def printfooter
  puts "</div></body></html>"
end

def calculate_row_class(row)
  if row['CallType'] == 'call' and row['AgentID'] == '0'
	'queueabandon'
  elsif row['CallType'] =~ /^temp/i
	'tempmetrics'
  else
	row['CallType']
  end
end

def print_call_row(row, showagents = true)
  brandid = "#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"
  #puts "BEFORE BRAND CHECK: #{brandid}<br/>"
  #p @brandlist
  return if !@brandlist.has_key?(brandid)
  #puts "AFTER BRAND CHECK"
  puts "<tr class=\"#{calculate_row_class(row)}\">"
  puts "<td>#{Time.at(row['Start'].to_i).strftime("%Y-%m-%d %H:%M:%S")}</td>"
  @callsmatch += 1
  if row['LastState'].to_i == CDRConstants::ABANDONQUEUE
	@abandoned += 1
  elsif row['LastState'].to_i == CDRConstants::ABANDONIVR
	@ivrdisco += 1
  end

  if row['AgentID'] == '0' or showagents == false
	puts "<td>Unknown</td>"
	@abandoned += 1 unless row['LastState'].to_i == CDRConstants::ABANDONQUEUE
  else
	puts "<td>#{@agentlist[row['AgentID']]}</td>"
  end

  # only offer a link to the recording if one exists and the call was within the last 30 days or so
  if ( row['CallType'] == 'call' or row['CallType'] == 'outgoing' ) && ((Time.now.to_i - row['Start'].to_i) < 2764800) &&
	File.exist?("#{@webroot}/#{@spoolroot}/monitor/#{Time.at(row['Start'].to_i).strftime("%Y-%m-%d")}/#{row['UniqueID']}.ogg")
	puts "<td><a href=\"#{@spoolroot}/monitor/#{Time.at(row['Start'].to_i).strftime("%Y-%m-%d")}/#{row['UniqueID']}.ogg\">#{row['UniqueID']}</a></td>"
  elsif row['CallType'] == 'voicemail'

	result2 = @cpx_dbh.query("SELECT VoicemailID FROM call_info WHERE UniqueID = '#{row['UniqueID']}' AND CallType = 'voicemail'")
	if result2 && result2.num_rows >= 1
	  result2.each_hash do |vmrow|
		if File.exist?("#{@webroot}/#{@spoolroot}/voicemail/#{brandid}/#{vmrow['VoicemailID']}.ogg")
		  puts "<td><a href=\"#{@spoolroot}/voicemail/#{brandid}/#{vmrow['VoicemailID']}.ogg\">#{row['UniqueID']}</a></td>"
		elsif File.exist?("#{@webroot}/#{@spoolroot}/monitor/#{Time.at(row['Start'].to_i).strftime("%Y-%m-%d")}/#{row['UniqueID']}.ogg")
		  puts "<td><a href=\"#{@spoolroot}/monitor/#{Time.at(row['Start'].to_i).strftime("%Y-%m-%d")}/#{row['UniqueID']}.ogg\">#{row['UniqueID']}</a></td>"
		else
		  puts "<td>#{row['UniqueID']}</td>"
		end
	  end
	else
	  puts "<td>#{row['UniqueID']}</td>"
	end
  elsif row['CallType'] == 'email'
	if (row['UniqueID'].match(/@/))
	  webpath = "#{@spoolroot}/monitor/#{Time.at(row['Start'].to_i).strftime("%Y-%m-%d")}"
	  filepath = "#{@webroot}/#{webpath}"
	  if File.exist?("#{filepath}/#{row['UniqueID']}.eml")
		puts "<td><a href=\"#{webpath}/#{row['UniqueID']}.eml\">#{row['UniqueID']}</a>"
		puts "<br/><a href=\"#{webpath}/#{row['UniqueID']}-reply.eml\">Reply</a>" if File.exist?("#{filepath}/#{row['UniqueID']}-reply.eml")
		puts "</td>"
	  else
		puts "<td>#{row['UniqueID']}</td>"
	  end
	else
	  result2 = @cpx_dbh.query("SELECT EmailID FROM call_info WHERE UniqueID = '#{row['UniqueID']}'")
	  if result2 && result2.num_rows >= 1
		result2.each_hash do |emrow|
		  puts "<td><a href=\"#{WebConfig::EMAILROOTURL}?id=#{emrow['EmailID']}&print=1&headers=1\">#{row['UniqueID']}</a></td>"
		end
	  else
		puts "<td>#{row['UniqueID']}</td>"
	  end
	end
  else
	puts "<td>#{row['UniqueID']}</td>"
  end

  puts "<td>#{row['CallType']}</td>"
  puts "<td>#{row['End'].to_i - row['Start'].to_i}</td>"
  puts "<td>#{row['InQueue']}</td>"
  puts "<td>#{row['InCall']}</td>"
  puts "<td>#{row['WrapUp']}</td>"
  puts "<td>#{@brandlist[brandid]}</td>"
  puts "<td><a href=\"calldetail.cgi?uniqueid=#{row['UniqueID']}\">Details</a></td>"
  puts "<tr>"
end

def printform(cgi,errmsg=nil)
  now = Time.now
  tomorrow = Time.now + 86400
  formstart = Time.mktime(now.year, now.month, now.day).strftime("%Y-%m-%d %H:%M")
  formend = Time.mktime(tomorrow.year, tomorrow.month, tomorrow.day).strftime("%Y-%m-%d %H:%M")
  puts "<div id='errormsg'>#{errmsg}</div><br>" if errmsg
  puts "<fieldset><legend>Query Historical Call Data</legend>"
  puts "<form method=\"POST\" action=\"?\">"
  puts "<label id='calltype'>Call Type:</label> <select name=\"calltype\">"
  puts "<option value=\"any\" #{"selected=\"selected\"" if !cgi.has_key?('calltype') or cgi['calltype'] == 'any'}>Any</option>"
  puts "<option value=\"call\" #{"selected=\"selected\"" if cgi.has_key?('calltype') and cgi['calltype'] == 'call'}>Inbound Call</option>"
  puts "<option value=\"chat\" #{"selected=\"selected\"" if cgi.has_key?('calltype') and cgi['calltype'] == 'chat'}>Chat</option>"
  puts "<option value=\"outgoing\" #{"selected=\"selected\"" if cgi.has_key?('calltype') and cgi['calltype'] == 'outgoing'}>Outbound Call</option>"
  puts "<option value=\"voicemail\" #{"selected=\"selected\"" if cgi.has_key?('calltype') and cgi['calltype'] == 'voicemail'}>Voicemail</option>"
  puts "<option value=\"email\" #{"selected=\"selected\"" if cgi.has_key?('calltype') and cgi['calltype'] == 'email'}>Email</option>"
  puts "<option value=\"temp\" #{"selected=\"selected\"" if cgi.has_key?('calltype') and cgi['calltype'] == 'temp'}>Temp Metrics</option>"
  puts "</select><hr />"
  puts "<label id='datestart'>Date Start:</label> <input type=\"text\" name=\"datestart\" value=\"#{cgi.has_key?('datestart') ? cgi['datestart'] : formstart}\"><br />"
  puts "<label id='dateend'>Date End:</label> <input type=\"text\" name=\"dateend\" value=\"#{cgi.has_key?('dateend') ? cgi['dateend'] :formend}\"><br /><hr />"
  puts "<label id='length'>Call Length:</label><input type='text' name='length' #{"value=\"#{cgi['length']}\"" if cgi.has_key?('length')}/><br /><br />"

  puts "<label id='brand'>Brand:</label> <select name=\"brand\">"
  puts "<option value=\"any\" #{"selected=\"selected\"" if !cgi.has_key?('brand') or cgi['brand'] == 'any'}>Any</option>"
  @brandlist.sort{|a,b| a[1].downcase <=> b[1].downcase}.each do |a|
	puts "<option value=\"#{a[0]}\" #{"selected=\"selected\"" if cgi.has_key?('brand') and cgi['brand'] == a[0]}>#{a[1]}</option>"
  end
  puts "</select><br />"
  unless @user['type'].to_i > 1 
	puts "<label id='agent'>Agent:</label> <select name=\"agent\">"
	puts "<option value=\"any\" #{"selected=\"selected\"" if !cgi.has_key?('agent') or cgi['agent'] == 'any'}>Any</option>"
	puts "<option value=\"none\">Unknown</option>"
	@agentlist.sort{|a,b| a[1].downcase <=> b[1].downcase}.each do |a|
	  puts "<option value=\"#{a[0]}\" #{"selected=\"selected\"" if cgi.has_key?('agent') and cgi['agent'] == a[0]}>#{a[1]}</option>"
	end
	puts "</select>"
  end
  puts "<hr />"
  puts "<div class='or'>-OR-</div>"
  puts "<label id='callerid'>Partial Caller ID:</label> <input type='text' name='callerid' #{"value=\"#{cgi['callerid']}\"" if cgi.has_key?('callerid')} /><br /><br /><hr />"
  puts "<div class='or'>-OR-</div>"
  puts "<label id='callid'>Call(unique) ID:</label> <input type='text' name='callid' #{"value=\"#{cgi['callid']}\"" if cgi.has_key?('callid')} /><br /><br /><hr />"
  puts "<div class='or'>-OR-</div>"
  puts "<label id='emailid'>Email ID:</label> <input type='text' name='emailid' #{"value=\"#{cgi['emailid']}\"" if cgi.has_key?('emailid')} /><br /><hr />"
  puts "<div class='or'>-OR-</div>"
  puts "<label id='caseid'>Case ID:</label> <input type='text' name='caseid' #{"value=\"#{cgi['caseid']}\"" if cgi.has_key?('caseid')} /><br /><hr />"

  puts "<label id='json'>JSON Output:</label> <input type='checkbox' name='json' value='json' #{"checked" if cgi.has_key?('json')} />"

  puts "<input type=\"hidden\" name=\"action\" value=\"submitted\">"
  puts "<p class='submit'><input type=\"submit\" name=\"submit\" value=\"submit\"></p>"
  puts "</form>"
  puts "<form method=\"POST\" action=\"?\">"
  if cgi.has_key?('inactive')
	puts "<p class='submit'><input type='submit' value='Show Only Active Agents' /></p>"
  else
	puts "<input type='hidden' name='inactive' value='inactive'>"      
	puts "<p class='submit'><input type='submit' value='Show Inactive Agents' /></p>"
  end
  puts "</form>"
  puts "</fieldset>"
  puts "<fieldset><legend>Calls Grouped by CallerID</legend>"
  puts "<form method=\"POST\" action=\"?\">"
  puts "<label id='datestart'>Date Start:</label> <input type=\"text\" name=\"datestart\" value=\"#{cgi.has_key?('datestart') ? cgi['datestart'] : formstart}\"><br />"
  puts "<label id='dateend'>Date End:</label> <input type=\"text\" name=\"dateend\" value=\"#{cgi.has_key?('dateend') ? cgi['dateend'] : formend}\"><br /><hr />"
  puts "<label id='brand'>Brand:</label> <select name=\"brand\">"
  puts "<option value=\"any\" #{"selected=\"selected\"" if !cgi.has_key?('brand') or cgi['brand'] == 'any'}>Any</option>"
  @brandlist.sort{|a,b| a[1].downcase <=> b[1].downcase}.each do |a|
	puts "<option value=\"#{a[0]}\" #{"selected=\"selected\"" if cgi.has_key?('brand') and cgi['brand'] == a[0]}>#{a[1]}</option>"
  end
  puts "</select><br /><br />"
  puts "<label id='laststate'>Last State:</label> <select name=\"laststate\">"
  puts "<option value=\"any\" #{"selected=\"selected\"" if !cgi.has_key?('laststate') or cgi['laststate'] == 'any'}>Any</option>"
  puts "<option value=\"#{CDRConstants::ABANDONQUEUE},#{CDRConstants::ABANDONIVR}\" #{"selected=\"selected\"" if cgi.has_key?('laststate') and cgi['laststate'] == "#{CDRConstants::ABANDONQUEUE},#{CDRConstants::ABANDONIVR}"}>Abandoned</option>"
  puts "<option value=\"#{CDRConstants::ABANDONQUEUE}\" #{"selected=\"selected\"" if cgi.has_key?('laststate') and cgi['laststate'] == "#{CDRConstants::ABANDONQUEUE}"}>Abandonded in Queue</option>"
  puts "<option value=\"#{CDRConstants::ABANDONIVR}\" #{"selected=\"selected\"" if cgi.has_key?('laststate') and cgi['laststate'] == "#{CDRConstants::ABANDONIVR}"}>Abandonded in IVR</option>"
  puts "<option value=\"#{CDRConstants::LEFTVOICEMAIL}\" #{"selected=\"selected\"" if cgi.has_key?('laststate') and cgi['laststate'] == "#{CDRConstants::LEFTVOICEMAIL}"}>Left VoiceMail</option>"
  puts "<option value=\"#{CDRConstants::ENDCALL}\" #{"selected=\"selected\"" if cgi.has_key?('laststate') and cgi['laststate'] == "#{CDRConstants::ENDCALL}"}>Ended Normally</option>"
  puts "</select><br /><br />"
  puts "<button type=\"submit\" name=\"action\" value=\"grouped\">Submit</button>"
  puts "</fieldset>"
end

def inputerror(cgi,field,msg)
  if (cgi.has_key? 'json') or (cgi.user_agent == '')
	cgi.out({
	  'Status' => '400',
	  'Content-type' => 'text/json'
	}){
	  JSON.pretty_generate({
	  'success' => false, 
	  'message' => msg,
	  'need_login' => true}, 
	  :max_nesting => 0)
	}
	exit
  else
	printheader(field)
	printform(cgi,msg)
	printfooter
	exit
  end
end

###### Main Execution Line ######
cgi = CGI.new
fields = cgi.keys

begin
  sess = CGI::Session.new(cgi, 'prefix' => 'lookup_sess_')
rescue Exception => e
  puts "Content-type: text/html\r\n\r\n"
  puts "<br>#{e.classname}<br>#{e.message}"
  exit
end

if sess['cpxkey'] == nil
  if (cgi.has_key? 'json') or (cgi.user_agent == '')
	cgi.out({
	  'Status' => '302',
	  'Location' => 'login.cgi',
	  'Content-type' => 'text/json'
	}){
	  JSON.pretty_generate({
	  'success' => false, 
	  'message' => 'login first',
	  'need_login' => true}, 
	  :max_nesting => 0)
	}
	exit
  end
  cgi.out({'Status' => '302', 'Location' => "login.cgi#{"?#{cgi.query_string}" if cgi.query_string}"}){
	"redirecting to login page..."
  }
  exit
end

@user = JSON.parse(sess['fullresult'])
#@user = {'type' => '1'}

# Initialize the DB handle early, will need it to retrieve a valid brand/agent list.
@cpx_dbh = Mysql.real_connect(WebConfig::DBHOST, WebConfig::DBUSER, WebConfig::DBPASS, WebConfig::DBNAME)

# Brand Name
@brandlist = {}
result = @cpx_dbh.query("SELECT tenant,brand,concat(lpad(tenant,4,'0'),lpad(brand,4,'0')) as brandid, brandname FROM brandlist");
result.each_hash do |row|
  @brandlist[row['brandid']] = row['brandname']
end

begin
  brands = JSON.parse(sess['companies'])
rescue
  if (cgi.has_key? 'json') or (cgi.user_agent == '')
	cgi.out({
	  'Status' => '302',
	  'Location' => 'login.cgi?action=logout&json=1',
	  'Content-type' => 'text/json'
	}){
	  JSON.pretty_generate({
	  'success' => false, 
	  'message' => 'login invalid, redirected to logout',
	  'need_login' => true}, 
	  :max_nesting => 0)
	}
	exit
  end
  cgi.out({'Status' => '302', 'Location' => 'login.cgi?action=logout'}){
	"redirecting to login page..."
  }
  exit
end

# XXX: Somehow this fails.. 
@brandlist.reject! do |k, v|
  !brands.include? k
end

active="WHERE Active=1"
active="" if cgi.has_key?('inactive')

# Agent List
@agentlist = {}
result = @cpx_dbh.query("SELECT AgentID,FirstName,LastName FROM tblAgent #{active}");
result.each_hash do |row|
  agentid = "#{row['AgentID'].to_i + 1000}"
  @agentlist[agentid] = "#{row['LastName']}, #{row['FirstName']} "
end

# modes of operation:
# get data, output html
# get data, output json
# no get, output html
# no get, output json
# json output is determined by:  blank useragent, or cgi.has_key?('json') == true

# Now begin the output
if !cgi.has_key?('action')
  # is this is a json request, but no action define, spit back an empty object.
  if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	cgi.out({'Content-type' => 'text/json'}){
	  JSON.pretty_generate({
	  'success' => true, 
	  'need_login' => false}, 
	  :max_nesting => 0)
	}		
	exit
  end
  printheader
  printform(cgi)
  printfooter
  exit
end

result=nil

if cgi.has_key?('action') && cgi['action'] == 'submitted' &&
  cgi.has_key?('callid') && !cgi['callid'].empty?

  result = @cpx_dbh.query("SELECT * FROM billing_summaries WHERE UniqueID='#{cgi['callid']}'")

elsif cgi.has_key?('action') && cgi['action'] == 'submitted' &&
  ( (cgi.has_key?('emailid') && !cgi['emailid'].empty?) || 
   (cgi.has_key?('caseid') && !cgi['caseid'].empty?) )
  # NOOP

elsif cgi.has_key?('action') && cgi['action'] == 'submitted' &&
  cgi.has_key?('callerid') && !cgi['callerid'].empty? &&
  cgi.has_key?('datestart') && cgi.has_key?('dateend')

  begin
	datestart = Time.parse(cgi['datestart'])
  rescue
	inputerror(cgi,"datestart","Invalid start date")
  end
  begin
	dateend = Time.parse(cgi['dateend'])
  rescue
	inputerror(cgi,"dateend","Invalid end date")
  end


  callerid = cgi['callerid']

  if cgi['brand'] != 'any'
	tenantid = cgi.params['brand'][0][0..3].to_i
	brandid = cgi.params['brand'][0][4..7].to_i
	brandquery = "AND s.TenantID = '#{tenantid}' AND s.BrandID = '#{brandid}'"
  end

  result = @cpx_dbh.query("SELECT * FROM billing_summaries s INNER JOIN call_info i ON s.UniqueID = i.UniqueID WHERE s.Start >= '#{datestart.to_i}' AND s.End <= '#{dateend.to_i}' AND (i.CallerIDName LIKE '%#{@cpx_dbh.quote(callerid)}%' OR i.CallerIDNum like '%#{@cpx_dbh.quote(callerid)}%') #{brandquery}") 

elsif cgi.has_key?('action') && cgi['action'] == 'submitted' &&
  cgi.has_key?('calltype') && cgi.has_key?('datestart') && cgi.has_key?('dateend')

  begin
	datestart = Time.parse(cgi['datestart'])
  rescue
	inputerror(cgi,"datestart","Invalid start date")
  end
  begin
	dateend = Time.parse(cgi['dateend'])
  rescue
	inputerror(cgi,"dateend","Invalid end date")
  end


  # Build Brand-based Query Parameters
  brandquery = ""
  if cgi['brand'] != 'any'
	tenantid = cgi.params['brand'][0][0..3].to_i
	brandid = cgi.params['brand'][0][4..7].to_i
	brandquery = "AND TenantID = '#{tenantid}' AND BrandID = '#{brandid}'"
  end

  # Build Agent-based Query Parameters
  agentquery = ""
  case cgi['agent']
  when 'any'
	# NOTHING
  when 'none'
	agentquery = "AND AgentID = '0'"
  else
	agentquery = "AND AgentID = '#{cgi['agent']}'"
  end

  # Build CallType-based Query Parameters
  calltypequery = ""
  if cgi['calltype'] == 'temp'
	calltypequery = 'AND CallType LIKE "temp%"'
  elsif cgi['calltype'] != 'any'
	calltypequery = "AND CallType = '#{cgi['calltype']}'"
  end

  # Start the magic!
  if cgi.has_key?('length') and !cgi['length'].empty? and !cgi['length'].nil? 
	if cgi['length'].match(/^[0-9]+$/)
	  otherquery = "AND InCall > #{cgi['length']}"
	else
	  inputerror(cgi,'length','Invalid Call Length')
	end
  else
	otherquery = ""
  end

  result = @cpx_dbh.query("SELECT * FROM billing_summaries WHERE Start >= '#{datestart.to_i}' AND End <= '#{dateend.to_i}' #{calltypequery} #{brandquery} #{agentquery} #{otherquery} ORDER BY End, Start")

elsif cgi.has_key?('action') && cgi['action'] == 'grouped' && cgi.has_key?('laststate') &&
  cgi.has_key?('brand') && cgi.has_key?('datestart') && cgi.has_key?('dateend')

  begin
	datestart = Time.parse(cgi['datestart'])
  rescue
	inputerror(cgi,"datestart","Invalid start date")
  end
  begin
	dateend = Time.parse(cgi['dateend'])
  rescue
	inputerror(cgi,"dateend","Invalid end date")
  end


  where = "s.Start >= '#{datestart.to_i}' AND s.End <= '#{dateend.to_i}'"

  endstates = cgi['laststate'].match(/^[0-9,]*$/)
  endstates = endstates[0] if endstates
  where += " AND s.LastState in (#{endstates})" if endstates

  if cgi['brand'] != 'any'
	brandid = cgi['brand'].match(/^[0-9]{8}$/)
	brandid = brandid[0] if brandid
	#						   tenantid = cgi.params['brand'][0][0..3]
	#						   brandid = cgi.params['brand'][0][4..7]
	where += "AND s.TenantID='#{brandid[0..3].to_i}' AND s.BrandID='#{brandid[4..7].to_i}'" if brandid.length == 8
  end

  result = @cpx_dbh.query("SELECT count(i.CallerIDNum) as Times, i.CallerIDNum, i.CallerIDName,s.TenantID, s.BrandID FROM billing_summaries s INNER JOIN call_info i ON s.UniqueID = i.UniqueID WHERE #{where} GROUP BY CallerIDNum ORDER BY Times DESC") 

  if ( cgi.user_agent == '' ) || (cgi.has_key?('json') )
	puts "Content-type: text/json\r\n\r\n"
	# create a helper hash
	begin
	  outjson = {}
	  if datestart
		outjson['datestart'] = datestart.strftime("%Y-%m-%d %H:%M:%S")
	  end
	  if dateend
		outjson['dateend'] = dateend.strftime("%Y-%m-%d %H:%M:%S")
	  end
	  outjson['laststate'] = endstates

	  outjson['results'] = []
	  result.each_hash do |row|
		brandid = "#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"
		row['brandname'] = @brandlist[brandid]
		outjson['results'].push(row) if @brandlist.has_key? brandid
	  end
	  puts JSON.pretty_generate({
		'success' => true,
		'results' => outjson.to_json
	  }, :max_nesting => 0)
	rescue Exception => e
	  puts JSON.pretty_generate({
		'success' => false,
		'error' => "#{e.class.name}:#{e.message}"
	  }, :max_nesting => 0)
	end
  else
	printheader
	puts "<b>Starting date</b>: #{datestart.strftime("%Y-%m-%d %H:%M:%S")} <br />" if datestart
	puts "<b>Ending date</b>: #{dateend.strftime("%Y-%m-%d %H:%M:%S")} <br />" if dateend
	puts "<b>Last State</b>: #{endstates.to_s.split(',').map {|s| CDRConstants::TRANSACTIONNAMES[s.to_i]}.join(',')}<br /><br />" if endstates
	puts "<br />"
	puts "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
	puts "<tr><td>Count</td><td>Caller ID Number</td><td>Caller ID Name</td><td>Brand</td></tr>"
	result.each_hash do |row|
	  brandid = "#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"
	  next if !@brandlist.has_key?(brandid)
	  puts "<tr><td>#{row['Times']}</td><td><a href='?action=submitted&calltype=any&agent=any&brand=#{cgi['brand']}&datestart=#{cgi['datestart']}&dateend=#{cgi['dateend']}&callerid=#{row['CallerIDNum']}' target='_blank'>#{row['CallerIDNum']}</a></td><td>#{row['CallerIDName']}</td><td>#{@brandlist[brandid]}</td></tr>"
	end
	puts"</table>"
	printfooter
  end
  exit
else
  if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	puts JSON.pretty_generate({'success' => false, 'message' => 'missing parameters'}, :max_nesting => 0)
	exit
  end
  printheader	
  puts "missing parameters!"
  printfooter
  exit
end

if result && result.num_rows >= 1
  # json check!
  if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	puts "Content-type: text/json\r\n\r\n"
	# create a helper hash
	outjson = {}
	if datestart
	  outjson['datestart'] = datestart.strftime("%Y-%m-%d %H:%M:%S")
	end

	if dateend
	  outjson['dateend'] = dateend.strftime("%Y-%m-%d %H:%M:%S")
	end

	outjson['results'] = []
	result.each_hash do |row|
	  if @brandlist.has_key? "#{row['tenant'].to_s.rjust(4, '0')}#{row['brand'].to_s.rjust(4, '0')}"
		outjson['results'].push(row)
	  end
	end
	outjson['success'] = true
	outjson['total_rows'] = outjson['results'].size
	#puts outjson.to_json
	puts JSON.pretty_generate(outjson, :max_nesting => 0)

	exit
  end

  printheader

  puts "<b>Starting date</b>: #{datestart.strftime("%Y-%m-%d %H:%M:%S")} <br />" if datestart
  puts "<b>Ending date</b>: #{dateend.strftime("%Y-%m-%d %H:%M:%S")} <br /><br />" if dateend

  puts "<br />"
  puts "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
  puts "<tr class='header'><td>Date</td><td>Agent</td><td>UniqueID</td><td>Call Type</td><td>TotalDuration</td><td>InQueue</td><td>InCall</td><td>Wrapup</td><td>Brand</td><td>&nbsp;</td></tr>"
  result.each_hash do |row|
	print_call_row(row, @user['type'].to_i < 2)
  end
  puts "</table>"
  puts "<p>Calls matching query: #{result.num_rows}, queue abandoned: #{@abandoned}, ivr disconnect: #{@ivrdisco}"
elsif cgi.has_key?('callid') and !cgi['callid'].empty?
  result = @cpx_dbh.query("SELECT * FROM billing_transactions WHERE UniqueID='#{cgi['callid']}' AND Transaction=0")
  if result.num_rows == 1
	if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	  jsonbit = '&json=1'
	else
	  jsonbit = ''
	end
	rowhash = result.fetch_hash
	if @brandlist.member? "#{rowhash['TenantID'].to_s.rjust(4, '0')}#{rowhash['BrandID'].to_s.rjust(4, '0')}"
	  puts "Status: 302 Moved"
	  puts "location: calldetail.cgi?uniqueid=#{cgi['callid']}#{jsonbit}"
	else
	  if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
		puts JSON.pretty_generate({
		  'success' => false,
		  'message' => 'No results found'
		}, :max_nesting => 0)
		exit
	  end

	  printheader()
	  puts "No results found."
	end
  else
	if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	  puts JSON.pretty_generate({
		'success' => false,
		'message' => 'No results found'
	  }, :max_nesting => 0)
	  exit
	end

	printheader()
	puts "No results found."
  end

elsif cgi.has_key?('emailid') and !cgi['emailid'].empty?
  result = @cpx_dbh.query("SELECT UniqueID, TenantID FROM call_info WHERE EmailID='#{cgi['emailid']}'")
  if result.num_rows == 1
	reshash = result.fetch_hash
	uniqueid = reshash['UniqueID']
	if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	  jsonbit = '&json=1'
	else
	  jsonbit = ''
	end

	if @brandlist.member?  "#{reshash['TenantID'].to_s.rjust(4, '0')}#{reshash['BrandID'].to_s.rjust(4, '0')}"
	  puts "Status: 302 Moved"
	  puts "location: calldetail.cgi?uniqueid=#{uniqueid}#{jsonbit}"
	else
	  if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
		puts "Content-type: text/json\r\n\r\n"
		puts JSON.pretty_generate({
		  'success' => false,
		  'message' => 'No results found'
		}, :max_nesting => 0)
		exit
	  end

	  printheader()
	  puts "No results found."
	end
  elsif result.num_rows == 0
	# json check!
	if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	  puts "Content-type: text/json\r\n\r\n"
	  puts JSON.pretty_generate({
		'success' => false,
		'message' => 'No results found'
	  }, :max_nesting => 0)
	  exit
	end

	printheader(cgi, sess['bgcolor'])
	puts "No results found."
  else
	# json check!
	if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	  puts "Content-type: text/json\r\n\r\n"

	  outjson = []

	  result.each_hash do |row|
		result2 = @cpx_dbh.query("SELECT * FROM billing_summaries WHERE UniqueID='#{row['UniqueID']}'")

		if result2.num_rows == 1
		  result2.each_hash do |row2|
			outjson.push(row2)
		  end
		else
		  brandid = "#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"
		  row['brandname'] = @brandlist[brandid]
		  outjson.push(row) if @brandlist.member? row['brandid']
		end

	  end

	  puts JSON.pretty_generate({
		'success' => true,
		'results' => outjson.to_json
	  }, :max_nesting => 0)
	  exit
	end


	printheader sess['bgcolor']
	puts "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
	puts "<tr class='header'><td>Date</td><td>Agent</td><td>UniqueID</td><td>Call Type</td><td>TotalDuration</td><td>InQueue</td><td>InCall</td><td>Wrapup</td><td>Brand</td><td>&nbsp;</td></tr>"
	result.each_hash do |row|
	  result2 = @cpx_dbh.query("SELECT * FROM billing_summaries WHERE UniqueID='#{row['UniqueID']}'")
	  if result2.num_rows == 1
		result2.each_hash do |row2|
		  print_call_row(row2, @user['type'].to_i < 2)
		end
	  else
		brandid = "#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"
		if @brandlist.member? row['brandid'].to_i
		  puts "<tr><td>-</td><td>-</td><td>#{row['UniqueID']}</td><td>-</td><td>-</td><td>-</td><td>-</td><td>#{@brandlist[brandid]}</td><td><a href='calldetail.cgi?uniqueid=#{row['UniqueID']}'>Details</a></td></tr>"
		end
	  end
	end
	puts "</table>"
	puts "<p>Calls matching query: #{result.num_rows}, queue abandoned: #{@abandoned}, ivr disconnect: #{@ivrdisco}"
  end
  result.free

elsif cgi.has_key?('caseid') and !cgi['caseid'].empty?
  result = @cpx_dbh.query("SELECT UniqueID, TenantID, BrandID FROM call_info WHERE CaseID='#{cgi['caseid']}'")
  if result.num_rows == 1
	reshash = result.fetch_hash
	uniqueid = reshash['UniqueID']
	if @brandlist.member? "#{reshash['TenantID'].to_s.rjust(4, '0')}#{reshash['BrandID'].to_s.rjust(4, '0')}"
	  puts "Status: 302 Moved"
	  if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
		jsonbit = '&json=1'
	  else
		jsonbit = ''
	  end
	  puts "location: calldetail.cgi?uniqueid=#{uniqueid}#{jsonbit}"
	else
	  # json check!
	  if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
		puts "Content-type: text/json\r\n\r\n"
		puts JSON.pretty_generate({
		  'success' => false,
		  'message' => 'No results found'
		}, :max_nesting => 0)
		exit
	  end

	  printheader
	  puts "No results found."
	end	
  elsif result.num_rows == 0
	# json check!
	if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	  puts "Content-type: text/json\r\n\r\n"
	  puts JSON.pretty_generate({
		'success' => false,
		'message' => 'No results found'
	  }, :max_nesting => 0)
	  exit
	end


	printheader
	puts "No results found."
  else
	# json check
	if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	  puts "Content-type: text/json\r\n\r\n"
	  outjson = []

	  result.each_hash do |row|
		result2 = @cpx_dbh.query("SELECT * FROM billing_summaries WHERE UniqueID='#{row['UniqueID']}'")
		if result2.num_rows == 1
		  result2.each_hash do |row2|
			outjson.push(row2) if @brandlist.has_key? "#{row2['TenantID'].to_s.rjust(4, '0')}#{row2['BrandID'].to_s.rjust(4, '0')}"
		  end
		else
		  brandid = "#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"
		  row['brandname'] = @brandlist[brandid]
		  outjson.push(row) if @brandlist.has_key? brandid
		end
	  end

	  puts JSON.pretty_generate({
		'success' => true,
		'results' => outjson.to_json
	  }, :max_nesting => 0)
	  exit
	end

	printheader
	puts "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
	puts "<tr class='header'><td>Date</td><td>Agent</td><td>UniqueID</td><td>Call Type</td><td>TotalDuration</td><td>InQueue</td><td>InCall</td><td>Wrapup</td><td>Brand</td><td>&nbsp;</td></tr>"
	result.each_hash do |row|
	  result2 = @cpx_dbh.query("SELECT * FROM billing_summaries WHERE UniqueID='#{row['UniqueID']}'")
	  if result2.num_rows == 1
		result2.each_hash do |row2|
		  print_call_row(row2, @user['type'].to_i < 2)
		end
	  else
		brandid = "#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"
		if @brandlist.member? brandid
		  puts "<tr><td>-</td><td>-</td><td>#{row['UniqueID']}</td><td>-</td><td>-</td><td>-</td><td>-</td><td>#{@brandlist[brandid]}</td><td><a href='calldetail.cgi?uniqueid=#{row['UniqueID']}'>Details</a></td></tr>"
		end
	  end
	end
	puts "</table>"
	puts "<p>Calls matching query: #{@callsmatch}, queue abandoned: #{@abandoned}, ivr disconnect: #{@ivrdisco}"
  end
  result.free

else
  # json check

  if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	puts "Content-type: text/json\r\n\r\n"
	puts JSON.pretty_generate({
	  'success' => false,
	  'total_rows' => 0,
	  'results' => [],
	  'message' => 'No results found'
	}, :max_nesting => 0)
	exit
  end

  printheader
  puts "No results found."
end

printfooter
exit

