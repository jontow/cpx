#!/usr/local/bin/ruby
################################################################################
#
# 2009-01-22 -- jontow@
#
# Custom Report to get average call and wrapup lengths over a given date range
# disincluding one tenantid.
#
################################################################################

$LOAD_PATH << File.dirname(__FILE__)

require 'cgi'

begin
	require 'rubygems'
rescue LoadError
end

begin
	require 'mysql'
rescue LoadError
end

require 'time'
require 'localconfig'

include WebConfig

cgi = CGI.new
@cpx_dbh = Mysql.real_connect(DBHOST, DBUSER, DBPASS, DBNAME)

# Brand Name: to be used later?
@brandlist = Hash.new
result = @cpx_dbh.query("SELECT tenant,brand,brandname FROM brandlist");
result.each_hash do |row|
	brandid = "#{row['tenant'].to_s.rjust(4, '0')}#{row['brand'].to_s.rjust(4, '0')}"
	@brandlist[brandid] = row['brandname']
end

# Agent List
@agentlist = Hash.new
result = @cpx_dbh.query("SELECT AgentID,FirstName,LastName FROM tblAgent") #WHERE Active=1")
result.each_hash do |row|
	agentid = "#{row['AgentID'].to_i + 1000}"
	@agentlist[agentid] = "#{row['LastName']}, #{row['FirstName']}"
end

if !cgi.has_key?('action')
	puts "Content-type: text/html\r\n\r\n"
	puts "<html><head><title>Call/Wrapup Average Length</title></head><body>"
	puts "<form method=\"POST\" action=\"?\">"

	now = Time.now
	tomorrow = Time.now + 86400
	formstart = Time.mktime(now.year, now.month, now.day).strftime("%Y-%m-%d %H:%M")
	formend = Time.mktime(tomorrow.year, tomorrow.month, tomorrow.day).strftime("%Y-%m-%d %H:%M")
	puts "<label>Date Start:</label> <input type=\"text\" name=\"datestart\" value=\"#{formstart}\"><br />"
	puts "<label>Date End:</label> <input type=\"text\" name=\"dateend\" value=\"#{formend}\"><br /><br />"

	puts "<label>Brand:</label> <select name=\"brand\">"
	puts "<option value=\"any\" selected=\"selected\">Any</option>"
	#puts "<option value=\"allnoesoft\">Any (without Esoft)</option>"
	@brandlist.sort{|a,b| a[1].downcase <=> b[1].downcase}.each do |a|
		puts "<option value=\"#{a[0]}\">#{a[1]}</option>"
	end
	puts "</select><br />"

	puts "<label>Agent:</label> <select name=\"agent\">"
	puts "<option value=\"any\" selected=\"selected\">Any</option>"
	puts "<option value=\"none\">Unknown</option>"
	@agentlist.sort{|a,b| a[1].downcase <=> b[1].downcase}.each do |a|
		puts "<option value=\"#{a[0]}\">#{a[1]}</option>"
	end
	puts "</select><br />"

	puts "<label>View Format:</label> <select name=\"viewformat\">"
	puts "<option value=\"onscreen\" select=\"selected\">In browser</option>"
	puts "<option value=\"download\">Download CSV</option>"
	puts "</select><br />"

	puts "<input type=\"hidden\" name=\"action\" value=\"submitted\">"
	puts "<p class='submit'><input type=\"submit\" name=\"submit\" value=\"submit\"></p>"
	puts "</form>"
	puts "</body></html>"
	exit
end

viewformat = nil
if cgi.has_key?('viewformat') and cgi['viewformat'] == 'download'
	puts "Content-type: application/binary\r\n"
	puts "Content-disposition: attachment; filename=#{Time.now.strftime("%Y-%m-%d_%H:%M:%S")}-avglen.csv\r\n\r\n"
	viewformat = "csv"
else
	puts "Content-type: text/html\r\n\r\n"
	viewformat = "html"
end

if cgi.has_key?('datestart') and cgi.has_key?('dateend')
	datestart = Time.parse(cgi['datestart'])
	dateend = Time.parse(cgi['dateend'])
else
	puts "Can't parse date."
	exit
end

callquery = "SELECT AgentID,InCall,WrapUp FROM billing_summaries WHERE Start > #{datestart.to_i} AND End < #{dateend.to_i} AND InCall > '0' AND AgentID != '0' AND CallType = 'call'"

curagent = nil
rowcount = 0
calltime = 0
wrapuptm = 0
result = nil

agents = {}

if cgi.has_key?('brand') and !cgi['brand'].empty?
	case cgi['brand']
		when 'any'
			# let it happen..
		#when 'allnoesoft'
		#	callquery += " AND TenantID != '17'"
		else
			tenantid = cgi.params['brand'].to_s[0..3].to_i
			brandid = cgi.params['brand'].to_s[4..7].to_i
			callquery += " AND TenantID = '#{tenantid}' AND BrandID = '#{brandid}'"
		end
end

#p callquery

if cgi.has_key?('agent') and !cgi['agent'].empty?
	if cgi['agent'] != 'any'
		curagent = cgi['agent']
		callquery += " AND AgentID = #{cgi['agent']}"
	end

	result = @cpx_dbh.query(callquery)

	if result && result.num_rows >= 1
		result.each_hash do |row|
			#p row
			if cgi['agent'] == 'any'
				curagent = row['AgentID']
				#puts "Setting curagent to #{row['AgentID']}"
			end

			if agents[curagent].nil?
				agents[curagent] = [0, 0, 0]
			end
			agents[curagent][0] += row['InCall'].to_i
			agents[curagent][1] += row['WrapUp'].to_i
			agents[curagent][2] += 1
		end
	end

	#p @agentlist
	#p agents
	#puts "\n\n\n\n\n"
	if viewformat == "html"
		puts "<html><head><title>Average Call Length</title></head><body>"
		puts "<h3>Average Call Length: #{datestart.strftime("%Y-%m-%d %H:%M:%S")} - #{dateend.strftime("%Y-%m-%d %H:%M:%S")}</h3>"
		puts "<br>"
		puts "<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\">"
		puts "<tr><th>Agent</th><th>Avg Call Time</th><th>Avg Wrapup Time</th><th>Number of Calls</th></tr>"
	else
		puts "# Agent,avgcall,avgwrapup,numcalls"
	end
	agents.each do |k, v|
		avgcall = sprintf("%.2f", ((v[0] / v[2]) / 60.0))
		avgwrap = sprintf("%.2f", ((v[1] / v[2]) / 60.0))
		agentname = @agentlist[k]

		if agentname.nil? or agentname.empty?
			if viewformat == "html"
				puts "<tr><td>#{k}</td><td>#{avgcall}</td><td>#{avgwrap}</td><td>#{v[2]}</td></tr>"
			else
				puts "\"#{k}\",#{avgcall},#{avgwrap},#{v[2]}"
			end
		else
			if viewformat == "html"
				puts "<tr><td>#{@agentlist[k]}</td><td>#{avgcall}</td><td>#{avgwrap}</td><td>#{v[2]}</td></tr>"
			else
				puts "\"#{@agentlist[k]}\",#{avgcall},#{avgwrap},#{v[2]}"
			end
		end
	end

	if viewformat == "html"
		puts "</table></body></html>"
	end
end

exit
