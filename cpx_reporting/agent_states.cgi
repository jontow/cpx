#!/usr/local/bin/ruby

$LOAD_PATH << File.dirname(__FILE__)

require 'rubygems'
require 'cgi'
require 'mysql'
require 'time'

require 'localconfig'
include WebConfig
require 'constants/agentconstants.rb'
include AgentConstants

@webroot = WebConfig::WEBROOT
@spoolroot = WebConfig::SPOOLROOT

###### Main Execution Line ######
@cgi = CGI.new
@fields = @cgi.keys

# Initialize the DB handle early, will need it to retrieve a valid brand/agent list.
@cpx_dbh = Mysql.real_connect(WebConfig::DBHOST, WebConfig::DBUSER, WebConfig::DBPASS, WebConfig::DBNAME)

@releaseopts = {0=>["Default Release", -1]}
@cpx_dbh.query("SELECT id, label, utilbias from release_options").each do |r|
	@releaseopts[r[0].to_i] = [r[1], r[2].to_i]
end

# +, 0, - counters, effect on utilization
@posrelease = 0
@neurelease = 0
@negrelease = 0

###### Local Methods ######

def mysqlquery(querystring)
	querytype = querystring[0..6].downcase
	case querytype
	when 'select ', 'describ' then
#		puts "query submitted:<br />#{querystring}<br />"
		return @cpx_dbh.query(querystring)
	else
		puts "Unauthorized query cancelled.<br />"
		exit
#		puts "non-select query not submitted:<br />#{querystring}<br />"
	end
end

def formheader
	puts "<form action='' name='postform' method='POST'>\n"
end

def formfooter
	puts "</form>\n"
end
 
def printheader
	puts "Content-type: text/html\r\n\r\n"
	puts "<html><head>"
	puts "<META HTTP-EQUIV='expires' VALUE='Thu, 16 Mar 2000 11:00:00 GMT'>"
	puts "<META HTTP-EQUIV='pragma' CONTENT='no-cache'>"
	puts "<title>Agent States Report</title>\n</head>\n<body>\n"
end

def printfooter
	puts "\n</body></html>"
end

def decodedata(state, data)
	case state.to_i
	when AgentConstants::RELEASED
		data = 0 if data.nil?
		@releaseopts[data.to_i][0]
	else
		if data.nil? or data.empty?
			'&nbsp;'
		else
			"<a href='calldetail.cgi?uniqueid=#{data}'>#{data}</a>"
		end
	end
end

if !@cgi.has_key?('action')
	now = Time.now
	hourago = Time.now - 3600
	formstart = Time.mktime(hourago.year, hourago.month, hourago.day, hourago.hour, hourago.min).strftime("%Y-%m-%d %H:%M")
	formend = Time.mktime(now.year, now.month, now.day, now.hour, now.min).strftime("%Y-%m-%d %H:%M")
	printheader
	puts "<center>\n"
	formheader
	agents = mysqlquery("SELECT * FROM tblAgent WHERE AgentID > 0 AND Active = 1 ORDER BY LastName")
	puts "<h2>Select range:</h2><br />\n"
	puts "Agent: <select name='agent'>"
	puts "<option value='any' selected='selected'>Any</option>"
	agents.each_hash do |agent|
		puts "<!--"
		p agent
		puts "-->"
		puts "<option value='#{agent['AgentID'].to_i + 1000}'>#{agent['LastName']} #{agent['FirstName']}</option>"
	end	
	puts "</select><br />"
	puts "Date Start: <input type=\"text\" name=\"datestart\" value=\"#{formstart}\"><br />"
	puts "Date End: <input type=\"text\" name=\"dateend\" value=\"#{formend}\"><br /><br />"
	puts "<input type='submit' name='action' value='Submit' /><br /><br />\n"
	formfooter
	printfooter
	exit
else
	printheader
	case @cgi['action']
	when "Submit" then
		datestart = Time.parse(@cgi['datestart'])
		dateend = Time.parse(@cgi['dateend'])
		if @cgi.has_key?('fuzzy')
			realdatestart = datestart
			realdateend = dateend
			datestart -= 300
			dateend += 300
		end
		agentquery = ""
		if @cgi['agent'] != 'any'
			agentquery = "AND Agent = #{@cgi['agent']}"
		end
		result = mysqlquery("SELECT * FROM agent_states WHERE ((start > #{datestart.to_i} AND start < #{dateend.to_i}) OR (end > #{datestart.to_i} AND end < #{dateend.to_i})) #{agentquery}")
		if result.num_rows > 0
			states = []
			savedata = nil
			result.each_hash { |row| states << row }
			uniqstates = states.collect { |state| state['oldstate'] }.sort.uniq.collect {|state| {'state'=>state, 'count'=>0} }
			puts "<!---#{uniqstates.inspect}--->"                                                           
			puts "<br /><table border='2' cellborder='2'>"
			puts "<tr><td>Agent</td><td>Time</td><td>Old State</td><td>Elapsed</td><td>New State</td><td>Call ID</td></tr>"
			states.each do |state|
				if AgentConstants::STATENAMES[state['oldstate'].to_i] == 'Ringing' and AgentConstants::STATENAMES[state['newstate'].to_i] != 'OnCall'
					puts "<tr style='background:red'>"
				elsif @cgi.has_key?('fuzzy') and (Time.at(state['end'].to_i) > realdateend or Time.at(state['start'].to_i) < realdatestart)
					puts "<tr style='background:salmon'>"
				else
					puts "<tr style='background:white'>"
				end

				# XXX: magic number alert.. agent id base
				result = mysqlquery("SELECT * FROM tblAgent WHERE AgentID = '#{state['agent'].to_i - 1000}'")
				if result.num_rows == 1 
					result.each_hash { |agent| puts "<td>#{agent['Login']}</td>" } 
				else
					puts "<td>#{state['agent']}</td>"
				end
				puts "<td>#{Time.at(state['end'].to_i).strftime("%Y-%m-%d %H:%M:%S")}</td>"
				puts "<td>#{AgentConstants::STATENAMES[state['oldstate'].to_i]}</td>"
				elapse = state['end'].to_i - state['start'].to_i

				if AgentConstants::STATENAMES[state['newstate'].to_i] == 'Released'
					savedata = state['data']
				end

				if AgentConstants::STATENAMES[state['oldstate'].to_i] == 'Released'
					if savedata.nil? or savedata == ''
						# XXX: magic number alert: default release state id
						savedata = "0"
					end

					if @releaseopts[savedata.to_i][1] == -1
						@negrelease += elapse
					elsif @releaseopts[savedata.to_i][1] == 0
						@neurelease += elapse
					elsif @releaseopts[savedata.to_i][1] == 1
						@posrelease += elapse
					else
						puts "RELEASE-ERROR"
					end
				end

				uniqstates.detect { |u| u['state'].to_i == state['oldstate'].to_i}['count'] += elapse
				puts "<td>#{'%2.2d:%2.2d:%2.2d' % [elapse / 3600, elapse % 3600 / 60, elapse % 3600 % 60]}</td>"
				puts "<td>#{AgentConstants::STATENAMES[state['newstate'].to_i]}</td>"
				#if state['data'] and !state['data'].empty?
					#puts "<td><a href='calldetail.cgi?uniqueid=#{state['data']}'>#{state['data']}</a></td>"
				#else
					#puts "<td>&nbsp;</td>"
				#end
				puts "<td>#{decodedata(state['newstate'], state['data'])}</td>"
				puts "</tr>"
			end
			puts "</table><br />"
			total = 0
			uniqstates.each { |u| total += u['count'] }
			puts "<table border='2' cellborder='2'>"
			puts "<tr>"
			uniqstates.each do |u|
				next if AgentConstants::STATENAMES[u['state'].to_i] == "Released"
				puts "<td>#{AgentConstants::STATENAMES[u['state'].to_i]}</td>"
			end

			puts "<td>Positive Release</td>"
			puts "<td>Neutral Release</td>"
			puts "<td>Negative Release</td>"
			puts "</tr><tr>"
			uniqstates.each do |u|
				next if AgentConstants::STATENAMES[u['state'].to_i] == "Released"
				puts "<td>#{'%2.2d:%2.2d:%2.2d' % [u['count'] / 3600, u['count'] % 3600 / 60, u['count'] % 3600 % 60]}</td>"
			end
			# Positive Release
			puts "<td>#{'%2.2d:%2.2d:%2.2d' % [@posrelease / 3600, @posrelease % 3600 / 60, @posrelease % 3600 % 60]}</td>"
			# Neutral Release
			puts "<td>#{'%2.2d:%2.2d:%2.2d' % [@neurelease / 3600, @neurelease % 3600 / 60, @neurelease % 3600 % 60]}</td>"
			# Negative Release
			puts "<td>#{'%2.2d:%2.2d:%2.2d' % [@negrelease / 3600, @negrelease % 3600 / 60, @negrelease % 3600 % 60]}</td>"
			puts "</tr>"
			if total > 0
				puts "<tr>"
				uniqstates.each do |u|
				next if AgentConstants::STATENAMES[u['state'].to_i] == "Released"
					puts "<td>#{'%2.2f' % ((u['count'].to_f / total) * 100)}% </td>"
				end

				# Positive Release
				puts "<td>#{'%2.2f' % ((@posrelease.to_f / total) * 100)}% </td>"
				# Neutral Release
				puts "<td>#{'%2.2f' % ((@neurelease.to_f / total) * 100)}% </td>"
				# Negative Release
				puts "<td>#{'%2.2f' % ((@negrelease.to_f / total) * 100)}% </td>"

				puts "</tr>"
			end
			puts "</table>"
			puts "<!---#{uniqstates.inspect}--->"                                                           

			# XXX: this part is a bit of a hack, as it has intrinsic knowledge of what states exist,
			#      as well as what they mean.  This is fragile and will break amusingly.

			ut = {
				'offline' => 0,
				'idle' => 0,
				'ringing' => 0,
				'precall' => 0,
				'oncall' => 0,
				'outgoingcall' => 0,
				'wrapup' => 0,
				'posrelease' => 0,
				'neurelease' => 0,
				'negrelease' => 0
			}

			uniqstates.each do |u|
				ut['offline'] = u['count'] if u['state'].to_i == AgentConstants::OFFLINE
				ut['idle'] = u['count'] if u['state'].to_i == AgentConstants::IDLE
				ut['ringing'] = u['count'] if u['state'].to_i == AgentConstants::RINGING
				ut['precall'] = u['count'] if u['state'].to_i == AgentConstants::PRECALL
				ut['oncall'] = u['count'] if u['state'].to_i == AgentConstants::ONCALL
				ut['outgoingcall'] = u['count'] if u['state'].to_i == AgentConstants::OUTGOINGCALL
				ut['wrapup'] = u['count'] if u['state'].to_i == AgentConstants::WRAPUP
			end

			ut['posrelease'] = @posrelease
			ut['neurelease'] = @neurelease
			ut['negrelease'] = @negrelease

			total = ut['offline'] + ut['idle'] + ut['ringing'] + ut['precall'] + ut['oncall'] + ut['outgoingcall'] + ut['wrapup'] + ut['posrelease'] + ut['neurelease'] + ut['negrelease']
			loggedin = total - ut['offline'] - ut['neurelease']
			postime = loggedin - ut['idle'] - ut['negrelease']
			util = (postime.to_f / loggedin.to_f) * 100

			printf "<p>Utilization: %.2f%% (postime:%2.2d:%2.2d:%2.2d / loggedin:%2.2d:%2.2d:%2.2d)</p>\n", util, (postime / 3600), (postime % 3600 / 60), (postime % 3600 % 60), (loggedin / 3600), (loggedin % 3600 / 60), (loggedin % 3600 % 60)
		else
			puts "No records found."
		end
	else
		puts "I dunno"
	end

	printfooter
end

exit

