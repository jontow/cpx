#!/usr/local/bin/ruby

$LOAD_PATH << File.dirname(__FILE__)

require 'cgi'
require 'cgi/session'
require 'securerandom'

begin
	require 'rubygems'
rescue LoadError
	# noop
end

begin
	require 'json'
	require 'mysql'
rescue LoadError => e
	puts "Content-type: text/html\r\n\r\n"
	puts "Please install the json and mysql ruby libraries. <br/>"
	puts e.message
	exit
end

require 'localconfig'
include WebConfig

cgi = CGI.new("html4")
sess = CGI::Session.new(cgi,
	'prefix' => 'lookup_sess_'
)

def head(cgi)
	cgi.title("Call Lookup - Login") + "
	<style> 
		label { 
			width: 10em;
			float: left;
			text-align: right;
			margin-right: 0.1em;
			display:block;
			font-weight: bold;
			clear:both;
		}

		.err {
			color: #ff3300;
		}
	</style>"
end

def login_form(cgi, errmessage = '')
	unless errmessage == ''
		errmessage = "<p><label>&nbsp;</label><span class='err'>" + errmessage + "</span></p>"
	end
	cgi.body{ "\n" + 
		cgi.form(method = "post", ""){ "\n" + 
			errmessage + 
			"<p><label>Username</label>" + 
			cgi.text_field({
				'name' => "username",
				'size' => '20'
			}) + 
			"</p><p><label>Password</label>" + 
			cgi.password_field({
				'name' => "password",
				'size' => '20'
			}) + 
			"</p><p><label>&nbsp</label>" + 
			cgi.hidden("action", "login") + 
			"<input type='submit' value='submit' name='submit' />"
		}
	}
end

errmessage = ''
dojson = false
if(cgi.has_key? 'json') or (cgi.user_agent == '')
	dojson = true
	jsonhash = {}
end

if cgi.has_key? 'action' and cgi['action'] == 'login'
	username = cgi['username']
	password = cgi['password']
	sess = CGI::Session.new(cgi,
		'prefix' => 'lookup_sess_',
		'new_session' => true
	)
	begin


		@cpx_dbh = Mysql.real_connect(WebConfig::DBHOST, WebConfig::DBUSER, WebConfig::DBPASS, WebConfig::DBNAME)

		@brandlist = {}
		result = @cpx_dbh.query("SELECT tenant,brand,concat(lpad(tenant,4,'0'),lpad(brand,4,'0')) as brandid, brandname FROM brandlist");
		result.each_hash do |row|
			@brandlist[row['brandid']] = row['brandname']
		end

		sess['cpxkey'] = SecureRandom.base64
		sess['count'] = 1
		sess['user'] = username

		# XXX: lamest security ever
		if username != 'admin1' or (username == 'admin1' and password != 'admin')
			if dojson
				jsonhash['error'] = 'insufficient permission'
				jsonhash['success'] = false
				cgi.out({'Content-type' => 'text/json'}){
					JSON.pretty_generate(jsonhash, :max_nesting => 0)
				}
				exit
			end
			cgi.out{
				cgi.head{
					head cgi
				} + 
				cgi.body{
					login_form(cgi, 'insufficient permission')
				}
			}
			exit
		end
		companies = [] 
		begin
			# now hack 'companies' out of brandlist.
			@brandlist.each do |k,v|
				companies << k
			end
		rescue
			companies = []
		end
		sess['companies'] = companies.to_json

		# XXX: another hack, I have no idea what 'fullresult' should be.
		sess['fullresult'] = {'type'=>'1','security_level'=>'5'}.to_json

		sess.update
		if dojson
			cgi.out({'Content-type' => 'text/json'}){
				JSON.pretty_generate({'success' => true}, :max_nesting => 0)
			}
			exit
		end
		cgi.out({'Status' => 302, 'Location' => "lookup.cgi#{"?#{cgi.query_string}" if cgi.query_string}"}){
			cgi.html{
				cgi.body{
					"redirecting to lookup..."
				}
			}
		}
		exit
	rescue RuntimeError => e
		errmessage = e.message
	end
end

if cgi.has_key? 'action' and cgi['action'] == 'logout'
	sess.delete
	if (cgi.has_key? 'json') || (cgi.user_agent == '')
		cgi.out({'Content-type' => 'text/json'}){
			JSON.pretty_generate({'success' => true})
		}
		exit
	end
	cgi.out{
		cgi.head{
			head cgi
		} + 
		cgi.body{
			login_form(cgi, 'logged out')
		}
	}
	exit
end

if sess and sess['cpxkey'] != nil
	sess.update
end

unless sess['fullresult']
	cgi.out{
		cgi.head{
			head cgi
		} + 
		cgi.body{
			login_form(cgi)
		}
	}
	exit
end

loggedin = JSON.parse(sess['fullresult'])

# XXX: Debugging goo?
cgi.out{
	cgi.head{
		head cgi
	} +
	cgi.body{
		login_form(cgi, errmessage) + 
		"<p>" + 
		"<table border>" + 
		loggedin.map do |k, v|
			"<tr><th>#{k}</th><td>#{v}</td></tr>"
		end.to_s +
		"</table></p>" + 
		"<p>Company list</p><ol>" +  
		JSON.parse(sess['companies']).each do |v|
			"<li>#{v}</li>"
		end.to_s + 
		"</ol>"
	}
}
