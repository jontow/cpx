#!/usr/bin/env /usr/local/bin/ruby

$LOAD_PATH << File.dirname(__FILE__)

require 'cgi'
require 'cgi/session'
require 'time'
require 'net/smtp'

begin
	require 'rubygems'
rescue LoadError
	# NOOP
end

begin
	require 'json'
	require 'mysql'
rescue LoadError => e
	puts "Content-type: text/html\r\n\r\n"
	puts "Please install the json and mysql ruby libraries. <br/>"
	puts e.message
	exit
end

require 'constants/cdrconstants'

require 'localconfig'
include WebConfig

@webroot = WebConfig::WEBROOT
@spoolroot = WebConfig::SPOOLROOT

###### Local Methods ######
def printheader
	puts "<html><head><title>Edit Call Summary</title>"
	puts "<style type=\"text/css\">

	label.category {
		width: 8em;
		float: left;
		display: block;
	}

	table {
		//margin-left: 8em;
	}
	tr.header td {
		font-weight: bold;
		text-align: center;
	}
	</style>"
	puts "</head></body>"
end

def printfooter
	puts "</body></html>"
end

def send_email(from, from_alias, to, to_alias, subject, message)
	STDERR.puts to
	msg = "From: \"#{from_alias}\"<#{from}>\r\nTo: #{to}\r\nSubject: #{subject}\r\n\r\n#{message}"
	Net::SMTP.start("#{MAILSERVER}") do |smtp|
		to.split(",").each { |addr| smtp.send_message msg, from, addr }
	end
end


#def decode_data(transaction, data, callstart, callend, url=true)
#	case transaction
#	when CDRConstants::DIALOUTGOING
#		# decode brandid
#		@brandlist[data]
#	when CDRConstants::INWRAPUP, CDRConstants::ENDWRAPUP, CDRConstants::RINGING, CDRConstants::INCALL
#		if url
#		"<a href='agent_states.cgi?action=Submit&agent=#{data}&datestart=#{callstart.strftime('%Y/%m/%d-%H:%M:%S')}&dateend=#{callend.strftime('%Y/%m/%d-%H:%M:%S')}&fuzzy=1'>#{@agentlist[data]}</a>"
#		else
#			@agentlist[data]
#		end
#	when CDRConstants::INQUEUE, CDRConstants::ABANDONQUEUE
#		@queuelist[data]
#	else
#		data
#	end
#end

###### Main Execution Line ######
cgi = CGI.new
fields = cgi.keys
sess = CGI::Session.new(cgi,
	'prefix' => 'lookup_sess_'
)

if sess['cpxkey'] == nil
	cgi.out({'Status' => 302, 'Location' => 'login.cgi'}){
		"redirecting to login page..."
	}
	exit
end

user = JSON.parse(sess['fullresult'])

if user['type'].to_i > 1 or user['security_level'].to_i < 4
	cgi.out({'Status' => '403'}){
		"insufficient privledges"
	}
	exit
end

# json check
if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	puts "Content-type: text/json\r\n\r\n"
else
	puts "Content-type: text/html\r\n\r\n"
	printheader
end


if !cgi.has_key?('uniqueid')
	if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
		puts "{}"
		exit
	end

	outstring = "<form action='' method='POST'>Unique ID: <input type='text' name='uniqueid'><input type='submit' value='Search'></form>"
	puts outstring
	printfooter
	exit
end

cpx_dbh = Mysql.real_connect(WebConfig::DBHOST, WebConfig::DBUSER, WebConfig::DBPASS, WebConfig::DBNAME)

# Brand Name
@brandlist = Hash.new
result = cpx_dbh.query("SELECT tenant,brand,brandname FROM brandlist");
result.each_hash do |row|
	brandid = "#{row['tenant'].to_s.rjust(4, '0')}#{row['brand'].to_s.rjust(4, '0')}"
	@brandlist[brandid] = row['brandname']
end

# Agent List
@agentlist = Hash.new
result = cpx_dbh.query("SELECT AgentID,FirstName,LastName FROM tblAgent");
result.each_hash do |row|
	agentid = "#{row['AgentID'].to_i + 1000}"
	@agentlist[agentid] = "#{row['FirstName']} #{row['LastName']}"
end

## queue list
#@queuelist = Hash.new
#result = cpx_dbh.query("SELECT name, commonname FROM queues")
#result.each do |row|
#	@queuelist[row[0]] = row[1]
#end

# Start the magic!
outjson = Hash.new
outstring = ''

if cgi.has_key?('action') and (cgi['action'] == 'edit')
	if (!(cgi['inqueue'] and cgi['incall'] and cgi['wrapup']))
		puts "Something seems to be wrong here. Aborting."
	else
		q = cgi['inqueue'].to_i
		c = cgi['incall'].to_i
		w = cgi['wrapup'].to_i
		querystr = "SELECT * FROM billing_summaries WHERE UniqueID = '#{cpx_dbh.quote(cgi['uniqueid'])}'"
		result = cpx_dbh.query(querystr)
		row = result.fetch_hash
		oq = row['InQueue']
		oc = row['InCall']
		ow = row['WrapUp']
		send_email(EDITFROM,'CPX Call Summary Editor',EDITNOTIFY,'','Call Summary Edit',"A call record has been edited:\nCall Info:\nUnique ID: #{cgi['uniqueid']}\nBrand: #{@brandlist["#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"]}\nAgent: #{@agentlist[row['AgentID']]}\nTime: #{Time.at(row['Start'].to_i).strftime('%F %H:%M')}\n\nEdit Info:\n#{"In Queue time changed from #{oq} to #{q}\n" if q.to_i != oq.to_i}#{"In call time changed from #{oc} to #{c}\n" if c.to_i != oc.to_i}#{"Wrapup time changed from #{ow} to #{w}\n" if w.to_i != ow.to_i}Edit made by: #{cgi.remote_user}\nEdit made from: #{cgi.remote_addr}")
		querystr = "UPDATE billing_summaries SET InQueue='#{cpx_dbh.quote(q.to_s)}', InCall='#{cpx_dbh.quote(c.to_s)}', WrapUp='#{cpx_dbh.quote(w.to_s)}' WHERE UniqueID='#{cpx_dbh.quote(cgi['uniqueid'])}'"
		result = cpx_dbh.query(querystr)
	end
end

outstring += "<br />"
outstring += "<label class='category'><b>UniqueID</b>:</label> #{cgi['uniqueid']}<br />"

#outstring += "<br /><hr /><br />"

outstring += "<br/><label class='category'><b>Summary</b>:</label>"
# Being with the billing summary
result = cpx_dbh.query("SELECT * FROM billing_summaries WHERE UniqueID = '#{cgi['uniqueid']}'")
callstart = nil
callend = nil
outjson['summary'] = []

inqueue = 0
incall = 0
wrapup = 0

if result.num_rows >= 1

	outstring += "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
	outstring += "<tr class='header'><td>Brand</td><td>TenantID</td><td>BrandID</td><td>Start</td><td>End</td><td>InQueue</td><td>InCall</td><td>WrapUp</td><td>AgentID</td></tr>"
	result.each_hash do |row|
		outstring += "<tr>"
		outstring += "<td>#{@brandlist["#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"]}</td>"
		outstring += "<td>#{row['TenantID']}</td>"
		outstring += "<td>#{row['BrandID']}</td>"
		outstring += "<td>#{Time.at(row['Start'].to_i).strftime('%F %H:%M')}</td>"
		outstring += "<td>#{Time.at(row['End'].to_i).strftime('%F %H:%M')}</td>"
		outstring += "<td>#{row['InQueue']}</td>"
		outstring += "<td>#{row['InCall']}</td>"
		outstring += "<td>#{row['WrapUp']}</td>"
		outstring += "<td>#{@agentlist[row['AgentID']]}</td>"
		callstart = Time.at(row['Start'].to_i)
		callend = Time.at(row['End'].to_i)
		inqueue = row['InQueue']
		incall = row['InCall']
		wrapup = row['WrapUp']  
		sumrow = row
		sumrow['callstart'] = Time.at(row['Start'].to_i)
		sumrow['callend'] = Time.at(row['End'].to_i)
		outjson['summary'].push(sumrow)
	end
	outstring += "</table>"
else
	outstring += "No summary found.<br/>"
end

#outstring += "<br /><hr /><br />"
outstring += "<br/><label class='category'><b>Call Info</b>:</label>"
outjson['call_info'] = []
# Now the call_info crap..
result = cpx_dbh.query("SELECT * FROM call_info WHERE UniqueID = '#{cgi['uniqueid']}'")
if result.num_rows >= 1
	outstring += "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
	outstring += "<tr class='header'><td>TenantID</td><td>BrandID</td><td>CallType</td><td>DNIS</td><td>CallerID</td><td>DialedNumber</td><td>CaseID</td><td>VoiceMailID</td><td>EmailID</td></tr>"
	result.each_hash do |row|
		outstring += "<tr>"
		outstring += "<td>#{row['TenantID']}</td>"
		outstring += "<td>#{row['BrandID']}</td>"
		outstring += "<td>#{row['CallType']}</td>"
		outstring += "<td>#{row['DNIS'].nil? ? '-' : row['DNIS']}</td>"
		if row['CallerIDName'].nil? or row['CallerIDNum'].nil?
			outstring += '<td>-</td>'
		else
			outstring += "<td>#{row['CallerIDName']} (#{row['CallerIDNum']})</td>"
		end
		outstring += "<td>#{row['DialedNumber'].nil? ? '-' : row['DialedNumber']}</td>"
		outstring += "<td>#{row['CaseID'].nil? ? '-' : row['CaseID']}</td>"
		if row['VoicemailID'] and !row['VoicemailID'].empty?
			outstring += "<td><a href=\"?uniqueid=#{row['VoicemailID']}\">#{row['VoicemailID']}</a></td>"
		else
			outstring += '<td>-</td>'
		end
		if row['EmailID'] and !row['EmailID'].to_i.zero?
			outstring += "<td><a href=\"#{WebConfig::EMAILROOTURL}?id=#{row['EmailID']}&print=1&headers=1\">#{row['EmailID']}</a></td>"
		else
			outstring += '<td>-</td>'
		end

		outjson['call_info'].push(row)

		outstring += "</tr>"
	end
	outstring += "</table><br />"

else
	outstring += "No call info found.<br/>"
end

outstring += "<br/><form action='' method='GET'><br/>"
outstring += "<input type='hidden' name='uniqueid' value='#{cgi['uniqueid']}'>"
outstring += "<input type='hidden' name='action' value='edit'>"
outstring += "<fieldset label='Edit Call'>"
outstring += "Values are number of seconds."
outstring += "<table border='0' cellspacing='0'>"
outstring += "<tr><td>In Queue:</td><td><input type='text' name='inqueue' value='#{inqueue}'></input></td></tr>"
outstring += "<tr><td>In Call:</td><td><input type='text' name='incall' value='#{incall}'></input></td></tr>"
outstring += "<tr><td>Wrap Up:</td><td><input type='text' name='wrapup' value='#{wrapup}'></input></td></tr>"
outstring += "</table>"
outstring += "<input type='submit' value='Commit'>"
outstring += "</fieldset>"
outstring += "</form>"
outstring += "<table border='1' cellborder='0'>"
outstring += "<tr><td colspan='3'>Second Calculator</td></tr>"
outstring += "<tr><td>HH:MM:SS</td><td><input type='text' id='hour' name='hour' size='2' onchange='seconds();'>:<input type='text' id='minute' name='minute' size='2' onchange='seconds();'>:<input type='text' id='second' name='second' size='2' onchange='seconds();'></td><td><button onclick='seconds()'>Calculate</button></td></tr>"
outstring += "<tr><td>Number of Seconds</td><td><input type='text' id='sec' name='sec' onchange='t()'></td><td><button onclick='t()'>Calculate</button></td></tr>"
outstring += "</table>"
outstring += "<script type='text/javascript'>"
outstring += "function seconds() {"
outstring += "  var hour = parseInt(document.getElementById('hour').value);"
outstring += "  var minute = parseInt(document.getElementById('minute').value);"
outstring += "  var second = parseInt(document.getElementById('second').value);"
outstring += "  document.getElementById('sec').value= (hour * 60 + minute) * 60 + second;"
outstring += "}"
outstring += "function t() {"
outstring += " var s = parseInt(document.getElementById('sec').value);"
outstring += " var sec = s % 60;"
outstring += " s = parseInt(s / 60);"
outstring += " var min = s % 60;"
outstring += " hour = parseInt( s / 60);"
outstring += " document.getElementById('hour').value = hour;"
outstring += " document.getElementById('minute').value = min;"
outstring += " document.getElementById('second').value = sec;"
outstring += "}"
outstring += "</script>"



# been building both string and json helpers, time to fully determine which to spit.
if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	#puts outjson.to_json
	puts JSON.pretty_generate(outjson, :max_nesting => 0)
	exit
end

puts outstring
printfooter
exit
