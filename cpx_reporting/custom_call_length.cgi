#!/usr/local/bin/ruby

$LOAD_PATH << File.dirname(__FILE__)

require 'rubygems'
require 'cgi'
require 'mysql'
require 'time'

require 'localconfig'
include WebConfig

@webroot = WebConfig::WEBROOT
@spoolroot = WebConfig::SPOOLROOT

###### Local Methods ######
def printheader
	puts "Content-type: text/html\r\n\r\n"

	puts "<html><head><title>Call Lookup</title>"
	puts "<style type=\"text/css\">

	tr.email td {
		background-color: #b1f1f9;
	}

	tr.call td {
		background-color: #b8f9b1;
	}

	tr.queueabandon td {
		background-color: #f68080;
	}

	tr.voicemail td {
		background-color: #afbcbc;
	}

	tr.outgoing td {
		background-color: #f9e1a8;
	}

	tr.tempmetrics td {
		background-color: #f6b1f9;
	}

	tr.header td {
		font-weight: bold;
		text-align: center;
	}

	label {
		width: 7em;
		float: left;
		text-align: right;
		margin-right: 0.5em;
		display: block;
	}

	div.or {
		text-align: center;
		margin-right: auto;
		margin-left: auto;
		font-weight: bold;
		clear: both;
	}

	.submit input {
		margin-left: 9em;
	}

	fieldset {
		width: 25em;
		margin-left: auto;
		margin-right: auto;
	}

	legend {
		border: 1px solid black;
		padding: 0.3em;
	}
	</style>
	</head><body>"
end

def printfooter
	puts "</body></html>"
end

def calculate_row_class(row)
	if row['CallType'] == 'call' and row['AgentID'] == '0'
		'queueabandon'
	elsif row['CallType'] =~ /^temp/i
		'tempmetrics'
	else
		row['CallType']
	end
end

def print_call_row(row)
	brandid = "#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"

	puts "<tr class=\"#{calculate_row_class(row)}\">"
	puts "<td>#{Time.at(row['Start'].to_i).strftime("%Y-%m-%d %H:%M:%S")}</td>"
	if row['AgentID'] == '0'
		puts "<td>Unknown</td>"
	else
		puts "<td>#{@agentlist[row['AgentID']]}</td>"
	end

	if ( row['CallType'] == 'call' or row['CallType'] == 'outgoing' ) &&
		File.exist?("#{@webroot}/#{@spoolroot}/monitor/#{Time.at(row['Start'].to_i).strftime("%Y-%m-%d")}/#{row['UniqueID']}.ogg")
		puts "<td><a href=\"#{@spoolroot}/monitor/#{Time.at(row['Start'].to_i).strftime("%Y-%m-%d")}/#{row['UniqueID']}.ogg\">#{row['UniqueID']}</a></td>"
	elsif row['CallType'] == 'voicemail'

		result2 = @cpx_dbh.query("SELECT VoicemailID FROM call_info WHERE UniqueID = '#{row['UniqueID']}' AND CallType = 'voicemail'")
		if result2 && result2.num_rows >= 1
			result2.each_hash do |vmrow|
				if File.exist?("#{@webroot}/#{@spoolroot}/voicemail/#{brandid}/#{vmrow['VoicemailID']}.ogg")
					puts "<td><a href=\"#{@spoolroot}/voicemail/#{brandid}/#{vmrow['VoicemailID']}.ogg\">#{row['UniqueID']}</a></td>"
				end
			end
		else
			puts "<td>#{row['UniqueID']}</td>"
		end
	elsif row['CallType'] == 'email'
		result2 = @cpx_dbh.query("SELECT EmailID FROM call_info WHERE UniqueID = '#{row['UniqueID']}'")
		if result2 && result2.num_rows >= 1
			result2.each_hash do |emrow|
				puts "<td><a href=\"#{WebConfig::EMAILROOTURL}?id=#{emrow['EmailID']}&print=1&headers=1\">#{row['UniqueID']}</a></td>"
			end
		else
			puts "<td>#{row['UniqueID']}</td>"
		end
	else
		puts "<td>#{row['UniqueID']}</td>"
	end

	puts "<td>#{row['CallType']}</td>"
	puts "<td>#{row['End'].to_i - row['Start'].to_i}</td>"
	puts "<td>#{row['InQueue']}</td>"
	puts "<td>#{row['InCall']}</td>"
	puts "<td>#{row['WrapUp']}</td>"
	puts "<td>#{@brandlist[brandid]}</td>"
	puts "<td><a href=\"calldetail.cgi?uniqueid=#{row['UniqueID']}\">Details</a></td>"
	puts "<tr>"
end


###### Main Execution Line ######
cgi = CGI.new
fields = cgi.keys

# Initialize the DB handle early, will need it to retrieve a valid brand/agent list.
@cpx_dbh = Mysql.real_connect(WebConfig::DBHOST, WebConfig::DBUSER, WebConfig::DBPASS, WebConfig::DBNAME)

# Brand Name
@brandlist = Hash.new
result = @cpx_dbh.query("SELECT tenant,brand,brandname FROM brandlist");
result.each_hash do |row|
	brandid = "#{row['tenant'].to_s.rjust(4, '0')}#{row['brand'].to_s.rjust(4, '0')}"
	@brandlist[brandid] = row['brandname']
end

active="WHERE Active=1"
active="" if cgi.has_key?('inactive')

# Agent List
@agentlist = Hash.new
result = @cpx_dbh.query("SELECT AgentID,FirstName,LastName FROM tblAgent #{active}");
result.each_hash do |row|
	agentid = "#{row['AgentID'].to_i + 1000}"
	@agentlist[agentid] = "#{row['LastName']}, #{row['FirstName']} "
end

# Now begin the output


if !cgi.has_key?('action')
	printheader
	now = Time.now
	tomorrow = Time.now + 86400
	formstart = Time.mktime(now.year, now.month, now.day).strftime("%Y-%m-%d %H:%M")
	formend = Time.mktime(tomorrow.year, tomorrow.month, tomorrow.day).strftime("%Y-%m-%d %H:%M")
	puts "<fieldset><legend>Query Historical Call Data</legend>"
	puts "<form method=\"POST\" action=\"?\">"
	puts "<label>Call Type:</label> <select name=\"calltype\">"
	puts "<option value=\"any\" selected=\"selected\">Any</option>"
	puts "<option value=\"call\">Inbound Call</option>"
	puts "<option value=\"outgoing\">Outbound Call</option>"
	puts "<option value=\"voicemail\">Voicemail</option>"
	puts "<option value=\"email\">Email</option>"
	puts "<option value=\"temp\">Temp Metrics</option>"
	puts "</select><br />"
	puts "<label>Date Start:</label> <input type=\"text\" name=\"datestart\" value=\"#{formstart}\"><br />"
	puts "<label>Date End:</label> <input type=\"text\" name=\"dateend\" value=\"#{formend}\"><br /><br />"
	puts "<label>Call Length:</label><input type='text' name='length' /><br /><br />"

	puts "<label>Brand:</label> <select name=\"brand\">"
	puts "<option value=\"any\" selected=\"selected\">Any</option>"
	@brandlist.sort{|a,b| a[1].downcase <=> b[1].downcase}.each do |a|
		puts "<option value=\"#{a[0]}\">#{a[1]}</option>"
	end
	puts "</select><br />"

	puts "<label>Agent:</label> <select name=\"agent\">"
	puts "<option value=\"any\" selected=\"selected\">Any</option>"
	@agentlist.sort{|a,b| a[1].downcase <=> b[1].downcase}.each do |a|
		puts "<option value=\"#{a[0]}\">#{a[1]}</option>"
	end
	puts "</select><br />"

	puts "<div class='or'>-OR-</div>"
	puts "<label>Call ID:</label> <input type='text' name='callid' />"
	puts "<div class='or'>-OR-</div>"
	puts "<label>Email ID:</label> <input type='text' name='emailid' />"
	puts "<div class='or'>-OR-</div>"
	puts "<label>Case ID:</label> <input type='text' name='caseid' /><br />"

	puts "<input type=\"hidden\" name=\"action\" value=\"submitted\">"
	puts "<p class='submit'><input type=\"submit\" name=\"submit\" value=\"submit\"></p>"
	puts "</form>"
	puts "<form method=\"POST\" action=\"?\">"
	if cgi.has_key?('inactive')
		puts "<p class='submit'><input type='submit' value='Show Only Active Agents' /></p>"
	else
		puts "<input type='hidden' name='inactive' value='inactive'>"      
		puts "<p class='submit'><input type='submit' value='Show Inactive Agents' /></p>"
	end
	puts "</form>"
	puts "</fieldset>"
	printfooter
	exit
end

result=nil

if cgi.has_key?('action') && cgi['action'] == 'submitted' &&
   cgi.has_key?('callid') && !cgi['callid'].empty?
	
	result = @cpx_dbh.query("SELECT * FROM billing_summaries WHERE UniqueID='#{cgi['callid']}'")

elsif cgi.has_key?('action') && cgi['action'] == 'submitted' &&
   ( (cgi.has_key?('emailid') && !cgi['emailid'].empty?) || 
		(cgi.has_key?('caseid') && !cgi['caseid'].empty?) )
	# NOOP

elsif cgi.has_key?('action') && cgi['action'] == 'submitted' &&
   cgi.has_key?('calltype') && cgi.has_key?('datestart') && cgi.has_key?('dateend')

	datestart = Time.parse(cgi['datestart'])
	dateend = Time.parse(cgi['dateend'])

	# Build Brand-based Query Parameters
	brandquery = ""
	if cgi['brand'] != 'any'
		tenantid = cgi.params['brand'][0][0..3]
		brandid = cgi.params['brand'][0][4..7]
		brandquery = "AND TenantID = '#{tenantid}' AND BrandID = '#{brandid}'"
	end

	# Build Agent-based Query Parameters
	agentquery = ""
	if cgi['agent'] != 'any'
		agentquery = "AND AgentID = '#{cgi['agent']}'"
	end

	# Build CallType-based Query Parameters
	calltypequery = ""
	if cgi['calltype'] == 'temp'
		calltypequery = 'AND CallType LIKE "temp%"'
	elsif cgi['calltype'] != 'any'
		calltypequery = "AND CallType = '#{cgi['calltype']}'"
	end

	#puts "SELECT * FROM billing_summaries WHERE Start >= '#{datestart.to_i}' AND End <= '#{dateend.to_i}' #{calltypequery} #{brandquery} #{agentquery} ORDER BY UniqueID"
	# Start the magic!
	if cgi.has_key?('length') and !cgi['length'].empty? and !cgi['length'].nil?
		otherquery = "AND InCall > #{cgi['length']}"
	else
		otherquery = ""
	end

	result = @cpx_dbh.query("SELECT * FROM billing_summaries WHERE Start >= '#{datestart.to_i}' AND End <= '#{dateend.to_i}' #{calltypequery} #{brandquery} #{agentquery} #{otherquery} ORDER BY UniqueID")

else
	puts "missing parameters!"
	printfooter
	exit
end

if result && result.num_rows >= 1
	
	printheader

	puts "<b>Starting date</b>: #{datestart.strftime("%Y-%m-%d %H:%M:%S")} <br />" if datestart
	puts "<b>Ending date</b>: #{dateend.strftime("%Y-%m-%d %H:%M:%S")} <br /><br />" if dateend

	puts "<br />"
	puts "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
	puts "<tr class='header'><td>Date</td><td>Agent</td><td>UniqueID</td><td>Call Type</td><td>TotalDuration</td><td>InQueue</td><td>InCall</td><td>Wrapup</td><td>Brand</td><td>&nbsp;</td></tr>"
	result.each_hash do |row|
		print_call_row(row)
	end
	puts "</table>"
elsif cgi.has_key?('callid') and !cgi['callid'].empty?

	result = @cpx_dbh.query("SELECT * FROM billing_transactions WHERE UniqueID='#{cgi['callid']}' AND Transaction=0")
	if result.num_rows == 1
		puts "Status: 302 Moved"
		puts "location: calldetail.cgi?uniqueid=#{cgi['callid']}"
	else
		printheader
		puts "No results found."
	end

elsif cgi.has_key?('emailid') and !cgi['emailid'].empty?
	result = @cpx_dbh.query("SELECT UniqueID FROM call_info WHERE EmailID='#{cgi['emailid']}'")
	if result.num_rows == 1
		uniqueid = result.fetch_row[0]
		puts "Status: 302 Moved"
		puts "location: calldetail.cgi?uniqueid=#{uniqueid}"
	elsif result.num_rows == 0
		printheader
		puts "No results found."
	else
		printheader
		puts "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
		puts "<tr class='header'><td>Date</td><td>Agent</td><td>UniqueID</td><td>Call Type</td><td>TotalDuration</td><td>InQueue</td><td>InCall</td><td>Wrapup</td><td>Brand</td><td>&nbsp;</td></tr>"
		result.each_hash do |row|
			result2 = @cpx_dbh.query("SELECT * FROM billing_summaries WHERE UniqueID='#{row['UniqueID']}'")
			if result2.num_rows == 1
				result2.each_hash do |row2|
					print_call_row(row2)
				end
			else
				brandid = "#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"
				puts "<tr><td>-</td><td>-</td><td>#{row['UniqueID']}</td><td>-</td><td>-</td><td>-</td><td>-</td><td>#{@brandlist[brandid]}</td><td><a href='calldetail.cgi?uniqueid=#{row['UniqueID']}'>Details</a></td></tr>"
			end
		end
		puts "</table>"
	end
	result.free

elsif cgi.has_key?('caseid') and !cgi['caseid'].empty?
	result = @cpx_dbh.query("SELECT UniqueID, TenantID, BrandID FROM call_info WHERE CaseID='#{cgi['caseid']}'")
	if result.num_rows == 1
		uniqueid = result.fetch_row[0]
		puts "Status: 302 Moved"
		puts "location: calldetail.cgi?uniqueid=#{uniqueid}"
	elsif result.num_rows == 0
		printheader
		puts "No results found."
	else
		printheader
		puts "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
		puts "<tr class='header'><td>Date</td><td>Agent</td><td>UniqueID</td><td>Call Type</td><td>TotalDuration</td><td>InQueue</td><td>InCall</td><td>Wrapup</td><td>Brand</td><td>&nbsp;</td></tr>"
		result.each_hash do |row|
			result2 = @cpx_dbh.query("SELECT * FROM billing_summaries WHERE UniqueID='#{row['UniqueID']}'")
			if result2.num_rows == 1
				result2.each_hash do |row2|
					print_call_row(row2)
				end
			else
				brandid = "#{row['TenantID'].to_s.rjust(4, '0')}#{row['BrandID'].to_s.rjust(4, '0')}"
				puts "<tr><td>-</td><td>-</td><td>#{row['UniqueID']}</td><td>-</td><td>-</td><td>-</td><td>-</td><td>#{@brandlist[brandid]}</td><td><a href='calldetail.cgi?uniqueid=#{row['UniqueID']}'>Details</a></td></tr>"
			end
		end
		puts "</table>"
	end
	result.free

else
	printheader
	puts "No results found."
end

printfooter
exit

