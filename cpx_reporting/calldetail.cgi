#!/usr/local/bin/ruby

$LOAD_PATH << File.dirname(__FILE__)

require 'cgi'
require 'cgi/session'
require 'time'

begin
	require 'rubygems'
rescue LoadError
	# NOOP
end

begin
	require 'json'
	require 'mysql'
rescue LoadError => e
	puts "Content-type: text/html\r\n\r\n"
	puts "Please install the json and mysql ruby libraries. <br/>"
	puts e.message
	exit
end

require 'constants/cdrconstants'

require 'localconfig'
include WebConfig
require 'authconfig'

@webroot = WebConfig::WEBROOT
@spoolroot = WebConfig::SPOOLROOT

###### Local Methods ######
def printheader
	puts "<html><head><title>Call Detail</title></head><body>"
	puts "<style type=\"text/css\">

	label.category {
		width: 8em;
		float: left;
		display: block;
	}

	table {
		//margin-left: 8em;
	}
	tr.header td {
		font-weight: bold;
		text-align: center;
	}
	</style>"
	puts "</head></body>"
end

def printfooter
	puts "</body></html>"
end

def decode_data(transaction, data, callstart, callend, url=true)
	case transaction
	when CDRConstants::DIALOUTGOING, CDRConstants::PRECALL
		# decode brandid if possible
		if @brandlist[data]
			@brandlist[data]
		else
			# I guess it's something else
			data
		end
	when CDRConstants::INWRAPUP, CDRConstants::ENDWRAPUP, CDRConstants::RINGING, CDRConstants::INCALL
		if @user['type'].to_i < 2
			if url
				"<a href='agent_states.cgi?action=Submit&agent=#{data}&datestart=#{callstart.strftime('%Y/%m/%d-%H:%M:%S')}&dateend=#{callend.strftime('%Y/%m/%d-%H:%M:%S')}&fuzzy=1'>#{@agentlist[data]}</a>"
			else
				@agentlist[data]
			end
		else 
			nil
		end
	when CDRConstants::INQUEUE, CDRConstants::ABANDONQUEUE
		@queuelist[data] ? @queuelist[data] : data
	else
		data
	end
end

###### Main Execution Line ######
cgi = CGI.new
fields = cgi.keys
sess = CGI::Session.new(cgi,
	'prefix' => 'lookup_sess_'
)

if sess['cpxkey'] == nil
	cgi.out({'Status' => 302, 'Location' => 'login.cgi'}){
		"redirecting to login page..."
	}
	exit
end

@user = JSON.parse(sess['fullresult'])

# company perms of user
begin
	brands = JSON.parse(sess['companies'])
rescue => e
	cgi.out({'status' => '302', 'location' => 'login.cgi?action=logout'}){
		"error, going back to login"
	}
	exit
end

# json check
if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	puts "Content-type: text/json\r\n\r\n"
else
	puts "Content-type: text/html\r\n\r\n"
	printheader
end


if !cgi.has_key?('uniqueid')
	if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
		puts "{}"
		exit
	end

	printheader
	outstring += "<h1>No action taken.</h1>"
	puts outstring
	printfooter
	exit
end

cpx_dbh = Mysql.real_connect(WebConfig::DBHOST, WebConfig::DBUSER, WebConfig::DBPASS, WebConfig::DBNAME)

# Brand Name
@brandlist = {}
result = cpx_dbh.query("SELECT tenant,brand,concat(lpad(tenant,4,'0'),lpad(brand,4,'0')) as brandid, brandname FROM brandlist");
result.each_hash do |row|
	@brandlist[row['brandid']] = row['brandname'] if brands.include? row['brandid']
end

# Agent List
@agentlist = {}
result = cpx_dbh.query("SELECT AgentID,FirstName,LastName FROM tblAgent");
result.each_hash do |row|
	agentid = "#{row['AgentID'].to_i + 1000}"
	@agentlist[agentid] = "#{row['FirstName']} #{row['LastName']}"
end

# queue list
@queuelist = {}
result = cpx_dbh.query("SELECT name, commonname FROM queues")
result.each do |row|
	@queuelist[row[0]] = row[1]
end

# Start the magic!
outjson = {}
outstring = ''

outstring += "<br />"
outstring += "<label class='category'><b>UniqueID</b>:</label> #{cgi['uniqueid']}<br />"

#outstring += "<br /><hr /><br />"

outstring += "<br/><label class='category'><b>Summary</b>:</label>"
# Being with the billing summary
result = cpx_dbh.query("SELECT *,concat(lpad(tenantid,4,'0'),lpad(brandid,4,'0')) as brandmush FROM billing_summaries WHERE UniqueID = '#{cgi['uniqueid']}'")
callstart = nil
callend = nil
outjson['summary'] = []

if result.num_rows >= 1

	outstring += "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
	outstring += "<tr class='header'><td>TenantID</td><td>BrandID</td><td>Start</td><td>End</td><td>InQueue</td><td>InCall</td><td>WrapUp</td><td>AgentID</td></tr>"
	result.each_hash do |row|
		unless @brandlist.has_key? row['brandmush'].to_s
			# this will cause a local jump error, but I'm okay with that
			# since they aren't supposed to see the call anyway.
			return
		end
		outstring += "<tr>"
		outstring += "<td>#{row['TenantID']}</td>"
		outstring += "<td>#{row['BrandID']}</td>"
		outstring += "<td>#{Time.at(row['Start'].to_i).strftime('%F %H:%M')}</td>"
		outstring += "<td>#{Time.at(row['End'].to_i).strftime('%F %H:%M')}</td>"
		outstring += "<td>#{row['InQueue']}</td>"
		outstring += "<td>#{row['InCall']}</td>"
		outstring += "<td>#{row['WrapUp']}</td>"
		outstring += "<td>"
		if @user['type'].to_i < 2
			outstring += "#{@agentlist[row['AgentID']]}</td>"
		else
			outstring += "unknown"
		end
		callstart = Time.at(row['Start'].to_i)
		callend = Time.at(row['End'].to_i)
		sumrow = row
		sumrow['callstart'] = Time.at(row['Start'].to_i)
		sumrow['callend'] = Time.at(row['End'].to_i)
		outjson['summary'].push(sumrow)
	end
	outstring += "<tr><td colspan='0'><a href='#{SUMEDITURL}?uniqueid=#{cgi['uniqueid']}' target='_self'>Edit Summary</a></td></td>"
	outstring += "</table>"
else
	outstring += "No summary found.<br/>"
end

unless callstart and callend
	result = cpx_dbh.query("SELECT MIN(Start) AS Start, MAX(END) AS End FROM billing_transactions WHERE UniqueID='#{cgi['uniqueid']}'")
	callstart, callend = result.fetch_row.map{|x| Time.at(x.to_i)}
	outjson['callstart'] = callstart
	outjson['callend'] = callend
end

#outstring += callstart.to_s
#outstring += callend.to_s

#outstring += "<br /><hr /><br />"
outstring += "<br/><label class='category'><b>Transactions</b>:</label>"
outjson['transactions'] = []
# Now do transaction log..
result = cpx_dbh.query("SELECT * FROM billing_transactions WHERE UniqueID = '#{cgi['uniqueid']}' ORDER BY Start, End")

if result.num_rows >= 1
	outstring += "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
	outstring += "<tr class='header'><td>Transaction</td><td>Start</td><td>End</td><td>Duration</td><td>Data</td></tr>"
	result.each_hash do |row|
		callstart ||= Time.at(row['Start'].to_i)
		callend ||= Time.at(row['End'].to_i)
		outstring += "<tr>"
		outstring += "<td>#{CDRConstants::TRANSACTIONNAMES[row['Transaction'].to_i]}</td>"
		outstring += "<td>#{Time.at(row['Start'].to_i).strftime('%H:%M:%S')}</td>"
		outstring += "<td>#{Time.at(row['End'].to_i).strftime('%H:%M:%S')}</td>"
		outstring += "<td>#{row['End'].to_i - row['Start'].to_i}</td>"
		outstring += "<td>#{row['Data'] and row['Data'].empty? ? '-' : decode_data(row['Transaction'].to_i, row['Data'], callstart, callend)}</td>"
		tranrow = row
		tranrow['callstart'] = callstart
		tranrow['callend'] = callend
		tranrow['transactionname'] = CDRConstants::TRANSACTIONNAMES[row['Transaction'].to_i]
		tranrow['decodeddata'] = row['Data'].empty? ? nil : decode_data(row['Transaction'].to_i, row['Data'], callstart, callend, false)
		outjson['transactions'].push(tranrow)
	end
	outstring += "</table>"
else
	outstring += "No transactions found.<br/>"
end

#outstring += "<br /><hr /><br />"
outstring += "<br/><label class='category'><b>Call Info</b>:</label>"
outjson['call_info'] = []
# Now the call_info crap..
result = cpx_dbh.query("SELECT * FROM call_info WHERE UniqueID = '#{cgi['uniqueid']}'")
if result.num_rows >= 1
	outstring += "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">"
	outstring += "<tr class='header'><td>TenantID</td><td>BrandID</td><td>CallType</td><td>DNIS</td><td>CallerID</td><td>DialedNumber</td><td>CaseID</td><td>VoiceMailID</td><td>EmailID</td></tr>"
	result.each_hash do |row|
		outstring += "<tr>"
		outstring += "<td>#{row['TenantID']}</td>"
		outstring += "<td>#{row['BrandID']}</td>"
		outstring += "<td>#{row['CallType']}</td>"
		outstring += "<td>#{row['DNIS'].nil? ? '-' : row['DNIS']}</td>"
		if row['CallerIDName'].nil? or row['CallerIDNum'].nil?
			outstring += '<td>-</td>'
		else
			outstring += "<td>#{row['CallerIDName']} (#{row['CallerIDNum']})</td>"
		end
		outstring += "<td>#{row['DialedNumber'].nil? ? '-' : row['DialedNumber']}</td>"
		outstring += "<td>#{row['CaseID'].nil? ? '-' : row['CaseID']}</td>"
		if row['VoicemailID'] and !row['VoicemailID'].empty?
			outstring += "<td><a href=\"?uniqueid=#{row['VoicemailID']}\">#{row['VoicemailID']}</a></td>"
		else
			outstring += '<td>-</td>'
		end
		if row['EmailID'] and !row['EmailID'].to_i.zero?
			outstring += "<td><a href=\"#{WebConfig::EMAILROOTURL}?id=#{row['EmailID']}&print=1&headers=1\">#{row['EmailID']}</a></td>"
		else
			outstring += '<td>-</td>'
		end
		
		outjson['call_info'].push(row)
		
		outstring += "</tr>"
	end
	outstring += "</table><br />"

else
	outstring += "No call info found.<br/>"
end

outstring += "<label class='category'><b>Recordings</b>:</label>"

# limit recordings to last 30 days (or so)
if ((Time.now.to_i - callstart.to_i) < 2764800)  
  recordings = Dir["#{@webroot}#{@spoolroot}/monitor/#{callstart.strftime("%Y-%m-%d")}/#{cgi['uniqueid']}.*"] +
  Dir["#{@webroot}#{@spoolroot}/monitor/#{callstart.strftime("%Y-%m-%d")}/#{cgi['uniqueid']}-warmxfer-*.ogg"]
else
  recordings = []
end

outjson['recordings'] = recordings

unless recordings.empty?
	outstring += "<table border='1' cellspacing='0' cellpadding='5'>"
	outstring += "<tr class='header'><td>Recordings related to this call</td></tr>"

	recordings.each do |rec|
		rec.sub!(/^#{@webroot}/, '')
		outstring += "<tr><td><a href='#{rec}'>#{File.basename(rec)}</a></td></tr>"
	end
	outstring += "</table><br />"
else
	outstring += "No call recordings found.<br/>"
end

# been building both string and json helpers, time to fully determine which to spit.
if ( cgi.user_agent == '' ) || (cgi.has_key?('json') ) 
	#puts outjson.to_json
	puts JSON.pretty_generate(outjson, :max_nesting => 0)
	exit
end

puts outstring
printfooter
exit
