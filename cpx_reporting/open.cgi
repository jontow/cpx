#!/usr/local/bin/ruby
################################################################################
#
# 2014-07-22 -- jontow@mototowne.com
#
# Not a report, but a toggle.
#
# Should flip the "open for business" sign to allow/disallow calls at weird
# times of day/night, possibly for special events.
#
# This is tangentially hooked into the CBX dialplan, where in an IVR you'd
# normally have a check for business hours, this gets inserted right above
# it, like this:
#
# exten => s,n,GotoIf($["${STAT(e,/home/cpx/var/run/wwwcfg/openforbusiness.txt)}" = "1"]?openforbusiness)
# exten => s,n,GotoIfTime(08:00-16:30|mon-fri|*|*?openforbusiness) ; business hours
#
################################################################################

$LOAD_PATH << File.dirname(__FILE__)

require 'cgi'

begin
	require 'rubygems'
rescue LoadError
end

begin
	require 'mysql'
rescue LoadError
end

require 'time'
require 'localconfig'

include WebConfig

WWWCFGDIR = "/home/cpx/var/run/wwwcfg"
OPENSIGN = WWWCFGDIR + "/openforbusiness.txt"

cgi = CGI.new

if !cgi.has_key?('action')
	puts "Content-type: text/html\r\n\r\n"
	puts "<html><head><title>Open For Business?</title></head><body>"
	puts "<form method=\"POST\" action=\"?\">"

	puts "<label>Open For Business:</label> <select name=\"openforbusiness\">"
	if File.exists?(OPENSIGN)
		puts "<option value=\"yes\">Yes</option>"
		puts "<option value=\"no\" selected=\"selected\">No</option>"
	else
		puts "<option value=\"yes\" selected=\"selected\">Yes</option>"
		puts "<option value=\"no\">No</option>"
	end
	puts "</select><br />"

	puts "<input type=\"hidden\" name=\"action\" value=\"submitted\">"
	puts "<p class='submit'><input type=\"submit\" name=\"submit\" value=\"submit\"></p>"
	puts "</form>"

	toggle = ''
	if File.exists?(OPENSIGN)
		toggle = ''
	else
		toggle = 'not '
	end

	puts "<p>Currently #{toggle}accepting calls after-hours.</p>"
	puts "</body></html>"
	exit
end

viewformat = nil
if cgi.has_key?('openforbusiness') and cgi['openforbusiness'] == 'yes'
	puts "Content-type: text/html\r\n\r\n"
	puts ""
	if File.directory?(WWWCFGDIR)
		if File.exists?(OPENSIGN)
			puts "<html><head><title>Already Open!</title></head><body><h3>Already open for business.  Already accepting calls.</h3></body></html>"
		else
			puts "<html><head><title>Open For Business!</title></head><body><h3>Open for business.  Now accepting calls.</h3></body></html>"
			File.open(OPENSIGN, 'w') do |opensign|
				opensign.puts Time.now.strftime("%Y-%m-%d %H:%M:%S") + "\n"
			end
		end
	else
		puts "<html><head><title>Error: misconfiguration!</title></head><body><h3>Error: misconfiguration (wwwcfg dir doesn't exist).</h3></body></html>"
	end

	exit
elsif cgi.has_key?('openforbusiness') and cgi['openforbusiness'] == 'no'
	puts "Content-type: text/html\r\n\r\n"
	puts ""
	if File.directory?(WWWCFGDIR)
		if File.exists?(OPENSIGN)
			puts "<html><head><title>Not Open For Business!</title></head><body><h3>No longer open for business.  Calls going to voicemail.</h3></body></html>"
			File.unlink(OPENSIGN)
		else
			puts "<html><head><title>Not Open For Business!</title></head><body><h3>Already not open for business.  Calls still going to voicemail.</h3></body></html>"
		end
	else
		puts "<html><head><title>Error: misconfiguration!</title></head><body><h3>Error: misconfiguration (wwwcfg dir doesn't exist).</h3></body></html>"
	end

	exit
else
	puts "Content-type: text/html\r\n\r\n"
	puts ""
	puts "<html><head><title>Error: didn't understand!</title></head><body><h3>Error: didn't understand.</h3></body></html>"
	puts ""
	exit
end

# Shouldn't get here
exit
