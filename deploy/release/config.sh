#!/bin/sh

# What directory to use as a top-level release build
DESTDIR="$HOME/cpx-release"

# Where to find the agentui local svn working copy
AGENTUISRC="../../../agentui/trunk"
AGENTUIDST="$DESTDIR/cpx-agentui"

# Where to find the auiserver local svn working copy
AUISERVERSRC="../../../auiserver/trunk"
AUISERVERDST="$DESTDIR/cpx-auiserver"
