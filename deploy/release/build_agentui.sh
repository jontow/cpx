#!/bin/sh

# Get a version number if it was passed
if [ "x$1" != "x" ]; then
	VERSION="$1"
	FULLVERSION="Release $1"
fi

# Suck in config file
if [ -f ./config.sh ]; then
	. ./config.sh
else
	echo "Missing config.sh!"
	exit
fi

# Sanity checking for variables

if [ "x$DESTDIR" = "x" ]; then
	echo "DESTDIR isn't set"
	exit
fi

if [ "x$AGENTUISRC" = "x" ]; then
	echo "AGENTUISRC isn't set"
	exit
fi

if [ "x$AGENTUIDST" = "x" ]; then
	echo "AGENTUIDST isn't set"
	exit
fi

# Check for directory existance

if [ ! -d $AGENTUISRC ]; then
	echo "Missing agentui source directory!"
	exit
fi

if [ -d $AGENTUIDST ]; then
	echo "$AGENTUIDST exists!  Remove it and rerun."
	exit
fi

if [ ! -d $DESTDIR ]; then
	mkdir -p $DESTDIR
fi

cd $AGENTUISRC
svn export . $AGENTUIDST >/dev/null
svn export constants $AGENTUIDST/constants >/dev/null
svn export contrib/remotesyslog $AGENTUIDST/contrib/remotesyslog >/dev/null
rm -fr $AGENTUIDST/tests
rm -fr $AGENTUIDST/rakefile
SVNREV=`svnversion .`
CPXDIR=`basename $AGENTUIDST`

if [ "x$VERSION" = "x" ]; then
	VERSION="r$SVNREV"
	FULLVERSION="SVN ${VERSION}"
fi

#mv $AGENTUIDST/main.rb $AGENTUIDST/main.rb.tmp
#sed "1,\$s/Development\ Copy/${VERSION}/g" $AGENTUIDST/main.rb.tmp >$AGENTUIDST/main.rb
#rm $AGENTUIDST/main.rb.tmp
echo "VERSIONSTRING = '${FULLVERSION}'" > $AGENTUIDST/version.rb

echo ""

cd $DESTDIR
tar zcvf ${CPXDIR}-${VERSION}.tgz ${CPXDIR}
zip ${CPXDIR}-${VERSION}.zip -r ${CPXDIR}
cd ..

echo ""
echo "Done."
echo ""
ls -l $DESTDIR/${CPXDIR}-${VERSION}.tgz
ls -l $DESTDIR/${CPXDIR}-${VERSION}.zip
echo ""
exit
