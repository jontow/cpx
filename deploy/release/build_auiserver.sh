#!/bin/sh

# Get a version number if it was passed
if [ "x$1" != "x" ]; then
	VERSION="$1"
	FULLVERSION="Release $1"
fi

# Suck in config file
if [ -f ./config.sh ]; then
	. ./config.sh
else
	echo "Missing config.sh!"
	exit
fi

# Sanity checking for variable existance

if [ "x$DESTDIR" = "x" ]; then
	echo "DESTDIR isn't set"
	exit
fi

if [ "x$AUISERVERSRC" = "x" ]; then
	echo "AUISERVERSRC isn't set"
	exit
fi

if [ "x$AUISERVERDST" = "x" ]; then
	echo "AUISERVERDST isn't set"
	exit
fi

# Check for directory existance

if [ ! -d $AUISERVERSRC ]; then
	echo "Missing auiserver source directory!"
	exit
fi

if [ -d $AUISERVERDST ]; then
	echo "$AUISERVERDST exists!  Remove it and rerun."
	exit
fi

if [ ! -d $DESTDIR ]; then
	mkdir -p $DESTDIR
fi

cd $AUISERVERSRC
svn export . $AUISERVERDST >/dev/null
rm -fr $AUISERVERDST/old
rm -fr $AUISERVERDST/tests
rm -fr $AUISERVERDST/rakefile
svn export constants $AUISERVERDST/constants >/dev/null
svn export contrib/remotesyslog $AUISERVERDST/contrib/remotesyslog >/dev/null
SVNREV=`svnversion .`
CPXDIR=`basename $AUISERVERDST`

if [ "x$VERSION" = "x" ]; then
	VERSION="r$SVNREV"
	FULLVERSION="SVN ${VERSION}"
fi

echo ""

#mv $AUISERVERDST/new_server.rb $AUISERVERDST/new_server.rb.tmp
#sed "1,\$s/Development\ Copy/${VERSION}/g" $AUISERVERDST/new_server.rb.tmp >$AUISERVERDST/new_server.rb
#rm $AUISERVERDST/new_server.rb.tmp
echo "VERSIONSTRING = '${FULLVERSION}'" > $AUISERVERDST/version.rb

cd $DESTDIR
tar zcf ${CPXDIR}-${VERSION}.tgz ${CPXDIR}

echo ""
echo "Done."
echo ""
ls -l $DESTDIR/${CPXDIR}-${VERSION}.tgz
echo ""
exit
