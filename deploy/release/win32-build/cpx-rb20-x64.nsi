; cpx_ruby20_x64.nsi
;

;--------------------------------

!define MUI_PRODUCT "cpx-rb20-x64"
!define MUI_FILE "cpx-agentui"
!define MUI_VERSION ""
!define MUI_BRANDINGTEXT "CommPlex SVN-trunk"

;--------------------------------

; The name of the installer
Name "CPX"

; The file to write
OutFile "cpx-rb20-x64.exe"

; The default installation directory
InstallDir "$PROGRAMFILES\${MUI_PRODUCT}"

;--------------------------------
; Data
LicenseData license_cpx.txt

;--------------------------------

; Request application privileges for Windows Vista
RequestExecutionLevel admin

;--------------------------------
; Pages

Page license
Page directory
Page instfiles

;--------------------------------

; The stuff to install
Section "install"

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  ;File cpx-rb20-x64.nsi
  File /r bin
  File /r doc
  File /r include
  File /r lib
  File /r share
  File /r cpx-agentui
  File cpx-agentui.exe
  File cpx-agentui-debug.bat
  File license_cpx.txt

  ; Desktop shortcut
  CreateShortCut "$DESKTOP\${MUI_PRODUCT}.lnk" "$INSTDIR\${MUI_FILE}.exe" ""

  ; Start menu items
  CreateDirectory "$SMPROGRAMS\${MUI_PRODUCT}"
  CreateShortCut "$SMPROGRAMS\${MUI_PRODUCT}\${MUI_PRODUCT}.lnk" "$INSTDIR\${MUI_FILE}.exe" "" "$INSTDIR\${MUI_FILE}.exe" 0
SectionEnd ; end the section
