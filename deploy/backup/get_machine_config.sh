#!/bin/sh

if [ -f ./config.sh ]; then
	. ./config.sh
fi

cd /

tar zcvpf ${BACKUPDIR}/machine-configuration.tar.gz \
	/etc/rc.conf \
	/etc/rc.local \
	/etc/mail/aliases \
	/usr/local/etc/apache2 \
	/usr/local/etc/asterisk \
	/usr/local/etc/odbc.ini \
	/usr/local/etc/odbcinst.ini \
	/usr/local/etc/postfix \
	/usr/local/etc/sudoers

exit
