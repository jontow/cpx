#!/bin/sh

if [ -f ./config.sh ]; then
	. ./config.sh
fi

if [ "$CPXUSER" = "" ] || [ "$CPXGROUP" = "" ] || [ "$CPXHOME" = "" ] || [ "$CPXSHELL" = "" ]; then
	echo "Need CPXUSER, CPXGROUP, CPXHOME, and CPXSHELL set!"
	exit
fi

cd ${CPXHOME}
tar zcvpf ${BACKUPDIR}/cpx-user-home.tar.gz .
