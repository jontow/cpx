#!/bin/sh

if [ -f ./config.sh ]; then
	. ./config.sh
fi

if [ "$SQLDB" = "" ] || [ "$SQLUSER" = "" ]; then
	echo "Need both SQLDB and SQLUSER set!"
	exit
fi

if [ "$SQLPASS" = "" ]; then
	echo ""
	echo -n "Enter SQL password for ${SQLUSER}: "
	stty -echo
	read SQLPASS
	echo ""
fi

mysqldump -u ${SQLUSER} --password="${SQLPASS}" ${SQLDB} >${BACKUPDIR}/BACKUP-${SQLDB}.sql
