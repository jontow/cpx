#!/bin/sh

if [ -f ./config.sh ]; then
	. ./config.sh
fi

if [ "$PKGDB" = "" ] || [ "$PKGDIR" = "" ]; then
	echo "Need both PKGDB and PKGDIR set!"
	exit
fi

cd $PKGDIR

for PKG in `ls ${PKGDB}`; do {
	echo "*** Backing up $PKG"
	pkg_create -b ${PKG}
}; done

exit
