#!/usr/bin/env ruby

=begin license
  Copright (C) 2006-2008 Fused Solutions LLC

  This program is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free Software
	Foundation; either version 2 of the License, or (at your option) any later
	version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
	PARTICULAR PURPOSE. See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along with
	this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
	St, Fifth Floor, Boston, MA 02110-1301 USA 
=end

require 'mysql'
require File.join(File.expand_path(File.dirname(__FILE__)), '../genconstants.rb')
include GenConstants

begin
	require 'contrib/radp'
rescue LoadError
	load File.join(File.expand_path(File.dirname(__FILE__)), 'contrib/radp.rb')
end

cpx_dbh = Mysql.real_connect(CPXHOST, CPXUSER, CPXPASS, CPXDB)

toplevel = RADP.new(File.open('IVR/toplevel.ext', 'w'))
includes = RADP.new(File.open('IVR/includes.ext', 'w'))
includes.include_file('IVR/toplevel.ext')

toplevel.context 'toplevel' do
	result = cpx_dbh.query('SELECT tenant, brand, dnis FROM brandlist')
	result.each_hash do |row|
		brandid = "#{row['tenant'].rjust(4, '0')}#{row['brand'].rjust(4, '0')}"
		toplevel.ext row['dnis'] do
			toplevel.Set('CALLTYPE=call')
			toplevel.Set("BRANDID=#{brandid}")
			toplevel.Set("CALLERIDNUM=${CALLERID(num)}")
			toplevel.Answer
			toplevel.Goto("IVR-#{row['dnis']},s,1")
			toplevel.nl

			ivr = RADP.new(File.open("IVR/#{row['dnis']}.ext", 'w'))
			ivr.context "IVR-#{row['dnis']}" do
				ivr.includes "queue-inject"
				ivr.ext 's' do
					ivr.SayDigits(row['dnis'])
					ivr.Macro("queue,L1-#{brandid},3600")
					ivr.Hangup()
				end
			end

			includes.include_file "IVR/#{row['dnis']}.ext"
		end
	end
end
