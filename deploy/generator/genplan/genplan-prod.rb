#!/usr/bin/env ruby

=begin license
  Copright (C) 2006-2008 Fused Solutions LLC

  This program is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free Software
	Foundation; either version 2 of the License, or (at your option) any later
	version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
	PARTICULAR PURPOSE. See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along with
	this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
	St, Fifth Floor, Boston, MA 02110-1301 USA 
=end

require 'mysql'
require File.join(File.expand_path(File.dirname(__FILE__)), '../genconstants.rb')
include GenConstants

begin
	require 'contrib/radp'
rescue LoadError
	load File.join(File.expand_path(File.dirname(__FILE__)), 'contrib/radp.rb')
end

cpx_dbh = Mysql.real_connect(CPXHOST, CPXUSER, CPXPASS, CPXDB)

Dir.chdir(File.join(ENV['HOME'], 'etc/cbx'))

toplevel = RADP.new(File.open('IVR/toplevel.ext', 'w'))
includes = RADP.new(File.open('IVR/includes.ext', 'w'))

toplevel.nl
toplevel.comment('Do NOT modify this file under ANY circumstances!')
toplevel.comment('This file may be regenerated at any time.')
toplevel.comment('Make all custom changes to custom.ext.')
toplevel.nl

includes.nl
includes.comment('Do NOT modify this file under ANY circumstances!')
includes.comment('This file may be regenerated at any time.')
includes.comment('Make all custom changes to custom.ext.')
includes.nl

includes.include_file('IVR/toplevel.ext')

toplevel.context 'toplevel' do
	result = cpx_dbh.query('SELECT tenant, brand, dnis, brandname FROM brandlist')
	result.each_hash do |row|
		brandid = "#{row['tenant'].rjust(4, '0')}#{row['brand'].rjust(4, '0')}"
		res = cpx_dbh.query("SELECT name, context FROM queues WHERE name LIKE '%#{brandid}%' ORDER BY name LIMIT 1")

		if res.num_rows == 0
			puts "Cannot find any queues for #{row['brandname']}"
			next
		end

		queue, context = res.fetch_row
		toplevel.ext row['dnis'] do
			toplevel.comment("IVR for #{row['brandname']}")
			toplevel.Set('CALLTYPE=call')
			toplevel.Set("BRANDID=#{brandid}")
			toplevel.Set("CALLERIDNUM=${CALLERID(num)}")
			toplevel.Set("CALLERIDNAME=${CALLERID(name)}")
			toplevel.Set("DNIS=${EXTEN}")
			toplevel.Answer
			toplevel.Goto("IVR-#{row['dnis']},s,1")
			toplevel.nl

			fname = "IVR/#{row['dnis']}.ext"
	
			if File.file?(fname) and !File.read(fname).split("\n")[0].include?('radp')
				puts "IVR #{fname} is custom, not overriding"
				includes.include_file "IVR/#{row['dnis']}.ext"
				next
			end
			
			File.open(fname, 'w') do |f|
				ivr = RADP.new(f)
				ivr.nl
				ivr.comment("If you want to modify this file, remove this and the RADP header")
				ivr.comment("and commit it to the dialplan SVN folder. This file is subject")
				ivr.comment("to being overwritten otherwise!")
				ivr.nl
				ivr.comment("IVR for #{row['brandname']}")
				ivr.context "IVR-#{row['dnis']}" do
					wavdir = "/home/cpx/share/cbx/sounds/IVR/#{brandid}"

					ivr.includes "queue-inject"
					ivr.ext 's' do
						ivr.Playback("IVR/#{brandid}/brand") if File.file?(File.join(wavdir, 'brand.wav'))
						ivr.Macro('problem')
						ivr.Playback("IVR/#{brandid}/greeting") if File.file?(File.join(wavdir, 'greeting.wav'))
						unless File.file?(File.join(wavdir, 'brand.wav')) and File.file?(File.join(wavdir, 'greeting.wav'))
							#puts File.join(File.expand_path(File.dirname(__FILE__)), "../../dialplans/IVR/#{row['dnis']}.ext")
							unless File.file?(File.join(File.expand_path(File.dirname(__FILE__)), "../../dialplans/IVR/#{row['dnis']}.ext"))
								puts "WARNING: Default queue #{queue} for #{row['brandname']} has no brand/greeting"
							end
						end
						ivr.Playback('IVR/callmonitoring')
						ivr.Playback('IVR/exit_to_voicemail') if !context.nil? and !context.empty?
						ivr.Macro("queue,#{queue},3600")
						ivr.Hangup()
					end
				end
			end

			includes.include_file "IVR/#{row['dnis']}.ext"
		end
	end
end

includes.include_file "IVR/custom.ext"

File.open("IVR/custom.ext", 'w') {|f| f.puts "; Insert custom IVR stuff here" } unless File.exist?("IVR/custom.ext")
