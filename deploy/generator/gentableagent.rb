#! /usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


require 'mysql'
require './genconstants'
include GenConstants

cpx_dbh = Mysql.real_connect(CPXHOST, CPXUSER, CPXPASS, CPXDB)

ADMINPASS = "21232F297A57A5A743894A0E4A801FC3"
USERPASS = "EE11CBB19052E40B07AAC0CA060C23EE"

1.upto(NUMAGENT) do |x|
	if x <= NUMADMIN
		username = "admin#{x}"
		password = ADMINPASS
		seclevel = 2
		profile = 5
		cpx_dbh.query("INSERT INTO tblAgent (AgentID,FirstName,LastName,Login,Password,TierID,TypeID,SecurityLevelID,ViewID,Active,DefaultProfile) VALUES(#{x}, 'Test', 'User', '#{username}', '#{password}', 4, 1, #{seclevel}, 1, 1, #{profile})");
	else
		username = "user#{x}"
		password = USERPASS
		seclevel = 1
		profile = 1
		cpx_dbh.query("INSERT INTO tblAgent (AgentID,FirstName,LastName,Login,Password,TierID,TypeID,SecurityLevelID,ViewID,Active,DefaultProfile) VALUES(#{x}, 'Test', 'User', '#{username}', '#{password}', 1, 1, #{seclevel}, 1, 1, #{profile})");
	end

end
