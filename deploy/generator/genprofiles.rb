#! /usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


require 'mysql'
require './genconstants'
include GenConstants

cpx_dbh = Mysql.real_connect(CPXHOST, CPXUSER, CPXPASS, CPXDB)

cpx_dbh.query('TRUNCATE TABLE profiles')
cpx_dbh.query('TRUNCATE TABLE profile_lookup;')

result = cpx_dbh.query('SELECT * FROM brandlist')

level3 = [[31, 4], [17, 1], [40, 1], [48, 1], [12, 1], [18, 1]]

cpx_dbh.query("INSERT INTO profiles SET name='level1'")
cpx_dbh.query("INSERT INTO profiles SET name='level3'")
cpx_dbh.query("INSERT INTO profiles SET name='all'")

result.each_hash do |row|
	identifier = row['tenant'].rjust(4, '0') + row['brand'].rjust(4, '0')
	queuename = "L1-#{identifier}-q"
	if level3.include? [row['tenant'].to_i, row['brand'].to_i]
		cpx_dbh.query("INSERT INTO profile_lookup VALUES((SELECT profile FROM profiles WHERE name='level3'), '#{queuename}', 0)")
	else
		cpx_dbh.query("INSERT INTO profile_lookup VALUES((SELECT profile FROM profiles WHERE name='level1'), '#{queuename}', 0)")
	end
	
	cpx_dbh.query("INSERT INTO profile_lookup VALUES((SELECT profile FROM profiles WHERE name='all'), '#{queuename}', 0)")
end
