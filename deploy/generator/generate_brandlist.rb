#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


require 'mysql'
load './genconstants.rb'
include GenConstants

cpx_dbh = Mysql.real_connect(CPXHOST, CPXUSER, CPXPASS, CPXDB)

tenants = [['kpx_t01',1,'Tenant 1'],['kpx_t02',2,'Tenant 2'],['kpx_t03',3,'Tenant 3']]

tenants.each do |tenant|

	brands = [["#{tenant[2]} Brand 1",1],["#{tenant[2]} Brand 2",2],["#{tenant[2]} Brand 3",3]]
	brands.each do |brand|
			dnis = "55512#{tenant[1]}#{brand[1]}"
			if cpx_dbh.query("SELECT * FROM brandlist WHERE tenant=#{tenant[1]} AND brand=#{brand[1]}").num_rows == 1
				cpx_dbh.query("UPDATE brandlist SET brandname='#{cpx_dbh.quote(brand[0])}', dnis='#{dnis}' WHERE tenant=#{tenant[1]} AND brand=#{brand[1]}")
			else
				cpx_dbh.query("INSERT INTO brandlist SET brandname='#{cpx_dbh.quote(brand[0])}', dnis='#{dnis}', tenant=#{tenant[1]}, brand=#{brand[1]}")
			end
	end
end
