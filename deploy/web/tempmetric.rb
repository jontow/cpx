#!/usr/local/bin/ruby
#########################################
# tempmetric.rb
# Temp metrics tool for cpx 
#
# Copyright 2007 Fused Solutions
#
########################################
require 'rubygems'
require 'cgi'
require 'cgi/session'
require 'cgi/session/pstore'
require 'pstore'
require 'mysql'
require 'time'
require 'javamd5'
require 'digest/md5'

require 'localconfig'
include TempMetricConfig

require 'billing_utils/write_summary.rb'

###### Main Execution Line ######
@cgi = CGI.new
@fields = @cgi.keys

# cgi headers
def printheader
	puts "Content-type: text/html\r\n\r\n"
	puts "<html><head>"
	puts "<META HTTP-EQUIV='expires' VALUE='Thu, 16 Mar 2000 11:00:00 GMT'>"
	puts "<META HTTP-EQUIV='pragma' CONTENT='no-cache'>"
	puts "<title>Temp Metrics Tool</title>\n</head>\n<body>\n"
	puts "<center><h1>Temp Metrics</h1>"
end

def printfooter
	puts "\n</center></body></html>"
	@session.close if @session
end

def refresh
	printheader
	puts "<META HTTP-EQUIV='Refresh' CONTENT='0'>"
	printfooter
	exit
end


def sessioncleanup
	now=Time.now
	Dir.foreach(TempMetricConfig::SESSIONSTORE) do |filename| 
		filename.scan(/tempmetric_sess_/) do 
			expires = nil
			begin
				pstore = PStore.new(TempMetricConfig::SESSIONSTORE + "/" +filename)
				pstore.transaction do |store|
					exproot = store.roots.detect{ |root| store[root].has_key?('session_expires')}
					expires = store[exproot]['session_expires'] if exproot
				end
				File.delete(TempMetricConfig::SESSIONSTORE+"/"+filename) if !expires or expires < now
			rescue Errno::EACCES
			end
		end
	end
end

def getsession(id="")
  sessioncleanup
	begin
		if id != ""
			@session = CGI::Session.new(@cgi,
																'database_manager' => CGI::Session::PStore,  # use PStore
																'session_key' => '_tm_sess_id',              # custom session key
																'session_id' => id,
																'prefix' => 'tempmetric_sess_',
																'new_session' => false)
		else
			@session = CGI::Session.new(@cgi,
																'database_manager' => CGI::Session::PStore,  # use PStore
																'session_key' => '_tm_sess_id',              # custom session key
																'session_expires' => Time.now + TempMetricConfig::SESSIONTIMEOUT,
																'prefix' => 'tempmetric_sess_')
		end
		@session['session_expires'] = Time.now + TempMetricConfig::SESSIONTIMEOUT if @session and !@session['session_expires']
		return true
	rescue ArgumentError
		return false
	end
end

refresh if !getsession(@cgi['session']) or !@session

begin
	if Time.now > @session['session_expires'] 
		@session.delete if @session
		refresh
	end
rescue ArgumentError
	refresh
end


# Initialize the DB handle
@cpx_dbh = Mysql.real_connect(TempMetricConfig::DBHOST, TempMetricConfig::DBUSER, TempMetricConfig::DBPASS, TempMetricConfig::DBNAME)

# output all sql query strings as html comments for debugging
def mysqlquery(querystr)
	puts "<!--"
	puts "#{querystr}"
	result=@cpx_dbh.query(querystr)
	puts "\nReturned #{result.num_rows} rows." if querystr[0..5].downcase=='select'   
	puts "-->"
	return result
end

printheader

#@cgi.keys.sort.each { |k| puts "#{k} := #{@cgi[k]}<br>" }

if !@cgi.has_key?('action') then
	if !(@session['agent'])
		# login with username and password
		# pass the password across the network in it's MD5 Hash
		nonce=rand
		@session['nonce']=nonce
		puts "<script>
		#{JAVAMD5}
	 function dologin()
	 {
		datenow=new Date();
		nonce='#{nonce}';
		document.getElementById('serial').value=nonce
		clearbx=document.getElementById('clearpassword');
		hashbx=document.getElementById('hashpw');
		action=document.getElementById('action');
		action.value='Login';
		hash=hex_md5(nonce + hex_md5(clearbx.value));
		hashbx.value=hash
		document.loginform.submit();
	 }
	 function onEnter( evt, frm ) 
	 {
		var keyCode = null;

		if( evt.which ) {
			keyCode = evt.which;
		} else if( evt.keyCode ) {
			keyCode = evt.keyCode;
		}
		if( 13 == keyCode ) {
			frm.Login.click();
			return false;
		}
		return true;
	 }
	</script>
	 <form action='' method='POST' id='loginform' name='loginform'>
	 <input type='hidden' name='session' value='#{@session.session_id}'>
	 <table border=0 cellspacing=0>
	 <tr><td align='right'>User Name:</td>
	 <td><input name='username' onkeypress='return onEnter(event, this.form)'></td></tr>
	 <tr><td align='right'>Password:</td>
	 <input type='hidden' id='hashpw' name='hash'>
	 <input type='hidden' id='action' name='action'>
	 <input type='hidden' id='serial' name='serial'> 
	 <input type='hidden' name='tenant' value=#{@cgi['tenant']}>
	 <input type='hidden' name='brand' value=#{@cgi['brand']}>
	 <td><input type='password' id='clearpassword' onkeypress='return onEnter(event, this.form)'></td></tr>
	 <tr><td colspan=2 align='center'>
	 <input type='button' value='Login' name='Login' onclick='dologin()'></td></tr>
	 </table>
	 </form>
	 "
		printfooter
		exit
	else
		@tenant=@cgi['tenant']
		@brand=@cgi['brand']
		# Knowplex v5 should send agent name in agentfname and agentlname
		# Knowplex v6 should send agent id in agent
		if @session['agent']
			@agent=@session['agent']
			@agentid=@session['agent'] - 1000
		else
			agentrecord=mysqlquery("SELECT * FROM tblAgent WHERE Active=1 AND FirstName='#{@cpx_dbh.quote(@cgi['agentfname'])}' AND LastName='#{@cpx_dbh.quote(@cgi['agentlname'])}'")
			if agentrecord.num_rows == 1
				# if agent is found by name, get the AgentID 
				# to use for callbacks
				agenth=agentrecord.fetch_hash
				@agent=agenth['AgentID'].to_i + 1000
				@agentid=agenth['AgentID']
			else
				# agent not found, or agent name not unique
				puts "<h1>Required variable missing</h1>"
				printfooter
				exit
			end
		end
		brands=mysqlquery("SELECT * FROM brandlist ORDER BY brandname")
		# find and display outstanding calls this agent started within the last 2 hours
		cuttime = Time.now.to_i - TempMetricConfig::MAXCALLTIME
		callIDs=mysqlquery("SELECT UniqueID FROM billing_transactions WHERE Start > #{cuttime} AND Transaction=#{CDRConstants::INCALL} AND Data=#{@agent} AND UniqueID LIKE '%.0%' AND NOT UniqueID LIKE '%.0' AND NOT (UniqueID IN (SELECT UniqueID FROM billing_transactions WHERE Start > #{cuttime} AND Transaction=#{CDRConstants::CDREND}))")
		puts "<table border='1'><tr><td>"
		if callIDs.num_rows > 0 then
			puts "<table border='1' cellspacing='2' cellborder='1'>"
			puts "<tr><td colspan=4 align=center><b><big>Temp Metrics Calls In Progress</big></b></td></tr>"
			puts "<tr><td>Call ID</td><td>Brand</td><td>Start Time</td><tr>"
			callIDs.each_hash do |callid|
				cid=callid['UniqueID']
				qry="SELECT * FROM call_info i INNER JOIN brandlist b on (i.TenantID=b.tenant AND i.BrandID=b.brand) WHERE UniqueID='#{cid}'"
				callinfo=mysqlquery(qry)
				if callinfo.num_rows == 1 then
					qry="SELECT * from billing_transactions WHERE UniqueID='#{cid}' AND Transaction=#{CDRConstants::CDRINIT}" 
					inittran=mysqlquery(qry)
					if inittran.num_rows==1 then
						starttime=nil
						brandname=nil
						inittran.each_hash { |init| starttime=Time.at(init['Start'].to_i) }
						callinfo.each_hash { |info| brandname=info['brandname'] }
						puts "<tr><td>#{cid}</td><td>#{brandname}</td><td>#{starttime.strftime('%H:%M:%S')}</td>"
						print "<td><form action='?tenant=#{@tenant}&brand=#{@brand}&agent=#{@agentid}' method='POST'>"
						print "<input type='hidden' name='session' value='#{@session.session_id}'>"
						print "<input type='hidden' name='uid' value='#{cid}'>"
						print "<input type='hidden' name='tenant' value='#{@cgi['tenant']}'>"
						print "<input type='hidden' name='brand' value='#{@cgi['brand']}'>"
						print "<input type='hidden' name='agent' value='#{@agentid}'>"
						print "<br>"
						print "<input type='submit' name='action' value='End Call'>"
						puts "</form></td></tr>"
					else
						puts "<hr>ERROR!<br>Ambiguous returns for:<br>#{qry}<hr>"
					end
				else
					puts "<hr>ERROR!<br>Ambiguous returns for:<br>#{qry}<hr>"
				end
			end
			puts "</table>"
		else
			puts "<center>No Active Calls</center>"
		end
		puts "</td></tr><tr><td><hr></td></tr>"
		puts "<tr><td colspan=4 align=center><b><big>Start New Call</big></b></td></tr>"
		puts "<tr><td>"
		# start new call form
		puts "<form action='' method='POST'>"
		puts "<input type='hidden' name='session' value='#{@session.session_id}'>"
		puts "<center><br>Brand:"
		puts "<select name='clientid'>"
		puts "<option value='none'>Select Brand</option>"
		puts "#{[@tenant,@brand]}"
		brands.each_hash do |brand| 
			print "<option value='#{"%4.4d%4.4d" % [brand['tenant'],brand['brand']]}'"
				print " selected='selected'" if (brand['tenant']==@tenant and brand['brand']==@brand)
				puts ">#{brand['brandname']}</option>"
		end
		puts "</select><br><br>"
		puts "Call Type:"
		puts "<select name='calltype'>"
		puts "<option value='tempother'>Other</option>"
		puts "<option value='tempin'>Inbound</option>"
		puts "<option value='tempout'>Outbound</option>"
		puts "<option value='tempemail'>Email</option>"
		puts "</select>"
		puts "<br><br>"
		puts "<input type='submit' name='action' value='Start Call'>"
		puts "<input type='hidden' name='tenant' value='#{@cgi['tenant']}'>"
		puts "<input type='hidden' name='brand' value='#{@cgi['brand']}'>"
		puts "</center></form><br>"
		puts "</td></tr></table>"
	end
else
	case @cgi['action']
	when 'Login'
		if (@session['nonce'] and @cgi.has_key?('username') and @cgi.has_key?('hash'))
			# find the user
			result=mysqlquery("SELECT * FROM tblAgent WHERE Login='#{@cpx_dbh.quote(@cgi['username'])}' AND active=1")
			if result.num_rows==1
				user=result.fetch_hash
				pwhash=Digest::MD5.new
				pwhash << "#{@session['nonce']}#{user['Password'].downcase}"
				if pwhash.hexdigest==@cgi['hash']
					# password is good, generate a form to post the agentid
					# todo - implement some sort of session ID to cross ref with username to prevent tampering
					@session['agent'] = user['AgentID'].to_i + 1000
					puts "<form action='' name='blankform' method='POST'>
					<input type='hidden' name='session' value='#{@session.session_id}'>
					<input type='hidden' name='tenant' value=#{@cgi['tenant']}>
					<input type='hidden' name='brand' value=#{@cgi['brand']}>
					</form>
					<script>
					document.blankform.submit()
					</script>"
					printfooter
				else
					# bad password
					@session.delete
					puts "<META HTTP-EQUIV='Refresh' CONTENT='5'>"
					puts "<h2><center>Login Failed</center></h2>"
					printfooter
				end
			else
				# username not found or not unique
				@session.delete
				puts "<META HTTP-EQUIV='Refresh' CONTENT='5'>"
				puts "<h2><center>Login Failed</center></h2>"
				printfooter
			end
		else
			# missing username or hash
			@session.delete
			puts "<META HTTP-EQUIV='Refresh' CONTENT='5'>"
			puts "<h2><center>Login Failed</center></h2>"
			printfooter
		end
	when 'Start Call'
		puts "<head><title>Start Call</title>"
		if !(@cgi.has_key?('tenant') and @cgi.has_key?('brand') and @cgi.has_key?('clientid') and @cgi.has_key?('calltype') and @session['agent']) then
			# somebody is being funny
			puts "</head><body><center><h1>Error starting call.  Please close this window and try again.</h1></center></body>"
		else
			clientid=@cgi['clientid'].to_s
			if clientid=='none'
				# somebody is being dumb
				puts "</head><body><center><h1>Error: No brand selected.  Please go back and select a brand.</h1></center></body>"
				puts "<form action='' name='blankform' method='POST'>
					<input type='hidden' name='session' value='#{@session.session_id}'>
					</form>
					<script>
					setTimeout('document.blankform.submit();',3000);
					</script>"
			else
				tenantid=clientid.slice(0,4)
				brandid=clientid.slice(4,4)
				agentid=@session['agent']
				calltype=@cgi['calltype']
				newuid = Time.now.to_i.to_s + ".0" + (rand(899) + 100).to_s
				# make sure the unique id is not already in use
				until (mysqlquery("SELECT * FROM billing_transactions WHERE UniqueID='#{newuid}'").num_rows == 0) do
					newuid = Time.now.to_i.to_s + ".0" + (rand(899) + 100).to_s
				end
				callstart = Time.now.to_i
				puts "</head><body>Starting Call to #{clientid} at #{Time.at(callstart)}<br>"
				puts "<br>"
				# store a CDRINIT to establish the uniqueid and a INCALL to register the agent id
				mysqlquery("INSERT INTO billing_transactions (UniqueID, Transaction, Start, End, Data) " +
					 "VALUES('#{@cpx_dbh.quote(newuid.to_s)}',#{CDRINIT},#{callstart},#{callstart},'#{@cpx_dbh.quote(calltype.to_s)}')," + 
					 "(#{@cpx_dbh.quote(newuid.to_s)},#{INCALL},#{callstart},#{callstart},#{agentid})")
					 # save client id and calltype in call_info
					 mysqlquery("INSERT INTO call_info (UniqueID, TenantID, BrandID, CallType) VALUES('#{@cpx_dbh.quote(newuid.to_s)}',#{tenantid},#{brandid},'#{@cpx_dbh.quote(calltype.to_s)}')")
					 puts "<form action='' name='blankform' method='POST'>
					<input type='hidden' name='session' value='#{@session.session_id}'>
					</form>
					<script>
					setTimeout('document.blankform.submit();',3000);
					</script>"
			end
		end
	when 'End Call'
		puts "<head><title>End Call</title>"
		if !(@cgi.has_key?('uid') and @session['agent']) or mysqlquery("SELECT * FROM billing_transactions WHERE UniqueID='#{@cgi['uid']}' AND (Transaction=#{ENDCALL} or Transaction=#{CDREND})").num_rows > 0 
			# no call specified, no transactions for this call, or not logged in
			puts "</head><body><center><h1>Error completing call.  Please close this window and try again.</h1></center></body>"                         
		else
			uid=@cgi['uid']
			callend = Time.now.to_i
			# get most recent INCALL transaction for this call
			callqry=mysqlquery("SELECT * FROM billing_transactions WHERE UniqueID='#{uid}' AND Transaction=#{INCALL} ORDER BY Start DESC LIMIT 1")
			if callqry.num_rows != 1 then
				puts "</head><body><center><h1>Error locating record for call ID# #{uid}.  Please notify the administrator.</h1></center></body>"
			else
				# end that call, adding 0 length wrapup
				calldata=callqry.fetch_hash()
				callstart=calldata['End'].to_i
				agnt=calldata['Data']
				mysqlquery("UPDATE billing_transactions SET End=#{callend} where UniqueID='#{@cpx_dbh.quote(uid.to_s)}' AND Transaction='#{INCALL}'") 
				mysqlquery("INSERT INTO billing_transactions (UniqueID, Transaction, Start, End, Data) VALUES" +
						"('#{@cpx_dbh.quote(uid.to_s)}',#{ENDCALL}, #{callend}, #{callend}, '')," +
						"('#{@cpx_dbh.quote(uid.to_s)}',#{INWRAPUP},#{callend},#{callend},'#{@cpx_dbh.quote(agnt.to_s)}')," +
						"('#{@cpx_dbh.quote(uid.to_s)}',#{ENDWRAPUP},#{callend},#{callend},'#{@cpx_dbh.quote(agnt.to_s)}')," +
						"('#{@cpx_dbh.quote(uid.to_s)}',#{CDREND},#{callend},#{callend},'')")
						puts "<!--"
						p @cgi.keys.sort 
						puts "write_summary(@cpx_dbh, #{uid}, :insert)"
						# create summary from transactions
						# any error message from write_summary will be in html comment
						summarized = write_summary(@cpx_dbh, uid, :insert)
						puts "-->"
						if summarized
							puts "</head><body>Completing Call at #{Time.at(callend)}<br>"
							puts "<form action='' name='blankform' method='POST'>
					<input type='hidden' name='session' value='#{@session.session_id}'>
					</form>
					<script>
					setTimeout('document.blankform.submit();',3000);
					</script>"
						else
							puts "</head><body><center><h1>GAAAA!</h1><br>Something has gone horribly wrong.<br>"
							puts "Please copy and the following information and send it to the supervisor:<br><br>"
							puts "UniqueID: #{uid}<br>Start: #{callstart}<br>End: #{callend}<br>ID: #{agnt}<br><br>"
							puts "</body>"
						end
						puts "<br>"
			end

		end
	when 'Logout'
		@session.delete if @session
	else
		#somebody is trying to be funny
		puts "Bad User! #{@cgi['action']}"
		@session.delete if @session
	end
end

printfooter
