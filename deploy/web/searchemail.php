<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
	<head>
		<title>Email Search Tool</title>
	</head>
<body>
<?php

require_once('./config.php');

$action	= $_GET['action'];

$connect = @mysql_connect($dbhost, $dbuser, $dbpass);
$dbselect = @mysql_select_db($dbname);
	if (!$connect) {
		die("Database error, please give the supervisor the following info:<br /><br />\n<b>MySQL Error: ".mysql_error()."<br />\nMySQL Error Number: ".mysql_errno()."</b>");
	} else{
		if (!$dbselect) {
			die("Database error, please give the supervisor the following info:<br /><br />\n<b>MySQL Error: ".mysql_error()."<br />\nMySQL Error Number: ".mysql_errno()."</b>");
		}
	}

$clientquery = mysql_query("SELECT tenant, brand, brandname FROM $brandtable");
if (@mysql_num_rows($clientquery) == '0') {
	die("Database error, please give the supervisor the following info:<br /><br />\n<b>MySQL Error: ".mysql_error()."<br />\nMySQL Error Number: ".mysql_errno()."</b>");
} else {
	while ($clientrow = mysql_fetch_assoc($clientquery)) {
		$clients[] = $clientrow;
	}
}

if ($action == 'report') {
	$script = "$script?action=report";
	$reportquery = mysql_query("SELECT * FROM email WHERE replied='0'");
	if (mysql_num_rows($reportquery) == '0') {
		echo "There are no unreplied emails.";
	} else {
?>
	<h4>Currently open emails:</h4>
	<table border="1" cellspacing="0">
		<tr>
			<td>ID:</td>
			<td>From:</td>
			<td>Reply:</td>
			<td>Subject:</td>
			<td>Client:</td>
		</tr>

<?php
	while ($msg = mysql_fetch_array($reportquery)) {
	$id = $msg['id'];
	$from = $msg['msgfrom'];
	$reply = $msg['msgreply'];
	$subject = $msg['subject'];
	$client = "needs work";
	echo "\t\t<tr>\n\t\t\t<td><a href=\"email.php?id=$id\">$id</a></td>\n\t\t\t<td>$from</td>\n\t\t\t<td>$reply</td>\n\t\t\t<td>$subject</td>\n\t\t\t<td>$client</td>\n\t\t</tr>\n";
}
	echo "\t</table>\n";
	}
	echo "<br /><br /><hr width=\"75%\"><br /><br />";
}

$searchparam = $_POST['searchparam'];
if ($searchparam == 'email') {
	$searchdata = $_POST['email'];
} else {
	$searchdata = $_POST['client'];
}
$addparam = $_POST['addparam'];
$keyword = $_POST['keyword'];
$startmonth = $_POST['startmonth'];
$startday = $_POST['startday'];
$startyear = $_POST['startyear'];
$endmonth = $_POST['endmonth'];
$endday = $_POST['endday'];
$endyear = $_POST['endyear'];
$sent = $_POST['sent'];

$secperday = 60*60*24; //How many seconds per day, to add onto the $end variable so it ends the day at midnight.

$start = strtotime("$startmonth/"."$startday/"."$startyear");
$end = strtotime("$endmonth/"."$endday/"."$endyear")+$secperday;

if (isset($sent)) {
	if ((!$searchparam) || ($startmonth == 'NULL') || ($startday == 'NULL') || ($startyear == 'NULL') || ($endmonth == 'NULL') || ($endday == 'NULL') || ($endyear == 'NULL') || (!$searchdata) || ($searchdata == 'Email Address') || ($searchdata == 'NULL')) {
		echo "<h3>Make sure that all fields are filled out and try again.</h3>";
	} elseif (isset($addparam)) {
		if ((!$keyword) || ($keyword == 'Keyword Search')) {
			echo "<h3>Make sure that all fields are filled out and try again.</h3>";
		}
	} else {
		
	}
}
?>
	<h4>Search Parameters:</h4>
	<form action="<? echo "$script"; ?>" method="post">
		<table cellspacing="5">
			<tr>
				<td><input type="radio" name="searchparam" value="email" /></td>
				<td>Email Address:</td>
				<td><input type="text" name="email" size="50" value="Email Address" /></td>
			</tr>
			<tr>
				<td><input type="radio" name="searchparam" value="client" /></td>
				<td>Client:</td>
				<td>
					<select name="client">
						<option value="NULL">Client</option>
<?php
	while($row = mysql_fetch_assoc($clientquery)) {
	$clientname = $row['brandname'];
	$tenant = str_pad($row['tenant'], 4, 0, STR_PAD_LEFT);
	$brand = str_pad($row['brand'], 4, 0, STR_PAD_LEFT);
	$tenbrand = $tenant.$brand;
	echo "\t\t\t\t\t\t<option value=\"$tenbrand\">$clientname</option>\n";
	}
?>
					</select>
				</td>
			</tr>
			<tr>
				<td><input type="checkbox" name="addparam" value="keyword" /></td>
				<td>Additional Keyword Search:</td>
				<td><input type="text" name="keyword" size="50" value="Keyword Search" /></td>
			</tr>
			<tr>
				<th colspan="2">Search between:</th>
			</tr>
			<tr>
				<td></td>
				<td>Start Date:</td>
				<td>
					<select name="startmonth">
						<option value="NULL">Month</option>
<?php
	for($i=1; $i<=12; $i++) {
		$i = str_pad($i, 2, 0, STR_PAD_LEFT);
		echo "\t\t\t\t\t\t<option value=\"$i\">$i</option>\n";
	}
?>
					</select>
					<select name="startday">
						<option value="NULL">Day</option>
<?php
	for($i=1; $i<=31; $i++) {
		$i = str_pad($i, 2, 0, STR_PAD_LEFT);
		echo "\t\t\t\t\t\t<option value=\"$i\">$i</option>\n";
	}
?>
					</select>
					<select name="startyear">
						<option value="NULL">Year</option>
<?php
	$y = date('y');
	for($i=7; $i<=$y; $i++) {
		$i = str_pad($i, 2, 0, STR_PAD_LEFT);
		echo "\t\t\t\t\t\t<option value=\"$i\">$i</option>\n";
	}
?>
					</select>
				</td>
			</tr>
			<tr>
				<th colspan="2">and</th>
			</tr>
			<tr>
				<td></td>
				<td>End Date:</td>
				<td>
					<select name="endmonth">
						<option value="NULL">Month</option>
<?php
	for($i=1; $i<=12; $i++) {
		$i = str_pad($i, 2, 0, STR_PAD_LEFT);
		echo "\t\t\t\t\t\t<option value=\"$i\">$i</option>\n";
	}
?>
					</select>
					<select name="endday">
						<option value="NULL">Day</option>
<?php
	for($i=1; $i<=31; $i++) {
		$i = str_pad($i, 2, 0, STR_PAD_LEFT);
		echo "\t\t\t\t\t\t<option value=\"$i\">$i</option>\n";
	}
?>
					</select>
					<select name="endyear">
						<option value="NULL">Year</option>
<?php
	$y = date('y');
	for($i=7; $i<=$y; $i++) {
		$i = str_pad($i, 2, 0, STR_PAD_LEFT);
		echo "\t\t\t\t\t\t<option value=\"$i\">$i</option>\n";
	}
?>
					</select>
				</td>
			</tr>
		</table><br />
		<input type="hidden" name="sent" />
		<input type="submit" value="Search for Email" />
	</form>
<?php

?>
</body>
</html>
