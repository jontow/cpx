<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
		<head>
				<title>Email Responder</title>
		<script type="text/javascript">
		function toggleheaders()
		{
			//alert("die!");
			var e = document.getElementById('headers');
			if(e.style.display == "none" )
				e.style.display = "block";
			else
				e.style.display = "none";
		}
		</script>
</head>
<body>
<?php

require_once('./config.php');

/* Retrieve and store email ID number */
$id = $_GET['id'];

function sqldie() {
	die(
		"Database error, please give the supervisor the following info:" .
		"<br /><br />\n<b>MySQL Error: ".mysql_error()."<br />\n" .
		"MySQL Error Number: ".mysql_errno()."</b>"
	);
}

if (!is_numeric($id)) {
	die("No Message ID specified, sorry.");
} else {

	$connect = @mysql_connect($dbhost, $dbuser, $dbpass) or sqldie();
	$dbselect = @mysql_select_db($dbname) or sqldie();

	$result = mysql_query("SELECT * FROM $emailtable WHERE id=$id");# AND replied=0");
/*	if (@mysql_num_rows($result) == '0') {
		die("No Message found with that ID, or the message was already replied to, sorry.");
} else {*/
	$mail = mysql_fetch_assoc($result);
	$dbto = $mail['msgreply'];
	$dbfrom = $mail['msgto'];
	$dbsubject = $mail['subject'];
	$rawheaders = $mail['rawheaders'];
	$tenant = $mail['tenantid'];
	$brand = $mail['brandid'];
	$replied = $mail['replied'];
//	}

	$subject = $_POST['subject'];
	$to = $_POST['to'];
	$cc = $_POST['cc'];
	$body = $_POST['body'];
	$action = $_POST['action'];

	if (isset($action) && ($action == 'mail') && $replied == 0) {

		$from = $dbfrom;

		if ((!$to) || (!$subject) || (!$body)) {
			die("Make sure that all fields are filled out and try again.");
		} else {
			$mailhead = "From: $from" . "\r\n" . "Reply-To: $from" . "\r\n" . "Cc: $cc" . "\r\n";
			$sendmail = mail($to, $subject, $body, $mailhead);
			if (!$sendmail) {
				die("Could not send email, please send email through Knowplex, and notify the supervisor of the Case ID and Company name.");
			} else {
				echo "Message successfully sent.";
				$update = mysql_query("UPDATE $emailtable SET replied=1, replyto=\"$to\", replysubject=\"$subject\", replybody=\"$body\" WHERE id=\"$id\"") or sqldie();
			}
		}
	} elseif ($action == 'none' && $replied == 0) {
		$update = mysql_query("UPDATE $emailtable SET replied=1 WHERE id=\"$id\"") or sqldie();
		if ($update) {
			echo "Message successfully marked as resolved.";
		}
	} else {
		if (isset($_GET['print']) or $replied != 0) {
			echo "<b>From</b>: $dbfrom<br />\n";
			echo "<b>To</b>: $dbto<br />\n";
			echo "<b>Subject</b>: $dbsubject<br />\n";

			if (isset($_GET['headers'])) {
?>
			<!--<a href="#" onClick="toggleheaders();">Show/hide headers</a><br/>-->
			<div style="padding-left: 20px;" id="headers">
				<pre>
<?echo htmlentities($rawheaders);?>
				</pre>
			</div>
<?php
			}

			echo "<pre>\n";
			foreach(explode("\n", trim($mail['body'])) as $line) {
				echo "$line\n";
			}
			echo "</pre>\n";
		} else {
?>
	<form action="<?php echo "?id=$id"; ?>" method="post" />
			<b>Multiple To: or Cc: recipients must be separated by a comma and a space.</b>
			<table border="0">
					<tr>
						<td>To:</td>
						<td><input type="text" name="to" size="50" value="<? echo "$dbto"; ?>" /></td>
					</tr>
					<tr>
						<td>Cc:</td>
						<td><input type="text" name="cc" size="50" /></td>
					</tr>
					<tr>
						<td>Subject:</td>
						<td><input type="text" name="subject" size="50" value="RE: <? echo "$dbsubject"; ?>" /></td>
					</tr>
					<tr>
						<td>From:</td>
						<td><? echo "$dbfrom"; ?></td>
					</tr>
			</table>
			<a href="#" onClick="toggleheaders();">Show/hide headers</a><br/>
			<div style="padding-left: 20px; display: none;" id="headers">
				<pre>
<?echo htmlentities($rawheaders);?>
				</pre>
			</div>
			<textarea rows="20" cols="100" name="body"><?
				$headers = explode("\n", $rawheaders);
				foreach($headers as $header) {
					$line = explode(": ", $header);
					if ($line[0] == 'Date') {
						$date = $line[1];
					}
					if ($line[0] == 'From') {
						$headerfrom = $line[1];
					}
				}
			echo "\n\nOn $date, $headerfrom wrote:\n";
			foreach(explode("\n", trim($mail['body'])) as $line) {
				$line = '> '.$line;
				echo "$line\n";
			}
			?></textarea><br/><br />
			<input type="hidden" name="action" value="mail" />
			<input type="submit" value="Send Message" />
		</form>
		<form action="<?php echo "?id=$id"; ?>" method="post" />
			<input type="hidden" name="action" value="none" />
			<input type="submit" value="No Action Required" /> I.E. spam or notification email.
		</form>

<?php
		}

		if (!isset($_GET['print'])) {
			echo "<br /><br /><a href=\"{$_SERVER['PHP_SELF']}?id=$id&print=1\">Printer-friendly copy</a>\n";
		}

		$result = mysql_query("SELECT * FROM $emailattachtable WHERE emailid=$id");
		if (@mysql_num_rows($result) > '0') {
			echo "<br /><br />Attachments:";
			echo "<ul>";
			while ($row = mysql_fetch_assoc($result)) {
				echo "<li><a href=\"$emailattachdir/$id/{$row['filename']}\">{$row['filename']} ({$row['mimetype']})</a></li>";
			}
			echo "</ul>";
		}
?>
		


</body>
</html>
<?php
	}
}
?>
