<?php

$CDRINIT = 0;
$INIVR = 1;
$DIALOUTGOING = 2;
$INQUEUE = 3;
$INCALL = 4;
$INOUTGOING = 5;
$FAILEDOUTGOING = 6;
$TRANSFER = 7;
$WARMXFER = 8;
$WARMXFERCOMPLETE = 9;
$WARMXFERFAILED = 10;
$WARMXFERLEG = 11;
$INWRAPUP = 12;
$ENDWRAPUP = 13;
$ABANDONQUEUE = 14;
$ABANDONIVR = 15;
$ENDCALL = 16;
$UNKNOWNTERMINATION = 17;
$CDREND = 18;

require_once('config.php');

$connect = mysql_connect($dbhost, $dbuser, $dbpass);
$db = mysql_select_db($dbname);

$calltype   =   $_POST['calltype'];
$call       =   $_POST['call'];
$brandname  =   $_POST['brandname'];
$brand      =   $_POST['brand'];
$tenant     =   $_POST['tenant'];
$callstart  =   $_POST['callstart'];
$uniqueid   =   $_POST['uniqueid'];
$timestamp  =   mktime();
$clientid   =   str_pad($tenant, 4, 0, STR_PAD_LEFT).str_pad($brand, 4, 0, STR_PAD_LEFT);

function print_header(){
	echo"
<html>
<head>
	<title>Temp Metrics</title>
</head>

<body>";
}

if ($call == 'started') {

	if ($brandname == 'NULL') {
		print_header();
		echo "Please go back and select a client!";
	} else {
		$sql = ("INSERT INTO billing_transactions (UniqueID, Transaction, Start, End, Data) VALUES ('$uniqueid', $CDRINIT, '$timestamp', '$timestamp', '$calltype')");
		mysql_query($sql) or die( "Failed to execute query '$sql' with error: " . mysql_error());

		$callstart = mktime();
		$tenbrand = ("SELECT tenant, brand FROM brandlist WHERE brandname='$brandname'");
		$query = mysql_query($tenbrand);
		while ($row = mysql_fetch_array($query)) {
			$tenant = $row['tenant'];
			$brand = $row['brand'];
		}
		print_header();		
?>

	<form action="?" method="POST">
		<input type="hidden" name="call" value="ended" />
		<input type="hidden" name="calltype" value="<?php echo "$calltype"; ?>" />
		<input type="hidden" name="callstart" value="<?php echo "$callstart"; ?>" />
		<input type="hidden" name="uniqueid" value="<?php echo "$uniqueid"; ?>" />
		<input type="hidden" name="brand" value="<?php echo "$brand" ?>" />
		<input type="hidden" name="tenant" value="<?php echo "$tenant" ?>" />
		<input type="submit" value="End Call" />
	</form>

<?php
	}} elseif ($call == 'ended') {
		$callend = mktime();
		$calltime = abs($callstart - $callend);

		if ($calltype == 'Temp - In') {
			$tid = $INCALL;
		} else {
			$tid = $INOUTGOING;
		}
		
		$sql = "INSERT INTO billing_transactions (UniqueID, Transaction, Start, End, Data) VALUES('$uniqueid', $tid, '$callstart', '$callend', '$clientid'),('$uniqueid', $ENDCALL, '$timestamp', '$timestamp', ''),('$uniqueid', $INWRAPUP, '$timestamp', '$timestamp', ''),('$uniqueid', $ENDWRAPUP, '$timestamp', '$timestamp', ''),('$uniqueid', $CDREND, '$timestamp', '$timestamp', '')";

		mysql_query($sql) or die( "Failed to execute query '$sql' with error: " . mysql_error());
		
		$sql = "INSERT INTO billing_summaries (UniqueID, TenantID, BrandID, Start, End, InCall, LastState, CallType) VALUES ('$uniqueid', '$tenant', '$brand', '$callstart', '$callend', '$calltime', $ENDWRAPUP, '$calltype')";

		mysql_query($sql) or die( "Failed to execute query '$sql' with error: " . mysql_error());

		header("Location: {$_SERVER['PHP_SELF']}");
	} else {
		$rand = rand(100, 999);
		$uid = mktime().".$rand".".99999";
		$query = ("SELECT tenant, brand, brandname FROM brandlist ORDER BY brandname");
		$result = mysql_query($query);
		print_header();
?>

	<form action="?" method="POST">
		Call Type:<br />
		<select name="calltype">
			<option value="Temp - In">Inbound</option>
			<option value="Temp - Out">Outbound</option>
		</select><br /><br />
		Client:<br />
		<select name="brandname">
			<option value="NULL">Select a Client</option>
<?php
		while ($row = mysql_fetch_array($result)){
			$tenant = $row['tenant'];
			$brand = $row['brand'];
			$brandname = $row['brandname'];

			echo "\t\t\t<option value=\"$brandname\">$brandname</option>\n";
		}
?>
		</select><br /><br />
		<input type="hidden" name="call" value="started" />
		<input type="hidden" name="uniqueid" value="<?php echo "$uid"; ?>" />
		<input type="submit" value="Start Call" />
	</form>

<?php
	}
?>

</body>
</html>

