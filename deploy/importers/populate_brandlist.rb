#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

#
# This file only used as an example of integration, pulling a list of tenants/brands
# from an external DB and inserting it into the CPX DB.
#

require 'mysql'
load File.join(File.dirname(__FILE__), '../generator/genconstants.rb')
include GenConstants

kpx_dbh = Mysql.real_connect(KPXHOST, KPXUSER, KPXPASS, KPXDB)
cpx_dbh = Mysql.real_connect(CPXHOST, CPXUSER, CPXPASS, CPXDB)

kpx_dbh.select_db(KPXDB)

result = kpx_dbh.query('SELECT TenantSource, TenantID, TenantLabel FROM tblTenant')

tenants = []

while row = result.fetch_hash
	 tenants << row
end

result.free

tenants.each do |tenant|
	result = kpx_dbh.query("SHOW TABLES LIKE '#{tenant['TenantID']}_tblBrand'")
	if (result.num_rows != 1)
	  puts "Tenant #{tenant['TenantLabel']} (ID #{tenant['TenantID']}) does not have a brand table."
	 next
    end
	result = kpx_dbh.query("SELECT BrandListLabel, #{tenant['TenantID']}_tblBrand.BrandID, EmailFromAddress FROM #{tenant['TenantID']}_tblBrand INNER JOIN #{tenant['TenantID']}_tblBrandSystem WHERE Active=1 AND #{tenant['TenantID']}_tblBrand.BrandID=#{tenant['TenantID']}_tblBrandSystem.BrandID")
	brands = []
	while row = result.fetch_hash
		brands << row
	end
	result.free

	brands.each do |brand|
		tenantbrand = "#{tenant['TenantID'].rjust(4, '0')}#{brand['BrandID'].rjust(4, '0')}"

		# generate a callerid entry
		result = kpx_dbh.query("SELECT DataText FROM #{tenant['TenantID']}_tblBrandData WHERE datalabel='callerid' AND BrandID=#{brand['BrandID']}")
		if result.num_rows == 1
			calleridraw = result.fetch_row[0]
			calleridnum = calleridraw.split('').find_all { |r| r.match(/[0-9]/) }.join
			puts "Invalid callerid '#{calleridraw}' for brand '#{brand['BrandListLabel']}'" if calleridraw != calleridnum
			callerid = brand['BrandListLabel'][0..15] + ' <' + calleridnum + '>'
			cpx_dbh.query("UPDATE brandlist SET callerid='#{cpx_dbh.quote(callerid)}' WHERE tenant=#{tenant['TenantID']} AND brand=#{brand['BrandID']}")
		else
			cpx_dbh.query("UPDATE brandlist SET callerid=NULL WHERE tenant=#{tenant['TenantID']} AND brand=#{brand['BrandID']}")
		end
		result.free

		# email time of day stuff
		result = kpx_dbh.query("SELECT DataText FROM #{tenant['TenantID']}_tblBrandData WHERE (datalabel='emailstart' OR datalabel='emailend'  OR datalabel='emailoverride') AND BrandID=#{brand['BrandID']}")
		emailstart = (x = result.fetch_row) ? x[0] : nil
		emailend = (x = result.fetch_row) ? x[0] : nil
		emailoverride = (x = result.fetch_row) ? x[0] : nil
		if emailstart
			cpx_dbh.query("REPLACE INTO brand_settings SET tenant='#{tenant['TenantID']}', brand='#{brand['BrandID']}', keyname='emailstart', value='#{emailstart}'")
		else
			cpx_dbh.query("DELETE FROM brand_settings WHERE tenant='#{tenant['TenantID']}' AND brand='#{brand['BrandID']}' AND keyname='emailstart'")
		end

		if emailend
			cpx_dbh.query("REPLACE INTO brand_settings SET tenant='#{tenant['TenantID']}', brand='#{brand['BrandID']}', keyname='emailend', value='#{emailend}'")
		else
			cpx_dbh.query("DELETE FROM brand_settings WHERE tenant='#{tenant['TenantID']}' AND brand='#{brand['BrandID']}' AND keyname='emailend'")
		end

		if emailoverride
			cpx_dbh.query("REPLACE INTO brand_settings SET tenant='#{tenant['TenantID']}', brand='#{brand['BrandID']}', keyname='emailoverride', value='#{emailoverride}'")
		else
			cpx_dbh.query("DELETE FROM brand_settings WHERE tenant='#{tenant['TenantID']}' AND brand='#{brand['BrandID']}' AND keyname='emailoverride'")
		end
		result.free

		result = kpx_dbh.query("SELECT DNIS FROM tblTelephony WHERE TenantID=#{tenant['TenantID']} AND BrandID=#{brand['BrandID']}")
		if result.num_rows == 1
			dnis = result.fetch_row[0]
			if cpx_dbh.query("SELECT * FROM brandlist WHERE tenant=#{tenant['TenantID']} AND brand=#{brand['BrandID']}").num_rows == 1
				cpx_dbh.query("UPDATE brandlist SET brandname='#{cpx_dbh.quote(brand['BrandListLabel'])}', dnis='#{dnis}', email='#{brand['EmailFromAddress']}' WHERE tenant=#{tenant['TenantID']} AND brand=#{brand['BrandID']}")
			else
				cpx_dbh.query("INSERT INTO brandlist SET brandname='#{cpx_dbh.quote(brand['BrandListLabel'])}', dnis='#{dnis}', tenant=#{tenant['TenantID']}, brand=#{brand['BrandID']}, email='#{brand['EmailFromAddress']}'")
				puts "Added brand #{brand['BrandListLabel']} with DNIS #{dnis}"
			end
		else
			puts "missing telephony data for brand #{brand['BrandListLabel']}"
		end
		result.free
	end
end

# remove deactivated brands...

result = cpx_dbh.query("SELECT tenant, brand, brandname from brandlist")

result.each_hash do |row|
	tenants.detect{|x| x['TenantID'] == row['tenant']}
	#kpx_dbh.select_db(tenants.detect{|x| x['TenantID'] == row['tenant']}['TenantSource'])
	result2 = kpx_dbh.query("SELECT Active from #{row['tenant']}_tblBrand where BrandID=#{row['brand']}")
	if result2.num_rows == 1
		active = result2.fetch_row[0].to_i
		if active.zero?
			cpx_dbh.query("DELETE FROM brandlist WHERE tenant=#{row['tenant']} and brand=#{row['brand']}")
			puts "Deleted brand #{row['brandname']}"
		end
	else
		puts "Cannot find information for brand #{row['brandname']} (tenant: #{row['tenant']} brand: #{row['brand']})"
	end
	result2.free
end

result.free
