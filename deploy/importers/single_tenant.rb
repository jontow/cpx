#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2014, Jonathan Towne (jontow@mototowne.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the author, nor the names of contributors may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

################################################################################
#
# 2014-07-01 -- single tenant/brand database config builder
#

require 'mysql'
load File.join(File.dirname(__FILE__), '../generator/genconstants.rb')
include GenConstants

cpx_dbh = Mysql.real_connect(CPXHOST, CPXUSER, CPXPASS, CPXDB)

tenants = [{
	'TenantID' => '1',
	'brands' => [
		{
			'BrandID' => '1',
			'brandname' => 'Example Company',
			'emailfrom' => 'helpdesk@example.com',
			'callerid' => 'Example Com <8005551212>',
			'dnis' => '6800',
			'emailstart' => nil,
			'emailend' => nil,
			'emailoverride' => nil
		}
	]
}]

tenants.each do |tenant|
	tenant['brands'].each do |brand|
		tenantbrand = "#{tenant['TenantID'].rjust(4, '0')}#{brand['BrandID'].rjust(4, '0')}"

		cpx_dbh.query("REPLACE INTO brandlist SET tenant='#{tenant['TenantID']}', brand='#{brand['BrandID']}', email='#{brand['emailfrom']}', dnis='#{brand['dnis']}', brandname='#{brand['brandname']}'")

		# generate a callerid entry
		if !brand['callerid'].nil?
			cpx_dbh.query("UPDATE brandlist SET callerid='#{cpx_dbh.quote(brand['callerid'])}' WHERE tenant=#{tenant['TenantID']} AND brand=#{brand['BrandID']}")
		else
			cpx_dbh.query("UPDATE brandlist SET callerid=NULL WHERE tenant=#{tenant['TenantID']} AND brand=#{brand['BrandID']}")
		end

		# email time of day stuff
		if !brand['emailstart'].nil?
			cpx_dbh.query("REPLACE INTO brand_settings SET tenant='#{tenant['TenantID']}', brand='#{brand['BrandID']}', keyname='emailstart', value='#{brand['emailstart']}'")
		else
			cpx_dbh.query("DELETE FROM brand_settings WHERE tenant='#{tenant['TenantID']}' AND brand='#{brand['BrandID']}' AND keyname='emailstart'")
		end

		if !brand['emailend'].nil?
			cpx_dbh.query("REPLACE INTO brand_settings SET tenant='#{tenant['TenantID']}', brand='#{brand['BrandID']}', keyname='emailend', value='#{brand['emailend']}'")
		else
			cpx_dbh.query("DELETE FROM brand_settings WHERE tenant='#{tenant['TenantID']}' AND brand='#{brand['BrandID']}' AND keyname='emailend'")
		end

		if !brand['emailoverride'].nil?
			cpx_dbh.query("REPLACE INTO brand_settings SET tenant='#{tenant['TenantID']}', brand='#{brand['BrandID']}', keyname='emailoverride', value='#{brand['emailoverride']}'")
		else
			cpx_dbh.query("DELETE FROM brand_settings WHERE tenant='#{tenant['TenantID']}' AND brand='#{brand['BrandID']}' AND keyname='emailoverride'")
		end

		puts "Added brand #{brand['brandname']} (#{tenantbrand}) with DNIS #{brand['dnis']}"
	end
end
