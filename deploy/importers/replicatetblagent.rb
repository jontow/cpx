#! /usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

#
# This file only used as an example of integration, pulling a list of agents
# from an external DB and inserting it into the CPX DB.
#

require 'mysql'
load File.join(File.dirname(__FILE__), '../generator/genconstants.rb')
include GenConstants

kpx_dbh = Mysql.real_connect(KPXHOST, KPXUSER, KPXPASS, KPXDB)
cpx_dbh = Mysql.real_connect(CPXHOST, CPXUSER, CPXPASS, CPXDB)

TIER2PROFILE = {1=>1, 2=>3, 3=>4, 4=>5}

result = kpx_dbh.query("SELECT * FROM tblAgent")

result.each_hash do |row|
    #trim username after @ to prevent email addresses breaking sip
	m = row['Login'].match(/([^@]*)@.*$/)
    row['Login'] = m[1] if m  
	args = row.map{|k,v| "#{k}='#{cpx_dbh.quote(v)}'" if TBLAGENTCOLUMNS.include?(k)}.compact.join(',')
	aid = BASEID + row['AgentID'].to_i
	#if row['Active'].to_i == 1
		if res = cpx_dbh.query("SELECT #{TBLAGENTCOLUMNS.join(',')} from tblAgent WHERE AgentID=#{row['AgentID']}")and res.num_rows == 1
			#user already import, update their info
			res2 = res.fetch_hash
			oldtier = res2['TierID']
			oldusername = res2['Login']
			cpx_dbh.query("UPDATE tblAgent SET #{args} WHERE AgentID=#{row['AgentID']}")
			# always update internal agents, hosted only if still granted telaccess
			if (row['TypeID'] != '3') or ((res2['TelAccess'] == '1') and (row['TelAccess'] == '1'))
			  cpx_dbh.query("UPDATE sip SET callerid='#{cpx_dbh.quote(row['FirstName']+' '+row['LastName'])}', name='#{row['Login']}', username='#{row['Login']}', login='#{row['Login']}'  WHERE username='#{oldusername}'")
			  cpx_dbh.query("UPDATE agents SET name='#{row['Login']}' WHERE user=#{aid}")
			  if oldtier != row['TierID']
				cpx_dbh.query("UPDATE tblAgent SET DefaultProfile=#{TIER2PROFILE[row['TierID'].to_i]} WHERE AgentID=#{row['AgentID']}")
			  end
			end
			if (row['TypeID'] == '3') 
				#TelAccess revoked from hosted agent
				if (row['TelAccess'] == '0') and (res2['TelAccess'] == '1')
					cpx_dbh.query("DELETE FROM agents WHERE user=#{aid}")
					cpx_dbh.query("DELETE FROM sip WHERE username=#{aid}")
				#TelAccess granted to hosted agent
				elsif (row['TelAccess'] == '1') and (res2['TelAccess'] == '0')
					cpx_dbh.query("INSERT INTO agents VALUES(#{aid}, #{aid}, '#{cpx_dbh.quote(row['Login'])}')")
					cpx_dbh.query("INSERT INTO sip (name,username,login,type,secret,callerid,permit) VALUES('#{cpx_dbh.quote(row['Login'])}', '#{cpx_dbh.quote(row['Login'])}', '#{cpx_dbh.quote(row['Login'])}', 'friend', '', '#{cpx_dbh.quote(row['FirstName']+' '+row['LastName'])}', '#{SUBNET}')")
				end
			end
		else
			# insert new user
			cpx_dbh.query("INSERT INTO tblAgent SET #{args}, DefaultProfile=#{(row['TypeID']=='3') ? HOSTEDPROFILE : TIER2PROFILE[row['TierID'].to_i]}")
			if (row['TypeID'] != '3') or (row['TelAccess'] == '1')
			  cpx_dbh.query("INSERT INTO agents VALUES(#{aid}, #{aid}, '#{cpx_dbh.quote(row['Login'])}')")
			  cpx_dbh.query("INSERT INTO sip (name,username,login,type,secret,callerid,permit) VALUES('#{cpx_dbh.quote(row['Login'])}', '#{cpx_dbh.quote(row['Login'])}', '#{cpx_dbh.quote(row['Login'])}', 'friend', '', '#{cpx_dbh.quote(row['FirstName']+' '+row['LastName'])}', '#{SUBNET}')")
			end
		end
		res.free
	#else
		#cpx_dbh.query("DELETE FROM tblAgent WHERE AgentID=#{row['AgentID']}")
		#cpx_dbh.query("DELETE FROM agents WHERE user=#{aid}")
		#cpx_dbh.query("DELETE FROM sip WHERE username=#{aid}")
	#end
end

res = cpx_dbh.query("SELECT * FROM tblAgent WHERE Login='cpxmon'")
if res.num_rows == 0
	cpx_dbh.query("INSERT INTO tblAgent SET Login='cpxmon', FirstName='Cpx', LastName='Monitor', TierID=4, SecurityLevelID=4, DefaultProfile=1, Active=1, Password='4561dff90cb1adb0b45aaab526904dc4'")
	cpx_dbh.query("INSERT INTO agents VALUES(1000, 10000, 'cpxmon')")
end

res.free

cpx_dbh.query("UPDATE tblAgent SET AgentID=0 WHERE Login='cpxmon'")
