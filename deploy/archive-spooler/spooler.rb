#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

#
# 2007-05-10 -- jontow@mototowne.com
#

$LOAD_PATH << File.dirname(__FILE__)

require 'fileutils'

begin
	require "get-calldate"
	@date_from_db = true
rescue LoadError
	puts "Failed to load get-calldate module"
	@date_from_db = false
end

load File.join(File.dirname(__FILE__), 'spoolerconfig.rb')

def monitorprocess
	SpoolerConfig::MONITORSOURCES.each_with_index do |source, i|
		dest = SpoolerConfig::MONITORDESTINATIONS[i]
		begin
			Dir.chdir(source)
		rescue Errno::ENOENT
			STDERR.puts "Directory #{source} does not exist or is unaccessible, ignoring"
			SpoolerConfig::MONITORSOURCES.slice!(i)
			next
		end
		calls = Dir['*.wav']

		if !calls.empty?
			calls.each do |call|
				next if (Time.now - File.mtime(call)).to_i < 600
				begin
					fullpath = File.join(source, call)
					callfile = File.basename(call, '.wav')
					#date = Time.at(callfile.split('.', 2)[0].to_i)
					if @date_from_db
						cdate = get_call_time_from_db(callfile)
						if !cdate.nil? and (cdate.class == Time)
							date = Time.at(cdate.to_i)
							puts "Call time from DB: #{date}"
						else
							puts "CALL '#{callfile}' NOT FOUND IN DATABASE!  FALLING BACK TO MTIME!"
							date = File.mtime(call)
						end
					else
						date = File.mtime(call)
						#puts "MTIME: #{date}"
					end

					unless File.directory? File.join(dest, date.strftime('%F'))
						puts "Creating missing directory #{date.strftime("%F")}"
						Dir.mkdir(File.join(dest, date.strftime('%F')))
					end

					puts "Processing recorded call: #{call} (#{date.strftime("%Y-%m-%d")})"
					`mv #{fullpath} #{dest}/#{call}`
					if $? != 0
						raise SystemCallError, "Error moving #{fullpath} to #{dest}/#{call}"
					end

					`oggenc -q 0 -Q -o #{dest}/#{date.strftime('%F')}/#{callfile}.ogg #{dest}/#{call}`
					if $? != 0
							raise SystemCallError, "Error encoding #{dest}/#{call} to #{dest}/#{date.strftime('%F')}/#{callfile}.ogg"
					end

					File.delete("#{dest}/#{call}")
				rescue SystemCallError => e
					puts "Error: #{e.message}"
				end
			end
		end
	end
end

def vmprocess
	SpoolerConfig::VMSOURCES.each_with_index do |source, i|
		dest = SpoolerConfig::VMDESTINATIONS[i]
	
		begin
			Dir.chdir(source)
		rescue Errno::ENOENT
			STDERR.puts "Directory #{source} does not exist or is unaccessible, ignoring"
			SpoolerConfig::VMSOURCES.slice!(i)
			next
		end

		calls = Dir['*/*.wav']
	
		if !calls.empty?
			calls.each do |call|
				begin
					brandid = call.split('/', 2)[0]
					fullpath = File.join(source, call)
					callfile = call.sub(/\.wav/, '')
	
					Dir.mkdir(dest+"/"+brandid) unless File.directory?(dest+"/"+brandid)
	
					puts "Processing voicemail: #{call}"
					`mv #{fullpath} #{dest}/#{call}`
					if $? != 0
						raise SystemCallError, "Error moving #{fullpath} to #{dest}/#{call}"
					end
	
					`oggenc -q 0 -Q -o #{dest}/#{callfile}.ogg #{dest}/#{call}`
					if $? != 0
						raise SystemCallError, "Error encoding #{dest}/#{call} to #{dest}/#{callfile}.ogg"
					end
	
					File.delete("#{dest}/#{call}")
				rescue SystemCallError => e
					puts "Error: #{e.message}"
				end
			end
		end
	end
end

loop do
	#puts "#{Time.now.to_s}   :   Mark"
	monitorprocess
	vmprocess
	sleep 30
end
