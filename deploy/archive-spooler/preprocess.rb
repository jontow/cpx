#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2011, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

#
# 2011-01-06 -- jontow@mototowne.com
#

require 'fileutils'

MPLAYER = "/usr/local/bin/mplayer"
RAWDIR = '/home/incoming/raw'
COOKED = '/home/incoming/wav'
DEBUG = false
VERBOSE = true

if !File.directory?(RAWDIR) or !File.directory?(COOKED)
	STDERR.puts "Directories nonexistant or inaccessible, check config."
	exit
end

Dir.chdir(RAWDIR)

calls = Dir['*.{wma,mp3}']
if !calls.empty?
	calls.each do |call|
		# Found a call, strip the extension
		callfile = File.basename(call, '.*')

		puts "Found call: #{call} (#{callfile})" if DEBUG
		puts "Processing call: #{RAWDIR}/#{call}" if VERBOSE and !DEBUG

		# Check for output file, skip if its already there
		if File.exists?("#{COOKED}/#{callfile}.wav")
			puts "Output file #{COOKED}/#{callfile}.wav already exists! Skipping!" if VERBOSE or DEBUG
			next
		end

		# Convert (decode) file to destination/callfile.wav
		`#{MPLAYER} -really-quiet -vo null -vc dummy -af volume=0,resample=44100:0:1 -ao pcm:waveheader:file="#{COOKED}/#{callfile}.wav" "#{call}"`
		if $? != 0
			raise SystemCallError, "Error decoding #{call} to #{COOKED}/#{callfile}.wav"
		end

		# Remove original
		if File.exists?("#{COOKED}/#{callfile}.wav")
			puts "Call #{COOKED}/#{callfile}.wav successfully preprocessed, should remove original: #{RAWDIR}/#{call}" if VERBOSE or DEBUG
			File.unlink("#{RAWDIR}/#{call}") if !DEBUG
		end
	end
end	
