#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

#
# 2007-08-11 -- jontow@
#
# This should be run nightly on the archiving server; its purpose is simple:
# Make sure calls are sorted into a reasonable scheme for later viewing.
#

load File.join(File.dirname(__FILE__), 'spoolerconfig.rb')

SpoolerConfig::MONITORDESTINATIONS.each do |monitorroot|

	Dir.chdir(monitorroot)

	Dir['*.ogg'].each do |call|
		callmtime = File.mtime(call)
		calldate = callmtime.strftime("%Y-%m-%d")

		if (Time.now - callmtime) < 300
			#puts "Call #{call} still too new: not moving, it's a volatile operation."
			next
		end

		if !File.directory? calldate.to_s
			#puts "Making directory: #{calldate.to_s}"
			Dir.mkdir(calldate.to_s)
		end

		#puts "Moving #{call} to #{calldate.to_s}/"
		`mv #{call} #{calldate.to_s}/`
	end
end

exit
