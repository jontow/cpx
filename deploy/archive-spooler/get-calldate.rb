#!/usr/bin/env ruby
#
# 2011-01-10 -- jontow@mototowne.com
#

$LOAD_PATH << File.dirname(__FILE__)

require 'rubygems'
require 'mysql'

DEBUG = false
VERBOSE = false

def get_call_time_from_db(uniqueid)
	if uniqueid.nil?
		return nil
	end

	@dbh = Mysql.real_connect(SpoolerConfig::MYSQL_CREDENTIALS[:host],
				  SpoolerConfig::MYSQL_CREDENTIALS[:username],
				  SpoolerConfig::MYSQL_CREDENTIALS[:password],
				  SpoolerConfig::MYSQL_CREDENTIALS[:database])

	dbq = "SELECT End FROM billing_summaries WHERE UniqueID = '" + @dbh.quote(uniqueid) + "' LIMIT 1"
	puts dbq if DEBUG

	result = @dbh.query(dbq)
	if result.num_rows > 0
		puts "Got rows:" if DEBUG
		calltime = result.fetch_row[0]
		puts calltime.to_s if VERBOSE
		return calltime.to_s
	else
		puts "No returned rows" if VERBOSE
		return nil
	end
end

if DEBUG and !ARGV[0].nil?
	puts "Running from CLI with uniqueid: #{ARGV[0]}" if VERBOSE
	calltime = get_call_time_from_db(ARGV[0].chomp)
	if calltime.nil?
		puts "calltime nil"
		exit
	end
	decoded = `date -r #{calltime} +"%Y-%m-%d %H:%M:%S"`
	puts "#{calltime}: #{decoded}"
end
