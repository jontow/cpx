#!/bin/sh
#
# 2007-07-02 -- jontow@mototowne.com
#

# Set this to something valid to get status updates

CRASHLIST=$HOME/cpxmon.crash
export CRASHLIST
APPID="CPX Monitor"
export APPID

sleep 15

cd ${HOME}/CPX/cpxmon/trunk

while [ true ]; do {
  date +%s >> $CRASHLIST
  ruby main.rb
  sleep 5
  echo $APPDIR/crash.sh
  $APPDIR/crash.sh
}; done

exit

