#!/bin/sh

if [ "a$APPID" = "a" ]; then exit; fi
        etime=`date +%s`
        laststart=`tail -n 1 $CRASHLIST`
        echo "Died! restarting!"
				echo "$APPID restarted at `date`"
        if [ $((etime - laststart)) -gt 300 ]; then
            echo "$APPID restarted at `date`" | mail -s "$APPID Restart" $MAILTO
        else
            echo "Not mailing this time."
        fi

