#!/bin/sh
#
# 2007-07-02 -- jontow@mototowne.com
#

# Set this to something valid to get status updates

CRASHLIST=$HOME/auiserver.crash
export CRASHLIST
APPID="Agent Server"
export APPID

sleep 10

cd ${HOME}/CPX/auiserver/trunk

while [ true ]; do {
        date +%s >> $CRASHLIST
	ruby new_server.rb
	sleep 5
	$APPDIR/crash.sh
}; done

exit
