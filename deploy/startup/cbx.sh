#!/bin/sh
#
# 2007-07-02 -- jontow@mototowne.com
#

# Set this to something valid to get status updates
echo "`dirname $0`/script.conf"

CRASHLIST=${HOME}/cbx.crash
export CRASHLIST
APPID="CBX"
export APPID

cd $HOME
export TERM=xterm-color

while [ true ]; do {
	date +%s >> $CRASHLIST
	${HOME}/sbin/cbx -vvvvvvvvvvc
	sleep 5
	$DIR/crash.sh
}; done

exit
