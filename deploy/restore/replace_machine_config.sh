#!/bin/sh

if [ -f ./config.sh ]; then
	. ./config.sh
fi

cd /

tar zxvpf ${BACKUPDIR}/machine-configuration.tar.gz

/usr/local/bin/newaliases

exit
