#!/bin/sh

if [ -f ./config.sh ]; then
	. ./config.sh
fi

if [ "$CPXUSER" = "" ] || [ "$CPXGROUP" = "" ] || [ "$CPXHOME" = "" ] || [ "$CPXSHELL" = "" ]; then
	echo "Need CPXUSER, CPXGROUP, CPXHOME, and CPXSHELL set!"
	exit
fi

pw group add -n ${CPXGROUP}
pw user add -n ${CPXUSER} -d ${CPXHOME} -g ${CPXGROUP} -s ${CPXSHELL}

cd ${CPXHOME}

tar zxvf ${BACKUPDIR}/cpx-user-home.tar.gz
