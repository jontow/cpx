#!/bin/sh

if [ -f ./config.sh ]; then
	. ./config.sh
fi

if [ "$PKGDB" = "" ] || [ "$PKGDIR" = "" ]; then
	echo "Need both PKGDB and PKGDIR set!"
	exit
fi

for PKG in `ls ${PKGDIR}`; do {
	echo "*** Restoring $PKG"
	pkg_add ${PKG}
}; done

exit
