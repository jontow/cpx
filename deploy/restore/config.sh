#!/bin/sh

# SQL Nonsense--if you don't set password here, it'll prompt for it
SQLDB="cpx"
SQLUSER="root"
#SQLPASS="password"

# User details
CPXUSER="cpx"
CPXGROUP="cpx"
CPXHOME="/usr/home/cpx"
CPXSHELL="/usr/local/bin/zsh"

# Asterisk details
ASTERISKREVISION="80660"
ASTERISKSVNURL="http://svn.digium.com/svn/asterisk/branches/1.4"
ASTERISKDIRECTORY="asterisk-1.4-${ASTERISKREVISION}"

# Directory to place all backup items
BACKUPDIR="$HOME/cpx-backup"

if [ ! -d ${BACKUPDIR} ]; then
	echo "BACKUPDIR: ${BACKUPDIR} does not exist.  Creating."
	mkdir -p ${BACKUPDIR}
fi

# Location of FreeBSD package database: shouldn't need to change this.
PKGDB="/var/db/pkg"

if [ ! -d ${PKGDB} ]; then
	echo "PKGDB: ${PKGDB} does not exist."
	exit
fi

# Where to place your locally built packages
PKGDIR="${BACKUPDIR}/pkg"

if [ ! -d ${PKGDIR} ]; then
	echo "PKGDIR: ${PKGDIR} does not exist."
	mkdir -p ${PKGDIR}
fi
