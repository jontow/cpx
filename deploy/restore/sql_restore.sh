#!/bin/sh

if [ -f ./config.sh ]; then
	. ./config.sh
fi

if [ "$SQLDB" = "" ] || [ "$SQLUSER" = "" ]; then
	echo "Need both SQLDB and SQLUSER set!"
	exit
fi

if [ "$SQLPASS" = "" ]; then
	echo ""
	echo -n "Enter SQL password for ${SQLUSER} (will not echo): "
	stty -echo
	read SQLPASS
	echo ""
fi

echo "Creating database ${SQLDB}"
mysql -u ${SQLUSER} --password="${SQLPASS}" -e "create database ${SQLDB}"

echo "Restoring from BACKUP-${SQLDB}.sql into ${SQLDB} database"
mysql -u ${SQLUSER} --password="${SQLPASS}" ${SQLDB} <${BACKUPDIR}/BACKUP-${SQLDB}.sql
