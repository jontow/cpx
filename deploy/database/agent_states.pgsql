DROP TABLE IF EXISTS agent_states;
CREATE TABLE agent_states (
	agent int not null,
	oldstate int not null,
	newstate int not null,
	starttime int not null default '0',
	endtime int not null default '0',
	profile int not null default '1',
	data varchar(256)
);

CREATE INDEX state_idx ON agent_states (agent, oldstate, starttime);
