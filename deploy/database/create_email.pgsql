DROP TABLE IF EXISTS email CASCADE;
CREATE TABLE email (
  id SERIAL NOT NULL,
  msgto varchar(100) NOT NULL default '',
  msgfrom varchar(100) NOT NULL default '',
  msgreply varchar(100) NOT NULL default '',
  date integer NOT NULL default '0',
  subject varchar(255) NOT NULL default '',
  body text NOT NULL,
  rawheaders text NOT NULL,
  tenant integer NOT NULL,
  brand integer,
  replied boolean default FALSE,
  replyto varchar(100) NOT NULL default '',
  replysubject varchar(255) NOT NULL default '',
  replybody text NOT NULL,
  PRIMARY KEY (id),
	FOREIGN KEY (tenant, brand) REFERENCES brandlist
);

DROP TABLE IF EXISTS email_attachments;
CREATE TABLE email_attachments (
	id SERIAL NOT NULL,
	emailid integer NOT NULL REFERENCES email(id),
	mimetype varchar(100) NOT NULL,
	filename varchar(255) NOT NULL,
	PRIMARY KEY (id)
);

