DROP TABLE IF EXISTS `brand_settings`;
CREATE TABLE `brand_settings` (
	`tenant` smallint(6) NOT NULL DEFAULT '0',
	`brand` smallint(6) NOT NULL DEFAULT '0',
	`keyname` varchar(20) NOT NULL,
	`value` varchar(100),
	UNIQUE INDEX brandidkey	(tenant, brand, keyname)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
