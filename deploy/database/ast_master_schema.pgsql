DROP TABLE IF EXISTS cdr;
CREATE TABLE cdr (
  starttime integer NOT NULL default 0,
  answertime integer NOT NULL default 0,
  endtime integer NOT NULL default 0,
  clid varchar(80) NOT NULL default '',
  src varchar(80) NOT NULL default '',
  dst varchar(80) NOT NULL default '',
  dcontext varchar(80) NOT NULL default '',
  channel varchar(80) NOT NULL default '',
  dstchannel varchar(80) NOT NULL default '',
  lastapp varchar(80) NOT NULL default '',
  lastdata varchar(80) NOT NULL default '',
  duration integer NOT NULL default '0',
  billsec integer NOT NULL default '0',
  disposition integer NOT NULL default '0',
	amaflags integer NOT NULL default '0',
  accountcode varchar(20) NOT NULL default '',
  uniqueid varchar(32) NOT NULL default '',
  userfield varchar(255) NOT NULL default ''
);

DROP TABLE IF EXISTS extensions;
CREATE TABLE extensions (
  id SERIAL NOT NULL,
  context varchar(25) NOT NULL default '',
  exten varchar(25) NOT NULL default '',
  priority smallint NOT NULL default '0',
  app varchar(25) NOT NULL default '',
  appdata varchar(80) NOT NULL default '',
  PRIMARY KEY (id)
);

DROP TABLE IF EXISTS queue_members;
CREATE TABLE queue_members (
  queue_name varchar(128) NOT NULL default '',
  interface varchar(128) NOT NULL default '',
  penalty integer default 0,
  PRIMARY KEY (queue_name, interface)
);

DROP TABLE IF EXISTS queue_announce;
CREATE TABLE queue_announce (
  queue_name varchar(128) NOT NULL default '',
  frequency integer default 0,
  announcement varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY  (queue_name, frequency)
);

DROP TABLE IF EXISTS queues CASCADE;
CREATE TABLE queues (
 name varchar(128) NOT NULL,
 commonname varchar(128) NOT NULL,
 queuegroup integer NOT NULL DEFAULT 1,
 musiconhold varchar(128) NOT NULL default 'default',
 announce varchar(128) default NULL,
 context varchar(128) default NULL,
 timeout integer NOT NULL default 15,
 monitor_format varchar(128) default NULL,
 queue_youarenext varchar(128) default NULL,
 queue_thereare varchar(128) default NULL,
 queue_callswaiting varchar(128) default NULL,
 queue_holdtime varchar(128) default NULL,
 queue_minutes varchar(128) default NULL,
 queue_seconds varchar(128) default NULL,
 queue_lessthan varchar(128) default NULL,
 queue_thankyou varchar(128) default NULL,
 queue_reporthold varchar(128) default NULL,
 announce_frequency integer default NULL,
 announce_round_seconds integer default NULL,
 announce_holdtime varchar(4) CHECK (announce_holdtime IN ('yes', 'no', 'once')) default 'no',
 retry integer NOT NULL default 5,
 wrapuptime integer NOT NULL default 0,
 maxlen integer NOT NULL default 0,
 strategy varchar(20) CHECK (strategy IN ('ringall', 'roundrobin', 'leastrecent', 'fewestcalls', 'random', 'rrmemory')) default 'random',
 joinempty varchar(6) CHECK (joinempty IN ('yes', 'no', 'strict')) default 'yes',
 leavewhenempty varchar(6) CHECK (leavewhenempty IN ('yes', 'no', 'strict')) default 'no',
 eventmemberstatus boolean NOT NULL default FALSE,
 eventwhencalled boolean NOT NULL default TRUE,
 reportholdtime boolean NOT NULL default FALSE,
 memberdelay integer NOT NULL default 0,
 weight integer NOT NULL default '0',
 timeoutrestart boolean NOT NULL default FALSE,
 skill smallint default NULL,
 PRIMARY KEY (name)
);

DROP TABLE IF EXISTS sip;
CREATE TABLE sip (
  uniqueid SERIAL NOT NULL,
  name varchar(50) NOT NULL default '',
  username varchar(50) NOT NULL default '',
  accountcode varchar(50) NOT NULL default '',
  fromuser varchar(50) NOT NULL default '',
  login varchar(50) NOT NULL default '',
  type varchar(7) CHECK (type IN ('friend','peer','user')) default 'friend',
  secret varchar(20) NOT NULL default '',
  ipaddr varchar(20) NOT NULL default '',
  port varchar(10) NOT NULL default '5060',
  regseconds varchar(20) NOT NULL default '',
  nat varchar(3) CHECK (nat IN ('yes','no')) default 'no',
  host varchar(30) NOT NULL default 'dynamic',
  canreinvite varchar(3) CHECK (canreinvite IN ('yes','no')) default 'no',
  qualify varchar(10) NOT NULL default 'no',
  callerid varchar(80) NOT NULL default '',
  disallow varchar(25) NOT NULL default 'all',
  allow varchar(80) NOT NULL default 'ulaw;alaw;gsm',
  context varchar(50) NOT NULL default 'sip-agents',
  mailbox varchar(50) NOT NULL default '',
	deny varchar(50) NOT NULL default '0.0.0.0/0.0.0.0',
  permit varchar(50) NOT NULL default '0.0.0.0/0.0.0.0',
  dtmfmode varchar(8) CHECK (dtmfmode IN ('rfc2833','info','inband','auto')) default 'rfc2833',
  fullcontact varchar(100) NOT NULL default '',
  PRIMARY KEY  (uniqueid)
);

DROP TABLE IF EXISTS agents;
CREATE TABLE agents (
  username varchar(64) NOT NULL default '',
  password varchar(64) NOT NULL default '',
  fullname varchar(64) NOT NULL default '',
  PRIMARY KEY  (username)
);

DROP TABLE IF EXISTS vmrecord;
CREATE TABLE vmrecord (
	uniqueid varchar(32) NOT NULL default '',
	cidname varchar(64) NOT NULL default '',
	cidnum varchar(32) NOT NULL default '',
	brandid varchar(32) NOT NULL default '',
	filename varchar(255)
);

DROP TABLE IF EXISTS skills CASCADE;
CREATE TABLE skills (
	id SERIAL NOT NULL,
	name varchar(80) NOT NULL,
	PRIMARY KEY (id)
);

DROP TABLE IF EXISTS skill_lookup;
CREATE TABLE skill_lookup (
	agent integer NOT NULL,
	skill smallint NOT NULL REFERENCES skills(id)
);

CREATE INDEX cdr_idx on cdr (UniqueID); 
