#!/bin/sh
#
# 2007-01-08 -- jontow@mototowne.com
#
# Populate databases with table schemas
#
#####################################################################

ROOTUSER="root"
DATABASE="cpx"
SQLCMD="/usr/local/bin/mysql"

#####################################################################

echo ""
echo ""
echo ""
echo -n "Are you SURE your database contents are backed up? [y/n]  "
read YESNO

if [ "${YESNO}" != "y" ]; then
	echo ""
	echo ""
	echo "Ok, giving you a second chance."
	echo ""
	echo ""
	exit
fi

echo ""
echo ""
echo -n "Which RDBMS are you using? [mysql/postgres]  "
read RDBMS

if ( [ "${RDBMS}" = "mysql" ] || [ "${RDBMS}" = "postgres" ] ); then
	echo ""
	echo ""
	echo "Ok, using ${RDBMS}."
else
	echo ""
	echo ""
	echo "Invalid RDBMS ${RDBMS} selected, bailing."
	echo ""
	echo ""
	exit
fi

if [ "${RDBMS}" = "mysql" ]; then
	if [ `which mysql` ]; then
		SQLCMD=`which mysql`
	else
		echo ""
		echo ""
		echo "Cannot find 'mysql' binary in PATH, bailing."
		echo ""
		echo ""
		exit
	fi
elif [ "${RDBMS}" = "postgres" ]; then
	if [ -x `which psql` ]; then
		SQLCMD=`which psql`
	else
		echo ""
		echo ""
		echo "Cannot find 'pgsql' binary in PATH, bailing."
		echo ""
		echo ""
		exit
	fi
else
	echo "${RDBMS} unsupported: you're on your own."
	exit
fi

echo ""
echo ""
echo "Using ${SQLCMD}"

echo ""
echo ""
echo -n "Please enter the username of the database's admin account (default: root) "
read TMPROOTUSER

if [ "${TMPROOTUSER}" != "" ]; then
	ROOTUSER=$TMPROOTUSER
fi

echo ""
echo ""
echo "Ok, using the ${ROOTUSER} account."
	

echo ""
echo ""
stty -echo
echo -n "Enter your database ${ROOTUSER} password for bootstrap process: "
read ROOTPASS
stty echo
echo ""
echo ""
echo "Ok, blowing away your databases and reloading with clean schemas."
echo ""
echo ""

if [ "${RDBMS}" = "mysql" ]; then
	SQLARGS="-u ${ROOTUSER} --password=${ROOTPASS}"
	SUFFIX='sql'
elif [ "$RDBMS" = "postgres" ]; then
	SQLARGS="-U ${ROOTUSER}"
	SUFFIX='pgsql'
	PGPASSFILE="${HOME}/.pgpass-tmp"
	export PGPASSFILE
	`touch ${PGPASSFILE}`
	`chmod 600 ${PGPASSFILE}`
	echo "localhost:*:*:${ROOTUSER}:${ROOTPASS}" > ${PGPASSFILE}
	echo "If the script fails, please ensure you remove the ${PGPASSFILE} file, it contains sensitive credentials!"
	echo ""
	echo -n "press any key to continue"
	read FOO
fi

${SQLCMD} ${SQLARGS} ${DATABASE} < ./brandlist.${SUFFIX}
${SQLCMD} ${SQLARGS} ${DATABASE} < ./brand_settings.${SUFFIX}
${SQLCMD} ${SQLARGS} ${DATABASE} < ./sql_auth/create_tblAgent.${SUFFIX}
${SQLCMD} ${SQLARGS} ${DATABASE} < ./agent_states.${SUFFIX}
${SQLCMD} ${SQLARGS} ${DATABASE} < ./ast_master_schema.${SUFFIX}
${SQLCMD} ${SQLARGS} ${DATABASE} < ./create_email.${SUFFIX}
${SQLCMD} ${SQLARGS} ${DATABASE} < ./billing_records.${SUFFIX}
${SQLCMD} ${SQLARGS} ${DATABASE} < ./profiles.${SUFFIX}
${SQLCMD} ${SQLARGS} ${DATABASE} < ./profile_brand.${SUFFIX}
${SQLCMD} ${SQLARGS} ${DATABASE} < ./release_options.${SUFFIX}

echo ""
echo ""
echo "Done."
echo -n "Would you like to add GRANT credentials? [d/y/n]  "
read YESNO

case ${YESNO} in
	'y')
		echo ""
		echo "Be careful -- type exactly what you want or ^C now, you only get"
		echo "one chance here because I'm lazy and didn't want to add error checking. "
		echo ""
		echo -n "  Username: "
		read DBUSER
		stty -echo
		echo -n "  Password: "
		read DBPASS
		echo ""
		stty echo
		echo -n "  Hostname your AUI Server will connect from: "
		read DBHOST
		;;
	'd')
		DBUSER='cpx'
		DBPASS='ROADS'
		DBHOST='localhost'
		;;
	*)
		echo ""
		echo ""
		echo "Ok, good enough.  Do it yourself, then."
		rm ${PGPASSFILE}
		echo ""
		echo ""
		exit
		;;
esac
		
echo ""
echo ""

if [ "$RDBMS" = "mysql" ]; then
	${SQLCMD} ${SQLARGS} -e "GRANT ALL ON ${DATABASE}.* TO '${DBUSER}'@'${DBHOST}' IDENTIFIED BY '${DBPASS}'"
elif [ "$RDBMS" = "postgres" ]; then
	${SQLCMD} ${SQLARGS} ${DATABASE} -c "CREATE USER ${DBUSER} PASSWORD '${DBPASS}'"
fi

echo ""
echo ""
echo "Done.  Good luck."

if [ "$RDBMS" = "postgres" ]; then
	rm ${PGPASSFILE}
fi

echo ""
echo ""

exit
