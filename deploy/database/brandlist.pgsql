DROP TABLE IF EXISTS brandlist CASCADE;
CREATE TABLE brandlist (
  tenant smallint NOT NULL default 0,
  brand smallint NOT NULL default 0,
  email varchar(100) NOT NULL default '',
  dnis varchar(12),
  brandname varchar(100),
  callerid varchar(34),
	PRIMARY KEY (tenant, brand)
);
