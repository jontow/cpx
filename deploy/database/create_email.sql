DROP TABLE IF EXISTS `email`;
CREATE TABLE `email` (
  `id` int(11) NOT NULL auto_increment,
  `msgto` varchar(100) NOT NULL default '',
  `msgfrom` varchar(100) NOT NULL default '',
  `msgreply` varchar(100) NOT NULL default '',
  `date` int(11) NOT NULL default '0',
  `subject` varchar(255) NOT NULL default '',
  `body` text NOT NULL,
  `rawheaders` text NOT NULL,
  `tenantid` int(11) NOT NULL default '0',
  `brandid` int(11) NOT NULL default '0',
  `replied` tinyint(1) NOT NULL default '0',
  `replyto` varchar(100) NOT NULL default '',
  `replysubject` varchar(255) NOT NULL default '',
  `replybody` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `email_attachments`;
CREATE TABLE `email_attachments` (
	`id` int(11) NOT NULL auto_increment,
	`emailid` int(11) NOT NULL,
	`mimetype` varchar(100) NOT NULL,
	`filename` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

