/*
 *
 * 2006-10-27 -- jontow@mototowne.com
 *
 * Generate test table data for PPX authentication, not KPX compatible.
 *
 * To generate ADMINPASSWORD or USERPASSWORD, use this:
 *
 * $ md5 -s "PASSWORD" | awk -F' = ' '{print $2}' | tr "[:lower:]" "[:upper:]"
 *
 */

#include <stdio.h>

#define STARTNUM	100
#define ENDNUM		900
#define ENDADMIN	10
#define ADMINPASSWORD	"21232F297A57A5A743894A0E4A801FC3"	/* "admin" */
#define USERPASSWORD	"EE11CBB19052E40B07AAC0CA060C23EE"	/* "user" */

int main(void)
{
	int counter;
	int admins = 0;

	for (counter = STARTNUM; counter < ENDNUM; counter++) {
		admins++;

		printf("insert into tblAgent (AgentID,FirstName,LastName,Login,Password,TierID,TypeID,SecurityLevelID,ViewID) ");

		if (admins <= ENDADMIN)
			printf("values ('%d', 'Test', 'User', 'admin%d', '%s', '4', '1', '1', '1');\n", counter, counter, ADMINPASSWORD);
		else
			printf("values ('%d', 'Test', 'User', 'user%d', '%s', '1', '1', '1', '1');\n", counter, counter, USERPASSWORD);
	}

	return 0;
}
