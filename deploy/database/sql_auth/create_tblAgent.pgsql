DROP TABLE IF EXISTS tblAgent;
CREATE TABLE tblAgent (
  AgentID integer NOT NULL,
  FirstName varchar(100) NOT NULL,
  LastName varchar(100) NOT NULL,
  Login varchar(50) NOT NULL,
  Password varchar(250) NOT NULL,
  TierID integer NOT NULL,
  TypeID integer NOT NULL,
  SecurityLevelID integer NOT NULL,
  Active boolean NOT NULL default FALSE,
  DefaultProfile smallint not null default 1,
  LocationID smallint not null default 1,
  PRIMARY KEY  (AgentID)
);
