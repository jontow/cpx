DROP TABLE IF EXISTS `profile_brand`;
CREATE TABLE `profile_brand` (
	`tenant` smallint(6) NOT NULL,
	`brand` smallint(6) NOT NULL,
	`profile` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
