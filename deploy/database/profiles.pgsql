DROP TABLE IF EXISTS profiles CASCADE;
CREATE TABLE profiles (
	profile SERIAL,
	name varchar(80) not null,
	sortorder smallint,
	hidden boolean default FALSE,
	PRIMARY KEY (profile)
);

DROP TABLE IF EXISTS profile_lookup;
CREATE TABLE profile_lookup (
	profile int REFERENCES profiles ON DELETE CASCADE,
	queue varchar(80) not null REFERENCES queues(name),
	penalty smallint not null default 0
);

DROP TABLE IF EXISTS queuegroups;
CREATE TABLE queuegroups (
	groupid SERIAL,
	name varchar(80) not null,
	sortorder smallint,
	PRIMARY KEY (groupid)
);
