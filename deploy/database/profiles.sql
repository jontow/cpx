DROP TABLE IF EXISTS `profile_lookup`;
CREATE TABLE profile_lookup (
	profile tinyint unsigned not null,
	queue varchar(80) not null,
	penalty tinyint not null default 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE profiles (
	profile tinyint unsigned not null auto_increment,
	name varchar(80) not null,
	sortorder tinyint unsigned,
	hidden tinyint unsigned not null default 0,
	PRIMARY KEY (`profile`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `queuegroups`;
CREATE TABLE queuegroups (
	groupid tinyint unsigned not null auto_increment,
	name varchar(80) not null,
	sortorder tinyint unsigned,
	PRIMARY KEY (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
