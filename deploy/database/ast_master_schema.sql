DROP TABLE IF EXISTS `cdr`;
CREATE TABLE `cdr` (
  `start` int(11) NOT NULL default '0',
  `answer` int(11) NOT NULL default '0',
  `end` int(11) NOT NULL default '0',
  `clid` varchar(80) NOT NULL default '',
  `src` varchar(80) NOT NULL default '',
  `dst` varchar(80) NOT NULL default '',
  `dcontext` varchar(80) NOT NULL default '',
  `channel` varchar(80) NOT NULL default '',
  `dstchannel` varchar(80) NOT NULL default '',
  `lastapp` varchar(80) NOT NULL default '',
  `lastdata` varchar(80) NOT NULL default '',
  `duration` bigint(20) NOT NULL default '0',
  `billsec` bigint(20) NOT NULL default '0',
  `disposition` int(11) NOT NULL default '0',
  `amaflags` int(11) NOT NULL default '0',
  `accountcode` varchar(20) NOT NULL default '',
  `uniqueid` varchar(32) NOT NULL default '',
  `userfield` varchar(255) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `extensions`;
CREATE TABLE `extensions` (
  `id` int(11) NOT NULL auto_increment,
  `context` varchar(25) NOT NULL default '',
  `exten` varchar(25) NOT NULL default '',
  `priority` tinyint(4) NOT NULL default '0',
  `app` varchar(25) NOT NULL default '',
  `appdata` varchar(80) NOT NULL default '',
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `queue_members`;
CREATE TABLE `queue_members` (
  `queue_name` varchar(128) NOT NULL default '',
  `interface` varchar(128) NOT NULL default '',
  `penalty` int(11) default '0',
  PRIMARY KEY  (`queue_name`,`interface`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `queue_announce`;
CREATE TABLE `queue_announce` (
  `queue_name` varchar(128) NOT NULL default '',
  `frequency` int(11) unsigned default 0,
  `announcement` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY  (`queue_name`,`frequency`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `queues`;
CREATE TABLE `queues` (
 `name` varchar(128) NOT NULL,
 `commonname` varchar(128) NOT NULL,
 `queuegroup` int(11) NOT NULL DEFAULT 1,
 `musiconhold` varchar(128) NOT NULL default 'default',
 `announce` varchar(128) default NULL,
 `context` varchar(128) default NULL,
 `timeout` int(11) NOT NULL default '15',
 `monitor_format` varchar(128) default NULL,
 `queue_youarenext` varchar(128) default NULL,
 `queue_thereare` varchar(128) default NULL,
 `queue_callswaiting` varchar(128) default NULL,
 `queue_holdtime` varchar(128) default NULL,
 `queue_minutes` varchar(128) default NULL,
 `queue_seconds` varchar(128) default NULL,
 `queue_lessthan` varchar(128) default NULL,
 `queue_thankyou` varchar(128) default NULL,
 `queue_reporthold` varchar(128) default NULL,
 `announce_frequency` int(11) default NULL,
 `announce_round_seconds` int(11) default NULL,
 `announce_holdtime` enum('yes', 'no', 'once') default 'no',
 `retry` int(11) NOT NULL default '5',
 `wrapuptime` int(11) NOT NULL default '0',
 `maxlen` int(11) NOT NULL default '0',
 `strategy` enum('ringall', 'roundrobin', 'leastrecent', 'fewestcalls', 'random', 'rrmemory') NOT NULL default 'random',
 `joinempty` enum('yes', 'no', 'strict') NOT NULL default 'yes',
 `leavewhenempty` enum('yes', 'no', 'strict') NOT NULL default 'no',
 `eventmemberstatus` bool NOT NULL default '0',
 `eventwhencalled` bool NOT NULL default '1',
 `reportholdtime` bool NOT NULL default '0',
 `memberdelay` int(11) NOT NULL default '0',
 `weight` int(11) NOT NULL default '0',
 `timeoutrestart` bool NOT NULL default '0',
 `skill` tinyint unsigned default NULL,
 PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `sip`;
CREATE TABLE `sip` (
  `uniqueid` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `username` varchar(50) NOT NULL default '',
  `accountcode` varchar(50) NOT NULL default '',
  `fromuser` varchar(50) NOT NULL default '',
  `login` varchar(50) NOT NULL default '',
  `type` enum('friend','peer','user') NOT NULL default 'friend',
  `secret` varchar(20) NOT NULL default '',
  `ipaddr` varchar(20) NOT NULL default '',
  `port` varchar(10) NOT NULL default '5060',
  `regseconds` varchar(20) NOT NULL default '',
  `nat` enum('yes','no') NOT NULL default 'no',
  `host` varchar(30) NOT NULL default 'dynamic',
  `canreinvite` enum('yes','no') NOT NULL default 'no',
  `qualify` varchar(10) NOT NULL default 'no',
  `callerid` varchar(80) NOT NULL default '',
  `disallow` varchar(25) NOT NULL default 'all',
  `allow` varchar(80) NOT NULL default 'ulaw;alaw;gsm',
  `context` varchar(50) NOT NULL default 'sip-agents',
  `mailbox` varchar(50) NOT NULL default '',
  `deny` varchar(50) NOT NULL default '0.0.0.0/0.0.0.0',
  `permit` varchar(50) NOT NULL default '0.0.0.0/0.0.0.0',
  `dtmfmode` enum('rfc2833','info','inband','auto') NOT NULL default 'rfc2833',
  `fullcontact` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`uniqueid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `agents`;
CREATE TABLE `agents` (
  `user` varchar(64) NOT NULL default '',
  `pass` varchar(64) NOT NULL default '',
  `name` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `vmrecord`;
CREATE TABLE `vmrecord` (
	`uniqueid` varchar(32) NOT NULL default '',
	`cidname` varchar(64) NOT NULL default '',
	`cidnum` varchar(32) NOT NULL default '',
	`brandid` varchar(32) NOT NULL default '',
	`filename` varchar(255)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `skills`;
CREATE TABLE `skills` (
	`id` tinyint unsigned NOT NULL auto_increment,
	`name` varchar(80) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `skill_lookup`;
CREATE TABLE `skill_lookup` (
	`agent` int unsigned NOT NULL,
	`skill` tinyint unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE INDEX cdr_idx on cdr (UniqueID); 
