DROP TABLE IF EXISTS billing_transactions;
CREATE TABLE billing_transactions (
  uniqueid varchar(20) NOT NULL,
  transaction smallint NOT NULL,
  starttime integer NOT NULL,
  endtime integer NOT NULL,
  data varchar(100) default NULL
);

DROP TABLE IF EXISTS billing_summaries;
CREATE TABLE billing_summaries (
  uniqueid varchar(20) NOT NULL,
  dnis varchar(32) NOT NULL,
  tenant smallint NOT NULL,
  brand smallint NOT NULL,
  starttime integer NOT NULL,
  endtime integer NOT NULL,
  inqueue integer NOT NULL default 0,
  incall integer NOT NULL default 0,
  inwrapup integer NOT NULL default 0,
  agent smallint,
  laststate smallint NOT NULL,
  lastqueue varchar(32) REFERENCES queues(name),
  type varchar(10) NOT NULL,
	PRIMARY KEY (uniqueid),
	FOREIGN KEY (tenant, brand) REFERENCES brandlist
);

DROP TABLE IF EXISTS call_info;
CREATE TABLE call_info (
	uniqueid varchar(20) NOT NULL,
	tenant smallint NOT NULL,
	brand smallint NOT NULL,
	type varchar(10) NOT NULL,
	dnis varchar(50),
	calleridnum varchar(20),
	calleridname varchar(50),
	emailid integer REFERENCES email(id),
	voicemailid varchar(20), 
	dialednumber varchar(20),
	caseid integer,
	PRIMARY KEY (uniqueid),
	FOREIGN KEY (tenant, brand) REFERENCES brandlist
);

CREATE INDEX ts_idx ON billing_transactions (transaction, starttime);
CREATE INDEX uts_idx ON billing_transactions (uniqueID, transaction, starttime);
CREATE INDEX bsqtimes ON billing_transactions (transaction, starttime, data);
CREATE INDEX summary_calls_since ON billing_summaries (starttime,lastqueue,laststate);
