DROP TABLE IF EXISTS `release_options`;
CREATE TABLE `release_options` (
	`id` smallint(6) auto_increment,
	`label` varchar(30) NOT NULL,
	`utilbias` tinyint DEFAULT -1,
	`enabled` tinyint(2) DEFAULT 1,
	`sortorder` smallint(6) DEFAULT 0,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `release_options` (label) VALUES ('Unknown');
