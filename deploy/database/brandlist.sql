DROP TABLE IF EXISTS `brandlist`;
CREATE TABLE `brandlist` (
  `tenant` smallint(6) NOT NULL default '0',
  `brand` smallint(6) NOT NULL default '0',
  `email` varchar(100) NOT NULL default '',
  `dnis` varchar(12) NOT NULL default '0',
  `brandname` varchar(100) NOT NULL default '',
  `callerid` varchar(34),
  UNIQUE INDEX tenantbrand (tenant, brand)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
