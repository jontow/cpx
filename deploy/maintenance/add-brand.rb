#! /usr/bin/env ruby

=begin license
 * Copyright (c) 2014 Jonathan Towne <jontow@mototowne.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


require 'mysql'
require '../generator/genconstants'
include GenConstants

cpx_dbh = Mysql.real_connect(CPXHOST, CPXUSER, CPXPASS, CPXDB)

if ARGV[3].nil?
	puts "Syntax: add-brand <tenantid> <brandid> <dnis> \"<description>\" [[email] [\"<callerid>\"]]"
	puts "    ie. add-brand 1 5 1212 \"Network Department\" helpdesk@example.com \"Network Department <5505>\""
	exit
end

@email = DEFAULT_EMAIL
if !ARGV[4].nil?
	@email = ARGV[4]
end

@callerid = DEFAULT_CALLERID
if !ARGV[5].nil?
	@callerid = ARGV[5]
end

@tenantid = ARGV[0]
@brandid = ARGV[1]
@dnis = ARGV[2]
@desc = ARGV[3]

# This defaults to profile '1', you can tweak it if you want.
@defprofile = '1'

result = cpx_dbh.query("SELECT * FROM brandlist WHERE tenant = '#{@tenantid}' AND brand = '#{@brandid}'")
if result.num_rows != 0
	puts "Brand already exists."
	exit
end

# Add to brandlist
cpx_dbh.query("INSERT INTO brandlist
		(tenant, brand, email, dnis, brandname, callerid)
		VALUES
		('#{@tenantid}', '#{@brandid}', '#{@email}', '#{@dnis}', '#{@desc}', '#{@callerid}')")

# Generate default queue for new brand
@packedbrandid = "#{@tenantid.rjust(4, '0')}#{@brandid.rjust(4, '0')}"
@queuename = "L1-#{@packedbrandid}-q"
cpx_dbh.query("INSERT INTO queues SET name='#{@queuename}', context='qbreakout', commonname='#{@desc}'")
cpx_dbh.query("INSERT INTO profile_lookup SET queue='#{@queuename}',penalty=0,profile='#{@defprofile}'")

puts "Made it to end, added brand: #{@desc} (id:#{@tenantid}/#{@brandid}) with queue #{@queuename}"
puts "Now fix profiles: INSERT INTO profile_lookup (profileid,queue,penalty) VALUES ..."
puts "And add it to the correct queue group: UPDATE queues SET queuegroup = 'X' WHERE name = '#{@queuename}'"
