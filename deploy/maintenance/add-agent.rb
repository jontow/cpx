#! /usr/bin/env ruby

=begin license
 * Copyright (c) 2014 Jonathan Towne <jontow@mototowne.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


require 'mysql'
require '../generator/genconstants'
include GenConstants

cpx_dbh = Mysql.real_connect(CPXHOST, CPXUSER, CPXPASS, CPXDB)

if ARGV[4].nil? or ( ARGV[2] != "admin" and ARGV[2] != "user" )
	puts "Syntax: add-agent <username> <password> <admin|user> <firstname> <lastname>"
	exit
end

@username = ARGV[0]
@tmppassword = ARGV[1]
@password = nil
@permissions = ARGV[2]
@firstname = ARGV[3]
@lastname = ARGV[4]

# This defaults to profile '1', you can tweak it if you want.
@defprofile = '1'

@nextagentid = nil
@seclevel = nil
@tierid = nil

result = cpx_dbh.query("SELECT * FROM tblAgent WHERE Login = '#{@username}'")
if result.num_rows != 0
	puts "Agent already exists."
	exit
end

result = cpx_dbh.query("SELECT AgentID FROM tblAgent ORDER BY AgentID DESC LIMIT 1")
if result.num_rows == 0
	puts "No agents found: misconfiguration detected."
	exit
else
	# Generate next unused agent id
	result.each_hash do |row|
		@nextagentid = row['AgentID'].to_i + 1
	end
end

# Generate password hash
if !@tmppassword.nil?
	@password = `md5 -s "#{@tmppassword.chomp}"`.split(' = ')[1].upcase.chomp
end

if @permissions == "admin"
	@seclevel = 2
	@tierid = 4
else @permissions # user
	@seclevel = 1
	@tierid = 1
end

cpx_dbh.query("INSERT INTO tblAgent
		(AgentID,FirstName,LastName,Login,Password,TierID,TypeID,SecurityLevelID,Active,CreateDate,DefaultProfile)
		VALUES
		('#{@nextagentid}', '#{@firstname}', '#{@lastname}', '#{@username}', '#{@password}', '#{@tierid}', '1',
		 '#{@seclevel}', '1', '#{Time.now.strftime("%Y-%m-%d %H:%M")}', '#{@defprofile}')")

cpx_dbh.query("INSERT INTO agents (user,pass,name) VALUES ('#{@nextagentid + BASEID}', '#{@nextagentid + BASEID}', '#{@username}')")

puts "Made it to end, added agent: #{@username} (id:#{@nextagentid}/#{@nextagentid + BASEID})"
