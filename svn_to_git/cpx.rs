### Read in repo and guarantee flatness by ignoring branches/tags/etc,
###   works but inelegant, will require git post-processing
### See README and post-reposurgeon.sh for more info.
read <2014-07-27-svn-cpx --nobranch

# Map Subversion usernames to Git-style user IDs.
authors read <<EOF
jontow = Jonathan Towne <jontow@mototowne.com> America/New_York
andrew = Andrew Thompson <andrew@hijacked.us> America/New_York
joec = Joe Caswell <joec@fusedsolutions.com> America/New_York
lucas = Lucas Austin-Howe <lucas@fusedsolutions.com> America/New_York
micahw = Micah Warren <micahw@fusedsolutions.com> America/New_York
EOF

# Before we run any transformative commands, print repo stats
stats

# Check for and report glitches such as timestamp collisions,
# ill-formed committer/author IDs, multiple roots, etc.
!echo DEBUG: early lint run before transforms
lint

# Massage comments into Git-like form (with a topic sentence and a
# spacer line after it if there is following running text). Only
# done when the first line is syntactically recognizable as a whole
# sentence.
gitify

# de-tagify, somehow it happens automatically, but isn't super useful in this repo
# unless we want to inspect and individually name these 'milestone points' (almost
# all empty commits where directories get moved around)
tag delete /emptycommit-/

expunge --notagify /^client_data\//
expunge --notagify /^freeswitch\//

# Cleanup a specific binary-commit incident that sucks to have in repo history.. (see mark :7888)
expunge --notagify /^rbphone\/freeswitch\//
expunge --notagify /^rbphone\/FreeSwitch/
expunge --notagify /^rbphone\/[A-Za-z0-9-_]+\.(lib|exe|bat|dll)/
expunge --notagify /^rbphone\/conf\//
expunge --notagify /^rbphone\/db\//
expunge --notagify /^rbphone\/log\//
expunge --notagify /^rbphone\/mod\//
expunge --notagify /^rbphone\/sounds\//

expunge --notagify /trunk\/data\//
expunge --notagify /(branches|tags)\/[0-9A-Za-z.-]+\/data\//

expunge --notagify /trunk\/dialplans\//
expunge --notagify /(branches|tags)\/[0-9A-Za-z.-]+\/dialplans\//

expunge --notagify /^dialplans\//
expunge --notagify /^IVR\//

# Some problematic content in billing_utils, easiest way to handle
#   is to expunge the specific problems.  This script (call_data.php)
#   includes hardcoded passwords; may want to add it back in with redactions?
#   Too bad that loses history.  Luckily, the history for this file
#   is inconsequential.  It may also have been superceded by cdr2acd.rb.
expunge --notagify /^utils/trunk/call_data.php$/
expunge --notagify /^billing_utils/trunk/call_data.php$/
expunge --notagify /^billing_utils/(branches|tags)/[0-9A-Za-z.-]+/call_data.php$/

# This code isn't problematic, per se, but is MPL licensed and contains
# company references, which are undesirable at this time.
expunge --notagify /^cpxerl\//
expunge --notagify /^rbphone\//

###########################################
# Massage individual commit messages here..
###########################################

/^Add LocationID to tblagent/ msgin <<EOF
Committer: Andrew Thompson <andrew@hijacked.us>
Committer-Date: Tue, 06 Nov 2007 11:24:35 -0500
Legacy-ID: 1272
Check-Text: Add LocationID to tblAgent at Pete's request (to be us

Add LocationID to tblAgent at Pete's request (to be used to distinguish between tenants/brands)
EOF

########################################
/^Add custom\.ext file to generator to allow custom/ msgin <<EOF
Committer: Andrew Thompson <andrew@hijacked.us>
Committer-Date: Fri, 30 Nov 2007 12:34:43 -0500
Legacy-ID: 1406
Check-Text: Add custom.ext file to generator to allow custom chang

Add custom.ext file to generator to allow custom changes (eg for client number switches)
EOF

########################################
#
# [and other redacted single commits with leaks in the comments]
#
########################################

# Check for and report glitches such as timestamp collisions,
# ill-formed committer/author IDs, multiple roots, etc.
!echo DEBUG: late lint run after transforms
lint

# After we run any transformative commands, print repo stats
stats

# We want to write a Git repository
prefer git

# Do it
#rebuild GIT-cpx
