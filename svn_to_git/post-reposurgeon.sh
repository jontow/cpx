#!/bin/sh
#
# 2021-08-28 -- jontow@mototowne.com
#
# Post-processing shell script to be run AFTER reposurgeon does the hard/dirty work.
# Example of how to run:
#
# $ cd reposurgeon-working-dir
# $ reposurgeon
# reposurgeon% script cpx.rs
# reposurgeon% rebuild GIT-cpx
# reposurgeon% quit
# $ cd GIT-cpx && /bin/sh -x ../post-reposurgeon.sh 2>&1 | tee post.log
#
# Ideally the conversion is done at this point and ready or nearly ready to add a
# remote to and 'git push'.  "post.log" contains a very detailed record of what
# happens during this shell script.
#

if ! grep -q "Simulated Subversion default" .gitignore; then
    echo "ERROR: need to be in correct directory"
    exit 1
fi

HEAD_OF_REPO=$(pwd)

echo "Beginning post-processing of reposurgeon svn-to-git cpx repo conversion."

if ! git branch -l | grep -q '^..main'; then
    echo "Renaming branch: master --> main"
    git checkout master && git branch -m master main && git checkout main
fi

# removed earlier: client_data, rbphone, cpxerl
repolist="agentui amiproxy auiserver constants deploy mailcatcher cpxmon misc billing_utils cbx attic cpxapi cpx_reporting"

# these will need to be removed in basically all the subrepos
unbranched="add-copyright-header.rb export.rb header tag.rb"

# Loop over each repo we (want to) know about, manually configured in "repolist", above.
for subrepo in ${repolist}; do
    # Reset our position in the repo.  Doing it at start of every loop
    # in case something foolishly changes it.
    cd ${HEAD_OF_REPO}

    # Reset branch to main on every loop iteration
    git checkout main 2>/dev/null

    echo "*** REPO: Working on: ${subrepo}"

    if [ ! -e "${subrepo}" ]; then
        echo "*** SKIP: ${subrepo} does not exist as a directory"
        continue
    fi

    #----------------------------------------
    #
    # If the current subrepo is "attic", we want to pull it out
    # and treat it in a specific way.  It should turn into its own
    # branch, and not appear elsewhere.
    #
    if [ "${subrepo}" = "attic" ] && \
        ! git branch -l | grep -q '^  attic$'; then
        echo "*** SPECIAL: processing attic"
        rm_repos=$(echo ${repolist} | sed -e 's/\ attic//g')

        git checkout -b attic && \
        git rm -q -fr ${unbranched} && \
        git rm -q -fr ${rm_repos} && \
        git commit -m 'cleanup attic'

        git checkout main && \
        git rm -q -fr attic && \
        git commit -m 'cleanup attic'

        continue
    fi

    #----------------------------------------
    #
    # We just need to throw away the existing tags, in this repo they
    # really aren't very useful: we tagged some betas and such that
    # it shouldn't hurt to lose.  Can always pinpoint commits and
    # 'git tag' them later.
    #
    if [ -e ${subrepo}/tags ]; then
        echo "*** CLEANUP: tags"
        git rm -q -fr ${subrepo}/tags && \
        git commit -m "cleanup tags in ${subrepo}"
    fi

    #----------------------------------------
    #
    # This is where things get complicated.  We want to take the on-disk
    # svn style "branches" directory tree and turn them into valid git
    # branches, but keep the entire tree visible at one time.
    #
    # This means that, instead of "agentui/branches/1.2", "etc/branches/1.2"
    # existing in the main branch, we'll have an additional branch named
    # "1.2", but containing "agentui/", "etc/", ...
    #
    # Additionally, other branch directories and "trunk" should be removed
    # from each actual branch.  "trunk" should become the contents of the
    # git main branch.
    #
    echo "*** BRANCHES: separating into branches"
    if [ -d "${subrepo}/branches" ]; then
        branch_dirs=$(echo ${subrepo}/branches/*)

        # long_branch is something like "agentui/branches/1.2"
        for long_branch in ${branch_dirs}; do

            # Reset branch for every loop iteration!
            git checkout main

            # short_branch is something like "1.2"
            short_branch=$(echo ${long_branch} | awk -F/ '{print $NF}')
            if ! git branch -l | grep -q "^..${short_branch}$"; then
                echo "*** NEW-BRANCH: ${short_branch}"
                git branch ${short_branch}
            fi

            git checkout ${short_branch}
            echo "*** AUTO-BRANCHIFY: ${long_branch}"
            if [ -d ${subrepo}/trunk ]; then
                echo "*** FOUND 'trunk' INSIDE ${subrepo} on ${short_branch}, removing."
                git rm -q -fr ${subrepo}/trunk
            fi
            if [ -e ${subrepo}/tags ]; then
                echo "*** FOUND 'tags' INSIDE ${subrepo} on ${short_branch}, removing."
                git rm -q -fr ${subrepo}/tags
            fi

            # Construct list of repos that don't have this branch..
            echo "*** CONSTRUCT list of subrepos that are missing '${short_branch}' branch, remove them"
            repos_missing_branch=""
            for some_dir in $(echo *); do
                # Does the directory not contain our current short_branch?
                if [ -d ${some_dir} ] && [ ! -d ${some_dir}/branches/${short_branch} ]; then
                    if ! git log | grep -q "auto-branchify ${some_dir}/"; then
                        repos_missing_branch="${repos_missing_branch} ${some_dir}"
                    else
                        echo "*** IGNORE ${some_dir} because it has been auto branchified already"
                    fi
                fi
            done

            if [ ! -z "${repos_missing_branch}" ]; then
                echo "*** FOUND repos_missing_branch (${short_branch}) :: ${repos_missing_branch}"
                git rm -q -fr ${repos_missing_branch}
            fi

            # Dumb but necessary thing.. '.' style hidden files are not handled by the code below,
            # so we have to handle them explicitly.  We also guard against a null result by checking
            # to make sure our glob isn't handed back to us as a result, because sh is irritating:
            dotfiles="${subrepo}/branches/${short_branch}/.[A-Za-z0-9]*"
            if [ ! -z ${dotfiles} ] && ! echo ${dotfiles} | grep -q '*$'; then
                echo "*** FOUND dotfiles in ${subrepo}/branches/${short_branch}/ ....moving them"
                for dotfile in ${dotfiles}; do
                    # Note: ${dotfile} contains subrepo/branches/short_branch/filename, here:
                    git mv ${dotfile} ${subrepo}/
                done
            fi

            echo "*** MOVING ${subrepo}/branches/${short_branch}/* INTO ${subrepo}/ (and removing branches/)"
            echo "DEBUG: (pwd:$(pwd)) git mv ${subrepo}/branches/${short_branch}/* ${subrepo}/ && git rm -q -fr ${subrepo}/branches"
            git mv ${subrepo}/branches/${short_branch}/* ${subrepo}/ && \
                ( git rm -q -fr ${subrepo}/branches 2>/dev/null || rmdir ${subrepo}/branches )

            git commit -m "auto-branchify ${long_branch}"
        done
    fi

    #----------------------------------------
    git checkout main
    echo "*** TRUNK: converting ${subrepo} trunk to ${subrepo} main"
    if [ -d "${subrepo}/trunk" ]; then
        if [ -d ${subrepo}/branches ]; then
            echo "*** REMOVING 'branches' dir from ${subrepo} in main branch"
            git rm -q -fr ${subrepo}/branches
        fi

        # Dumb but necessary thing.. '.' style hidden files are not handled by the code below,
        # so we have to handle them explicitly.  We also guard against a null result by checking
        # to make sure our glob isn't handed back to us as a result, because sh is irritating:
        dotfiles=${subrepo}/trunk/.[A-Za-z0-9]*
        if [ ! -z ${dotfiles} ] && ! echo ${dotfiles} | grep -q '*$'; then
            echo "*** FOUND dotfiles in ${subrepo}/trunk/ ....moving them"
            for dotfile in ${dotfiles}; do
                # Note: ${dotfile} contains subrepo/trunk/filename, here:
                git mv ${dotfile} ${subrepo}/
            done
        fi

        # Note: the resulting 'trunk' empty dir won't be removed by git, but also won't be
        # appropriately tracked ... git doesn't like empty directories.  So just remove it
        # with system rmdir.  (Intentionally using rmdir so it errors on non-empty dirs).
        echo "*** MOVING ${subrepo}/trunk/* INTO ${subrepo}/ (and removing ${subrepo}/trunk/)"
        git mv ${subrepo}/trunk/* ${subrepo}/ && rmdir ${subrepo}/trunk

        git commit -m "converting ${subrepo} trunk to main branch format"
    else
        echo "*** NO-TRUNK: ${subrepo}"
    fi
done
