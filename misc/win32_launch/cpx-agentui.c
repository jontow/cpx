#include <windows.h>
#include <stdio.h>
#include <tchar.h>

int _tmain( VOID )
{
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    LPTSTR szCmdline=_tcsdup(TEXT("ruby cpx-agentui/main.rb"));
    LPTSTR envpath, newenvpath, path;
    DWORD retval;
    DWORD nowindow_flag = CREATE_NO_WINDOW;
 
    /* My string handling is terible FIX!!!! */
    
    envpath = (LPTSTR) malloc(2048*sizeof(TCHAR));
    newenvpath = (LPTSTR) malloc(4096*sizeof(TCHAR));
    path = (LPTSTR) malloc(1024*sizeof(TCHAR));

    GetCurrentDirectory(1024, path);

    printf("%s\n", path);

    retval = GetEnvironmentVariable(TEXT("PATH"), envpath, 2048);

    snprintf(newenvpath, 4096, "%s;%s\\bin;%s", path, path, envpath);

    SetEnvironmentVariable(TEXT("PATH"), newenvpath);

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    // Start the child process. 
    if(CreateProcess( NULL,   // No module name (use command line)
        szCmdline,      // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        TRUE,          // Set handle inheritance to FALSE
        nowindow_flag,  // Disable console window
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    )
    {
        //cleanup handles
        CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
        return 0;
    }
    else
    {
        printf( "CreateProcess failed (%d).\n", GetLastError() );
        return;
    }

    return 0;
}
