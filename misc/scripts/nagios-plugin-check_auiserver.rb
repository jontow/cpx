#!/usr/bin/env ruby
#
# 2007-12-01 -- jontow@mototowne.com
#
# Yanked from agentui main.rb, almost verbatim.. works great!
#

require 'socket'

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

if ARGV[2].nil?
	puts "Usage: nagios_auiserver_check <host> <port> <protocolversion>"
	exit UNKNOWN
end

begin
	@connection = TCPSocket.new(ARGV[0], ARGV[1].to_i)
rescue Errno::ECONNREFUSED, Errno::EBADF, Errno::EINVAL
	puts "No socket listening"
	exit CRITICAL
rescue SocketError => e
	puts "Other socket error: #{e.message}"
	exit CRITICAL
end

if result = select([@connection], nil, nil, 5)
	line = @connection.gets

	if line =~ /^Agent Server/
		@connection.puts "Protocol: #{ARGV[2]}"
		resp = @connection.gets
		if resp =~ /0\ OK/
			puts "CPX OK"
			exit OK
		else
			puts "Unknown response: #{resp}"
			exit CRITICAL
		end
	else
		puts "Unknown response: #{line}"
		exit CRITICAL
	end
else
	@connection.close
	puts "Connection timed out"
	exit CRITICAL
end

# ENOTOUCHY
exit OK
