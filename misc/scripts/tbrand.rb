#!/usr/bin/env ruby

require 'rubygems'
require 'mysql'

@dbh = nil

def usage
	puts "Usage: tbrand.rb <options>"
	puts "\t<-t tenant# [brand#]>"
	puts "\t<-d dnis>"
	puts "\t<-c commonname>"
end

def main
	if ARGV[0].nil?
		usage
		exit
	else
		case ARGV[0]
		when "-t"
			if ARGV[1].nil?
				usage
				exit
			else
				@tenantnum = ARGV[1]
				@querystr = "WHERE tenant = #{@dbh.quote(@tenantnum)}"
			end

			if ARGV[2].nil?
				# ignore, this is valid
			else
				@brandnum = ARGV[2]
				@querystr += " AND brand = #{@dbh.quote(@brandnum)}"
			end

		when "-d"
			if ARGV[1].nil?
				usage
				exit
			else
				@dnis = ARGV[1]
				@querystr = "WHERE dnis LIKE '%#{@dbh.quote(@dnis)}%'"
			end

		when "-c"
			if ARGV[1].nil?
				usage
				exit
			else
				@commonname = ARGV[1]
				@querystr = "WHERE brandname LIKE '%#{@dbh.quote(@commonname)}%'"
			end
		else
			usage
			exit
		end

	end

	result = @dbh.query("SELECT * FROM brandlist #{@querystr} ORDER BY tenant, brand")

	if result and result.num_rows >= 1
		result.each_hash do |row|
			puts "#{row['tenant'].rjust(4, '0')}#{row['brand'].rjust(4, '0')}:\t#{row['dnis']}\t\t(#{row['brandname']})"
		end
	else
		puts "Query failed.. no matches or you passed gibberish."
	end
end

def dbconnect
	if ENV['CPXHOST'].nil? or ENV['CPXUSER'].nil? or ENV['CPXPASS'].nil? or ENV['CPXDB'].nil?
		puts "Must have CPXHOST, CPXUSER, CPXPASS, and CPXDB set in environment."
		exit
	end

	@dbh = Mysql.real_connect(ENV['CPXHOST'], ENV['CPXUSER'], ENV['CPXPASS'], ENV['CPXDB'])
end

puts ""

dbconnect
main

puts ""

exit
