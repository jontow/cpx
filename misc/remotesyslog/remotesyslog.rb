# Simple remote syslog library

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

# Copyright 2006 Andrew Thompson
#
# This library is intended as a *very* simple and clean wrapper for sending
# syslog events over UDP to a remote syslog server. There is minimal input
# checking/sanitizing and as the transport is UDP we can't really check
# if something was received or not.
#
# Tested to work on FreeBSD, NetBSD, DragonflyBSD, Windows 2000
#
# The remote syslog used for testing was FreeBSD's and as this library
# cannot send out its packets on port 514 FreeBSD's syslog will ignore
# them by default. To make sure the packets are accepted, configure your
# syslog to have something like -a 192.168.1.0/24:* . This ensures that
# all packets, regardless of the source port, will be accepted.
#
# Other syslog implementations are untested.

require 'socket'

class RemoteSyslog
	#facilities - based on FreeBSD's syslog.h
	LOG_KERNEL = 0
	LOG_USER = 1
	LOG_MAIL = 2
	LOG_DAEMON = 3
	LOG_AUTH = 4
	LOG_SYSLOG = 5
	LOG_LPR = 6
	LOG_NEWS = 7
	LOG_UUCP = 8
	LOG_CRON = 9 # RFC says this is for clock
	LOG_AUTHPRIV = 10
	LOG_FTP = 11
	LOG_NTP = 12
	LOG_SECURITY = 13
	LOG_CONSOLE = 14
	# 15 omitted for murky reasons
	LOG_LOCAL0 = 16
	LOG_LOCAL1 = 17
	LOG_LOCAL2 = 18
	LOG_LOCAL3 = 19
	LOG_LOCAL4 = 20
	LOG_LOCAL5 = 21
	LOG_LOCAL6 = 22
	LOG_LOCAL7 = 23

	LOG_NFACILITIES = 24 # number of facilities

	# human readable names for the facilities
	SYSLOG_NAMES = {
		'kernel' => LOG_KERNEL,
		'user' => LOG_USER,
		'mail' => LOG_MAIL,
		'daemon' => LOG_DAEMON,
		'auth' => LOG_AUTH,
		'syslog' => LOG_SYSLOG,
		'lpr' => LOG_LPR,
		'news' => LOG_NEWS,
		'uucp' => LOG_UUCP,
		'cron' => LOG_CRON,
		'authpriv' => LOG_AUTHPRIV,
		'ftp' => LOG_FTP,
		'ntp' => LOG_NTP,
		'security' => LOG_SECURITY,
		'console' => LOG_CONSOLE,
		'local0' => LOG_LOCAL0,
		'local1' => LOG_LOCAL1,
		'local2' => LOG_LOCAL2,
		'local3' => LOG_LOCAL3,
		'local4' => LOG_LOCAL4,
		'local5' => LOG_LOCAL5,
		'local6' => LOG_LOCAL6,
		'local7' => LOG_LOCAL7
	}

	# severities
	LOG_EMERG = 0
	LOG_ALERT = 1
	LOG_CRIT = 2
	LOG_ERR = 3
	LOG_WARNING = 4
	LOG_NOTICE = 5
	LOG_INFO = 6
	LOG_DEBUG = 7

	LOG_NSEVERITIES = 7

	class InvalidSeverityError < StandardError
	end

	class InvalidFacilityError < StandardError
	end

	class << self
		@opened = false

		def open(host, ident=$0, facility=LOG_USER, port=514, &tagblock)
			raise RuntimeError if @opened
			if facility.kind_of? String
				oldfacility = facility
				unless facility = SYSLOG_NAMES[facility]
					raise InvalidFacilityError,
					"Unknown facility '#{oldfacility}'", caller
				end
			elsif facility.kind_of? Integer
				if facility < 0 or facility > LOG_NFACILITIES
					raise InvalidFacilityError,
					"Facility number out of range 0-#{LOG_NFACILITIES}", caller
				end
			else
				raise InvalidFacilityError,
				"Facility must be a String or Integer, got type #{facility.class}", caller
			end

			@facility = facility
			@host = host
			@port = port
			@ident = ident

			# if a block is passed to initialize, its used to generate the 'tag' when each event is sent
			if block_given?
				@tagblock = tagblock
			else
				@tagblock = lambda {|x|"#{@ident}[#{Process.pid}]"}
			end
			@opened = true
			self
		end

		def open!(*args)
			close
			open(*args)
		end

		alias reopen open!

		def opened?
			@opened || false
		end

		def close
			@opened = false
		end

		def ident
			@ident
		end

		def instance
			self
		end

		def tagblock=(&block)
			if block_given?
				@tagblock = block
			end
		end

		def log(severity, message, *args)
			raise RuntimeError unless @opened
			time = Time.now
			if !severity.kind_of? Integer or severity < 0 or severity > LOG_NSEVERITIES
				raise InvalidSeverityError,
				"Severity out of range, must be 0-#{LOG_NSEVERITIES}", caller
			end

			priority = (@facility*8)+severity #priority calculated as per RFC3164
			# RFC requires the day of the month to be ' ' right justified (no leading 0)
			day = time.strftime("%d").to_i.to_s.rjust(2, ' ')
			date = time.strftime("%b #{day} %H:%M:%S") #date according to RFC3164
			# hostname should have the domain name component removed
			hostname = Socket.gethostname.split('.', 2)[0] # TODO - will this always work?
			# to get the ip call IPSocket.getaddress on the result
			tag = @tagblock.call(self)

			# TODO - should we calculate the tag only once?
			# truncate the tag to 32 bytes, taking into account that any
			# non-alpha-numeric character terminates the tag
			if tag.length > 32
				# check if the tag terminates at a non-alphanumeric character
				idx = tag.index(/[^A-Za-z0-9]/)
				if idx > 31 or idx.nil?
					# if it does, but that point is later than 32 characters
					# or it doesn't, we truncate the tag.
					tag = tag[0..31]
				end
			end
			
			message = sprintf(message, *args) unless args.empty?
			# technically we're supposed to send the hostname, but freebsd's syslog doesn't handle it right...?
			#payload = "<#{priority}>#{date} #{hostname} #{tag} #{message}"
			payload = "<#{priority}>#{date} #{tag} #{message}"

			# truncate any payload over 1024 bytes
			if payload.length > 1024
				payload = payload[0..1023]
			end
			UDPSocket.open.send(payload, 0, @host, @port)
		end

		def emerg(*args)
			log(LOG_EMERG, *args)
		end

		alias emergency emerg

		def alert(*args)
			log(LOG_ALERT, *args)
		end

		def crit(*args)
			log(LOG_CRIT, *args)
		end

		alias critical crit

		def err(*args)
			log(LOG_ERR, *args)
		end

		alias error err

		def warning(*args)
			log(LOG_WARNING, *args)
		end

		def notice(*args)
			log(LOG_NOTICE, *args)
		end

		def info(*args)
			log(LOG_INFO, *args)
		end

		def debug(*args)
			log(LOG_DEBUG, *args)
		end
	end
end
