# FakeSocket - a DSL to allow faking the endpoint of a socket-based protocol
# Copyright 2006 Andrew Thompson
#
# FakeSocket lets you easily write simple scripts to fake the endpoint for a
# particular endpoint (HTTP, Asterisk AMI, etc). You specify 'events' which when
# events arrive from the socket that match it, the callback associated with the
# event is fired. You can also schedule events  to occur at various times. And
# if you only want an exent to be responded to in a certain way, use
# on_event_once


Thread.abort_on_exception = true
class FakeSocket

	# play with the metaclass
	class << self
		#change the default seperator of events
		def event_seperator=(sep)
			@sep = sep
		end

		alias :set_event_seperator :event_seperator=

		def event_seperator
			@sep || $/
		end
	end

	def self.protocol(&block)
		instance_eval &block
	end

	def self.inherited(klass)
		klass.class_eval do
			@events = {}
			@events_once = {}
			@scheduled_events = []
			@periodic_events = []
		end
	end


	#pass in an already connected socket
	def self.session(socket, &block)
		@time = 0
		@socket = socket
		@line = ''
		run(&block)
	end

	def self.run(&block)
		instance_eval &block

		@schedulethread = Thread.new do
			loop do
				@periodic_events.each do |x|
					if @time % x[:period] == 0
						instance_eval &x[:callback]
					end
				end
				#check if we have any scheduled events and if any are due.
				while(!@scheduled_events.empty? and @time >= @scheduled_events[0][:wait])
					instance_eval &@scheduled_events.shift[:callback]
				end

				sleep 1
				@time += 1
			end
		end

		loop do
			catch :done do
				if result = select([@socket], nil, nil, 0.1)
					input, output, err = result
					@line = input[0].gets(self.event_seperator)
					exit if @line.nil?
					p @line
					#check the onwe only events
					@events_once.keys.sort{|x,y| y.to_s.length <=> y.to_s.length}.each do |regex|
						if regex.match @line
							instance_eval &@events_once[regex]
							#remove it because it's run once now
							@events_once.delete(regex)
							throw :done #jump back up the stack a little
						end
					end
					
					#check the other events
					@events.keys.sort{|x,y| y.to_s.length <=> x.to_s.length}.each do |regex|
						if regex.match @line
							instance_eval &@events[regex]
							throw :done #as above
						end
					end
					#unhandled event...
					puts "unhandled input #{line}"
				else
					sleep 1
				end
			end
		end
	end

	def self.line
		@line
	end

	def self.reply(message)
		@socket.puts message
	end

	def self.on_event(regexp, &block)
		if block_given?
			@events[regexp] = block
		else
			raise ArgumentError, "must pass a block", caller
		end
	end

	def self.on_event_once(regexp, &block)
		if block_given?
			@events_once[regexp] = block
		else
			raise ArgumentError, "must pass a block", caller
		end
	end
	
	#scheduling is relative to the time it was called
	#anything parsed in the original session block is from 0 time
	#things scheduled on the fly are timed from when they were scheduled and are
	#automatically sorted by soonest executed and run appropiately. This is
	#checked every 1 second.
	def self.schedule(wait, &block)
		if block_given?
			@scheduled_events << {:wait=>@time+wait, :callback=>block}
		else
			raise ArgumentError, "must pass a block", caller
		end
		#sort by time
		@scheduled_events = @scheduled_events.sort{|x,y| x[:wait]<=>y[:wait]}
	end

	def self.periodic(period, &block)
		if block_given?
			@periodic_events << {:period=>period, :callback=>block}
		else
			raise ArgumentError, "must pass a block", caller
		end
	end
end
