require 'socket'

require 'lib/fakesocket'

#class to make ami stuff
class AMIEvent
	class << self
		def ev(name, params)
			str = "Event: #{name}\r\n"
			params.each do |k,v|
				str << "#{k}: #{v}\r\n"
			end
			str << "\r\n"
		end

		def rp(state, params)
			str = "Response: #{state}\r\n"
			params.each do |k,v|
				str << "#{k}: #{v}\r\n"
			end
			str << "\r\n"
		end
	end
end

socket = TCPServer.new('localhost', 5555).accept

class FakeAMISocket < FakeSocket; end

$userid = 199

FakeAMISocket.set_event_seperator("\r\n\r\n")

FakeAMISocket.session socket do
	on_event(/^Action: Login/) do
		/ActionID: (\d+)/.match line
		reply(AMIEvent.rp('Success', 'ActionID'=>$1))
	end

	on_event(/^Action: Agents/) do
		/ActionID: (\d+)/.match @line
		reply(AMIEvent.rp('Success', 'ActionID'=>$1))

		1200.upto(1300) do |id|
			reply <<THERE
Event: Agents\r
Agent: #{id}\r
Name: agent#{id}\r
Status: AGENT_LOGGEDOFF\r
Loggedinchan: SIP/1#{id}\r
LoggedinTime: 0\r
Talkingto: \r
\r
THERE
		end
	end

	on_event(/^Action: QueueStatus/) do
		/ActionID: (\d+)/.match line
		reply("Response: Success\r\nMessage: foo\r\nActionID: #{$1}\r\n\r\n")
		reply <<THERE
Event: QueueParams\r
Queue: testq\r
Max: 10\r
Holdtime: 0\r
Completed: 2\r
Abando
ServiceLevel: 60\r
ServicelevelPerf: 0.1\r
Weight: 0\r
\r
THERE
		1200.upto(1300) do |id|
			reply(AMIEvent.ev('QueueMember',
							  'Queue'=> 'testq',
							  'Location' => "Agent/#{id}",
							  'Membership' => 'Static',
							  'Penalty' => '0',
							  'CallsTaken' => '0',
							  'LastCall' => '0',
							  'Paused' => '0'))
		end
	end

	on_event(/^Action: AgentCallbackLogin/) do
		/ActionID: (\d+)/.match line
		reply("Response: Success\r\nMessage: foo\r\nActionID: #{$1}\r\n\r\n")
	end
	
	on_event(/^Action: AgentLogoff/) do
		/ActionID: (\d+)/.match line
		reply("Response: Success\r\nMessage: foo\r\nActionID: #{$1}\r\n\r\n")
	end

	on_event(/^Action: GetVar/) do
		/ActionID: (\d+)/.match line
		reply("Response: Success\r\nVariable: TYPE\r\nValue: \r\nActionID: #{$1}\r\n\r\n")
	end

	on_event(/Action: QueuePause/) do
		/ActionID: (\d+)/.match line
		reply("Response: Success\r\nMessage: whatever\r\nActionID: #{$1}\r\n\r\n")
	end

	on_event(/Action: Ping/) do
		/ActionID: (\d+)/.match line
		reply("Response: Pong\r\nActionID: #{$1}\r\n\r\n")
	end

	reply("Fake Asterisk Manager Interface 1.0\r\n")
	schedule 15 do
		periodic 3 do
			p 'userid'
			p $userid

			id = Time.now.to_i.to_s+'.'+Time.now.usec.to_s
			reply("Event: VarSet\r\nVariable: CALLTYPE\r\nValue: call\r\nChannel: Foo/1234\r\nUniqueID: #{id}\r\n\r\n")
			reply("Event: VarSet\r\nVariable: BRANDID\r\nValue: 00310001\r\nChannel: Foo/1234\r\nUniqueID: #{id}\r\n\r\n")
			schedule 3 do # IVR time
				reply("Event: Join\r\nChannel: Foo/1234\r\nCallerID: Grand Poobah\r\nCalleridname: \r\nUniqueID: #{id}\r\nQueue: testq\r\n\r\n")
				schedule 3 do # queue time
					reply("Event: Leave\r\nChannel: Foo/1234\r\nUniqueID: #{id}\r\nQueue: testq\r\n\r\n")
					reply("Event: VarSet\r\nVariable: BRIDGEPEER\r\nValue: Agent/1#{$userid}\r\nUniqueID: #{id}\r\n\r\n")
					reply("Event: Link\r\nChannel1: Foo/1234\r\nChannel2: Agent/1#{$userid}\r\nUniqueID1: #{id}\r\nUniqueID2: 9877654\r\n\r\n")
					schedule 270 do # talktime
						reply("Event: Unlink\r\nUniqueID1: #{id}\r\nUniqueID2: 987654}\r\n\r\n")
#						schedule 5 do 
						reply("Event: Hangup\r\nUniqueID: #{id}\r\n\r\n")
#						end
					end
				end
			end
		$userid += 1
		$userid = 200 if $userid > 300
		end
	end
end
#end
