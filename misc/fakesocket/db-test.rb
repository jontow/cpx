#! /usr/bin/env ruby
#
# Use this in combination with ami-{low,med,insane} to test DB/CDR
# consistancy given a known amount of calls.
#
# Note: you may want to clear the tables prior to running your test
# if you wish to have any reasonable amount of luck.
#

require 'mysql'

ppx_dbh = Mysql.real_connect('localhost', 'ppx', 'ROADS', 'ppx')

# Expected values:
expduration = 300
expcalltime = 264
expwrapuptm = 30

# Initialization
duration = 0
incall = 0
wrapup = 0

result = ppx_dbh.query("SELECT (End - Start) as duration, InCall, WrapUp FROM billing_summaries");

numcalls = result.num_rows

result.each {|x|
	duration += x[0].to_i
	incall += x[1].to_i
	wrapup += x[2].to_i
}

puts
puts "Number of calls: #{numcalls}"
puts
puts "Expected values:"
puts "	Expected Duration: #{expduration * numcalls}"
puts "	Expected In-Call: #{expcalltime * numcalls}"
puts "	Expected Wrap-up: #{expwrapuptm * numcalls}"
puts
puts "From CDR:"
puts "	Total Duration: #{duration}"
puts "	Total In-Call: #{incall}"
puts "	Total In Wrapup: #{wrapup}"
puts
puts "Discrepancies:"
puts "	Duration difference: #{duration - (expduration * numcalls)}" 
puts "	In-call difference: #{incall - (expcalltime * numcalls)}"
puts "	In-wrapup difference: #{wrapup - (expwrapuptm * numcalls)}"
puts

