require 'socket'

require 'lib/fakesocket'

200.upto(550) do |id|
	unless fork
#		id = 200
		socket = TCPSocket.new('localhost', 1337)
		klass = Class::new(FakeSocket)
		klass.session socket do
			counter = 0
			
			on_event_once(/^ACK 1 (.+)/) do
				puts 'logged in'
				puts $1
				on_event_once (/^ASTATE (\d+) 5/) do
					id = line.split(' ')[1]
					p id
					reply("ACK #{id}")
					reply("STATE #{counter+=1} 2")
				end

			end

			on_event(/^ASTATE \d+ 6/) do
				id = line.split(' ')[1]
				reply("ACK #{id}")
				schedule 30 do # wrapup time
					reply("ENDWRAPUP #{counter+=1}")
				end
			end

			on_event(/^(ACK|ERR)/) do
				p line
			end

			on_event(/./) do
				event, count, data = line.split(' ', 3)
				p "ACK #{count}"
				reply("ACK #{count}")
			end


			reply("LOGIN #{counter+=1} user#{id}:user")
		end
		sleep 3
	end
end

