#!/usr/bin/env ruby
###################################################
#       Mom Tool
#       (POPTool was taken)
#
#	Mailbox cleaning tool
#
#       Copyright 20007 Fused Solutions
#
#       History
#       11/6/2007 jhc
#
###################################################

require 'tk'
require 'net/pop'
require 'date'
require 'time'

class Mailbox

	attr_reader :host, :user, :port

	def initialize
		@pop=nil
		@messages=[]
		@host=nil
		@user=nil
		@pass=nil
		@port='110'
		@host='mail.example.com'
		@user='wrapupcode'
		@pass='Password123'
	end

	def active?
		if @pop and @pop.active?
			return true
		else
			return false
		end
	end

	def configured?
		if @host and @user
			return true
		else
			return false
		end
	end

	def delete(msg)
		if @pop and @pop.active?
			mail=@pop.mails.detect { |email| email.number==msg }
			mail.delete if not mail.deleted?
			@messages[msg]['deleted']='*'
		end
	end

	def deleteall
		if @pop and @pop.active?
			@pop.delete_all
			@messages.each { |msg| msg['deleted']='*' if msg and msg.kind_of? Hash }
		end
	end

	def messages
		return @messages
	end

	def msgcount
		if @pop and @pop.active?
			return @pop.mails.length
		else
			return 0
		end
	end

	def connect(hst,prt,us,ps)
		@host=hst
		@user=us
		@pass=ps
		@port=prt
		@pop.finish if @pop and @pop.active?
		@messages=[]
		@pop=Net::POP3.new(hst,prt)
		@pop.start(us,ps)
		if @pop.active?
			if not @pop.mails.empty?
				@pop.each_mail do |msg|
					@messages[msg.number]={'deleted'=>' ', 'number'=>msg.number,'size'=>msg.length}
					@messages[msg.number].default=""
				end
			end
			return true
		else
			return false
		end
	end

	def reconnect
		connect(@host, @port, @user, @pass)
	end

	def reset
		@pop.reset
	end

	def close
		@pop.finish if @pop and @pop.active?
		@pop=nil
	end

	def getheader(msg)
		return if msg < 1
		if email=@pop.mails.detect { |mail| mail.number == msg } and not email.deleted? then header=email.header end
		if header then
			if match=header.match(/^Date:[ ]*(.*)\n[^ ]*/i) then datestr=match[1].gsub(/[\x00-\x1f]/," ") end 
			if match=header.match(/^From:[ ]*(.*)\n[^ ]*/i) then from=match[1].gsub(/[\x00-\x1f]/," ") end 
			if match=header.match(/^Subject:[ ]*(.*)\n[^ ]*/i) then subject=match[1].gsub(/[\x00-\x1f]/," ") end
			begin
				date=Time.parse(datestr).strftime("%Y-%m-%d %H:%M")
			rescue Exception
				date=""
			end
			@messages[msg].merge!({'date'=>date,'from'=>from,'subject'=>subject})
		end
	end

	def getallheaders
		@pop.mails.each do |mail|
			if mail and not mail.deleted?
				header=mail.header
				if match=header.match(/^Date:[ ]*(.*)\n[^ ]*/i) then datestr=match[1].gsub(/[\x00-\x1f]/," ") end 
				if match=header.match(/^From:[ ]*(.*)\n[^ ]*/i) then from=match[1].gsub(/[\x00-\x1f]/," ") end 
				if match=header.match(/^Subject:[ ]*(.*)\n[^ ]*/i) then subject=match[1].gsub(/[\x00-\x1f]/," ") end
				date=Time.parse(datestr).strftime("%Y-%m-%d %H:%M") if datestr
				if header then
					@messages[mail.number].merge!({'date'=>date,'from'=>from,'subject'=>subject})
				end
			end

		end
	end

	def getmessage(msg)
		mail=@pop.mails.detect{ |mail| mail.number==msg }
		return "Message marked for deletion!" if mail.deleted? 
		return mail.top(100).gsub("\r", '')
	rescue Exception => e
		return "#{e.class}\n#{e.message}\nwhile retrieving message #{msg}."
	end

end

class RootWindow

	attr_reader :inbox, :window, :menu

	def initialize(win)
		@window=win
		@window.withdraw
		@window.protocol('WM_DELETE_WINDOW',proc{quit})
		@menu=Menus.new(self).create("File","Connection","Sort By")
		@sortfield='number'
		@sortorder=1
		@inbox = Mailbox.new
	end

	def unpackall
		@msgframe.unpack if @msgframe
		@loginframe.unpack if @loginframe
		@mailframe.unpack if @mailframe
		@menu.hide('all')
	end

	def showstatus(msg=nil)
		if msg
			@statusframe = TkFrame.new(@window,:borderwidth=>1,:background=>:gray) if not @statusframe
			statusmessage=TkLabel.new(@statusframe).pack(:anchor=>:center,:expand=>true,:fill=>:both)
			statusmessage.text=msg
			msgwd=statusmessage.winfo_reqwidth
			msght=statusmessage.winfo_reqheight
			winwd=@window.winfo_reqwidth
			winht=@window.winfo_reqheight
			ht=Float(msght)/winht
			if msgwd <= winwd
				wd=Float(msgwd)/winwd
			else
				wd=1
				ht=ht * (msgwd/winwd)
			end
			x=(1-wd)/2
			y=(1-ht)/2
			@statusframe.place(:relx=>x,:rely=>y)
			@statusframe.raise
			Tk.update
		else
			if @statusframe
				@statusframe.place_forget
				@statusframe.destroy
				@statusframe=nil
			end
		end
	end

	def definemainframe
		@loginframe=TkFrame.new(@window)
		upperframe=TkFrame.new(@loginframe).pack(:side=>:top)
		labelframe=TkFrame.new(upperframe).pack(:side=>:left)
		inputframe=TkFrame.new(upperframe).pack(:side=>:right)
		buttonframe=TkFrame.new(@loginframe).pack(:side=>:top)
		TkLabel.new(labelframe, :text=>"POP3 Host:").pack(:side=>:top)
		TkLabel.new(labelframe, :text=>"Port:").pack(:side=>:top)
		TkLabel.new(labelframe, :text=>"Username:").pack(:side=>:top)
		TkLabel.new(labelframe, :text=>"Password:").pack(:side=>:top)
		@hostname=TkEntry.new(inputframe).pack(:side=>:top)
		@port=TkEntry.new(inputframe, :text=>"Port:").pack(:side=>:top)
		@port.value='110'
		@username=TkEntry.new(inputframe).pack(:side=>:top)
		@password=TkEntry.new(inputframe).pack(:side=>:top)
		TkButton.new(buttonframe, :text=>"Login", :command=>proc{login}).pack(:side=>:left)
		TkButton.new(buttonframe, :text=>"Reconnect", :command=>proc{login("again")}).pack(:side=>:left)
		TkButton.new(buttonframe, :text=>"Exit", :command=>proc{quit}).pack(:side=>:left)
	end

	def showmain
		unpackall
		@menu.show("file")
		definemainframe if not @loginframe
		@loginframe.pack(:fill=>:both, :padx=>10, :expand=>true)
		@window.deiconify
		@window.bind("Return"){login}
		@window.bind("Escape"){quit}
	end

	def delete(msg)
		@inbox.delete(msg)
		listmessages
	end

	def delselected
		showstatus("Deleting Selected Messages")
		selected=@messagelist.curselection
		return if not selected.kind_of? Array or selected.empty?
		selected.each do |msgline|
			msg=@listindex[msgline]
			@inbox.getheader(msg)
			@inbox.delete(msg)
		end
		showmessagewin
		selected.each { |msg| @messagelist.selection_set(msg) }
		@messagelist.see(selected[0])
	end

	def deleteall
		confirm=TkDialog.new(:title=>"Confirm Delete", :message=>"You are about to delete all message from this inbox.\nContinue?", :buttons=>['Yes','No'], :default=>1, :default_button=>1)
		showstatus("Deleting Messages")
		@inbox.deleteall if confirm.value==0
		showmessagewin
	end

	def getheaders(getall=false)
		tries=0 if not tries
		showstatus("Retrieving Headers")
		prevsel=@messagelist.curselection
		if getall
			begin
			@inbox.getallheaders
			rescue Net::POPError => e
				tries += 1
				showstatus
				showstatus("#{e.message}\nPerhaps the server needs a few seconds to catch up?")
				@inbox.reset
				@inbox.close
				sleep 5
				@inbox.reconnect
				retry if tries == 1
				closeconnection if tries > 1
			else
				tries=0
			end
		else
			if prevsel.kind_of? Array and not prevsel.empty?
				prevsel.each do |msgline|
					msg=@listindex[msgline]
					begin
						@inbox.getheader(msg)
					rescue Net::POPError => e
						tries += 1
						showstatus
						showstatus("#{e.message}\nPerhaps the server needs a few seconds to catch up?")
						@inbox.reset
						@inbox.close
						sleep 5
						@inbox.reconnect
						retry if tries == 1
					else
				tries=0
			end

				end
			end
		end
		showmessagewin(@messagelist.curselection)
		if prevsel.kind_of? Array and not prevsel.empty?
			prevsel.each { |idx| @messagelist.selection_set(idx) }
			@messagelist.see(prevsel[0])
		end
	end


	def definemessageframe
		@window.withdraw
		@msgframe=TkFrame.new(@window).pack(:side=>:top, :fill=>:both, :expand=>true)
		buttonframe=TkFrame.new(@msgframe).pack(:side=>:top)
		messageframe=TkFrame.new(@msgframe).pack(:side=>:top, :fill=>:both, :expand=>true)
		TkButton.new(buttonframe, :text=>"Get Header", :command=>proc{getheaders}).pack(:side=>:left)
		TkButton.new(buttonframe, :text=>"Get All Headers", :command=>proc{getheaders(true)}).pack(:side=>:left)
		TkButton.new(buttonframe, :text=>"Delete Selected", :command=>proc{delselected}).pack(:side=>:left)
		TkButton.new(buttonframe, :text=>"Delete All", :command=>proc{deleteall}).pack(:side=>:left)
		TkButton.new(buttonframe, :text=>"View", :command=>proc{showemail}).pack(:side=>:left)
		TkButton.new(buttonframe, :text=>"Reload", :command=>proc{reloadbox}).pack(:side=>:left)
		TkButton.new(buttonframe, :text=>"Purge Deleted", :command=>proc{purge}).pack(:side=>:left)
		TkButton.new(buttonframe, :text=>"Cancel", :command=>proc{closeconnection}).pack(:side=>:left)
		@userlabel = TkLabel.new(messageframe, :text=>"User at Server").pack(:side=>:top)
		@messagecountlabel = TkLabel.new(messageframe, :text=>"Total: #{@inbox.msgcount} messages").pack(:side=>:top)
		listfont = TkFont.new('fixed', :size=>8)
		listframe=TkFrame.new(messageframe).pack(:side=>:top, :fill=>:both, :expand=>true)
		msgscrollx = TkXScrollbar.new(listframe, :orient=>:horizontal).pack(:side=>:bottom, :fill=>:x)
		msgscrolly = TkYScrollbar.new(listframe, :orient=>:vertical).pack(:side=>:right, :fill=>:y)
		@messagelist = TkListbox.new(listframe, :font=>listfont, :selectmode=>:extended).pack(:side=>:bottom, :anchor=>:s, :expand=>true, :fill=>:both)
		@messagelist.xscrollbar(msgscrollx)
		@messagelist.yscrollbar(msgscrolly)
		@titlelist = TkListbox.new(listframe, :font=>listfont, :height=>1).pack(:side=>:bottom, :anchor=>:s, :fill=>:x)
		msgscrollx.command do |*arg| 
			@messagelist.xview(*arg)
			@titlelist.xview(*arg)
		end
		@window.deiconify
	end

	def showmessagewin(viewsel=nil)
		unpackall
		@menu.show("file","Connection")
		definemessageframe if not @msgframe
		@msgframe.pack(:fill=>:both, :padx=>10, :expand=>true)
		listmessages(viewsel)
		@menu.show("Sort By") if @inbox.messages.length > 1
		@window.bind("Escape"){closeconnection}
		@window.bind("Control-a"){@messagelist.selection_set(0,'end')}
		showstatus
	end

	def listmessages(viewsel=nil)
		return if not @msgframe
		showstatus("Loading Messages")
		@listindex=[]
		# set initial widths to the widths of the titles
		widths=[3,4,4,4,7]
		@messagelist.clear
		messages = @inbox.messages.sort do |a,b| 
			if a and b then 
					@sortorder * (a[@sortfield] <=> b[@sortfield])
			else 
				if a then
				       -1
			       else
				       1
			       end
			end
		end	
		@oldsortfield=@sortfield
		@oldsortorder=@sortorder
		@messagecountlabel.text="Total: #{@inbox.msgcount} messages"
		@userlabel.text="#{@inbox.user} at #{@inbox.host}:#{@inbox.port}"
		messages.each do |msg|
			if msg
				widths[0] = "#{msg['number']}".length if "#{msg['number']}".length > widths[0]
				widths[1] = "#{msg['size']}".length if "#{msg['size']}".length > widths[1]
				widths[2] = "#{msg['date']}".length if "#{msg['date']}".length > widths[2]
				widths[3] = "#{msg['from']}".length if "#{msg['from']}".length > widths[3]
				widths[4] = "#{msg['subject']}".length if "#{msg['subject']}".length > widths[4]
			end
		end
		messages
		@titlelist.clear
		titlestr="*"
		titlestr=titlestr + "Num".ljust(widths[0]+1)
		titlestr=titlestr + "Size".ljust(widths[1]+1)
		titlestr=titlestr + "Date".ljust(widths[2]+1)
		titlestr=titlestr + "From".ljust(widths[3]+1)
		titlestr=titlestr + "Subject".ljust(widths[4]+1)
		@titlelist.insert('end',titlestr)
		messages.each do |msg|
			if msg
				msgstr = ""
				msgstr << "#{msg['deleted']}"
				msgstr << "#{msg['number']}".ljust(widths[0]+1)
				msgstr << "#{msg['size']}".ljust(widths[1]+1)
				msgstr << "#{msg['date']}".ljust(widths[2]+1)
				msgstr << "#{msg['from']}".ljust(widths[3]+1)
				msgstr << "#{msg['subject']}".ljust(widths[4]+1)
				@listindex[@messagelist.index('end')]=msg['number']
				@messagelist.insert('end', msgstr)
			end
		end
		if viewsel.kind_of? Array and not viewsel.empty? then
			viewsel.each { |line| @messagelist.selection_set(line)}
			@messagelist.see(viewsel[0])
			viewsel=nil
		end
		showstatus
	end

	def showemail
		showstatus("Retrieving Message")
		@messagelist.curselection.each do |selected|
			return if not selected
			msg=@listindex[selected]
			win=MessageWindow.new(self, msg)
			win.mailtext.value=@inbox.getmessage(msg)
		end
		showstatus
	end

	def login(again=nil)
		begin
			showstatus("Connecting")
			@listindex=[]
			if again
				@inbox.reconnect
			else
				@inbox.connect(@hostname.value, @port.value, @username.value, @password.value)
			end
		rescue Net::POPAuthenticationError, Net::POPError => e
			TkDialog.new(:title=>"Login", :message=>"#{e.class}\n#{e.message}\nwhile logging in to\n server: #{@hostname.value}:#{@port.value}\nas\n User: #{@username.value}", :buttons=>["Ok"])
		rescue Exception => e
			TkDialog.new(:title=>"Login?", :message=>"#{e.class}\n#{e.message}\nwhile logging in to\n server: #{@hostname.value}:#{@port.value}\nas\n User: #{@username.value}\n#{e.backtrace}", :buttons=>["Ok"])
		else
			@window.bind_remove("Return")
			@window.bind_remove("Escape")
			showmessagewin
		ensure
			showstatus
		end
	end

	def reloadbox
		showstatus("Reloading Inbox")
		@inbox.reset
		@inbox.close
		@inbox.reconnect
		showmessagewin
	end

	def purge
		@inbox.close
		@inbox.reconnect
		showmessagewin
#		closeconnection(true)
#		login("again")
	end

	def closeconnection(commit=false)
		@inbox.reset if not commit
		@inbox.close
		showmain
	end

	def quit
		if @inbox and @inbox.active?
			confirm=TkDialog.new(:title=>'Closing', :message=>'The mailbox is still open.\nCommit changes, or Revert?', :buttons=>['Commit','Revert','Cancel'], :default=>1, :default_button=>1)
			return if confirm.value==2
			@inbox.reset if confirm.value==1
			@inbox.close
		end
		@window.destroy 
	end

	def listsort(sorton=nil, order=nil)
		@sortfield=sorton.value.downcase if sorton
		if order=='asc' then @sortorder=1 else @sortorder=-1 end
		listmessages
	end



end


class MessageWindow
	attr_accessor :msg
	attr_reader :window, :msgmen, :mailtext, :mailframe, :parent

	def initialize(parent, msg)
		@msg=msg
		@window=TkToplevel.new
		@msgmen=Menus.new(self)
		@msgmen.create("file","message")
		@mailframe=TkFrame.new(@window)
		TkButton.new(@mailframe, :text=>'Close', :command=>proc{@window.destroy}).pack(:side=>:top)
		mailscroll=TkYScrollbar.new(@mailframe).pack(:side=>:right, :fill=>:y)
		@mailtext=TkText.new(@mailframe).pack(:side=>:top, :fill=>:both, :expand=>true)
		@mailtext.yscrollbar(mailscroll)
		@window.title="Message #{msg}"
		@mailframe.pack(:side=>:top, :fill=>:both, :expand=>true)
		@msgmen.show("all")
		@parent=parent
	end

	def reloadbox
		@parent.reloadbox
	end

	def closeconnection
		@parent.closeconnection
	end

	def delete
		@parent.delete(@msg)
		@window.destroy
	end
end

class Menus

	def initialize(parent)
		@allmenus=[]
		@window=parent.window
		@mainmenu=TkFrame.new(@window, :relief=>:raised, :border=>2).pack(:side=>:top, :fill=>:x)
		@parent=parent
	end

	def create(*menulst)
		menulst.each do |menu|
			if menu.kind_of? String
				menu.upcase!
				case menu
				when "FILE"
					if !@filemenu then
						@filemenu=TkMenubutton.new(@mainmenu, :text=>"File", :underline=>0)
						filemenulist=TkMenu.new(@filemenu, :tearoff=>false)
						filemenulist.add("command", :label=>"Quit",:command=>proc{@window.destroy}, :underline=>0)
						@filemenu[:menu]=filemenulist
						@allmenus << [@allmenus.length, @filemenu, @filemenu[:text].upcase]
					end
				when "CONNECTION"
					if !@connmenu
						@connmenu=TkMenubutton.new(@mainmenu, :text=>"Connection", :underline=>0)
						connmenulist=TkMenu.new(@connmenu, :tearoff=>false)
						connmenulist.add("command", :label=>"Reload Mailbox(Cancel Changes)", :command=>proc{@parent.reloadbox}, :underline=>0)
						connmenulist.add("command", :label=>"Close Connection(Purge Deleted)", :command=>proc{@parent.closeconnection(true)}, :underline=>0)
						@connmenu[:menu]=connmenulist
						@allmenus << [@allmenus.length, @connmenu, @connmenu[:text].upcase]
					end
				when "SORT BY"
					if !@sortmenu
						@sortmenu=TkMenubutton.new(@mainmenu, :text=>"Sort By", :underline=>0)
						sortmenulist=TkMenu.new(@sortmenu, :tearoff=>false)
						@sorton=TkVariable.new('number')
						@order=TkVariable.new("asc")
						sortmenulist.add("radiobutton", :label=>"ID", :variable=>@sorton, :value=>'number', :command=>proc{@parent.listsort(@sorton,@order)}, :underline=>0)
						sortmenulist.add("radiobutton", :label=>"Date", :variable=>@sorton, :value=>'Date', :command=>proc{@parent.listsort(@sorton,@order)}, :underline=>0)
						sortmenulist.add("radiobutton", :label=>"Size", :variable=>@sorton, :value=>'Size', :command=>proc{@parent.listsort(@sorton,@order)}, :underline=>0)
						sortmenulist.add("radiobutton", :label=>"From", :variable=>@sorton, :value=>'From', :command=>proc{@parent.listsort(@sorton,@order)}, :underline=>0)
						sortmenulist.add("radiobutton", :label=>"Subject", :variable=>@sorton, :value=>'Subject', :command=>proc{@parent.listsort(@sorton,@order)}, :underline=>1)
						sortmenulist.add("checkbutton", :label=>"Descending", :variable=>@order, :onvalue=>'desc', :offvalue=>'asc', :command=>proc{@parent.listsort(@sorton,@order)}, :underline=>1)
						@sortmenu[:menu]=sortmenulist
						@allmenus << [@allmenus.length, @sortmenu, @sortmenu[:text].upcase]
					end
				when "MESSAGE"
					if !@messmenu
						@messmenu=TkMenubutton.new(@mainmenu, :text=>"Message", :underline=>0)
						sortmenulist=TkMenu.new(@messmenu, :tearoff=>false)
						sortmenulist.add("command", :label=>"Delete", :command=>proc{@parent.delete}, :underline=>0)
						sortmenulist.add("command", :label=>"Close", :command=>proc{@parent.window.destroy}, :underline=>0)
						@messmenu[:menu]=sortmenulist
						@allmenus << [@allmenus.length, @messmenu, @messmenu[:text].upcase]
					end
				end
			end
		end
		return self
	end

	def show(*menulst)
		if menulst.length == 1 and menulst[0].kind_of? String and menulst[0].upcase == 'ALL' then
			@allmenus.each { |menu| show(menu[0]) }
		else
			menulst.each do |menu| 
				menu.upcase! if menu.kind_of? String
				if menuary = @allmenus.detect { |m| m.include? menu }
					menuary[1].pack(:side=>:left)
				end
			end
		end
		return self
	end

	def hide(*menulst)
		if menulst.length == 1 and menulst[0].kind_of? String and menulst[0].upcase == 'ALL' then
			@allmenus.each { |menu| hide(menu[0]) }
		else
			menulst.each do |menu|
				menu.upcase! if menu.kind_of? String
				if menuary = @allmenus.detect { |m| m.include? menu }
					menuary[1].unpack
				end
			end
		end
		return self
	end
end

main = RootWindow.new(TkRoot.new)
main.showmain
Tk.mainloop
