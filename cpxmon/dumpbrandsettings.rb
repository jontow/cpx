require 'mysql'
load './conf/cpxmon.conf'
include CpxMonConf

dbh = Mysql.real_connect(CpxMonConf::MYSQL_CREDENTIALS[:host],
												 CpxMonConf::MYSQL_CREDENTIALS[:username],
												 CpxMonConf::MYSQL_CREDENTIALS[:password],
												 CpxMonConf::MYSQL_CREDENTIALS[:database])

filename = File.join(File.dirname(__FILE__),"brandsettings.js")

brandsets = []

result = dbh.query('select brandname,s.* from brand_settings s left join brandlist b on s.tenant=b.tenant and s.brand=b.brand')
result.each_hash { |h| brandsets << h }

brandsets.sort! do |a,b| 
	if a['brandname'] == b['brandname']
		a['keyname'] <=> b['keyname']
	elsif !a['brandname']
		1
	elsif !b['brandname']
		-1
	else
		a['brandname'] <=> b['brandname']
	end
end
File.open(filename,'w') do |fil|
	lastbrand="<none>"
	fil.print "brsets={"
	needcomma = false
	brandsets.each do |a|
		if a['brandname'] != lastbrand
			fil.print "}," if lastbrand != "<none>"
			if !a['brandname']
				br = "default"
			else
				br = a['brandname'].tr("'",'"')
			end
			fil.print "'#{br}':{"
			needcomma = false
			lastbrand = a['brandname']
		end
		fil.print "," if needcomma
		fil.print "'#{a['keyname'].tr("'",'"')}':'#{a['value'].tr("'",'"')}'"
		needcomma = true
	end
	fil.print "}}"
end
