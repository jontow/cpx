=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'socket'
require 'thread'
require 'monitor'
require 'digest/md5'
require 'cgi'

begin
	require 'RRD'
rescue LoadError 
	STDERR.puts "RRD library not found. Disabling graphing functions."
	$NORRD=true
end

require 'mysql'

$LOAD_PATH << File.dirname(__FILE__)

require 'constants/agentconstants'
require 'constants/cdrconstants'
require 'constants/protocol'
require 'constants/brandsettings.rb'
include BrandSetConst

begin
	load 'conf/cpxmon.conf'
rescue LoadError
	STDERR.puts "Missing configuration file conf/cpxmon.conf"
end


Thread.abort_on_exception = true

class InvalidEvent < StandardError
end

class CpxAgent
	include BrandSetConst
	include AgentConstants

	def self.calculate_duration(i, skew, precision=2)
		seconds = (Time.now.to_i - (i + skew).abs).to_i
		if seconds > 60
			minutes = seconds / 60
			seconds %= 60
		end

		if minutes and minutes > 60
			hours = minutes / 60
			minutes %= 60
		end

		if hours and hours > 24
			days = hours / 24
			hours %= 24
		end
		#and so on, if we ever needed it...

		# Beware the ternary operator, for it is subtle and quick to error
		x = []
		x << "#{seconds} second#{seconds != 1 ? 's' : ''}"
		x << "#{minutes} minute#{minutes != 1 ? 's' : ''}" if minutes
		x << "#{hours} hour#{hours != 1 ? 's' : ''}" if hours
		x << "#{days} day#{days != 1 ? 's' : ''}" if days

		if precision > x.length
			x.reverse.join(' ')
		else
			x[precision*-1..-1].reverse.join(' ')
		end
	end

	attr_reader :id, :name
	attr_accessor :time, :state, :statedata, :profile, :calltime, :wrapuptime, :idletime, :talkingto
	def initialize(id, name)
		@id = id
		@name = name
		@calltime = 0
		@wrapuptime = 0
		@idletime = 0
	end

	def state=(state)
		if state <= IDLE or state == IDLE or state == RELEASED
			@talkingto = nil
		end
		@state = state
	end
end

class CpxQueue
	attr_reader :name, :group, :members, :callers
	attr_accessor :calls, :abandoned, :completed, :avg_time, :max_time, :weight
	def initialize(name, group)
		@name = name
		@group = group
		@members = []
		@callers = []
		@weight = 0
	end

	def add_member(agent)
		@members << agent
	end
	
	def remove_member(agent)
		@members.delete(agent)
	end

	def add_caller(call, position)
		@callers.insert(position, call)
	end
	
	def remove_caller(call)
		@callers.delete(call)
	end
end

class CpxProfile
	attr_reader :name, :id, :order
	def initialize(name, id, order)
		@name = name
		@id = id
		@order = order
	end

	def <=>(other)
		@order <=> other.order
	end
end

class CpxCaller
	attr_reader :uniqueid
	attr_accessor :enteredqueue, :type, :brandid, :callerid, :leftqueue, :queue, :abandoned
	def initialize(uniqueid)
		@uniqueid = uniqueid
	end
end

#wrap this all in a nice class...
class Main
	ICT = 600
	WRAPT = 300
	RET = 300
	ICP = 1200
	WRAPP = 300
	REP = 300

#	PROFILE_ORDERING = %w{Probationary Level1 Magic Level2 Level3 Graveyard Supervisor CustomerService CLEC Empty}
	CALLTYPE_PRIORITIES = [:call, :voicemail, :email]

	class KickAssError < StandardError; end

	attr_reader :agent_name, :agent_tier, :agent_state, :brandlist, :agent_profile, :profiles, :config, :defaultconfig, :queuelist, :root, :brandsettings
	attr_accessor :timeskew
	def initialize

		@counter = 0
		@outgoing = Queue.new
		@incoming = Queue.new

		@sent_events = {}
		@resent_events = {}

		@reply_callbacks = {}

		@agents = {}
		
		@brandlist = {}
		@brandsettings = {}
		@queuelist = {}
		@profiles = []
		@releaseoptions = []

		@queues = {}
		@queuegroups = {}

		@calls = {}

		@timeskew = 0
		login
	end

	#wrapper for incrementing the counter
	def next_count
		@counter += 1
		@counter %= 65535 #make the counter wrap at (2**16)-1
	end

	#handle a login attempt
	def login
		
		begin
			@connection = TCPSocket.new(CpxMonConf::HOST, CpxMonConf::PORT)
		rescue Errno::ECONNREFUSED, Errno::EBADF, Errno::EINVAL
			STDERR.puts 'no server listening'
			return
		rescue SocketError => e

			STDERR.puts "Socket Error: #{e.message}"
			return
		end

		if result = select([@connection], nil, nil, 1)
			line = @connection.gets
			if line =~ /^Agent Server/
				puts "Connected to agent server"
				@connection.puts "Protocol: #{AgentProtocol::MAJOR}.#{AgentProtocol::MINOR}"
				resp = @connection.gets
				code, message = resp.chomp.split(" ",2)
				case code.to_i
				when 0
					# NOOP
				when 1
					STDERR.puts message
				when 2
					STDERR.puts message
					return
				else
					STDERR.puts "Invalid response code in protocol handshake"
					return
				end
			else
				@connection.close
				STDERR.puts "Unexpected server response: #{line}"
				return
			end
		else
			@connection.close
			STDERR.puts "timed out waiting for server response"
			return
		end
		
		@connection.puts("GETSALT 1")
		line = @connection.gets
		event, counter, data = line.strip.split(' ', 3)
		begin
			salt = Integer(data)
		rescue ArgumentError
			STDERR.puts "Server returned an invalid salt: #{data}"
			return
		end
		
		saltedpass = Digest::MD5.hexdigest("#{salt}#{Digest::MD5.hexdigest(CpxMonConf::PASSWORD)}")
		@connection.puts("LOGIN 2 #{CpxMonConf::USERNAME}:#{saltedpass}")
		line = @connection.gets
		event, counter, data = line.strip.split(' ', 3)
		if event == 'ACK' and counter == '2'
			tier, profile, timestamp = data.split(' ')

			@timeskew = Time.now.to_i - timestamp.to_i
			bootstrap
			listen
		elsif event == 'ERR'
			@connection.close
			STDERR.puts "Received error when logging in #{data}"
			return
		else
			@connection.close
			STDERR.puts "Received unexpected event #{line}, disconnecting"
			return
		end
	end

	def bootstrap
		send('AGENTS', 'ALL')
		send('QUEUES', 'ALL') do |a, b, c|
			send('QUEUEMEMBERS', 'ALL')
			send('QUEUECALLERS', 'ALL')
			send('BRIDGEDCALLERS', 'ALL')
			send('QUEUENAMES') do |result, data, event|
				if result
					data[1..-2].split('),(').each do |e|
						queuename,commonname = e.split('|')
						@queuelist[queuename] = commonname
					end
				end
				send('QUEUEGROUPS') do |result, data, event|
					data.split(' ').map{|x| x.split(':', 2)}.each {|groupid, grouplabel| @queuegroups[groupid.to_i] = grouplabel }
					send('BRANDLIST') do |result, data, event|
						data[1..-2].split('),(').map{|x| x.split('|')}.sort_by{|x| [x[1].downcase]}.each do |brandid, brandlabel|
							@brandlist[brandid] = brandlabel
						end

						send('PROFILES') do |result, data, event|
							data.split(' ').map{|x| x.split(':', 2)}.each_with_index do |profilearr, index|
								@profiles[profilearr[0].to_i] = CpxProfile.new(profilearr[1], profilearr[0].to_i, index)
							end
							send('RELEASEOPTIONS') do |result, data, event| 
								data.split(',').map{ |x| x.split(':',3)}.each { |o| @releaseoptions[o[0].to_i] = [ o[1], o[2] ] }
								bootstrap_old_calls
								html_output
								xml_output
								initrrd if !$NORRD
							end
						end
					end
				end
			end
		end
	end

	def bootstrap_old_calls
		cutoff = (Time.now - 86400).to_i #(CpxMonConf::TREND_INTERVAL * CpxMonConf::TREND_SAMPLES)).to_i
		dbh = Mysql.real_connect(CpxMonConf::MYSQL_CREDENTIALS[:host],
														 CpxMonConf::MYSQL_CREDENTIALS[:username],
														 CpxMonConf::MYSQL_CREDENTIALS[:password])
		dbh.select_db(CpxMonConf::MYSQL_CREDENTIALS[:database])

		result = dbh.query("SELECT profile FROM profiles WHERE hidden=1")
		@hiddenprofiles = []
		while row = result.fetch_row
			@hiddenprofiles << row[0].to_i
		end
		result.free

		result = dbh.query("SELECT UniqueID, Data, Start FROM billing_transactions WHERE Transaction=#{CDRConstants::CDRINIT} AND End > #{cutoff}")
		while row = result.fetch_row
			next if @calls[row[0]]
			call = CpxCaller.new(row[0])
			call.type = row[1]
			
			res2 = dbh.query("SELECT Data, Start, End FROM billing_transactions WHERE UniqueID='#{row[0]}' AND Transaction=#{CDRConstants::INQUEUE}")
			if res2.num_rows == 1
				row2 = res2.fetch_row
				call.queue = @queues[row2[0]]
				call.enteredqueue = row2[1].to_i
				call.leftqueue = row2[2].to_i
			else
				call.enteredqueue = row[2].to_i
				call.leftqueue = row[2].to_i
			end
			res2.free
		
			res2 = dbh.query("SELECT Data FROM billing_transactions WHERE UniqueID='#{row[0]}' AND (Transaction=#{CDRConstants::ABANDONQUEUE} OR Transaction=#{CDRConstants::ABANDONIVR})")
			if res2.num_rows == 1
				call.abandoned = true
			end
			res2.free
		
			@calls[row[0]] = call
		end
		result.free
	end

	#are we already connected?
	# TODO - should the connection be killed after each login failure?
	def listening?
		return @listen || false
	end

	def getbrandsettings
		dbh = Mysql.real_connect(CpxMonConf::MYSQL_CREDENTIALS[:host],
														 CpxMonConf::MYSQL_CREDENTIALS[:username],
														 CpxMonConf::MYSQL_CREDENTIALS[:password])
		dbh.select_db(CpxMonConf::MYSQL_CREDENTIALS[:database])

		@brandsettings={}
		@brandsettings['loaded'] = Time.now.to_i
		
		result = dbh.query("SELECT tenant,brand,keyname,value FROM brand_settings ORDER BY tenant,brand,keyname,value")

		result.each_hash do |row|
		  brandid="#{row['tenant'].rjust(4,'0')}#{row['brand'].rjust(4,'0')}"
		  @brandsettings[brandid] = {} if !@brandsettings[brandid]
		  @brandsettings[brandid][row['keyname']] = row['value']
		end

	end

	# send an event to the server
	def send(command, *data, &callback)
		count = next_count
		str = "#{command} #{count} #{data.join(' ')}"
		@sent_events[count] = {:time=>(Time.now.to_i)+300, :event=>str}
		@reply_callbacks[count] = callback
		@outgoing.enq str
	end

	# acknowledge an event
	def ack(counter, data)
		@outgoing.enq "ACK #{counter} #{data}"
	end

	# error in response to a bad event
	def err(counter, error)
		@outgoing.enq "ACK #{counter} #{error}"
	end

	#route an incoming event/reply/error
	#pulls items off the incoming queue
	def route_incoming(event)
		command, counter, data = event.split(' ', 3)

		if ['ACK', 'ERR'].include? command
			@sent_events.delete(counter.to_i)
			@resent_events.delete(counter.to_i)
			result = (command == 'ACK')
			if @reply_callbacks[counter.to_i]
				@reply_callbacks[counter.to_i].call(result, data, event)
				@reply_callbacks.delete(counter.to_i)# remove the callback
			end
		else
			begin
				puts "[1;35m#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}[0;0m [1;32m#{command}[0;0m #{counter} #{data}"
				res = handle_event(command, counter, data)
				res ||= data
			rescue StandardError => e
				puts e.message
				puts e.backtrace
				err(counter, e.message)
			else
				ack(counter, res)
			end
		end
	end

	def handle_event(command, counter, data)
		case command
		when 'STATE'
			time, id, name, state, statedata, profile, calltime, wrapuptime, idletime = data.split(' ')
			return if name == CpxMonConf::USERNAME

			# remove logged out agents
			if state.to_i < CpxAgent::IDLE
				if agent = @agents[id.to_i]
					@agents.delete(id.to_i)
					@queues.each do |x,q|
						q.remove_member(agent)
					end
				else
					return
				end
			elsif !(agent = @agents[id.to_i])
				agent = CpxAgent.new(id.to_i, name)
				@agents[id.to_i] = agent
			end

			agent.time = time.to_i
			agent.state = state.to_i
			agent.statedata = statedata
			agent.profile = profile.to_i
			agent.calltime = calltime.to_i
			agent.wrapuptime = wrapuptime.to_i
			agent.idletime = idletime.to_i

		when 'QUEUE'
			name, group, weight, calls, completed, abandoned, avg_time, max_time = data.split(' ')
			unless queue = @queues[name]
				queue = CpxQueue.new(name, group.to_i)
				@queues[name] = queue
				unless @queuelist[name]
					send('QUEUENAMES') do |result, data, event|
						if result
							data[1..-2].split('),(').each do |e|
								queuename,commonname = e.split('|')
								@queuelist[queuename] = commonname
							end
						end
					end
				end
			end

			queue.calls = calls.to_i
			queue.weight = weight.to_i
			queue.completed = completed.to_i
			queue.abandoned = abandoned.to_i
			queue.avg_time = avg_time.to_i
			queue.max_time = max_time.to_i
		
		when 'QUEUEMEMBER'
			id, name, statetime, state, queues = data.split(' ', 5)
			return if name == CpxMonConf::USERNAME
			#if agent = @agents[id.to_i] #or state.to_i >= CpxAgent::IDLE
			agent = @agents[id.to_i]
				unless agent
					agent = CpxAgent.new(id.to_i, name)
					@agents[id.to_i] = agent
					agent.profile = 0
				end

				agent.state = state.to_i
				agent.time = statetime.to_i
				if queues.nil? or queues.empty?
					queues = []
				else
					queues = queues.split(':')
				end

				@queues.each do |x,q|
					if queues.include? q.name
						q.add_member(agent)
					else
						q.remove_member(agent)
					end
				end
			#end

		when 'QUEUECALLER'
			time, name, uniqueid, type, position, brandid, callerid = data.split(' ')
			if queue = @queues[name]

				unless call = @calls[uniqueid]
					call = CpxCaller.new(uniqueid)
				end
				
				cutoff = (Time.now - 86400).to_i
				# trim old calls out
				@calls = Hash[*(@calls.select{|id,c| c.enteredqueue > cutoff or !c.leftqueue }.flatten)]

				#p call, queue
				unless queue.callers.detect {|c| c.uniqueid == uniqueid}
					@calls[uniqueid] = call
					call.queue = queue
					call.enteredqueue = time.to_i
					call.type = type
					call.brandid = brandid
					call.callerid = callerid
					queue.calls += 1
					queue.add_caller(call, position.to_i)
				end
			else
				puts "Could not find queue #{queue}"
				send('QUEUES', queue)
			end

		when 'QUEUECALLERREM'
			name, uniqueid, completed, abandoned, avg_queuetime, max_queuetime = data.split(' ')
			if queue = @queues[name]
				if call = @calls[uniqueid]
					#p call, queue
					call.leftqueue = Time.now.to_i
					queue.remove_caller(call)
					queue.calls -= 1
					queue.completed = completed.to_i
					if abandoned.to_i == queue.abandoned + 1
						call.abandoned = true
					end
					queue.abandoned = abandoned.to_i
					queue.avg_time = avg_queuetime.to_i
					queue.max_time = max_queuetime.to_i
				else
					puts "Could not find call #{uniqueid}"
				end
			else
				puts "Could not find queue #{queue}"
				send('QUEUES', queue)
			end

		when 'BRIDGECALLER'
			agentid, uniqueid, brandid, callerid = data.split(' ')
			if agent = @agents[agentid.to_i] and call = @calls[uniqueid]
				agent.talkingto = call
			elsif agent and  agent.state == CpxAgent::OUTGOINGCALL
				call = CpxCaller.new(uniqueid)
				@calls[uniqueid] = call
				call.type = 'outgoing'
				call.brandid = brandid
				call.callerid = callerid
				# stub out these values so stuff works
				call.enteredqueue = Time.now.to_i
				call.leftqueue = Time.now.to_i

				agent.talkingto = call
			elsif agent
				call = CpxCaller.new(uniqueid)
				@calls[uniqueid] = call
				call.brandid = brandid
				call.callerid = callerid
				call.enteredqueue = Time.now.to_i
				call.leftqueue = Time.now.to_i

				send('CALLINFO', uniqueid) do |result, data, event|
					brandid, type, callerid, extra = data.split(' ', 4)
					call.type = type
				end

				agent.talkingto = call

			end

		#when 'UNBRIDGECALLER'
			#agentid, uniqueid = data.split(' ')
			#if agent = @agents[agentid.to_i]
			#end
			#

		else
			#puts "#{command} #{counter} #{data}"
		end

	end

	#listen on the connection
	def listen

		@listen = true

		Thread.new do
			Thread.current.priority = 5
			sleep 10
			begin
				counter = 0
				loop do
					if counter == 3
						graph_util
						graph_agentcount
						graph_queuetimes
						counter = 0
					end
					html_output
					xml_output
					nagger
					counter += 1
					sleep 5
				end
			rescue KickAssError => e
				puts e.backtrace
				#retry
			end
		end

			catch :disconnect do
				loop do

					if result = select([@connection], nil, nil, 0.25)
						if socket = result[0][0]
							begin
								string = socket.gets
							rescue Errno::ECONNRESET
								throw :disconnect
							end
							if string
								route_incoming(string.chomp)
							else
								throw :disconnect
							end
						end
					end

					while !@outgoing.empty?
						string = @outgoing.shift
						begin
							@connection.puts string
						rescue Errno::EPIPE
							throw :disconnect
						end
					end

					# TODO - is this the best place for this? I'd prefer not to
					# have *another thread for this

					#check dead events and resend them and put them in the resent
					#events list
					check_dead_events(@sent_events) do |e|
						STDERR.puts "resending event \"#{e}\" because it's older than 10 seconds"
						command, counter, data = e.split(' ', 3)
						newcount = next_count
						str = "#{command} #{newcount} #{data}"
						@outgoing.enq str
						@resent_events[newcount] = {:time=>(Time.now.to_i)+600, :event=>str}
					end

					#check resent events for dead ones
					check_dead_events(@resent_events) do |e|
						#a resent event has failed to return an ack in 60 seconds.
						#this is very bad and indicates something shit itself
						#somewhere along the line.
						STDERR.puts "Event \"#{e}\" was resent and failed to return after 60 seconds"
						# TODO - freak out
					end
				end
			end
			STDERR.puts 'connection died!'
	
		#end

	end

	#abstraction for checking for dead events
	def check_dead_events(events, &resendcallback)
		if events.length > 0 #only bother to check if we've got events
			#select any expired events
			events.select{|k,v| v[:time] < Time.now.to_i}.each do |e|
				events.delete(e[0]) #delete them from this queue
				resendcallback.call(e[1][:event]) #execute the callback
			end
		end
	end

	def nagger
	  t = Time.now.to_i
	  getbrandsettings if !@brandsettings['loaded'] or (t - @brandsettings['loaded'].to_i) > 3600

	  @agents.values.find_all{ |x| (x.state == CpxAgent::ONCALL) or (x.state == CpxAgent::OUTGOINGCALL) }.each do |a|
		brandid = a.talkingto.brandid if a.talkingto
		brandname = @brandlist[brandid]
		agentname = a.name
		talksec = (t - a.time)
		talktime = "#{talksec / 60}:#{talksec % 60}"
		brandname ||= brandid
		if brandid and (@brandsettings[brandid] or @brandsettings['00000000'])
		  if @brandsettings[brandid]
			hlimit = @brandsettings[brandid][BrandSetConst::HLIMIT['value']]
			slimit = @brandsettings[brandid][BrandSetConst::SLIMIT['value']]
			suplimit = @brandsettings[brandid][BrandSetConst::SUPLIMIT['value']]
		  end
		  hlimit = @brandsettings['00000000'] if !hlimit and @brandsettings['00000000'] and @brandsettings['00000000'][BrandSetConst::HLIMIT['value']]
		  slimit = @brandsettings['00000000'] if !slimit and @brandsettings['00000000'] and @brandsettings['00000000'][BrandSetConst::SLIMIT['value']]
		  suplimit = @brandsettings['00000000'] if !suplimit and @brandsettings['00000000'] and @brandsettings['00000000'][BrandSetConst::SUPLIMIT['value']]

		  if ( hlimit or slimit or suplimit )
			if hlimit and (talksec >= hlimit.to_i)
			  if ((t - a.time - hlimit.to_i) % 180) < 5
				message = @brandsettings[0][BrandSetConst::HLMSG['value']] if @brandsettings[0]
				message = @brandsettings[brandid][BrandSetConst::HLMSG['value']] if @brandsettings[brandid] and @brandsettings[brandid][BrandSetConst::HLMSG['value']]
				message = "Your #{brandname} call is running very long, please finish quickly." if !message
				while mt=message.match(/#\{([^\}]*)\}/)
				  message.gsub!(mt[0],eval("#{mt[1]}"))
				end
				send('ABLAB', a.id, message)
				message = "Agent '#{agentname}' is in a '#{brandname}' call and has exceeded the hard call time limit"
				send('ABLAB', 'Supervisor', message)
			  end
			elsif suplimit and ((talksec - suplimit.to_i + 5 ) /5).to_i == 1
				message = @brandsettings[0][BrandSetConst::SUPLMSG['value']] if @brandsettings[0]
				message = @brandsettings[brandid][BrandSetConst::SUPLMSG['value']] if @brandsettings[brandid] and @brandsettings[brandid][BrandSetConst::SUPLMSG['value']]
				message = "Agent '#{agentname}' #{brandname} call has reached #{talktime}, please check on them." if !message
				while mt=message.match(/#\{([^\}]*)\}/)
					message.gsub!(mt[0],eval("#{mt[1]}"))
				end
				send('ABLAB', 'Supervisor', message)
			elsif slimit and ((talksec - slimit.to_i + 5) / 5).to_i == 1  
			  message = @brandsettings[0][BrandSetConst::SLMSG['value']] if @brandsettings[0]
			  message = @brandsettings[brandid][BrandSetConst::SLMSG['value']] if @brandsettings[brandid] and @brandsettings[brandid][BrandSetConst::SLMSG['value']]
			  message = "Your #{brandname} call is running long, please finish quickly." if !message
			  while mt=message.match(/#\{([^\}]*)\}/)
				message.gsub!(mt[0],eval("#{mt[1]}"))
			  end
			  send('ABLAB', a.id, message)
			end
		  end
		end
	  end
	end

	def html_output
	    updatetime = Time.now.utc.to_s
		clientdata = CpxMonConf::CLIENTHTML
		clientdata.each do |c|
		  c['htmldata'] = ''
		  c['calls'] = 0
		  c['xmldata'] = ''
		end
		File.open("#{CpxMonConf::OUTDIR}/#{CpxMonConf::HTMLOUT}.tmp", 'w') do |f|
			f << File.read('templates/mon-header.html')
			f << "\n<!-- updated: #{updatetime} -->\n"
			f << File.read('templates/mon-queues.html')
			clientdata.each do |c|
			  c['htmldata'] += File.read("templates/#{c['client']}/mon-header.html")
			  c['htmldata'] += File.read("templates/#{c['client']}/mon-queues.html")
			  c['htmldata'] += "\n<!-- updated: #{updatetime} -->\n"
			  c['xmldata'] += "<?xml version=\"1.0\"?>\n<fsmonitor>\n<update time='#{updatetime}' />\n<queuedata>\n"
			end
			queues = @queues.values.reject{|x| x.callers.empty? or (@queuegroups[x.group] == "Hidden")}
			t = Time.now.to_i
			# Order queues by weight, calltime of top call and the priority of the top
			# call. This ensures that the call that cbx is trying hardest to push
			# to an agent is at the top of the list.
			queues.sort_by{|x| [x.weight*-1, CALLTYPE_PRIORITIES.index(x.callers[0].type.to_sym).to_i, (x.callers[0].enteredqueue)]}.each do |q|
				longest = q.callers[0]
				queuedata = ''

				styles = "queue='#{q.name}'" +
					 " class='#{"prem" if q.group == @queuegroups.key("Premium")}queue #{@queuegroups[q.group].tr('^a-zA-Z0-9-','_')}group #{q.name} #{longest.type.tr('^a-zA-Z0-9-','_')}'" +
					 " group='#{@queuegroups[q.group]}'" +
					 " count='#{q.callers.length}' time='#{t - longest.enteredqueue}' " +
					 " brandid='#{longest.brandid}' brandname='#{@brandlist[longest.brandid]}'"

				queuedata += "<tr #{styles}><td><span #{styles}>#{@queuelist[q.name]}"
				queuedata += "#{q.name.include?(longest.brandid) ? '' : ' (Brand: '+@brandlist[longest.brandid]+')'}"
				queuedata += "#{longest.type == 'call' ? '' : '('+longest.type+')'}</span></td>"
				queuedata += "<td><span #{styles}>#{q.callers.length}</span></td><td><span #{styles}>#{CpxAgent.calculate_duration(longest.enteredqueue, @timeskew)}"
				queuedata += "</span></td></tr>\n"
				f << queuedata
				clientdata.each do |c| 
				  if c['queues'].include?(q.name) or c['queuegroups'].include?(@queuegroups[q.group]) or (c['queuegroups'][0].downcase == 'all')
					c['htmldata'] += queuedata 
					c['calls'] += 1
					c['xmldata'] += "<queue name='#{@queuelist[q.name]}' calls='#{q.callers.length}'>\n"
					q.callers.each { |call| c['xmldata'] += "<call uniqueid='#{call.uniqueid}' brandname='#{@brandlist[call.brandid]}' type='#{call.type}' time='#{CpxAgent.calculate_duration(call.enteredqueue, @timeskew)}' seconds='#{(Time.now.to_i - (call.enteredqueue + @timeskew).abs).to_i}' callerid='#{call.callerid if call.callerid}' />\n" }
					c['xmldata'] += "</queue>\n"
				  end
				end
			end

			clientdata.each do |c| 
			  c['htmldata'] += "<tr><td colspan='3'>No calls</td></tr>\n" if c['calls'] == 0 
			  c['xmldata'] += "</queuedata>\n<agentdata>\n"
			end

			if queues.empty?
				queuedata = "<tr><td colspan='3'>No calls</td></tr>\n"
				f << queuedata
			end

			f << File.read('templates/mon-agents.html')
			clientdata.each { |c| c['htmldata'] += File.read("templates/#{c['client']}/mon-agents.html") }

			# create an array of arrays which are intended to represent the agent
			# profiles. 
			agentlist = @agents.values.reject { |x| x.state < CpxAgent::IDLE or (@hiddenprofiles.methods.include? 'include?' and @hiddenprofiles.include? x.profile) }.inject([]) do |memo, obj| 
				memo[obj.profile] ||= []; memo[obj.profile] << obj; memo
			end

			# sort the profiles by an arbitrary order dictated by tyrants.
			begin	
				agentlist = agentlist.compact.sort_by{|x| [@profiles[x[0].profile].order]}
			rescue ArgumentError
				puts "WARNING: ArgumentError sorting agentlist:"
			end
			agentlist.each_with_index do |group, profile|
				unless @profiles[group[0].profile].kind_of?(CpxProfile)
					send('PROFILES') do |result, data, event|
						data.split(' ').map{|x| x.split(':', 2)}.each_with_index do |profilearr, index|
							@profiles[profilearr[0].to_i] = CpxProfile.new(profilearr[1], profilearr[0].to_i, index)
						end
					end
				end
				groupdata = "<tr><td colspan='2'>#{@profiles[group[0].profile].name if @profiles[group[0].profile].kind_of?(CpxProfile)}</td></tr>"
				f << groupdata

				clientdata.each { |c| c['htmldata'] += groupdata if c['agentprofiles'].length > 1 and (c['agentprofiles'].include?(@profiles[group[0].profile].name) or (c['agentprofiles'][0].downcase == 'all'))}
				
				group.sort{|a,b| a.name.downcase <=> b.name.downcase}.each do |a|

					agentdata = ''

					if (a.state == CpxAgent::RELEASED) and (a.statedata.to_i != 0)
						unless @releaseoptions[a.statedata.to_i]
							send('RELEASEOPTIONS') do |result, data, event| 
								data.split(',').map{ |x| x.split(':',3)}.each { |o| @releaseoptions[o[0].to_i] = [ o[1], o[2] ] }
							end
						end
					end
  
					styling = "class='agent' agentid='#{a.id}' agentname='#{a.name.downcase}'"
					statename = CpxAgent::STATENAMES[a.state].gsub(' ','-').downcase
					styling += " state='#{statename}' time='#{t - a.time}'"
					styling += " profile='#{@profiles[a.profile].name.downcase}'" if @profiles[a.profile].kind_of?(CpxProfile)
					sd = a.statedata.to_i
					if (a.state == CpxAgent::RELEASED)
					    if @releaseoptions[sd] 
						styling += " ReleasedBias='#{@releaseoptions[sd][1].downcase}'"
						styling += " ReleasedCode='#{@releaseoptions[sd][0].downcase}'"
					    else
						styling += " ReleasedBias='-1'"
						end
					end
					talkbrand = ''
					styling += " uniqueid='#{a.talkingto.uniqueid}'" if  ((a.talkingto) and (a.talkingto.uniqueid))
					styling += " calltype='#{a.talkingto.type}'" if  ((a.talkingto) and (a.talkingto.type))
					if a.talkingto and a.talkingto.brandid
						if @brandlist[a.talkingto.brandid]
							talkbrand += @brandlist[a.talkingto.brandid]
						else
							talkbrand += a.talkingto.brandid
							send('BRANDLIST') do |result, data, event|
								data[1..-2].split('),(').map{|x| x.split('|')}.sort_by{|x| [x[1].downcase]}.each do |brandid, brandlabel|
									@brandlist[brandid] = brandlabel
								end
							end
						end
						styling += " talkingto='#{talkbrand}'"
					end

					agentdata += "<tr #{styling}><td>&nbsp;&nbsp;"
					agentdata += "<span #{styling}>#{a.name}</span></td>"
					agentdata += "<td><span #{styling}>#{CpxAgent::STATENAMES[a.state]}#{"(#{a.talkingto.type})" if ((a.talkingto) and (a.talkingto.type) and (a.talkingto.type != 'call') and !(CpxAgent::STATENAMES[a.state].match(/^#{a.talkingto.type}/i)))}"
					agentdata += "#{"(#{@releaseoptions[sd][0].gsub(' ','-')})" if (a.state == CpxAgent::RELEASED) and @releaseoptions[sd]}"

					agentdata += " #{talkbrand}</span></td>"

					agentdata += "<td><span #{styling}>#{CpxAgent.calculate_duration(a.time, @timeskew)}</span></td></tr>\n"
					f << agentdata
					clientdata.each do |c| 
					  if (c['agents'].include?(a.id) or c['agentprofiles'].include?(@profiles[a.profile].name) or (c['agentprofiles'][0].downcase == 'all'))
						c['htmldata'] += agentdata
						c['xmldata'] += "<agent name='#{a.name}' id='#{a.id}' profile='#{@profiles[a.profile].name if @profiles[a.profile].kind_of?(CpxProfile)}'  state='#{CpxAgent::STATENAMES[a.state]}' time='#{CpxAgent.calculate_duration(a.time, @timeskew)}' seconds='#{(Time.now.to_i - (a.time + @timeskew).abs).to_i}' #{"calltype='#{a.talkingto.type}'" if a.talkingto and a.talkingto.type } #{"queuetime='#{((a.talkingto.leftqueue - a.talkingto.enteredqueue + @timeskew).abs)}'" if a.talkingto and a.talkingto.type == 'call' and a.talkingto.enteredqueue and a.talkingto.leftqueue } talkingto='#{@brandlist[a.talkingto.brandid] if a.talkingto and a.talkingto.brandid}' uniqueid='#{a.talkingto.uniqueid if  ((a.talkingto) and (a.talkingto.uniqueid))}' callerid='#{a.talkingto.callerid if a.talkingto and a.talkingto.callerid}' />\n"
					  end
					end
				end
			end

			# client html output ends here
			clientdata.each do |c| 
			  File.open("#{c['htmlfile']}.tmp",'w') { |cf| cf << c['htmldata'] << File.read("templates/#{c['client']}/mon-footer.html") }
			  File.rename("#{c['htmlfile']}.tmp", c['htmlfile'])
			  if c['xmlfile']
				File.open("#{c['xmlfile']}.tmp",'w') { |xf| xf << c['xmldata'] << "</agentdata>\n</fsmonitor>" }
				File.rename("#{c['xmlfile']}.tmp",c['xmlfile'])
			  end
			end


			f << File.read('templates/mon-trends.html')

			# NOTE: from here on down things become awful, I'm sorry

			round = Time.now + (CpxMonConf::TREND_INTERVAL - (Time.now.to_i % CpxMonConf::TREND_INTERVAL)) # closest next 1/2 hour
			cutoff = round - (CpxMonConf::TREND_INTERVAL * CpxMonConf::TREND_SAMPLES) # get the cutoff point
			times = {}
			# populate the keys with the END of each interval
			CpxMonConf::TREND_SAMPLES.times do |x|
			times[round - (x * CpxMonConf::TREND_INTERVAL)] = []
			end

			# remove the calls still in queue and the ones that are older than the cutoff
			@calls.values.reject{|x| x.leftqueue.nil? or Time.at(x.leftqueue) < cutoff}.each do |c|
				times[times.keys.sort.detect{|x| c.leftqueue < x.to_i}] << c
			end

			# draw the header line
			f << "<tr><td class='header'>Group</td>"
			times.keys.sort.each do |t|
				f << "<td class='trendheader'>#{(t-CpxMonConf::TREND_INTERVAL).strftime('%H:%M')}-#{t.strftime('%H:%M')}</td>"
			end

			f << '</tr>'
			
			# get a list of groups that have calls we're interested in
			groups = @calls.values.reject{|x| x.queue.nil? or x.leftqueue.nil? or Time.at(x.leftqueue) < cutoff}.map{|x| x.queue.group}.uniq

			groups.sort.each do |group|
				# apparently you can't cleanly run reject on a hash and get a hash back :(
				times2 = Hash[*(times.map{|k,v| [k,v.reject{|y| y.queue.nil? or y.queue.group != group}]}.inject([]){|sum,n| n.kind_of?(Array) ? sum + n : sum << n})]
				
				next if times2.empty?
				
				# generate a trending row for this group
				f << "<tr><td>#{@queuegroups[group]}</td>"
				times2.keys.sort.each do |time|
					if times2[time].empty?
						avg = max = 0.0
						f << "<td>&nbsp;</td>"
					else
						avg = (times2[time].inject(0){|memo, obj| memo + (obj.leftqueue - obj.enteredqueue)} / times2[time].length)
						max = (times2[time].map{|x| x.leftqueue - x.enteredqueue}.max)
						#f << "<td>#{times2[time].length} (#{sprintf('%.2f',avg)}/#{sprintf('%.2f', max)})</td>"
						# Eschew metric time in deference to the ancient, capricious babylonians and their fetish for base 60
						f << "<td class='trenddata'>#{times2[time].length} (#{sprintf('%d:%02d', (avg / 60), (avg % 60))}/#{sprintf('%d:%02d', (max / 60), (max % 60))})</td>"
					end
				end
				f << '</tr>'
			end

			f << File.read('templates/mon-footer.html')
		end

		File.rename("#{CpxMonConf::OUTDIR}/#{CpxMonConf::HTMLOUT}.tmp", "#{CpxMonConf::OUTDIR}/#{CpxMonConf::HTMLOUT}")

	end

	def xml_output
		outputgroups = {}

		# XXX: This is absolutely awful: these are the equivalent of magic numbers.
		# We put L1 and premium in the same group due to legacy formatting
		#outputgroups[1] = @queues.values.select{|x| x.group == @queuegroups.key("Level1") or x.group == @queuegroups.key("Premium")}
		#outputgroups[2] = @queues.values.select{|x| x.group == @queuegroups.key("Level2")}
		#outputgroups[3] = @queues.values.select{|x| x.group == @queuegroups.key("Level3")}
		outputgroups[1] = @queues.values.select{|x| x.name == "L1-00010001-q"}
		outputgroups[2] = @queues.values.select{|x| x.name == "CE-00010002-q"}
		outputgroups[3] = @queues.values.select{|x| x.name == "VIP-00010003-q"}
		outputgroups[4] = @queues.values.select{|x| x.group == @queuegroups.key("Default")}

		File.open("#{CpxMonConf::OUTDIR}/#{CpxMonConf::XMLOUT}.tmp", 'w') do |f|
			f.puts '<?xml version="1.0"?>'
			f.puts '<fsmonitor>'
			f.puts '<queuedata>'
			outputgroups.each do |k,v|
				calls = v.inject(0){|memo, obj| memo + obj.callers.length}
				# get the oldest call in queue's timestamp
				if longest = v.reject{|x| x.callers.select{|c| c.type == 'call'}.empty?}.map{|x| x.callers.detect{|c| c.type == 'call'}.enteredqueue}.min
					longest = (Time.now - longest).to_f
				else
					longest = 0.0
				end
				longest = sprintf('%d:%02d', (longest / 60), (longest % 60))
				f.puts "<queue id=\"#{k}\" calls=\"#{calls}\" longest=\"#{longest}\" />"
			end
			f.puts '</queuedata>'
			f.puts '<agentstates>'
			# This block is super nasty and extremely arbitrary
			# Find the values here in sql table 'profiles'
			# magic numbers: 1 = IT_Staff, 2 = IT_Student, 3 = All, 5 = Supervisor
			# This lets us put 7 categories of agents into 4 groups while preventing double-counting them.
			agentgroup = {}
			agentgroup[1] = [1]
			agentgroup[2] = [2]
			agentgroup[3] = [3]
			agentgroup[4] = [7]
			# Example of multiples into one group:
			#agentgroup[4] = [5,7,9]

			outputgroups.each do |index, queues|
				agents = queues.inject([]){|memo, queue| memo + queue.members}.uniq.select{|agent| agent.state >= CpxAgent::IDLE and agentgroup[index].include?(agent.profile)}
				av = agents.select{|x| [CpxAgent::IDLE, CpxAgent::RINGING].include? x.state}
				ic = agents.select{|x| [CpxAgent::ONCALL, CpxAgent::OUTGOINGCALL, CpxAgent::WARMXFER].include? x.state}
				re = agents.select{|x| x.state == CpxAgent::RELEASED}
				un = agents.select{|x| x.state == CpxAgent::WRAPUP}

				f.puts "<queue id=\"#{index}\" total=\"#{agents.length}\">"
				f.puts "<state name=\"AV\" percentage=\"#{av.length}\" />"
				f.puts "<state name=\"IN\" percentage=\"#{ic.length}\" />"
				f.puts "<state name=\"RE\" percentage=\"#{re.length}\" />"
				f.puts "<state name=\"WR\" percentage=\"#{un.length}\" />"
				f.puts "</queue>"
			end
			f.puts '</agentstates>'
			f.puts '<callstats>'
			f.puts '<period name="1H">'
			cutoff = (Time.now - 3600).to_i
			lasthour = @calls.values.select{|x| x.leftqueue and x.leftqueue > cutoff}
			if lasthour.empty?
				abandonpercent = 0
			else
				abandonpercent = ((lasthour.select{|x| x.abandoned}.length/(lasthour.length.to_f))*100).to_i
			end
			f.puts "<stat name=\"IN\" value=\"#{lasthour.select{|x| x.type != 'outgoing'}.length}\" />"
			f.puts "<stat name=\"OUT\" value=\"#{lasthour.select{|x| x.type == 'outgoing'}.length}\" />"
			f.puts "<stat name=\"ABN\" value=\"#{abandonpercent}%\" />"
			f.puts '</period>'

			f.puts '<period name="24H">'
			cutoff = (Time.now - 86400).to_i
			lastday = @calls.values.select{|x| x.leftqueue and x.leftqueue > cutoff}
			if lastday.empty?
				abandonpercent = 0
			else
				abandonpercent = ((lastday.select{|x| x.abandoned}.length/(lastday.length.to_f))*100).to_i
			end
			f.puts "<stat name=\"IN\" value=\"#{lastday.select{|x| x.type != 'outgoing'}.length}\" />"
			f.puts "<stat name=\"OUT\" value=\"#{lastday.select{|x| x.type == 'outgoing'}.length}\" />"
			f.puts "<stat name=\"ABN\" value=\"#{abandonpercent}%\" />"
			f.puts '</period>'
			f.puts '</callstats>'

			problems = Dir["#{CpxMonConf::PROBLEM_FOLDER}/**/problem.wav"]
			f.puts "<globalissues count=\"#{problems.length}\">"
			problems.each do |x|
				brandid = File.dirname(x).split('/')[-1][1..-1]
				if @brandlist[brandid]
					f.puts "<issue client=\"#{@brandlist[brandid]}\">has an IVR posted</issue>"
				else
					puts "Cannot determine brand name for #{brandid}"
				end
			end
			f.puts '</globalissues>'
			
			# Level 1
			ict = @agents.values.select{|x| x.profile <= 2 and [CpxAgent::ONCALL, CpxAgent::OUTGOINGCALL].include?(x.state) and x.time < (Time.now - ICT).to_i}
			wrapt = @agents.values.select{|x| x.profile <= 2 and x.state == CpxAgent::WRAPUP and x.time < (Time.now - WRAPT).to_i}
			ret = @agents.values.select{|x| x.profile <= 2 and x.state == CpxAgent::RELEASED and x.time < (Time.now - RET).to_i}

			# Level 2
			icd = @agents.values.select{|x| x.profile == 3 and [CpxAgent::ONCALL, CpxAgent::OUTGOINGCALL].include?(x.state) and x.time < (Time.now - ICT).to_i}
			wrapd = @agents.values.select{|x| x.profile == 3 and x.state == CpxAgent::WRAPUP and x.time < (Time.now - WRAPT).to_i}
			red = @agents.values.select{|x| x.profile == 3 and x.state == CpxAgent::RELEASED and x.time < (Time.now - RET).to_i}
		
			# Level 3
			icp = @agents.values.select{|x| x.profile == 4 and [CpxAgent::ONCALL, CpxAgent::OUTGOINGCALL].include?(x.state) and x.time < (Time.now - ICP).to_i}
			wrapp = @agents.values.select{|x| x.profile == 4 and x.state == CpxAgent::WRAPUP and x.time < (Time.now - WRAPP).to_i}
			rep = @agents.values.select{|x| x.profile == 4 and x.state == CpxAgent::RELEASED and x.time < (Time.now - REP).to_i}

			# Premium Calls
			prems = @queues.values.select { |q| q.name[0].chr == 'P' and q.callers.length > 0 }

			# Many Calls
			mcalls = @queues.values.select { |q| q.callers.select{|c| c.type == 'call'}.length > 3 } 

			f.puts "<agentalerts count=\"#{ict.length + wrapt.length + ret.length + icp.length + wrapp.length + rep.length + prems.length + mcalls.length}\">"

			ict.each do |x|
				f.print '<issue type="AGENT" queue="L1">'
				f.print "#{x.name} In-Call #{(Time.now - x.time).to_i / 60} min."
				f.puts '</issue>'
			end

			wrapt.each do |x|
				f.print '<issue type="AGENT" queue="L1">'
				f.print "#{x.name} Wrap-Up #{(Time.now - x.time).to_i / 60} min."
				f.puts '</issue>'
			end
	
			ret.each do |x|
				f.print '<issue type="AGENT" queue="L1">'
				f.print "#{x.name} Released #{(Time.now - x.time).to_i / 60} min."
				f.puts '</issue>'
			end

			icd.each do |x|
				f.print '<issue type="AGENT" queue="L2">'
				f.print "#{x.name} In-Call #{(Time.now - x.time).to_i / 60} min."
				f.puts '</issue>'
			end

			wrapd.each do |x|
				f.print '<issue type="AGENT" queue="L2">'
				f.print "#{x.name} Wrap-Up #{(Time.now - x.time).to_i / 60} min."
				f.puts '</issue>'
			end
	
			red.each do |x|
				f.print '<issue type="AGENT" queue="L2">'
				f.print "#{x.name} Released #{(Time.now - x.time).to_i / 60} min."
				f.puts '</issue>'
			end
	
			icp.each do |x|
				f.print '<issue type="AGENT" queue="L3">'
				f.print "#{x.name} In-Call #{(Time.now - x.time).to_i / 60} min."
				f.puts '</issue>'
			end

			wrapp.each do |x|
				f.print '<issue type="AGENT" queue="L3">'
				f.print "#{x.name} Wrap-Up #{(Time.now - x.time).to_i / 60} min."
				f.puts '</issue>'
			end
	
			rep.each do |x|
				f.print '<issue type="AGENT" queue="L3">'
				f.print "#{x.name} Released #{(Time.now - x.time).to_i / 60} min."
				f.puts '</issue>'
			end

			prems.each do |q|
				f.puts "<issue type='CLIENT' queue='Q'>#{CGI.escapeHTML(@queuelist[q.name])} has a premium call.</issue>" 
			end

			mcalls.each do |q|
				f.puts "<issue type='CLIENT' queue='T'>#{CGI.escapeHTML(@queuelist[q.name])} has #{q.callers.length} calls.</issue>"
			end

			f.puts '</agentalerts>'
			f.puts '</fsmonitor>'

		end

		File.rename("#{CpxMonConf::OUTDIR}/#{CpxMonConf::XMLOUT}.tmp", "#{CpxMonConf::OUTDIR}/#{CpxMonConf::XMLOUT}")
	end

	def graph_util
		return if $NORRD
		agents = {}
		agents['Level1'] = @agents.values.select{|x| x.profile <= 2}
		agents['Level2'] = @agents.values.select{|x| x.profile == 3}
		agents['Level3'] = @agents.values.select{|x| x.profile == 4}
		agents['Magic'] = @agents.values.select{|x| x.profile == 9}
		agents['Graveyard'] = @agents.values.select{|x| x.profile == 7}

		utilization = {}
		compositegood = 0
		compositetotal = 0

		agents.each do |k,v|
			if v.empty?
				utilization[k] = 0
				next
			end

			inc = v.inject(0){|memo, obj| memo + obj.calltime}
			wra = v.inject(0){|memo, obj| memo + obj.wrapuptime}
			idl = v.inject(0){|memo, obj| memo + obj.idletime}

			v.each do |x|
				case x.state
				when CpxAgent::ONCALL, CpxAgent::OUTGOINGCALL, CpxAgent::WARMXFER
					inc += Time.now.to_i - x.time
				when CpxAgent::WRAPUP
					wra += Time.now.to_i - x.time
				when CpxAgent::IDLE, CpxAgent::RINGING
					idl += Time.now.to_i - x.time
				when CpxAgent::RELEASED
					if foo = @releaseoptions[x.statedata.to_i]
						if (foo[1].to_i) < 0
							idl += Time.now.to_i - x.time
						elsif (foo[1].to_i) > 0
							inc += Time.now.to_i - x.time
						end
					else
						idl += Time.now.to_i - x.time
					end
				end
			end

			goodtime = (inc + (wra*0.8))
			total = inc + wra + idl

			compositegood += goodtime
			compositetotal += total

			begin
				utilization[k] = ((goodtime/total)*100).to_i
			rescue FloatDomainError => e
				STDERR.puts "#{e.class} #{e.message} in utilization calculation! (incall: #{inc} wrapup: #{wra} idle: #{idl})"
				utilization[k] = 0
			end
		end

		count = agents.reject{|x,y| y.empty?}.length

		if count == 0
			composite = 0
		else
			begin
				composite = ((compositegood/compositetotal)*100).to_i
			rescue FloatDomainError => e
				composite = 0
			end
			#composite = utilization.values.inject(0){|memo, obj| memo + obj} / count
		end

		RRD.update("#{CpxMonConf::OUTDIR}/data.rrd", "#{(Time.now.to_i - ((Time.now.to_i - 5) % 15))}:#{agents['Level1'].empty? ? 'U' : utilization['Level1']}:#{agents['Level2'].empty? ? 'U' : utilization['Level2']}:#{agents['Level3'].empty? ? 'U' : utilization['Level3']}:#{agents['Magic'].empty? ? 'U' : utilization['Magic']}:#{agents['Graveyard'].empty? ? 'U' : utilization['Graveyard']}:#{composite}")
	end

	def graph_agentcount
		return if $NORRD
		agents = @agents.values.select{|x| [0,1,2,3,4,9,7].include? x.profile}
		calls = agents.map{|x| x.talkingto}.compact + @calls.values.select{|x| x.leftqueue.nil?}
		calls.reject!{|x| x.queue and x.queue.name =~ /^(CSR|CLEC|OUT)/ }
		RRD.update("#{CpxMonConf::OUTDIR}/agentcalls.rrd", "#{(Time.now.to_i - ((Time.now.to_i - 5) % 15))}:#{agents.empty? ? 'U' : agents.length}:#{calls.empty? ? 'U' : calls.length}")
	end

	def graph_queuetimes
		return if $NORRD
		abandoned = @calls.values.select{|x| x.abandoned}
		level1 = @calls.values.select{|x| x.queue and @queuegroups[x.queue.group] == 'Level1' and x.type == 'call'}
		level2 = @calls.values.select{|x| x.queue and @queuegroups[x.queue.group] == 'Level2' and x.type == 'call'}
		level3 = @calls.values.select{|x| x.queue and @queuegroups[x.queue.group] == 'Level3' and x.type == 'call'}
		premium = @calls.values.select{|x| x.queue and @queuegroups[x.queue.group] == 'Premium' and x.type == 'call'}

		level1avg = level1.empty? ? 'U' : level1.map{|x| ((x.leftqueue ? x.leftqueue : Time.now) - x.enteredqueue).to_i}.inject{|x,m| x+m}/level1.length.to_f
		level2avg = level2.empty? ? 'U' : level2.map{|x| ((x.leftqueue ? x.leftqueue : Time.now) - x.enteredqueue).to_i}.inject{|x,m| x+m}/level2.length.to_f
		level3avg = level3.empty? ? 'U' : level3.map{|x| ((x.leftqueue ? x.leftqueue : Time.now) - x.enteredqueue).to_i}.inject{|x,m| x+m}/level3.length.to_f
		premiumavg = premium.empty? ? 'U' : premium.map{|x| ((x.leftqueue ? x.leftqueue : Time.now) - x.enteredqueue).to_i}.inject{|x,m| x+m}/premium.length.to_f

		composite = level1+premium+level2+level3
		compositeavg = composite.empty? ? 'U' : composite.map{|x| ((x.leftqueue ? x.leftqueue : Time.now) - x.enteredqueue).to_i}.inject{|x,m| x+m}/composite.length.to_f 

		cutoff = (Time.now - 900).to_i
		level1rec = level1.select{|x| x.leftqueue.nil? or x.leftqueue > cutoff}
		premiumrec = premium.select{|x| x.leftqueue.nil? or x.leftqueue > cutoff}
		level2rec = level2.select{|x| x.leftqueue.nil? or x.leftqueue > cutoff}
		level3rec = level3.select{|x| x.leftqueue.nil? or x.leftqueue > cutoff}

		level1recavg = level1rec.empty? ? 'U' : level1rec.map{|x| ((x.leftqueue ? x.leftqueue : Time.now) - x.enteredqueue).to_i}.inject{|x,m| x+m}/level1rec.length.to_f
		premiumrecavg = premiumrec.empty? ? 'U' : premiumrec.map{|x| ((x.leftqueue ? x.leftqueue : Time.now) - x.enteredqueue).to_i}.inject{|x,m| x+m}/premiumrec.length.to_f
		level2recavg = level2rec.empty? ? 'U' : level2rec.map{|x| ((x.leftqueue ? x.leftqueue : Time.now) - x.enteredqueue).to_i}.inject{|x,m| x+m}/level2rec.length.to_f
		level3recavg = level3rec.empty? ? 'U' : level3rec.map{|x| ((x.leftqueue ? x.leftqueue : Time.now) - x.enteredqueue).to_i}.inject{|x,m| x+m}/level3rec.length.to_f

		compositerec = level1rec+premiumrec+level2rec+level3rec
		compositerecavg = compositerec.empty? ? 'U' : compositerec.map{|x| ((x.leftqueue ? x.leftqueue : Time.now) - x.enteredqueue).to_i}.inject{|x,m| x+m}/compositerec.length.to_f 

		abandonedpercent = abandoned.empty? ? 'U' : (abandoned.length/@calls.length.to_f) * 100

		RRD.update("#{CpxMonConf::OUTDIR}/queuetimes.rrd", "#{(Time.now.to_i - ((Time.now.to_i - 5) % 15))}:#{abandoned.empty? ? 'U' : abandoned.length}:#{abandonedpercent}:#{level1avg}:#{premiumavg}:#{level2avg}:#{level3avg}")
		RRD.update("#{CpxMonConf::OUTDIR}/queuetimes2.rrd", "#{(Time.now.to_i - ((Time.now.to_i - 5) % 15))}:#{level1recavg}:#{premiumrecavg}:#{level2recavg}:#{level3recavg}:#{compositeavg}:#{compositerecavg}")
	end

	def initrrd
		return if $NORRD
		if !File.exist?("#{CpxMonConf::OUTDIR}/data.rrd")
			puts "Can't find data.rrd, creating!"
			RRD.create( "#{CpxMonConf::OUTDIR}/data.rrd", "--start", "#{Time.now.to_i - 15}", "--step", "15", 
									"DS:Level1:GAUGE:15:0:100", 
									"DS:Level2:GAUGE:15:0:100", 
									"DS:Level3:GAUGE:15:0:100", 
									"DS:Magic:GAUGE:15:0:100", 
									"DS:Graveyard:GAUGE:15:0:100", 
									"DS:Composite:GAUGE:15:0:100",
									"RRA:LAST:0.5:1:6307200")
		end

		if !File.exist?("#{CpxMonConf::OUTDIR}/agentcalls.rrd")
			puts "Can't find agentcalls.rrd, creating!"
			RRD.create( "#{CpxMonConf::OUTDIR}/agentcalls.rrd", "--start", "#{Time.now.to_i - 15}", "--step", "15", 
									"DS:Agents:GAUGE:30:0:100", 
									"DS:Calls:GAUGE:30:0:100", 
									"RRA:LAST:0.5:1:6307200")
		end


		if !File.exist?("#{CpxMonConf::OUTDIR}/queuetimes.rrd")
			puts "Can't find queuetimes.rrd, creating!"
			RRD.create( "#{CpxMonConf::OUTDIR}/queuetimes.rrd", "--start", "#{Time.now.to_i - 15}", "--step", "15", 
									"DS:Abandoned:COUNTER:30:0:1000", 
									"DS:AbandonedPercent:GAUGE:30:0:100", 
									"DS:Level1:GAUGE:30:0:1800", 
									"DS:Premium:GAUGE:30:0:1800", 
									"DS:Level2:GAUGE:30:0:1800", 
									"DS:Level3:GAUGE:30:0:1800", 
									"RRA:LAST:0.5:1:6307200")
		end

		if !File.exist?("#{CpxMonConf::OUTDIR}/queuetimes2.rrd")
			puts "Can't find queuetimes2.rrd, creating!"
			RRD.create( "#{CpxMonConf::OUTDIR}/queuetimes2.rrd", "--start", "#{Time.now.to_i - 15}", "--step", "15", 
									"DS:Level1Rec:GAUGE:30:0:1800", 
									"DS:PremiumRec:GAUGE:30:0:1800", 
									"DS:Level2Rec:GAUGE:30:0:1800", 
									"DS:Level3Rec:GAUGE:30:0:1800", 
									"DS:Composite:GAUGE:30:0:1800", 
									"DS:CompositeRec:GAUGE:30:0:1800", 
									"RRA:LAST:0.5:1:6307200")
		end
	end
end

trap('SIGINT') do
	puts caller
	Thread.list.each do |t|
		next if t == Thread.current
		puts "Thread #{t.inspect}"
		t.raise(Main::KickAssError)
	end
	exit
end

trap('SIGTERM') do
	puts caller
	Thread.list.each do |t|
		next if t == Thread.current
		puts "Thread #{t.inspect}"
		t.raise(Main::KickAssError)
	end
	exit
end

begin
	#get the show on the road...
	$main = Main.new
rescue SystemExit
	# NOOP
rescue Exception => e # rescue *everything*
	puts "Encountered unhandled exception of type: #{e.class}"
	puts e.message
	puts e.backtrace
	exit 1
end

