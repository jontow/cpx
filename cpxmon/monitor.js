var req; 
var reloadtimer;
var shifttimer;
var refreshtimer;
var agentflash;
var queueflash;
var qflash = new Array(false,false,false,false);
var shiftdelay = 300000;
var refreshdelay = shiftdelay * 3;
var qAlertTime = 5;
var statdiv = document.getElementById("statdiv");
var globaldiv = document.getElementById("globaldiv");
var agentdiv = document.getElementById("agentdiv");
var globaldatadiv = document.getElementById("globaldatadiv");
var globallabeldiv = document.getElementById("globallabeldiv");
var globalcountdiv = document.getElementById("globalcountdiv");
var agentdatadiv = document.getElementById("agentdatadiv");
var agentlabeldiv = document.getElementById("agentlabeldiv");
var agentcountdiv = document.getElementById("agentcountdiv"); 
var cqlabeldiv = document.getElementById("curqlabeldiv");
var cqdatadiv = document.getElementById("curqdatadiv");
var calabeldiv = document.getElementById("curalabeldiv");
var cadatadiv = document.getElementById("curadatadiv");
var hlabeldiv = document.getElementById("histlabeldiv");
var hdatadiv = document.getElementById("histdatadiv");

var resizelist = new Array(globallabeldiv,globalcountdiv,agentlabeldiv,agentcountdiv,cqlabeldiv,calabeldiv,hlabeldiv,globaldatadiv,agentdatadiv,cqlabeldiv,calabeldiv,hlabeldiv);
//var resizetable = new Array(cqdatadiv,cadatadiv,hdatadiv);
var resizetable = new Array(cqdatadiv,cadatadiv);


var ie = ( navigator.userAgent.indexOf("MSIE",0) > 0);

function setmargin(target,el) {
 	widthdiff = target.offsetWidth - el.offsetWidth - 2;
	if (widthdiff > 1) {
		el.style.marginLeft = parseInt(widthdiff/2);
		el.style.marginRight = parseInt(widthdiff/2);
	} else {
		el.style.marginLeft = 0;
		el.style.marginRight = 0;
	}
 	heightdiff = target.offsetHeight - el.offsetWidth - 2;
	if (heightdiff > 1) {
		el.style.marginTop = parseInt(heightdiff/2);
		el.style.marginBottom = parseInt(heightdiff/2);
	} else {
		el.style.marginTop = 0;
		el.style.marginBottom = 0;
	}  
}

function sizetable(target) {
 	var table = target.getElementsByTagName("table")[0];
	var i = 1;
	var step = 1;
	if (target.offsetHeight < table.offsetHeight || target.offsetWidth < table.offsetWidth) step = -1;
	step = step * Math.ceil(table.offsetHeight / 10);
	table.style.fontSize = "" + i;
	table.style.margin=0;

	var working = true;
	var dx = 1;
	var dy = 1;
	var oldstep = step;
	var count = 0;
	var totalcount = 0;
	var switchcount = 0;
	var ht = target.offsetHeight;
	var wd = target.offsetWidth;
	while (working == true) {
		totalcount = totalcount + 1;
		if (totalcount > 200) working = false;
		i = i + step;
		table.style.fontSize = "" + i;
		if (count > 2) step = step * 2;
		dx = wd - table.offsetWidth;
		dy = ht - table.offsetHeight;
		if (dx >= 0 && dy >= 0) {
			step = Math.abs(step);
		} else {
			step = 0 - Math.abs(step);
		}
		if (step != oldstep) {
			tmp = Math.ceil(step / 2);
			if (step >= 0 && count == 0 && step < 3) working = false;
			oldstep = step;
			count = 0;
			switchcount = switchcount + 1;
			step = tmp;
		} else {
			count = count + 1;
		}

	};

     /*
	while (table.offsetHeight <= target.offsetHeight && table.offsetWidth <= target.offsetWidth && i < 200) {
		i = i + step;
		table.style.fontSize=i;
	};
	*/
	table.style.fontSize = i - step;
	setmargin(target,table);
}

function iesizetext(target) {
	target.style.fontSize = 1;
	var i = 1;
	wd = target.offsetWidth;
	ht = target.offsetHeight;

	working = true;
	step = 25;
	dx = 1;
	dy = 1;
	oldstep = step;
	count = 0;
	totalcount = 0;
	switchcount = 0;
	
	while (working == true) {
		totalcount = totalcount + 1;
		if (totalcount > 20) working = false;
		i = i + step;
		target.style.fontSize = i;
		if (count > 2) step = step * 2;
		dx = wd - target.offsetWidth;
		dy = ht - target.offsetHeight;
		if (dx >= 0 && dy >= 0) {
			step = Math.abs(step);
		} else {
			step = 0 - Math.abs(step);
		}
		if (step != oldstep) {
			tmp = Math.ceil(step / 2);
			if (step >= 0 && count == 0 && step < 3) working = false;
			oldstep = step;
			count = 0;
			switchcount = switchcount + 1;
			step = tmp;
		} else {
			count = count + 1;
		}

	};

	/*
	while ( target.offsetWidth <= wd && target.offsetHeight <= ht && i < 200 ) {
		i = i + 1;
		target.style.fontSize = i;
	}
	i = i - 1;
     */ 
	target.style.fontSize = i;
}

function sizetext(target) { 
	var rulerSpan = document.getElementById("ruler");
//	var rulerSpan = document.getElementById("target");
	rulerSpan.innerHTML = target.innerHTML.replace(/<br>/,"&nbsp;&nbsp;<br>") + "&nbsp;&nbsp;";
	rulerSpan.style.font = target.style.font;
	rulerSpan.style.fontWeight = target.style.fontWeight;
	rulerSpan.style.whiteSpace = 'nowrap';
        sz = parseInt(target.style.fontSize);
	if (sz > 0) {
		var i = sz;
	} else {
		var i = 1;
	}
	var step = parseInt(parseInt(rulerSpan.offsetHeight) / 10) + 1;
	rulerSpan.style.fontSize = i;
	if (target.innerHTML.length > 0) {

		var working = true;
		var dx = 1;
		var dy = 1;
		var oldstep = step;
		var count = 0;
		var totalcount = 0;
		var switchcount = 0;
		var ht = target.offsetHeight;
		var wd = target.offsetWidth;
		while (working == true) {
			ht = target.offsetHeight;
			wd = target.offsetWidth;
			totalcount = totalcount + 1;
			if (totalcount > 200) working = false;
			i = i + step;
			rulerSpan.style.fontSize = "" + i;
			dx = wd - rulerSpan.offsetWidth;
			dy = ht - rulerSpan.offsetHeight;
			if (dx >= 0 && dy >= 0) {
				step = Math.abs(step);
			} else {
				step = 0 - Math.abs(step);
			}
			if (step != oldstep) {
				tmp = Math.ceil(step / 2);
				if (step >= 0 && count == 0 && step < 3) working = false;
				count = 0;
				switchcount = switchcount + 1;
				step = tmp;
				if (step == 0) working = false;
			} else {
				count = count + 1;
				if (count > 2) step = step * 2;
			}
			oldstep = step;

		};
		/*
		while (rulerSpan.offsetHeight <= target.offsetHeight && rulerSpan.offsetWidth <= target.offsetWidth && i < 200) {
			i = i + 1;
			rulerSpan.style.fontSize=i;
		};
		*/
	};
	target.style.fontSize = i - 1;
	rulerSpan.style.fontSize = i - 1;
}

function xmlhandler() {
	if (this.readyState == this.DONE) {
		if (this.status == 200 &&
		    this.responseXML != null) {
			req.responseXML = this.responseXML;
			displaymain();
		}
	}

	return false;
}

function loadit(url) {
	url += "?rnd=" + (new Date()).getTime();
	if (window.XMLHttpRequest) {
		req = new XMLHttpRequest();
		req.onreadystatechange = xmlhandler;
		req.open("GET",url);
	}
	else
	{
		req = new ActiveXObject("Microsoft.XMLHTTP");
		req.open("GET",url, false);
	}
	req.send("");
}
 
function shiftscreen() {
  switch (statdiv.className) {
	case 'statdiv1':
	  statdiv.className='statdiv2';
	  agentdiv.className='agentdiv2';
	  globaldiv.className='globaldiv2';
	  break;
	case 'statdiv2':
	  statdiv.className='statdiv3';
	  agentdiv.className='agentdiv3';
	  globaldiv.className='globaldiv3';
	  break;
	case 'statdiv3':
	  statdiv.className='statdiv1';
	  agentdiv.className='agentdiv1';
	  globaldiv.className='globaldiv1';
	  break;
  }
/*	if (statdiv.style.top == '') {
		statdiv.style.top = '0';
		globaldiv.style.top = '33%';
		agentdiv.style.top = '66%';
		}
	var tp = statdiv.style.top;
	statdiv.style.top = globaldiv.style.top;
	globaldiv.style.top = agentdiv.style.top;
	agentdiv.style.top = tp;*/
	shifttimer = setTimeout("shiftscreen();", shiftdelay);
}
 
function displaymain() {
	xml = req.responseXML;
	if (!xml) {
		return;
	}

     //populate current queue data from xml
	var queues = xml.getElementsByTagName("queuedata")[0].getElementsByTagName("queue");
	qhtml = "<table><tr>";
	for (i=0;i<queues.length;i++){
		qhtml += "<td class='q" + queues[i].getAttribute('id') + "'>";
		qhtml += queues[i].getAttribute('calls');
		qhtml += "</td>";
	}
	qhtml += "</tr><tr>";
 	for (i=0;i<queues.length;i++){
		qhtml += "<td class='q" + queues[i].getAttribute('id') + "'>";
		qhtml += queues[i].getAttribute('longest');
		qhtml += "</td>";
		if (parseInt(queues[i].getAttribute('longest')) >= qAlertTime) setflash=true; else setflash=false;
		qflash[parseInt(queues[i].getAttribute('id')) - 1] = setflash;
	}
	qhtml += "</tr></table>";
	cqdatadiv.innerHTML = qhtml;

     //populate current agent states from xml
	var states = xml.getElementsByTagName("agentstates")[0].getElementsByTagName("queue");
     shtml = "<table><tr><td>#</td>";
	statelist = states[0].getElementsByTagName("state");
	//column names first
	for (i=0;i<statelist.length;i++) {
		shtml += "<td>";
		shtml += statelist[i].getAttribute("name");
		shtml += "</td>";
	}
	shtml += "</tr>";
	//then the data
	for (i=0;i<states.length;i++){
		shtml += "<tr>";
		shtml += "<td class='q" + states[i].getAttribute('id') + "'>";
		shtml += states[i].getAttribute('total');
		shtml += "</td>";
		statelist = states[i].getElementsByTagName("state");
		for(j=0;j<statelist.length;j++){
			shtml += "<td class='q" + states[i].getAttribute('id') + "'>";
			shtml += statelist[j].getAttribute('percentage');
			shtml += "</td>";
		}
		shtml += "</tr>";
	}
	shtml += "</table>";
     cadatadiv.innerHTML = shtml;

// the following exerpt is superceded by db based report
	//populate call stats from xml
    var statcol = new Array();
	statcol.push(new Array());
	statcol[0][0]="";
	var periods = xml.getElementsByTagName("callstats")[0].getElementsByTagName("period");
	var stat = periods[0].getElementsByTagName("stat");

	//fill the row name column
	for(i=0;i<stat.length;i++){
		statcol[0].push(stat[i].getAttribute("name")); 
	}

	//one column for each data series
	for(i=0;i<periods.length;i++){
		stat = periods[i].getElementsByTagName("stat");
		arrpos = statcol.push(new Array()) - 1;
          statcol[arrpos].push(periods[i].getAttribute("name"));
		for(j=0;j<stat.length;j++){
			statcol[arrpos].push(stat[j].getAttribute("value"));
		}
	}
	//build the table from the array
	chtml = "<table>";
     for(i=0;i<statcol[0].length;i++){
		chtml += "<tr>";
		for(j=0;j<statcol.length;j++){
			chtml += "<td class='q" + j  + "'>" + statcol[j][i] + "</td>";
		}
		chtml += "</tr>";
	}
	chtml += "</table>"
//    hdatadiv.innerHTML = chtml;
// end of excerpt

     //global alerts
	var issue = xml.getElementsByTagName("globalissues")[0];
	html = "";
	issues = issue.getElementsByTagName("issue");
	for(i=0;i<issues.length;i++){
		if (i > 0) html += "<br>";
		html += issues[i].getAttribute('client') + " " + issues[i].childNodes[0].nodeValue;
	}
	globaldatadiv.innerHTML = html;
	globalcountdiv.innerHTML = issues.length;

	//agent alerts
 	var issue = xml.getElementsByTagName("agentalerts")[0];
	html = "";
	issues = issue.getElementsByTagName("issue");
	for(i=0;i<issues.length;i++){
		if (i > 0) html += "<br>";
		html += i + 1 + ") " + issues[i].getAttribute("queue") + " " + issues[i].childNodes[0].nodeValue;
	}
	agentdatadiv.innerHTML = html;
	agentcountdiv.innerHTML = issues.length;
	for(i=0;i<resizelist.length;i++) if (ie) iesizetext(resizelist[i]); else sizetext(resizelist[i]);
	for(i=0;i<resizetable.length;i++) sizetable(resizetable[i]);
}

function flash(act) {
	if (agentflash) clearTimeout(agentflash);
	if (act == 'on') {
		agentdatadiv.className = "agentdataflash";
		agentflash = setTimeout("flash('off');",500);
	} else {
		agentdatadiv.className = "agentdatadiv";
		if (act == 'off') agentflash = setTimeout("flash('on');",1000);
	}
}

function flashqueue(act) {
	if (queueflash) clearTimeout(queueflash);
	cells = cqdatadiv.getElementsByTagName("td");
	for (i=0;i<cells.length;i++){
		qclass = cells[i].className;
		if (qclass != '') {
			qnum = parseInt(qclass.substring(2,1));
			fclass = 'q' + qnum;
			if (act == 'on' && qflash[qnum - 1]) {
			   fclass += 'f';
			}
			cells[i].className = fclass
		}
	}
	if (act == 'on') queueflash=setTimeout("flashqueue('off');",500);
	if (act == 'off') queueflash=setTimeout("flashqueue('on');",1000);
}

function displayhist() {
  xml=req.responseXML;
  //populate call stats from xml
  var statcol = new Array();
  statcol.push(new Array());
  statcol[0][0]="";
  var periods = xml.getElementsByTagName("callstats")[0].getElementsByTagName("period");
  var stat = periods[0].getElementsByTagName("stat");

  //fill the row name column
  for(i=0;i<stat.length;i++){
	statcol[0].push(stat[i].getAttribute("name"));
  }

  //one column for each data series
  for(i=0;i<periods.length;i++){
	stat = periods[i].getElementsByTagName("stat");
	arrpos = statcol.push(new Array()) - 1;
	statcol[arrpos].push(periods[i].getAttribute("name"));
	for(j=0;j<stat.length;j++){
	  statcol[arrpos].push(stat[j].getAttribute("value"));
	}
  }
  //build the table from the array
  chtml = "<table>";
  for(i=0;i<statcol[0].length;i++){
	chtml += "<tr>";
	for(j=0;j<statcol.length;j++){
	  chtml += "<td class='q" + j  + "'>" + statcol[j][i] + "</td>";
	}
	chtml += "</tr>";
  }
  chtml += "</table>";
  hdatadiv.innerHTML = chtml;

  sizetable(hdatadiv);
}

function load() {
	//req = null;
	//loadit("./monabn.rb");
	//displayhist();
	req = null;
	loadit("./data.xml");
	displaymain();
	if (agentdatadiv.innerHTML.toLowerCase().indexOf("premium",0) > 0) {
		flash('on');
	} else {
		flash('clear');
	}
	if (qflash.toString().toLowerCase().indexOf('true') == -1) flashqueue('clear'); else flashqueue('on');
	refreshtimer = setTimeout("load();", 5000);
}

window.onresize = displaymain;
load();
shifttimer = setTimeout("shiftscreen();", shiftdelay);
reloadtimer = setTimeout("window.location.reload(false);", refreshdelay);
