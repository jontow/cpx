
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'contrib/rica'

class ChatModule < TabbedModule
	title 'Chat'
	index 5
	noautoload true

	HELP = <<HELP
Required commands are denoted by <>, optional ones by []

/server <host> [nickname] -- Connect to a server (alias is /connect)
/join <channel> -- Join a channel
/msg <username> -- Send a private message to a user
/action <action> -- Perform a CTCP ACTION (alias is /me)
/names -- Display list of users in the current channel
/list [channel] -- List all channels on a server, or just a selected one
/part [message] -- Leave the current channel and optionally send a part message
/quit [message] -- Disconnect from the current server and optionally send a quit message (alias is /disconnect)
/fgcolor <color> -- Change the foreground color, can be a colorname or 3, 6, 9 or 12 hex number with a leading #
/bgcolor <color> -- Change the background color, can be a colorname or 3, 6, 9 or 12 hex number with a leading #
/help -- Display this message
	
To change the current server at the Status tab, press Ctrl-x
HELP
	

	class IRCConnection < Rica::MessageProcessor
		def initialize(parent)
			@parent = parent
			super()
		end

		def default_action(msg)
			p msg
			@parent.line_entry(@parent.get_tab_by_id(0), "#{msg.command} - #{msg.origin}")
		end

		### COMMANDS ###
		
		def on_recv_cmnd_join(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if(msg.isSelfMessage?)
				@parent.add_tab(msg.to.chomp, server, :channel, true).activate
				@parent.redraw
				#cmnd_topic(msg.server, msg.to)
				#cmnd_names(msg.server, msg.to)
			elsif tab = @parent.get_tab(msg.to, server)
				@parent.line_entry(tab, msg.string("%T -!- %f [%F] has joined %t"))
			end
		end

		def on_recv_cmnd_part(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if msg.isSelfMessage?
				@parent.remove_tab(@parent.get_tab(msg.to, server))
			elsif tab = @parent.get_tab(msg.to, server)
				@parent.line_entry(tab, msg.string("%T -!- %f [%F] has left %t [%a]"))
			end
		end

		def on_recv_cmnd_quit(msg)
			# TODO - track channel userlists so we can print a valid quit message
			if msg.isSelfMessage?
				@parent.disconnect_server(msg.server, msg.selfNick)
			end
		end

		def on_recv_cmnd_privmsg(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if !server
				puts 'ERROR: cannot find a corresponding server'
			end

			if msg.isPriv?
				if msg.isSelfMessage?
					tab = @parent.get_or_create_tab(msg.to, server, :query)
				else
					tab = @parent.get_or_create_tab(msg.fromNick, server, :query)
				end
				@parent.line_entry(tab, msg.string("%T <%f> %a"))
			else
				if tab = @parent.get_tab(msg.to, server)
					@parent.line_entry(tab, msg.string("%T <%f> %a"))
				else
					puts "ERROR: not connected to channel #{msg.to}"
				end
			end
		end

		def on_recv_cmnd_notice(msg)
			p msg
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if !server
				puts 'ERROR: cannot find a corresponding server'
			end

			if msg.isPriv?
				# send all notices not to a channel to the status tab
				@parent.line_entry(@parent.get_tab_by_id(0), msg.string("%T -%f:%t- %a"))
			else
				if tab = @parent.get_tab(msg.to, server)
					@parent.line_entry(tab, msg.string("%T -%f:%t- %a"))
				else
					puts "ERROR: not connected to channel #{msg.to}"
				end
			end
		end

		def on_recv_cmnd_nick(msg)
			if msg.isSelfMessage?
				server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
				server.nickname = msg.to
			end
		end

		def on_recv_cmnd_mode(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if tab = @parent.get_tab(msg.to, server)
				@parent.line_entry(tab, msg.string("%T -!- Mode [%a] by %f"))
			else
				@parent.line_entry(@parent.get_tab_by_id(0), msg.string("%T -!- %s Your mode changed to [%a]"))
			end
		end
	
		def on_recv_cmnd_kick(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if !server
				puts 'ERROR: cannot find a corresponding server'
				p msg
			end

			if tab = @parent.get_tab(msg.to, server)
				tab.deactivate
				if @parent.current_tab == tab
					@parent.redraw
				end
				@parent.line_entry(tab, msg.string("%T -!- #{msg.args[0]} was kicked from #{msg.to} by %f [#{msg.args[1]}]"))
			end
		end

		def on_recv_cmnd_topic(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if !server
				puts 'ERROR: cannot find a corresponding server'
				p msg
			end

			if tab = @parent.get_tab(msg.to, server)
				@parent.line_entry(tab, msg.string("%T -!- %f changed the topic of %t to: %a"))
			end
		end

		### CTCP ###

		def on_recv_cmnd_ctcp_query_version(msg)
			ctcp_answer_version(msg.server, msg.fromNick, "Cthulhu Chat (version unnameable)")
		end

		def on_recv_cmnd_ctcp_query_ping(msg)
			ctcp_answer_ping(msg.server, msg.fromNick, Time.now.to_f.to_s.split('.').join(' '))
		end

		def on_recv_cmnd_ctcp_query_time(msg)
			ctcp_answer_time(msg.server, msg.fromNick, Time.now.to_s)
		end

		def on_recv_cmnd_ctcp_query_action(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if !server
				puts 'ERROR: cannot find a corresponding server'
				p msg
			end

			if tab = @parent.get_tab(msg.to, server)
				@parent.line_entry(tab, msg.string("%T * %f %a"))
			else
				tab = @parent.get_or_create_tab(msg.fromNick, server, :query)
				@parent.line_entry(tab, msg.string("%T * %f %a"))
			end
		end

		### REPLIES ###
		
		def on_recv_rpl_namreply(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if !server
				puts 'ERROR: cannot find a corresponding server'
				p msg
			end

			if tab = @parent.get_tab(msg.args[1], server)
				unless tab.namelist?
					tab.namelist = true
					@parent.line_entry(tab, msg.string("%T [Users #{msg.args[1]}]"))
				end
				@parent.line_entry(tab, msg.string("%T [#{msg.args[2].split(' ').join('] [')}]"))
			end

		end

		def on_recv_rpl_endofname(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if !server
				puts 'ERROR: cannot find a corresponding server'
				p msg
			end

			if tab = @parent.get_tab(msg.args[0], server)
				tab.namelist = false
			end
		end

		def on_recv_rpl_liststart(msg)
			@parent.line_entry(@parent.get_tab_by_id(0), msg.string("%T -!- %s %a"))
		end

		alias on_recv_rpl_list on_recv_rpl_liststart
		alias on_recv_rpl_listend on_recv_rpl_liststart

		def on_recv_rpl_topic(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if !server
				puts 'ERROR: cannot find a corresponding server'
				p msg
			end

			if tab = @parent.get_tab(msg.args[0], server)
				tab.topic = msg.args[-1]
				@parent.line_entry(tab, msg.string("%T -!- Topic for #{msg.args[0]} is #{msg.args[-1]}"))
			end
		end

		def on_recv_rpl_topicwhotime(msg)
			server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			if !server
				puts 'ERROR: cannot find a corresponding server'
				p msg
			end

			if tab = @parent.get_tab(msg.args[0], server)
				@parent.line_entry(tab, msg.string("%T -!- Topic set by #{msg.args[1]} at [#{Time.at(msg.args[-1].to_i)}]"))
			end

		end
	

		#def on_recv_rpl_notopic(msg)
			#server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
			#if !server
				#puts 'ERROR: cannot find a corresponding server'
				#p msg
			#end

			#p msg
				#if tab = @parent.get_tab(msg.to, msg.server)
					#p 'FOUND'
				#end
		#end

		def on_recv_rpl_motd(msg)
			@parent.line_entry(@parent.get_tab_by_id(0), msg.string("%T -!- %s %a"))
		end

		def on_recv_rpl_endofmotd(msg)
			if @parent.main.config[:savechannels] and server = @parent.main.config[:savedchannels].detect{|x| x[:host] = msg.from}
				server[:channels].each do |chan|
					@parent.irc.cmnd_join(msg.from, chan)
				end
			end
			@parent.line_entry(@parent.get_tab_by_id(0), msg.string("%T -!- %s %a"))
		end

		alias on_recv_rpl_motdstart on_recv_rpl_motd
		#alias on_recv_rpl_notopic on_recv_rpl_topic

		def on_recv_rpl_init(msg)
			@parent.add_server(msg.server, msg.to)
		end


		### ERRORS ###

		def on_recv_err_nicknameinuse(msg)
			# NOTE - there's probably a better way to handle this than disconnecting
			# if the nick is already in use at login time
			if server = @parent.servertab.servers.detect{|x| x.host == msg.server and x.nickname == msg.selfNick}
				@parent.line_entry(@parent.get_tab_by_id(0), msg.string("%T -!- %s %a"))
			else
				@conn.close(msg.server)
				@parent.line_entry(@parent.get_tab_by_id(0), msg.string("%T -!- %s %a")+' Disconnecting.')
			end
		end

		def on_link_failed(msg)
				@parent.line_entry(@parent.get_tab_by_id(0), msg.string("%T -!- Connection to %f failed. "))
		end

	end

	class Tab
		attr_reader :name, :buffer, :type, :server

		def initialize(name, buffer, server, type)
			@name = name
			@buffer = buffer
			@type = type
			@server = server
			@topic = ''
			@namelist = false
			@active = false
		end

		def channelname
			@active ? @name : ''
		end

		def status
			"[#{@server.nickname}] [#{@server.host}]"
		end

		def topic=(topic)
			@topic = topic
		end

		def namelist=(x)
			@namelist = x ? true : false
		end

		def namelist?
			@namelist
		end

		def activate
			@active = true
		end

		def deactivate
			@active = false
		end

		def activated?
			@active
		end
	end

	class StatusTab
		attr_reader :servers, :name, :buffer
		
		def initialize(buffer)
			@servers=[]
			@buffer = buffer
			@name = 'Status'
			@current = 0
		end

		def add_server(server)
			@servers << server
			@current = @servers.length-1
		end

		def remove_server(server)
			@servers.delete(server)
			next_server
		end

		def channelname
			""
		end

		def status
			if @servers.empty?
				"No servers connected, please use /server <host> [nickname]"
			else
				"[#{@servers[@current].host}] [#{@servers[@current].nickname}] (Change with Ctrl-x)"
			end
		end

		def next_server
			if @servers.empty?
				0
			else
				@current = (@current+1) % @servers.length
			end
		end
		
		def server
			current
		end

		def current
			@servers[@current]
		end

		def type
			:status
		end
	end

	class IRCServer
		attr_reader :host
		attr_accessor :nickname
		
		def initialize(host, nickname)
			@host = host
			@nickname = nickname
		end
	end

	def self.warn(*args)
		line_entry(@tabs[0], args.join(' '))
	end

	def self.flashtab(tab)
		flashwin(get_name)
		return if current_tab == tab
		p @tabs.index(tab)
		@channel_tabs.tabconfigure(@tabs.index(tab), :compound=>:right)
	end

	def self.create(main)
		super
		@fgcolor = @main.config[:fgcolor] || 'white'
		@bgcolor = @main.config[:bgcolor] || 'black'
		@tabbar = @main.tabs
		@frame = Tk::Tile::TFrame.new(@tabbar)#, :bg=>'blue')
		@scroll = Tk::Tile::TScrollbar.new(@frame).pack(:side=>:right, :fill=>:y)
		@inputframe = Tk::Tile::TFrame.new(@frame).pack(:side=>:bottom, :fill=>:x)

		@channellabel = Tk::Tile::TLabel.new(@inputframe, :text=>'').pack(:side=>:left)
		@input = Tk::Tile::TEntry.new(@inputframe).pack(:side=>:left, :fill=>:x, :expand=>true)
		@statusframe = Tk::Tile::TFrame.new(@frame).pack(:side=>:bottom, :fill=>:x)
		@clock = Tk::Tile::TLabel.new(@statusframe, :text=>Time.now.strftime('[%H:%M]')).pack(:side=>:left)

		@channel_tabs = Tk::Tile::TNotebook.new(@frame).pack(:side=>:top, :expand=>true, :fill=>:both)
		@statusline = Tk::Tile::TLabel.new(@statusframe, :justify=>:left, :anchor=>:w).pack(:side=>:top, :fill=>:x)
		@tabcount = 1
		@tabs = []
		statustab = TkText.new(@channel_tabs).pack(:expand=>true, :fill=>:both)

		statustab.configure(:foreground=>@fgcolor, :background=>@bgcolor) rescue RuntimeError

		statustab.bindtags_unshift(b = TkBindTag.new)
		b.bind("Control-c", proc{|x| TkClipboard.clear; TkClipboard.append(x.widget.tag_ranges('sel')) unless x.widget.tag_ranges('sel').empty?})
		b.bind('Key', proc{ Tk.callback_break })
		b.bind('Button1-ButtonRelease', proc{|x| @input.focus if x.widget.tag_ranges('sel').empty?})
	
		@channel_tabs.add(statustab, :text=>'Status', :image=>TabbedModule::FLASHIMG, :compound=>:text)
		@tabs[0] = StatusTab.new(statustab)
		set_statusline current_tab.status
		@irc = IRCConnection.new(self)

    @channel_tabs.bind("<NotebookTabChanged>") do
			set_statusline current_tab.status
			@channellabel.configure(:text=>current_tab.channelname)
			@channel_tabs.tabconfigure(@tabs.index(current_tab), :compound=>:text)
		end

		def self.irc
			@irc
		end

		@inputbuffer = []
		@inputpos = 0

		@input.bind("Return") do
			@inputbuffer << @input.value unless @input.value.empty? or @inputbuffer[-1] == @input.value
			@inputpos = @inputbuffer.length
			process_input(@input.value)
			@input.value=''
		end

		@input.bind("Up") do
			p @inputbuffer, @inputpos
			oldpos = @inputpos
			@inputpos -= 1
			@inputpos = 0 if @inputpos < 0
	
			@inputbuffer << @input.value unless @input.value.empty? or @inputbuffer[-1] == @input.value or @inputbuffer[oldpos] == @input.value
			@input.value = @inputbuffer[@inputpos]
		end

		@input.bind("Down") do
			p @inputbuffer, @inputpos
			oldpos = @inputpos
			@inputpos += 1
			@inputpos = @inputbuffer.length if @inputpos > @inputbuffer.length

			@inputbuffer << @input.value unless @input.value.empty? or @inputbuffer[-1] == @input.value or @inputbuffer[oldpos] == @input.value
			@input.value = @inputbuffer[@inputpos]
		end

		@input.bind("Control-Key-x")do
			@tabs[0].next_server
			set_statusline current_tab.status
		end

		@input.bind("Control-Key-u"){@input.value=''}

		if @main.config[:savechannels] and @main.config[:savedchannels]
			@main.config[:savedchannels].each do |server|
				# TODO support username/realname
				nickname = server[:nickname] || @main.agent_name
				@irc.open(server[:host], [nickname, nickname], nickname)
			end
		end

		add_keybindings
	end

	def self.destroy
		super
		if @main.config[:savechannels]
			@main.config[:savedchannels] = export_connections
		end
		@irc.closeAll
		@irc.destroy
	end

	def self.add_keybindings
		(Array(*[1..9])+%w{0 q w e r t y u i o p}).each_with_index do |key, index|
			@input.bind("Alt-Key-#{key}") do
				begin
					@channel_tabs.select(index)
					@input.focus
				rescue RuntimeError => e
					# bleh...
				end
			end
		end
	end

	def self.redraw
		set_statusline current_tab.status
		@channellabel.configure(:text=>current_tab.channelname)
	end

	def self.set_statusline(text)
		@statusline.configure(:text=>"#{text}")
	end

	def self.add_server(host, nickname)
		@tabs[0].add_server IRCServer.new(host, nickname)
		set_statusline current_tab.status
	end

	def self.add_tab(name, server, type, switch=false)
		if tab = get_tab(name, server)
			return tab
		end

		#topic = Tk::Tile::TLabel.new(@statusframe, :justify=>:left, :anchor=>:w).pack(:side=>:left, :fill=>:x)
		log = TkText.new(@channel_tabs).pack(:expand=>true, :fill=>:both)

		log.configure(:foreground=>@fgcolor, :background=>@bgcolor) rescue RuntimeError

		log.bindtags_unshift(b = TkBindTag.new)
		b.bind("Control-c", proc{|x| TkClipboard.clear; TkClipboard.append(x.widget.tag_ranges('sel')) unless x.widget.tag_ranges('sel').empty?})
		b.bind('Key', proc{ Tk.callback_break })
		b.bind('Button1-ButtonRelease', proc{|x| @input.focus if x.widget.tag_ranges('sel').empty?})

		tab = Tab.new(name, log, server, type)
		@tabs[@tabcount] = tab
		@channel_tabs.add(log, :text=>name, :image=>TabbedModule::FLASHIMG, :compound=>:text)
		@tabcount += 1
		if switch
			@channel_tabs.select @tabcount-1
		end
		tab
	end

	def self.remove_tab(tab)
		i = @tabs.index(tab)
		@channel_tabs.forget(i)
		tab.buffer.destroy
		@tabs.delete tab
		@tabcount -= 1
	end

	def self.get_tab(name, server)
		@tabs.detect{|u| u.name == name and u.server == server}
	end

	def self.get_or_create_tab(name, server, type)
		tab = get_tab(name, server)
		unless tab
			tab = add_tab(name, server, type)
		end
		tab
	end

	def self.get_tab_by_id(id)
		@tabs[id]
	end

	def self.current_tab
		@tabs[@channel_tabs.index(:current)]
	end

	def self.servertab
		@tabs[0]
	end

	def self.disconnect_server(host, nickname)
		server = @tabs[0].servers.detect{|s| s.host == host and s.nickname == nickname}
		unless server
			#warn "Could not find server corresponding to #{host} #{username}"
			return
		end
		@tabs[1..-1].select{|t| t.server == server}.each{|t|remove_tab(t)}
		@tabs[0].remove_server(server)
		set_statusline current_tab.status
	end

	def self.process_input(input)
		tab = current_tab
		if input[0,1] == '/'
			command, data = input[1..-1].split(' ', 2)
			command.downcase!

			case command
			when 'server', 'connect'

				if data.nil? or data.empty?
					warn 'Must supply server'
					return
				end

				server, nickname = data.split(' ', 2)

				if nickname.nil?
					nickname = @main.agent_name
				end

				# For now, default 'username', 'realname' to 'nickname'.
				# We don't have a means to do anything else, yet.
				username = nickname
				realname = nickname

				@irc.open(server, [username, realname], nickname)

			when 'quit', 'disconnect'
				if @tabs[0].servers.length > 0
					@irc.cmnd_quit(tab.server.host, data)
				else
					warn 'no servers connected'
				end

			when 'join'
				if @tabs[0].servers.length > 0
					@irc.cmnd_join(tab.server.host, *(data.split(' ')))
				else
					warn 'no servers connected'
				end

			when 'close', 'part'
				p 'close'
				tab = current_tab
				if tab.type == :channel
					@irc.cmnd_part(tab.server.host, tab.name, data)
				elsif tab.type == :query
					remove_tab(tab)
				else
					warn "ERROR: cannot close this tab"
				end

			when 'move'
				if data =~ /^[0-9]+$/
					newindex = data.to_i - 1
					if current_tab == @tabs[0]
						warn 'Cannot move the status tab'
					elsif newindex < 1
						warn 'Cannot move tab to a position less than 2'
					else
						if newindex > @channel_tabs.index(:end)
							newindex = @channel_tabs.index(:end) -1
						end

						tab = current_tab
						oldindex = @tabs.index(tab)
						
						@tabs.insert(newindex, @tabs.delete(tab))
#						@channel_tabs.forget oldindex
						@channel_tabs.insert(newindex, tab.buffer)
					end
				end
				
			when 'msg'
				if data.nil? or data.empty?
					warn 'Will not send blank message'
				elsif @tabs[0].servers.length > 0
					@irc.cmnd_privmsg(tab.server.host, *(data.split(' ', 2)))
				else
					warn 'no servers connected'
				end

			when 'nick'
				if data.nil? or data.empty?
					warn 'please supply a new nickname'
				elsif @tabs[0].servers.length > 0
					@irc.cmnd_nick(tab.server.host, data)
				else
					warn 'no servers connected'
				end

			when 'me', 'action'
				@irc.cmnd_privmsg(tab.server.host, tab.name, "\001ACTION #{data}\001")

			when 'names'
				@irc.cmnd_names(tab.server.host, tab.name) if tab.kind_of?(ChatModule::Tab)

			when 'help'
				warn(HELP)

			when 'bgcolor'
				begin
					@tabs.each do |tab|
						tab.buffer.configure(:background=>data)
					end
				rescue RuntimeError => e
					warn e.message
				else
					@bgcolor = data
					@main.config[:bgcolor] = @bgcolor
				end

			when 'fgcolor'
				begin
					@tabs.each do |tab|
						tab.buffer.configure(:foreground=>data)
					end
				rescue RuntimeError => e
					warn e.message
				else
					@fgcolor = data
					@main.config[:fgcolor] = @fgcolor
				end

			when 'list'
				if data.nil? or data.empty?
					@irc.cmnd_list(tab.server.host)
				else
					@irc.cmnd_list(tab.server.host, data)
				end

			when 'dump'
				@tabs[0].servers.each do |serv|
					p export_connections
				end
			when 'reload'
				warn 'attempting reload'
				begin
					load(__FILE__)
				rescue LoadError, SyntaxError => e
					warn "#{e.class}:#{e.message}\n#{e.backtrace}"
				end
			else
				line_entry(tab, "Unknown command #{command}")
			end
		else
			@irc.cmnd_privmsg(tab.server.host, tab.name, input)
		end
	end

	def self.export_connections
		connections = []
		@tabs[0].servers.each do |serv|
			server = {:host=>serv.host, :nickname=>serv.nickname, :channels=>[]}
			@tabs.select{|x| x.kind_of?(ChatModule::Tab) and x.server == serv and ['#', '&'].include?(x.name[0..0])}.each do |tab|
				server[:channels] << tab.name
			end
			connections << server
		end
		connections
	end

	def self.line_entry(tab, value)
		unless tab
			warn "couldnt find a tab that matched #{buffer.dump}"
			exit
		end
		log = tab.buffer
		unless value.kind_of? String
			p value
			return
		end
		#appending a new line all the time sucks...
		#just prepend one as long as the log ain't empty
		pos = @scroll.get # snapshot the current position
		log.value += "\n" unless log.value.empty?
		log.value += value.chomp
		#         @log.value += "\n"
		# TODO - figure out how to preserve the scroll position if we're not
		# at the end
		#         if pos[1] == 1.0
		log.yview_moveto(1.0)
		#         else
		#             @log.yview_moveto(pos[0]+((pos[1]-pos[0])/2)) #this sucks
		#         end
		flashtab(tab)
	end

	def self.tick
		@clock.configure(:text=>Time.now.strftime('[%H:%M]'))
	end
end	
