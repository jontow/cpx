
=begin license
 * Copyright (c) 2014, Jonathan Towne <jontow@mototowne.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'net/http'

class LinksModule < TabbedModule
	title 'Links'
	index 10
	noautoload false

	def self.create(main)
		super
		@tabbar = @main.tabs
		@frame = Tk::Tile::TFrame.new(tabbar)#, :bg=>'blue')
		@scroll = Tk::Tile::TScrollbar.new(@frame).pack(:side=>:right, :fill=>:y)
		@log = TkText.new(@frame).pack(:side=>:top, :expand=>true, :fill=>:both)
		@log.yscrollbar(@scroll)

                # This makes the log area readonly.
                @log.bindtags_unshift(b = TkBindTag.new)
                b.bind('Key', proc{ Tk.callback_break })

                @log.tag_bind('hyperlink', 'Button 1') do | event |
			range1 = @log.index("@#{event.x},#{event.y} linestart")
			range2 = @log.index("@#{event.x},#{event.y} lineend")
			@link = "http:" + @log.get(range1, range2).split('http:', 2)[1].split(' ')[0]

			$urlpop.call(@link)
		end

                @log.tag_bind('hyperlink', 'Enter') do | event |
                        @log.configure(:cursor, 'hand2')
                end

                @log.tag_bind('hyperlink', 'Leave') do | event |
                        @log.configure(:cursor, 'xterm')
                end

		if @main.config[:agentlinks]
			url = URI.parse(@main.config[:agentlinks])
			http = Net::HTTP.new(url.host, url.port)
			http.open_timeout = http.read_timeout = 5
			response, body = http.get(url.request_uri, '')

			@log.insert(:end, "\n\n")
			body.split("\n").each do |link|
				@log.insert(:end, "   " + link + "\n", 'hyperlink')
			end
		else
			@log.insert(:end, "   No link source configured.\n")
		end
	end
    
	def self.auto_save
	end
end
