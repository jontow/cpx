
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'timeout'

class MainModule < TabbedModule
	title 'Main'
	index 0
	accepts :BLAB, :URL, :LOGOUT, :ASTATE, :PROFILE, :CALLINFO, :ASK, :WARMXFERFAILED

	def self.bootstrap
		# Be passing the key to the Dial event for billing
		brandcounter = 0
		queuecounter = 0
		send('BRANDLIST') do |result, data, event|
			if result
				data[1..-2].split('),(').map{|x| x.split('|')}.sort_by{|x| [x[1].downcase]}.each do |e|
					brandcounter+=1
					brandid,brandlabel = e
					@main.brandlist[brandid] = brandlabel
					if (brandcounter % 20) == 1
						@dialbrand.add(:command, :label=>brandlabel, :columnbreak=>true, :command=>proc{@brandvar.value=brandlabel; show_calldetails; dialout(brandid)})
					else
						@dialbrand.add(:command, :label=>brandlabel, :command=>proc{@brandvar.value=brandlabel; show_calldetails; dialout(brandid)})
					end

					# Print out the brandlist on startup for debugging.
					#puts "BRANDID: #{brandid} / BRANDLABEL: #{brandlabel}"
				end
			end
			send('PROFILES') do |result, data, event|
				if result
					data.split(' ').each do |x|
						id, name = x.split(':', 2)
						@main.profiles.store(id.to_i, name)
					end
					@main.agent_profile = @main.agent_profile
				end
				send('QUEUENAMES') do |result, data, event|
					if result
						data[1..-2].split('),(').sort{|x,y| x.split('|')[1].downcase <=> y.split('|')[1].downcase}.each do |e|
							queuecounter+=1
							queuename,commonname = e.split('|')
							@main.queuelist[queuename] = commonname
							transproc = Proc.new{|x|
								if x == 'yes'
									puts 'transfering queue'
									send('TRANSFER', 'queue', queuename)
								end
							}
							dialogpop = Proc.new{
								dialog = Tk::Tile::Dialog.new(
									:type=>'yesno',
									:message=>'Carry out Transfer?',
									:title=>'Transfer Confirmation',
									:command=>transproc
								)
								dialog.show
							}
							if (queuecounter % 20) == 1
								@transferqueuemenu.add(:command, :label=>commonname, :columnbreak=>true, :command=>dialogpop)
							else
								@transferqueuemenu.add(:command, :label=>commonname, :command=>dialogpop)
							end
						end
					end
					send('RELEASEOPTIONS') do |result, data, event|
						if result
							@main.releaseoptions = []
							data.split(',').each do |r|
								id, label, utilbias = r.split(':')
								id = Integer(id)
								utilbias = Integer(utilbias)
								@main.releaseoptions << {:id=>id, :label=>label, :utilbias=>utilbias}
							end
						end
						@ready = true
					end
				end
			end
		end
	end

	def self.create(main)
		super
		@tabbar = @main.tabs

		#
		# Expose some variables to Tk
		#

		@namevar = TkVariable.new(@main.agent_name)
		@statevar = TkVariable.new(@main.get_agent_state_name(@main.agent_state, @main.agent_statedata))
		@brandvar = TkVariable.new()
		@calleridvar = TkVariable.new()
		@calltypevar = TkVariable.new()

		#
		# Create some frames: somehow, they're ordered from the bottom up..? Damn you, Tk!
		#

		@frame = Tk::Tile::TFrame.new(tabbar)#, :bg=>'red')
		
		@toppadframe = Tk::Tile::TFrame.new(@frame).pack(:side=>:top, :fill=>:x, :pady=>10)
		@buttonframe = Tk::Tile::TFrame.new(@frame).pack(:side=>:bottom, :anchor=>:s, :pady=>3)
		@magicframe = Tk::Tile::TFrame.new(@frame).pack(:side=>:left, :fill=>:none, :padx=>5, :anchor=>:nw) 

		@phoneframe = Tk::Tile::TFrame.new(@frame).pack(:side=>:right, :anchor=>:ne) 
		@callinfoframe = Tk::Tile::TFrame.new(@magicframe).pack(:side=>:left, :padx=>10, :anchor=>:nw) 
		@nameframe = Tk::Tile::TFrame.new(@callinfoframe).pack(:side=>:top, :anchor=>:nw, :pady=>3) 
		@stateframe = Tk::Tile::TFrame.new(@callinfoframe).pack(:side=>:top, :anchor=>:nw, :pady=>3) 
		@brandframe = Tk::Tile::TFrame.new(@callinfoframe).pack(:side=>:top, :anchor=>:nw, :pady=>3) 
		@calleridframe = Tk::Tile::TFrame.new(@callinfoframe).pack(:side=>:top, :anchor=>:nw, :pady=>3) 
		@calltypeframe = Tk::Tile::TFrame.new(@callinfoframe).pack(:side=>:top, :anchor=>:nw, :pady=>3) 
		

		# XXX reloading the tab will fuxor this!
		if @main.phone
			Thread.new{@main.phone.start(@phoneframe)}
		end

		#
		# Agent/State label reporting
		#

		Tk::Tile::TLabel.new(@nameframe, :text=>'Agent: ', :font=>'Helvetica').pack(:side=>:left)
		Tk::Tile::TLabel.new(@nameframe, :textvariable=>@namevar).pack(:side=>:left)

		Tk::Tile::TLabel.new(@stateframe, :text=>' State: ', :font=>'Helvetica').pack(:side=>:left)
		Tk::Tile::TLabel.new(@stateframe, :textvariable=>@statevar).pack(:side=>:left)

		@brandidtag = Tk::Tile::TLabel.new(@brandframe, :text=>' Brand: ', :font=>'Helvetica')
		@brandidinfo = Tk::Tile::TLabel.new(@brandframe, :textvariable=>@brandvar)

		@calleridtag = Tk::Tile::TLabel.new(@calleridframe, :text=>'CallerID: ', :font=>'Helvetica')
		@calleridinfo = Tk::Tile::TLabel.new(@calleridframe, :textvariable=>@calleridvar)

		@calltypetag = Tk::Tile::TLabel.new(@calltypeframe, :text=>'Call Type: ', :font=>'Helvetica')
		@calltypeinfo = Tk::Tile::TLabel.new(@calltypeframe, :textvariable=>@calltypevar)

		#
		# Release/Unrelease MenuButton
		#

		@unreleasebutton = Tk::Tile::TButton.new(@buttonframe, :text=>'Go Available')
		@releasemenubutton = Tk::Tile::Menubutton.new(@buttonframe, :text=>'Go Released', :width=>-1)

		@releasemenu = TkMenu.new(@releasemenubutton, :tearoff=>false, :postcommand=>proc{create_release_menu})
		@releasemenubutton.menu(@releasemenu)
		#@releasemenu.add(:command, :label=>'Bathroom', :command=>proc{send('STATE', Agent::RELEASED)})
		#@releasemenu.add(:command, :label=>'Cigarette', :command=>proc{send('STATE', Agent::RELEASED)})
		#@releasemenu.add(:command, :label=>'Outbound Call', :command=>proc{send('STATE', Agent::RELEASED)})
		#@releasemenu.add(:command, :label=>'Other', :command=>proc{send('STATE', Agent::RELEASED)})

	
		#
		# Outbound call widgets
		#

		@dialbrandbutton = Tk::Tile::Menubutton.new(@buttonframe, :text=>'Outbound Call To')
		@dialbrand = TkMenu.new(@dialbrandbutton, :tearoff=>false)
		@brandmatchbuffer = ''
		@dialbrand.bind('KeyRelease', proc{|x| match_brand(x.char)})
		@dialbrandbutton.menu(@dialbrand)

		@diallabel = Tk::Tile::TLabel.new(@buttonframe, :text=>'Dial Outbound')
		@dialinput = Tk::Tile::TEntry.new(@buttonframe, :validate=>:all, :validatecommand=>proc{|x|
			# This is a cool hack to strip any non-numeric input
			x.widget.insert(x.index, x.string.gsub(/[^0-9]+/, '')) if x.action == 1
			x.action.zero? ? true : false
		})

		@dialbutton = Tk::Tile::TButton.new(@buttonframe, :text=>'Dial')
		@dialcancel = Tk::Tile::TButton.new(@buttonframe, :text=>'Cancel')
		@dialtoggle = :outbound

		@dialagentinput = Tk::Tile::TEntry.new(@buttonframe)
		@dialagentbutton = Tk::Tile::TButton.new(@buttonframe, :text=>'Transfer')
		@dialagentcancel = Tk::Tile::TButton.new(@buttonframe, :text=>'Cancel')

		#
		# Example of tab-flashing, doesn't involve nudity.
		#
		#@button = Tk::Tile::TButton.new(@frame, :text=>"flash chat tab").pack(:side=>:bottom)
		#@button.command{flashwin('Chat')}

		#
		# Transfer MenuButton
		#

		@transfermenubutton = Tk::Tile::Menubutton.new(@buttonframe, :text=>'Transfer To', :width=>-1)
		@transfermenu = TkMenu.new(@transfermenubutton, :tearoff=>false)
		@transfermenubutton.menu(@transfermenu)
		@transferqueuemenu= TkMenu.new(@transfermenu, :tearoff=>false)
		@transferagentmenu= TkMenu.new(@transfermenu, :tearoff=>false, :postcommand=>proc{generate_transfer_agent_menu})
		@transfermenu.add(:cascade, :label=>'Agent', :menu=>@transferagentmenu)#:command=>proc{transfer_queue})
		#@transfermenu.add(:command, :label=>'Agent', :command=>proc{transfer_agent})
		@transfermenu.add(:command, :label=>'Third Party', :command=>proc{transfer_party})
		@transfermenu.add(:command, :label=>'Blind Transfer', :command=>proc{transfer_blind})
		@transfermenu.add(:cascade, :label=>'Queue', :menu=>@transferqueuemenu)#:command=>proc{transfer_queue})
		@transfercompletebutton = Tk::Tile::Button.new(@buttonframe, :text=>'Complete Transfer')
		@transfercancelbutton = Tk::Tile::Button.new(@buttonframe, :text=>'Cancel Transfer')

		#
		# Add a hangup button to the 'main' menu
		#


		#puts @main.mainmenu.index(:end)
		@main.mainmenu.insert(0, :command, :label=>'Hangup', :command=>proc{send('HANGUP')})
		@main.mainmenu.entryconfigure(0, :state=>:disabled)
		#puts @main.mainmenu.index(:end)

		#
		# Wrapup Button
		#

		@wrapupbutton = Tk::Tile::Button.new(@buttonframe, :text=>'End Wrapup')

		#
		# Startup Routine (ordering is important)
		#
	
		hide_all_buttons
		hide_calldetails
		
		show_buttons_for_state(@main.agent_state)

		#
		# Methods for widget actions
		#

		@dialbutton.command do
			if !@dialinput.value.empty?
				@dialinput.state(:disabled)
				@dialbutton.state(:disabled)
				if @dialtoggle == :outbound
					send('DIAL', @dbrandid, 'outbound', @dialinput.value, '1') do |result, data, l|
						if result
							if @main.phone
								@main.phone.rbphone.autoanswer = true
							end
							@calleridvar.value = @dialinput.value
							show_calldetails
						else
							@dbrandid = nil
							hide_calldetails
							puts 'something broke calling outbound!'
							puts data
							Thread.new{ Tk::Tile::Dialog.new(:message=>"There was an error making the outbound call.", :title=>"Outbound Dial Error", :type=>'ok', :icon=>:warning).show }
							hide_dial
							show_dial(:BRAND)
						end
						@dialinput.configure(:state=>:normal)
						@dialbutton.configure(:state=>:normal)
					end
				
				elsif @dialtoggle == :warmxfer
					send('WARMXFER', 'START', @dialinput.value) do |result, data, l|
							@dialinput.configure(:state=>:normal)
							@dialbutton.configure(:state=>:normal)
						if result
							if @main.phone
								@main.phone.rbphone.autoanswer = true
							end
						else
							Thread.new{ Tk::Tile::Dialog.new(:message=>data, :title=>"Warm Transfer Error", :type=>'ok', :icon=>:warning).show }
						end
					end
					#show_transfer(:END)
				
				elsif @dialtoggle == :blind
					send('TRANSFER', 'blind', @dialinput.value) do |result, data, l|
						@dialinput.configure(:state=>:normal)
						@dialbutton.configure(:state=>:normal)
						if result
							if @main.phone
								@main.phone.rbphone.autoanswer = true
							end
						else
							Thread.new{ Tk::Tile::Dialog.new(:message=>data, :title=>"Warm Transfer Error", :type=>'ok', :icon=>:warning).show }
						end
					end

				else
					puts 'missing the stuff'
					@dialinput.configure(:state=>:normal)
					@dialbutton.configure(:state=>:normal)
				end
			else
				hide_calldetails
				Thread.new{ Tk::Tile::Dialog.new(:message=>"Outbound number was empty, please provide a number.", :title=>"Outbound Dial Error", :type=>'ok', :icon=>:warning).show }
			end
		end

		@dialinput.bind("Return") do
			@dialbutton.invoke
		end

		@dialinput.bind("Button-3") do |x|
			menu = generate_copypaste_menu(x.widget)
			menu.post(*@tabbar.winfo_pointerxy) if menu.index(:end)
		end

		@dialcancel.command do
			case @dialtoggle
			when :outbound
				if $main.agent_state == Agent::PRECALL
					send('STATE', Agent::IDLE)
				end
				@dbrandid = nil
				show_dial(:BRAND)
				hide_calldetails
			when :warmxfer
				hide_dial
				show_transfer(:START)
			end
		end

		@dialagentbutton.command do
			@dialagentinput.configure(:state=>:disabled)
			@dialagentbutton.configure(:state=>:disabled)
			send('TRANSFER', 'agent', @dialagentinput.value) do |result, data, l|
				unless result
					Thread.new{ Tk::Tile::Dialog.new(:type=>'ok', :message=>data, :title=>'Failed', :icon=>:warning).show }
				end
				@dialagentinput.configure(:state=>:normal)
				@dialagentbutton.configure(:state=>:normal)
			end
		end

		@dialagentinput.bind("Return") do
			@dialagentbutton.invoke
		end

		@dialagentcancel.command do
			hide_dial
			show_transfer(:START)
		end

		@unreleasebutton.command do
			@unreleasebutton.state(:disabled)
			send('STATE', Agent::IDLE) do |*args|
				@unreleasebutton.configure(:state=>:normal)
			end
		end

		@wrapupbutton.command do
			@wrapupbutton.state(:disabled)
			send('ENDWRAPUP') do |result, data, l|
				@wrapupbutton.configure(:state=>:normal)
			end
		end

		@transfercompletebutton.command do
			@transfercompletebutton.state(:disabled)
			send('WARMXFER', 'COMPLETE') do |result, data, l|
				@transfercompletebutton.configure(:state=>:normal)
				unless result
					Thread.new{ Tk::Tile::Dialog.new(:message=>data, :title=>"Warm Transfer Error", :type=>'ok', :icon=>:warning).show }
				end
			end
		end

		@transfercancelbutton.command do
			@transfercancelbutton.state(:disabled)
			send('WARMXFER', 'CANCEL') do |*args|
				@transfercancelbutton.configure(:state=>:normal)
			end
		end

		#@transferagentbutton.command do
			#if !@transferagentinput.value.strip.empty?
				#@transferagentbutton.state(:disabled)
				#send('TRANSFER', 'agent', @transferagentinput.value) do |result, data, l|
					#unless result
						#Thread.new{ Tk::Tile::Dialog.new(:type=>'ok', :message=>data, :title=>'Failed').show }
					#end
					#@transferagentbutton.configure(:state=>:normal)
				#end
			#else
				#puts 'missing agent ID'
			#end
		#end
	end

	def self.destroy
		delete_hangup_menu_entries
		super
	end

	#def self.unload
		#puts 'NO!'
		#false
	#end

	def self.delete_hangup_menu_entries
		e = @main.mainmenu.index(:end)
		j = 0
		0.upto(e) do |i|
			if @main.mainmenu.entrycget(i-j, :label) == 'Hangup'
				puts "deleting #{@main.mainmenu.entrycget(i-j, :label)}"
				@main.mainmenu.delete(i-j)
				j += 1
			end
		end
	end

	def self.toggle_hangup_menu_entry(active)
		e = @main.mainmenu.index(:end)
		0.upto(e) do |i|
			if @main.mainmenu.entrycget(i, :label) == 'Hangup'
				@main.mainmenu.entryconfigure(i, :state=>(active ? :normal : :disabled))
			end
		end
	end

	def self.create_release_menu
		menuend = @releasemenu.index(:end)
		@releasemenu.delete(0, menuend)

		if @main.releaseoptions
			@main.releaseoptions.each do |opt|
				@releasemenu.add(:command, :label=>opt[:label], :command=>proc do
					send('STATE', Agent::RELEASED, opt[:id]) do |r, *args|
						if r
							@main.agent_statedata = opt[:id]
						end
					end
				end
				)
			end
		end
		@releasemenu.add(:command, :label=>'Default Release', :command=>proc do
			send('STATE', Agent::RELEASED) do |r, *args|
				if r
					@main.agent_statedata = nil
				end
			end
		end
		)
	end
	
	def self.match_brand(char)
		if ('a'..'z').include? char.downcase
			str = @brandmatchbuffer + char.downcase
			if match =  @main.brandlist.to_a.sort_by{|x| [x[1].downcase]}.detect{|b| b[1][0...(str.length)].downcase == str}
				idx =  @main.brandlist.to_a.sort_by{|x| [x[1].downcase]}.index(match)
				@dialbrand.activate(idx)
				@brandmatchbuffer += char.downcase
			else
				@brandmatchbuffer = char.downcase
			end
		end
		nil
	end

	def self.transfer_agent
		show_transfer(:AGENT)
		hide_transfer(:START)
	end

	def self.transfer_blind
		@dialtoggle = :blind
		show_dial(:NUMBER)
		hide_transfer(:START)
	end

	def self.transfer_party
		#show_transfer_party_comment
		@dialtoggle = :warmxfer
		show_dial(:NUMBER)
		hide_transfer(:START)
	end

	def self.hide_all_buttons
		hide_transfer
		hide_dial
		hide_wrapup
	end

	def self.hide_transfer(type=nil)
		case type
		when :COMMENT # ???
		when :CANCEL
			@transfercancelbutton.unpack
		when :START
			@transfermenubutton.unpack
		when :END
			@transfercompletebutton.unpack
		else
			@transfermenubutton.unpack
			@transfercompletebutton.unpack
			@transfercancelbutton.unpack
		end
	end

	def self.show_transfer(type=nil)
		case type
		when :AGENT
			show_dial(:AGENT)
			hide_transfer(:start)
		when :COMMENT
		when :CANCEL
			@transfercancelbutton.configure(:state=>:normal)
			@transfercancelbutton.pack(:side=>:left, :padx=>15)
		when :START
			@transfermenubutton.pack(:side=>:left, :padx=>15)
		when :END
			@transfermenubutton.unpack
			@transfercompletebutton.configure(:state=>:normal)
			@transfercompletebutton.pack(:side=>:left, :padx=>15)
		else
		end
	end

	def self.hide_dial(type=nil)
		case type
		when :BRAND
			@dialbrandbutton.unpack
		when :NUMBER
			@diallabel.unpack
			@dialinput.unpack
			@dialbutton.unpack
			@dialcancel.unpack
		when :AGENT
			@dialagentinput.unpack
			@dialagentbutton.unpack
			@dialagentcancel.unpack
		else
			@dialbrandbutton.unpack
			@diallabel.unpack
			@dialinput.unpack
			@dialbutton.unpack
			@dialcancel.unpack
			@dialagentinput.unpack
			@dialagentbutton.unpack
			@dialagentcancel.unpack
		end
	end
		
	def self.show_dial(type=nil)
		case type
		when :BRAND
			hide_dial(:NUMBER)
			hide_dial(:AGENT)
			@dialbrandbutton.pack(:side=>:left)
		when :NUMBER
			hide_dial(:BRAND)
			hide_dial(:AGENT)
			@dialinput.configure(:state=>:normal)
			@dialinput.pack(:side=>:left)
			@dialbutton.configure(:state=>:normal)
			@dialbutton.pack(:side=>:left)
			@dialcancel.pack(:side=>:left)
		when :AGENT
			hide_dial(:BRAND)
			hide_dial(:NUMBER)
			@dialagentinput.configure(:state=>:normal)
			@dialagentinput.pack(:side=>:left)
			@dialagentbutton.configure(:state=>:normal)
			@dialagentbutton.pack(:side=>:left)
			@dialagentcancel.pack(:side=>:left)
		else
		end
	end

	def self.dialout(clientid=nil)
		if clientid
			send('STATE', Agent::PRECALL) do |result, *args|
				puts result
				if result
					@dialtoggle = :outbound
					show_dial(:NUMBER)
					@dbrandid = clientid
				else
					puts *args
					# TODO error?
				end
			end
		else
			puts "ERROR: does not compute! (busted clientid)"
		end
	end

	def self.hide_wrapup
		@wrapupbutton.unpack
	end

	def self.show_wrapup
		@wrapupbutton.configure(:state=>:normal)
		@wrapupbutton.pack(:side=>:left, :padx=>15)
	end

	def self.show_release
		@unreleasebutton.unpack
		@releasemenubutton.configure(:state=>:normal)
		@releasemenubutton.pack(:side=>:left)
	end

	def self.show_unrelease
		@releasemenubutton.unpack
		@unreleasebutton.configure(:state=>:normal)
		@unreleasebutton.pack(:side=>:left)
	end

	def self.hide_calldetails
		@brandvar.value = ''
		@calleridvar.value = ''
		@calltypevar.value = ''
		@brandidtag.unpack
		@brandidinfo.unpack
		@calleridtag.unpack
		@calleridinfo.unpack
		@calltypetag.unpack
		@calltypeinfo.unpack
	end

	def self.show_calldetails
		unless @brandvar.value.empty?
			@brandidtag.pack(:side=>:left)
			@brandidinfo.pack(:side=>:left)
		end

		unless @calleridvar.value.empty?
			@calleridtag.pack(:side=>:left)
			@calleridinfo.pack(:side=>:left)
		end

		unless @calltypevar.value.empty?
			@calltypetag.pack(:side=>:left)
			@calltypeinfo.pack(:side=>:left)
		end
	end

	def self.generate_copypaste_menu(entry)
		menu = TkMenu.new(entry, :tearoff=>false)
		menu.add(:command, :label=>'Cut', :command=>proc{ (TkClipboard.clear; TkClipboard.append(entry.get[entry.index('sel.first')..entry.index('sel.last')-1]); entry.delete(entry.index('sel.first'), entry.index('sel.last'))) if entry.selection_present}) if entry.selection_present

		menu.add(:command, :label=>'Copy', :command=>proc{ (TkClipboard.clear; TkClipboard.append(entry.get[entry.index('sel.first')..entry.index('sel.last')-1])) if entry.selection_present}) if entry.selection_present

		begin
			TkClipboard.get
		rescue RuntimeError
		else
			menu.add(:command, :label=>'Paste', :command=>proc{ entry.delete(entry.index('sel.first'), entry.index('sel.last')) if entry.selection_present; entry.insert(:insert, TkClipboard.get)})
		end

		menu
	end

	def self.generate_transfer_agent_menu
		@transferagentmenu.delete(0, @transferagentmenu.index(:end))
		result, data, event = send_blocking('AVAILAGENTS')
		if data.empty?
			@transferagentmenu.add(:command, :label=>'No Available Agents')
			return
		end

		callback = proc do |agent|
			dialog = Tk::Tile::Dialog.new(
				:type=>'yesno',
				:message=>'Carry out transfer?',
				:title=>'Transfer Confirmation',
				:command=>Proc.new{|x| 
					if x == 'yes'
						send('TRANSFER', 'agent', agent) do |result, data, event|
						unless result
							Thread.new{ Tk::Tile::Dialog.new(:type=>'ok', :message=>data, :title=>'Transfer Failed',
																							 :icon=>:warning).show }
						end
					end
				end
			})
			dialog.show
		end

		data.split(' ').each do |name|
			agentname, state, profileid = name.split(':', 3)
			@transferagentmenu.add(:command,
				:label=>"#{agentname}(#{state.to_i == Agent::IDLE ? 'I' : state.to_i == Agent::RELEASED ? 'R' : '?'}) - #{$main.profiles[profileid.to_i]}",
				:command=>proc{callback.call(agentname)})
		end
	end

	def self.show_buttons_for_state(state)
		return if state == Agent::PRECALL # don't do anything for precall state
		hide_all_buttons
		case state
			when Agent::ONCALL, Agent::OUTGOINGCALL
				show_transfer(:START)
				show_release
				toggle_hangup_menu_entry(true)
			when Agent::WRAPUP
				show_wrapup
				show_release
				toggle_hangup_menu_entry(false)
			when Agent::RELEASED
				show_unrelease
				show_dial(:BRAND)
				if !@brandvar.value.empty? or !@calleridvar.value.empty? or !@calltypevar.value.empty?
					hide_calldetails
				end
			when Agent::WARMXFER
				show_transfer(:END)
				show_transfer(:CANCEL)
			else
				show_release
				show_dial(:BRAND)

				if !@brandvar.value.empty? or !@calleridvar.value.empty? or !@calltypevar.value.empty?
					hide_calldetails
				end
			end
	end

	def self.handle_event(event)
		command, counter, data = event.split(' ', 3)
		case command
		when 'BLAB'
			Thread.new{ Tk::Tile::Dialog.new(:type=>'ok', :message=>data, :title=>'Incoming Message').show }
			
		when 'URL'
			if @main.config[:urlpop]
				$urlpop.call(data)
			end
		
		when 'ASTATE'
			@main.agent_state = data.to_i
			@statevar.value = @main.get_agent_state_name(@main.agent_state, @main.agent_statedata)
			show_buttons_for_state(@main.agent_state)

		when 'PROFILE'
			@main.agent_profile = data.to_i

			# Due to popular demand, this is now disabled.
			#Thread.new{ Tk::Tile::Dialog.new(:type=>'ok', :message=>"Your profile has been switched to #{@main.profiles[data.to_i]}", :title=>"Profile Switch").show }
		when 'CALLINFO'
			if !data.strip.empty?
				#Thread.new do
					#sleep 1 until @ready
					tempbrandvar, @calltypevar.value, cid = data.split(' ', 3)
					@calleridvar.value = URI.unescape(cid)
					@brandvar.value = @main.brandlist[tempbrandvar] ? @main.brandlist[tempbrandvar] : tempbrandvar
					show_calldetails
				#end
			else
				# TODO?! -- what now, jim?
			end
		when 'LOGOUT'
			puts "Logged off by server"
			exit
		when 'ASK'
			questiontype, question = data.split(' ', 2)
			ret = ''
				
			# NOTE: getting this thing to timeout and be destroyed correctly was a real pain in the ass
			# and I'm not thrilled with the result. Someone should figure out how to do this right someday.
			dialog = Tk::Tile::Dialog.new(:type=>questiontype, :message=>question)
			begin
				Timeout.timeout(20) do
					dialog.command{|x| ret = x}
					dialog.show
					sleep 2 until !ret.empty?
				end
			rescue Timeout::Error
				puts 'dialog timed out, destroying'
				ret = 'timeout'
				dialog.destroy
			end

			#begin
				#Timeout.timeout(10){ret = Tk.messageBox(:message=>question, :title=>'?', :parent=>@frame, :type=>questiontype)}
			#rescue Timeout::Error
				#ret = 'timeout'
				#dialog = @frame.winfo_children.detect{|x| x.class == TkWidget_Dialog} # trick to get the dialog widget
				#dialog.destroy # destroy the dialog
				 #NOTE: 
			#end
			return ret
		when 'WARMXFERFAILED'
			show_transfer(:START)
			hide_transfer(:END)
		end
	end

	def self.tick
		if Time.now.to_i % 5 == 0
			send('PING') do |result, data, event|
				if result
					@main.timeskew = Time.now.to_i - data.to_i
				else
					puts "PING failed: #{data}"
				end
			end
		end
	end
end
