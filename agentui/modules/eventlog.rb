
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

class EventLogModule < TabbedModule
	title 'Event Log'
	index 9
	accepts :ASTATE, :URL, :PROFILE, :BLAB, :CALLINFO

	def self.create(main)
		super
		@tabbar = @main.tabs
		@frame = Tk::Tile::TFrame.new(tabbar)#, :bg=>'blue')
		@scroll = Tk::Tile::TScrollbar.new(@frame).pack(:side=>:right, :fill=>:y)
		@log = TkText.new(@frame).pack(:side=>:top, :expand=>true, :fill=>:both)
		@log.yscrollbar(@scroll)

		@counter = 1
		@logmaxsize = 201

		# This makes the log area readonly.
		@log.bindtags_unshift(b = TkBindTag.new)
		b.bind('Key', proc{ Tk.callback_break })

		@log.tag_bind('hyperlink', 'Button 1') do | event |
			range1 = @log.index("@#{event.x},#{event.y} linestart")
			range2 = @log.index("@#{event.x},#{event.y} lineend")
			@link = @log.get(range1, range2).split('URL: ', 2)[1]

			#Pop URL to Explorer or Firefox when URL is clicked.
			#if $platform == :windows
        #require 'win32/open3'
				#Open3.popen3("start \"\" \"#{@link}\"")
			#else
				#`firefox -remote "openURL(#{@link})"`
			#end

			$urlpop.call(@link)

		end

		@log.tag_bind('hyperlink', 'Enter') do | event |
			@log.configure(:cursor, 'hand2')
		end
	
		@log.tag_bind('hyperlink', 'Leave') do | event |
			@log.configure(:cursor, 'xterm')
		end

	end

	def self.handle_event(event)
		
		timestamp = Time.now.strftime("%m/%d/%y %H:%M")
		eventtype, counter, data = event.split(' ',3)

		case eventtype
		when 'ASTATE'
			case data.to_i
			when Agent::UNKNOWN
				@log.insert(:end, "#{timestamp} - You are in an unknown state.\n")
			when Agent::OFFLINE
				@log.insert(:end, "#{timestamp} - You are going offline.  Goodbye.\n")
			when Agent::RINGING
				@log.insert(:end, "#{timestamp} - You are currently ringing.\n")
			when Agent::IDLE
				@log.insert(:end, "#{timestamp} - You have gone available.\n")
			when Agent::ONCALL
				@log.insert(:end, "#{timestamp} - You are currently in an inbound call.\n")
			when Agent::OUTGOINGCALL
				@log.insert(:end, "#{timestamp} - You are currently in an outbound call.\n")
			when Agent::RELEASED
				@log.insert(:end, "#{timestamp} - You have gone released.\n")
			when Agent::WARMXFER
				@log.insert(:end, "#{timestamp} - You are in a warm transfer.\n")
			when Agent::WRAPUP
				@log.insert(:end, "#{timestamp} - You have gone to wrapup.\n")
			when Agent::UNKNOWNPAUSE
				@log.insert(:end, "#{timestamp} - You are in an unknown pause.\n")
			else
			  if Agent::STATENAMES[data.to_i]
				@log.insert(:end,"#{timestamp} - You are now in #{Agent::STATENAMES[data.to_i]}\n")
			  else
				@log.insert(:end, "#{timestamp} - You are in an invalid state(#{data}).\n")
			  end
			end
		when 'URL'
			@log.insert(:end, "#{timestamp} - You have received the following URL: ")
			@log.insert(:end, "#{data}\n", 'hyperlink')
		when 'PROFILE'
			@log.insert(:end, "#{timestamp} - Your profile was changed to your #{@main.profiles[data.to_i]} profile.\n")
		when 'BLAB'
			@log.insert(:end, "#{timestamp} - You received the following message: #{data}\n")
		when 'CALLINFO'
			brand, type, callerid = data.split(' ', 3)
			@log.insert(:end, "#{timestamp} - Incoming #{type}: #{URI.unescape(callerid)} for client #{@main.brandlist[brand]}\n")
		else
			@log.insert(:end, "#{timestamp} - How should this be handled?:  #{event}\n")
		end
		
		@counter += 1

		if @counter > @logmaxsize
			@log.delete(1.0, 2.0)
		end

		@log.yview_moveto(1.0)

	end

end	
