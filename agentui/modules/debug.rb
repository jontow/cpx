
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

class DebugModule < TabbedModule
	title 'Debug'
	index 10
	noautoload true
	accepts :ALL

	def self.create(main)
		super
		@tabbar = @main.tabs
		@frame = Tk::Tile::TFrame.new(@tabbar)#, :bg=>'blue')
		@scroll = Tk::Tile::TScrollbar.new(@frame).pack(:side=>:right, :fill=>:y)
		@input = Tk::Tile::TEntry.new(@frame).pack(:side=>:bottom, :fill=>:x)
		@log = TkText.new(@frame).pack(:side=>:top, :expand=>true, :fill=>:both)
		@log.yscrollbar(@scroll)
		
		@input.bind("Return"){line_entry}

		@counter = 1
		@logmaxsize = 201

		# This makes the log area readonly.
		@log.bindtags_unshift(b = TkBindTag.new)
		b.bind('Key', proc{ Tk.callback_break })

	end

	def self.line_entry
		@log.insert(:end, "#{@counter} - SENT: #{@input.value}\n")

		@counter += 1

		if @counter > @logmaxsize
			@log.delete(1.0, 2.0)
		end

		command, data = @input.value.split(' ', 2)
		@input.value=''

		send(command, data) do |result, resdata, l|
			@log.insert(:end, "#{@counter} - RECV: #{l}\n")

			@counter += 1

			if @counter > @logmaxsize
				@log.delete(1.0, 2.0)
			end

			@log.yview_moveto(1.0)
		end

		@log.yview_moveto(1.0)
	end

	def self.handle_event(event)
		@log.insert(:end, "#{@counter} - RECV: #{event}\n")
		@counter += 1

		if @counter > @logmaxsize
			@log.delete(1.0, 2.0)
		end

		@log.yview_moveto(1.0)
		return nil
	end
end	
