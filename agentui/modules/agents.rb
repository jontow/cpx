
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

class AgentsModule < TabbedModule
	title 'Agents'
	index 1
	accepts :STATE, :BRIDGECALLER
	seclevel 2

	UPIMG = TkPhotoImage.new(:file=>File.join(File.dirname(__FILE__), '../images/uparrow.gif'));
	DOWNIMG = TkPhotoImage.new(:file=>File.join(File.dirname(__FILE__), '../images/downarrow.gif'));
	TILDEIMG = TkPhotoImage.new(:file=>File.join(File.dirname(__FILE__), '../images/tilde.gif'));

	BIASMAP = {-1 => DOWNIMG, 0 => TILDEIMG, 1 => UPIMG}

	class WatchedAgent
		attr_reader :name, :id
		attr_accessor :state, :time, :rowid, :data, :callsecs, :wrapupsecs, :idlesecs, :profile, :statedata

		def initialize(agentid, name, state=0, time=0)
			@id = agentid
			@name = name
			@state = state
			@time = time
			@rowid = ''
			@data = ''
			@profile = 0
			@realtimecallsecs = 0
			@realtimewrapupsecs = 0
			@realtimeidlesecs = 0
		end

		def realtime_metrics(main)
			@realtimecallsecs = @callsecs
			@realtimewrapupsecs = @wrapupsecs
			@realtimeidlesecs = @idlesecs

			case @state
			when Agent::IDLE
				@realtimeidlesecs += (Time.now - @time).to_i
			when Agent::RELEASED
				if main and main.releaseoptions and @statedata and res = main.releaseoptions.detect{|x| x[:id] == @statedata.to_i}
					if res[:utilbias] > 0
						@realtimecallsecs += (Time.now - @time).to_i
					elsif res[:utilbias] < 0
						@realtimeidlesecs += (Time.now - @time).to_i
					end
				end
			when Agent::ONCALL, Agent::OUTGOINGCALL
				@realtimecallsecs += (Time.now - @time).to_i
			when Agent::WRAPUP
				@realtimewrapupsecs += (Time.now - @time).to_i
			end
		end

		def callsecs
			@realtimecallsecs > @callsecs ? @realtimecallsecs : @callsecs
		end

		def idlesecs
			@realtimeidlesecs > @idlesecs ? @realtimeidlesecs : @idlesecs
		end

		def wrapupsecs
			@realtimewrapupsecs > @wrapupsecs ? @realtimewrapupsecs : @wrapupsecs
		end

		def util
			return 0 if self.callsecs + self.wrapupsecs + self.idlesecs == 0

			(((self.callsecs + self.wrapupsecs.to_f)/(self.callsecs + self.wrapupsecs + self.idlesecs))*100).to_i
		end

		def statetime
			Agent.calculate_duration(@time)
		end
	end

	class AgentProfile
		attr_accessor :rowid
		attr_reader :agents, :id

		def initialize(id)
			@agents = []
			@id = id
		end

		def add_agent(agent)
			@agents << agent
		end

		def del_agent(agent)
			@agents.delete agent
		end

		def name
			$main.profiles[id]
		end
	
		def state
			-1
		end

		def statetime
			''
		end

		def statedata
			nil
		end

		def bias
			return 0 if @agents.empty?
			v = @agents.inject(0){|sum, n| sum + AgentsModule.agent_util_bias(n) }
			v == 0 ? 0 : v < 0 ? -1 : 1
		end

		def data
			''
		end

		def realtime_metrics(n)
			#return [0,0,0] if @agents.empty?
			#@agents.inject([]){|sum,n| sum << n.realtime_metrics}.transpose.map{|x| x.inject{|sum,n| sum + n}/@agents.length}
		end

		def callsecs
			return 0 if @agents.empty?
			@agents.inject(0){|sum, n| sum + n.callsecs}/@agents.length
		end

		def wrapupsecs
			return 0 if @agents.empty?
			@agents.inject(0){|sum, n| sum + n.wrapupsecs}/@agents.length
		end
	
		def idlesecs
			return 0 if @agents.empty?
			@agents.inject(0){|sum, n| sum + n.idlesecs}/@agents.length
		end
	
		def util
			return 0 if @agents.empty?
			@agents.inject(0){|sum, n| sum + n.util}/@agents.length
		end

		def summarize
			[name, '', callsecs, wrapupsecs, idlesecs, util]
		end
	end

	def self.create(main)
		super
		@tabbar = @main.tabs
		@frame = Tk::Tile::TFrame.new(tabbar)
		@scroll = Tk::Tile::TScrollbar.new(@frame).pack(:side=>:right, :fill=>:y)
		@agentlist = Tk::Tile::Treeview.new(@frame, :columns=>[:name, :state, :callsecs, :wrapupsecs, :idlesecs, :util, :time, :data], :padding=>0).pack(:expand=>true, :fill=>:both)

		@agentlist.headingconfigure(:name, :text=>'Name')
		@agentlist.headingconfigure(:state, :text=>'State')
		@agentlist.headingconfigure(:callsecs, :text=>'Call')
		@agentlist.headingconfigure(:wrapupsecs, :text=>'Wrapup')
		@agentlist.headingconfigure(:idlesecs, :text=>'Idle')
		@agentlist.headingconfigure(:util, :text=>'Util')
		@agentlist.headingconfigure(:time, :text=>'Time')
		@agentlist.headingconfigure(:data, :text=>'Caller Brand')


		@agentlist.columnconfigure('#0', :width=>50)
		@agentlist.columnconfigure(:name, :width=>100)
		@agentlist.columnconfigure(:state, :width=>80)
		@agentlist.columnconfigure(:callsecs, :width=>40)
		@agentlist.columnconfigure(:wrapupsecs, :width=>50)
		@agentlist.columnconfigure(:idlesecs, :width=>40)
		@agentlist.columnconfigure(:util, :width=>40)

		@agentlist.yscrollbar(@scroll)

		$rightclick.each do |bind|
			@agentlist.tag_bind('agent', bind){ create_agent_menu(get_selected_agent).post(*@tabbar.winfo_pointerxy)if get_selected_agent }
		end

		@agentlist.tag_bind('agent', 'Button 1'){@agentmenu.unpost if @agentmenu; @agentmenu = nil}
		@mutex = Mutex.new
		@agents = {}
		@profiles = {}

	end

	def self.bootstrap
		send('AGENTS', 'ALL') do |result, data, event|#get the list of current logged on agents
			@ready = true

			sort_agents

			@agents.each do |k, a|
				update_row(a)
			end
			@profiles.each do |k, a|
				update_row(a)
			end
		end
	end

	def self.sort_agents
		return unless self.ready?
		@profiles.values.sort_by{|x| [x.id]}.each_with_index do |profile, i|
			@agentlist.move(profile.rowid, '', i)
			@agents.values.select{|x| x.profile == profile.id}.sort_by{|x| [x.name.downcase]}.each_with_index do |a, j|
				@agentlist.move(a.rowid, profile.rowid, j)
			end
		end
	end

	def self.update_row(agent)
		return unless self.ready?
		if agent.respond_to?(:bias)
			@agentlist.itemconfigure(agent.rowid, :image, BIASMAP[agent.bias])
		else
			@agentlist.itemconfigure(agent.rowid, :image, BIASMAP[agent_util_bias(agent)])
		end
		@agentlist.set(agent.rowid, :name,  agent.name)
		@agentlist.set(agent.rowid, :state, @main.get_agent_state_name(agent.state, agent.statedata))
		@agentlist.set(agent.rowid, :callsecs, (agent.callsecs/60).to_s)
		@agentlist.set(agent.rowid, :wrapupsecs, (agent.wrapupsecs/60).to_s)
		@agentlist.set(agent.rowid, :idlesecs, (agent.idlesecs/60).to_s)
		@agentlist.set(agent.rowid, :util, "#{agent.util}%")
		@agentlist.set(agent.rowid, :time, agent.statetime)
		@agentlist.set(agent.rowid, :data, agent.data)
	end

	def self.get_selected_agent
		@agents.values.detect{|x| x.rowid == @agentlist.selection[-1] }
	end

	def self.create_agent_menu(agent)
		@agentmenu.unpost if @agentmenu
		@agentmenu = TkMenu.new(@frame, :tearoff=>false)
		if agent.state == Agent::ONCALL or agent.state == Agent::OUTGOINGCALL
			@agentmenu.add(:command, :label=>'Spy', :command=>proc{ send('DIAL', "00000000 util chanspy 1 AGENT=#{agent.id}") })
			#@agentmenu.add(:command, :label=>'Spy and Whisper', :command=>proc{ send('DIAL', "00000000 util chanspywhisper 1 AGENT=#{agent.id}") })
		elsif agent.state == Agent::WRAPUP
			@agentmenu.add(:command, :label=>'End Wrapup', :command=>proc{ send('AENDWRAPUP', agent.id) })
		elsif agent.state == Agent::IDLE
			@agentmenu.add(:command, :label=>'Set Released', :command=>proc{ send('ASTATE', agent.id, Agent::RELEASED) })
		elsif agent.state == Agent::RELEASED
			@agentmenu.add(:command, :label=>'Set Idle', :command=>proc{ send('ASTATE', agent.id, Agent::IDLE) })
		end

		@agentmenu.add(:command, :label=>'Kick', :command=>proc{ send('KICK', agent.id) })

		profilemenu = TkMenu.new(@agentmenu, :tearoff=>false)
		@main.profiles.keys.sort.each do |k|
			v = @main.profiles[k]
			profilemenu.add(:command, :label=>v, :command=>proc{ send('APROFILE', agent.id, k) } )
		end
		@agentmenu.add(:cascade, :menu=>profilemenu, :label=>'Set profile')
		@agentmenu
	end

	def self.agent_util_bias(agent)
		case agent.state
		when Agent::IDLE
			-1
		when Agent::RELEASED
			if agent.state == Agent::RELEASED and agent.statedata and @main.releaseoptions and
				res = main.releaseoptions.detect{|x| x[:id] == agent.statedata.to_i}
				res[:utilbias]
			else
				-1
			end
		when Agent::ONCALL, Agent::OUTGOINGCALL, Agent::WRAPUP
			1
		else
			0
		end
	end

    def self.handle_event(event)
		@mutex.synchronize do
			cmd, num, data = event.split(' ', 3)
			case cmd
			when 'STATE'
				time, agentid, name, state, state_data, aprofile, callsecs, wrapupsecs, idlesecs = data.split(' ')
				agentid = agentid.to_i
				
				if profile = @profiles[aprofile.to_i]
					#puts "profile #{profile.name} already exists"
				else
					profile = AgentProfile.new(aprofile.to_i)
					@profiles[aprofile.to_i] = profile
					profile.rowid = @agentlist.insert('', :end, :open=>false, :values=>profile.summarize)
					#puts "added profile #{profile.name}"
				end

				if @agents[agentid] and @agentlist.exist? @agents[agentid].rowid
					agent = @agents[agentid]
					if state.to_i < Agent::IDLE
						@agentlist.delete(agent.rowid)
						@agents.delete(agentid)
						profile = @profiles[agent.profile]
						profile.del_agent(agent)
						# cleanup any empty profiles
						if profile.agents.empty?
							@agentlist.delete(profile.rowid)
							@profiles.delete agent.profile
						end
						return
					end

					agent.state = state.to_i
					agent.time = time.to_i
					agent.callsecs = callsecs.to_i
					agent.wrapupsecs = wrapupsecs.to_i
					agent.idlesecs = idlesecs.to_i
					agent.statedata = state_data
					
					if agent.profile != aprofile.to_i
						oldprofile = @profiles[agent.profile]
						oldprofile.del_agent(agent)
						@agentlist.delete(agent.rowid)
						if oldprofile.agents.empty?
							#puts "#{oldprofile.name} is empty"
							@agentlist.delete(oldprofile.rowid)
							@profiles.delete agent.profile
						else
							update_row(oldprofile)
						end
						profile.add_agent(agent)
						agent.profile = aprofile.to_i
						agent.rowid = @agentlist.insert(profile.rowid, :end, :open=>false, :values=>[name, @main.get_agent_state_name(agent.state, agent.statedata), callsecs, wrapupsecs, idlesecs, "#{agent.util}%", agent.statetime], :tags=>['agent'])
					end

					if agent.state != Agent::ONCALL and agent.state != Agent::WRAPUP
						agent.data = ''
					end
					
					sort_agents
					update_row(profile)
					update_row(agent)
				else
					agent = WatchedAgent.new(agentid, name, state.to_i, time.to_i)

					@agents[agentid] = agent
					agent.callsecs = callsecs.to_i
					agent.wrapupsecs = wrapupsecs.to_i
					agent.idlesecs = idlesecs.to_i
					agent.profile = aprofile.to_i
					agent.statedata = state_data
					profile.add_agent(agent)
					agent.rowid = @agentlist.insert(profile.rowid, :end, :open=>false, :values=>[name, @main.get_agent_state_name(agent.state, agent.statedata), callsecs, wrapupsecs, idlesecs, "#{agent.util}%", agent.statetime], :tags=>['agent'])

					update_row(profile)
				end

			when 'BRIDGECALLER'
				agentid, uniqueid, brandid, callerid = data.split(' ')
				cid = URI.unescape(callerid)
				if agent = @agents[agentid.to_i]
					agent.data = "#{@main.brandlist[brandid]} (#{cid})"
					update_row(agent)
				end
			end
		end
	end

	def self.tick
		if @mutex.locked?
			puts 'avoiding deadlock when ticking agents tab'
			return
		end
		@mutex.synchronize do

			if (Time.now.to_i % 10) == 0
				@agents.merge(@profiles).each do |id, agent|
					#p agent.rowid
					# TODO - use update_row() here instead?
					@agentlist.set(agent.rowid, :time, agent.statetime)

					agent.realtime_metrics(@main) # ugh, passing in main sucks

					#@agentlist.itemconfigure(agent.rowid, :image, '')
					if agent.respond_to?(:bias)
						@agentlist.itemconfigure(agent.rowid, :image, BIASMAP[agent.bias])
					else
						@agentlist.itemconfigure(agent.rowid, :image, BIASMAP[agent_util_bias(agent)])
					end
					@agentlist.set(agent.rowid, :util, "#{agent.util}%")
					@agentlist.set(agent.rowid, :idlesecs, (agent.idlesecs/60).to_s)
					@agentlist.set(agent.rowid, :callsecs, (agent.callsecs/60).to_s)
					@agentlist.set(agent.rowid, :wrapupsecs, (agent.wrapupsecs/60).to_s)
				end
			end
		end
	end
end
