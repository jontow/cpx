
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


class ConfigModule < TabbedModule
	title 'Config'
	index 20

	def self.create(main)
		super
		@tabbar = @main.tabs
		@hidden = true
		@frame = Tk::Tile::TFrame.new(tabbar)#, :bg=>'red')
		@buttonframe = Tk::Tile::TFrame.new(@frame).pack(:side=>:bottom, :anchor=>:center, :pady=>20)
		@applybutton = Tk::Tile::TButton.new(@buttonframe, :text=>"Apply").pack(:side=>:left, :padx=>10)
		@cancelbutton = Tk::Tile::TButton.new(@buttonframe, :text=>'Cancel').pack(:side=>:left, :padx=>10)
		@resetbutton = Tk::Tile::TButton.new(@buttonframe, :text=>'Reset to Defaults').pack(:side=>:left, :padx=>10)
		@savedimensions = TkVariable.new(0)
		@urlpop = TkVariable.new(0)
		@crmpop = TkVariable.new(0)
		@savenotes = TkVariable.new(0)
		@autoloadfiles = TkVariable.new(0)
		@savechannels = TkVariable.new(0)

		savedimensionsbutton = Tk::Tile::CheckButton.new(@frame, :text=> 'Save window dimensions', :variable=>@savedimensions).pack
		savedimensionsbutton.invoke if @main.config[:savedimensions]

		urlpopbutton = Tk::Tile::CheckButton.new(@frame, :text=> 'Automatically pop URLs', :variable=>@urlpop).pack
		urlpopbutton.invoke if @main.config[:urlpop]

		crmpopbutton = Tk::Tile::CheckButton.new(@frame, :text=> 'Automatically open CRM on login', :variable=>@crmpop).pack
		crmpopbutton.invoke if @main.config[:crmpop]



		savenotesbutton = Tk::Tile::CheckButton.new(@frame, :text=> 'Save notes automatically', :variable=>@savenotes).pack
		savenotesbutton.invoke if @main.config[:savenotes]

		autoloadfiles = Tk::Tile::CheckButton.new(@frame, :text=> 'Automatically reload current tab configuration', :variable=>@autoloadfiles).pack
		autoloadfiles.invoke if @main.config[:autoload]
	
		savechannels = Tk::Tile::CheckButton.new(@frame, :text=> 'Save list of joined channels', :variable=>@savechannels).pack
		savechannels.invoke if @main.config[:savechannels]

        
		@applybutton.command{ apply }
		@cancelbutton.command{ cancel }
		@resetbutton.command{ reset }
	end

	def self.apply
		save
		@tabbar.tabconfigure(TabbedModule.get_tab_index(get_name), :state=>:hidden)
	end

	def self.cancel
		@tabbar.tabconfigure(TabbedModule.get_tab_index(get_name), :state=>:hidden)
	end

	def self.reset
		@main.config.clear # wipe the config
		@main.config.merge!(@main.defaultconfig) # re-insert the defaults
		@tabbar.tabconfigure(TabbedModule.get_tab_index(get_name), :state=>:hidden)
	end

	def self.destroy
		super
		save
	end

	def self.save
		# save the config
		config = @main.config
		config[:savedimensions] = !@savedimensions.value.to_i.zero?
		config[:urlpop] = !@urlpop.value.to_i.zero?
		config[:crmpop] = !@crmpop.value.to_i.zero?
    config[:savenotes] = !@savenotes.value.to_i.zero?
		config[:autoload] = !@autoloadfiles.value.to_i.zero?
		config[:savechannels] = !@autoloadfiles.value.to_i.zero?

		unless config[:savedimensions]
			config.delete(:width)
			config.delete(:height)
		else
			config[:width] = @main.root.winfo_width
			config[:height] = @main.root.winfo_height
		end

		config.delete(:autoloadfiles) unless config[:autoload]

		File.mkdir(File.dirname($configfile)) unless File.directory? File.dirname($configfile)
		File.open($configfile, 'w') {|f| f.puts YAML.dump(config)}
	end
end

