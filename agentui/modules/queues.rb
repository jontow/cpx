
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

class QueuesModule < TabbedModule
	title 'Queues'
	index 2
	accepts :QUEUE, :QUEUECALLER, :QUEUECALLERREM, :STATE, :QUEUEMEMBER
	seclevel 2

	class WatchedQueue
		attr_reader :agents
		attr_accessor :calls, :completed, :abandoned, :rowid, :callers, :agents, :avg_queuetime, :max_queuetime, :group, :weight

		def initialize(name)
			@name = name
			@callers = []
			@agents = {}
			@calls = 0
			@weight = 0
		end

		def group?
			false
		end

		def add_agent(agent)
			@agents[agent.id] = agent
		end

		def del_agent(agent)
			@agents.delete(agent)
		end

		def add_caller(call, position)
			@callers.insert(position, call)
			@callers.compact!
		end

		def del_caller(call)
			@callers.delete(call)
		end

		def agent_count
			@agents.length
		end

		def avail_agent_count
			@agents.values.select{|a| a.state == Agent::IDLE}.length
		end

		def name
			$main.queuelist[@name] || @name
		end
	end

	class WatchedAgent
		attr_reader :id, :name
		attr_accessor :state, :time, :statedata

		def initialize(id, name)
			@name = name
			@id = id
			@time = Time.now.to_i
		end
	end

	class WatchedAgentOnQueue
		attr_accessor :rowid
		def initialize(agent)
			@agent = agent
		end

		def method_missing(sym, *args)
			@agent.__send__(sym, *args)
		end

		def ==(other)
			other == @agent
		end

		def id
			@agent.id
		end
	end

	class WatchedCaller
		attr_reader :uniqueid
		attr_accessor :brandid, :callerid, :rowid, :enteredqueue, :type

		def initialize(uniqueid)
			@uniqueid = uniqueid
		end
	end

	class QueueGroup
		attr_accessor :rowid
		attr_reader :queues, :name, :id

		def initialize(id, name)
			@queues = []
			@id = id
			@name = name
			@open = false
		end

		def open
			@open = true
		end

		def close
			@open = false
		end

		def open?
			@open
		end

		def group?
			true
		end

		def add_queue(queue)
			@queues << queue
		end

		def del_queue(queue)
			@queues.delete queue
		end

		def calls
			return 0 if @queues.empty?
			@queues.inject(0){|sum, n| sum + n.calls}
		end

		def callers
			return [] if @queues.empty?
			@queues.inject([]){|sum, n| sum + n.callers}
		end

		def agents
			@queues.inject({}){|sum, n| sum.merge(n.agents)}
		end

		def group
			self
		end

		def completed
			return 0 if @queues.empty?
			@queues.inject(0){|sum, n| sum + n.completed}
		end

		def abandoned
			return 0 if @queues.empty?
			@queues.inject(0){|sum, n| sum + n.abandoned}
		end

		def agent_count
			return [] if @queues.empty?
			@queues.inject([]){|sum, n| sum + n.agents.values.map{|a| a.id}}.uniq.length
		end

		def avail_agent_count
			return [] if @queues.empty?
			@queues.inject([]){|sum, n| sum + n.agents.values.select{|a| a.state == Agent::IDLE}.map{|a| a.id}}.uniq.length
		end

		def avg_queuetime
			return 0 if @queues.empty?
			@queues.inject(0){|sum, n| sum + n.avg_queuetime}/@queues.length
		end

		def max_queuetime
			return 0 if @queues.empty?
			@queues.inject(0){|sum, n| sum + n.max_queuetime}/@queues.length
		end

		def summarize
			[@name]
		end
	end


	def self.create(main)
		super
		@tabbar = @main.tabs
		@frame = Tk::Tile::TFrame.new(tabbar)
		@statframe = Tk::Tile::TFrame.new(@frame).pack(:side=>:top)

		@statlabelvar = TkVariable.new()
		@statlabel = Tk::Tile::Label.new(@statframe, :textvariable=>@statlabelvar).pack

		@vpane = Tk::Tile::Paned.new(@frame, :orient=>:vertical).pack(:expand=>true, :fill=>:both)
		@qframe = Tk::Tile::TFrame.new(@vpane)
		@queuelist = Tk::Tile::Treeview.new(@qframe, :columns=>[:name, :calls, :agents, :avagents, :completed, :abandoned, :avg_queue, :max_queue], :padding=>0).pack(:expand=>true, :fill=>:both, :side=>:left)
		@qscroll = Tk::Tile::TScrollbar.new(@qframe).pack(:side=>:right, :fill=>:y)
		@queuelist.yscrollbar(@qscroll)
		@vpane.add(@qframe, :weight=>5)
		@hpane = Tk::Tile::Paned.new(@vpane, :orient=>:horizontal)
		@vpane.add(@hpane, :weight=>1)

		@aframe = Tk::Tile::TFrame.new(@hpane)
		@ascroll = Tk::Tile::TScrollbar.new(@aframe).pack(:side=>:right, :fill=>:y)
		@agentlist = Tk::Tile::Treeview.new(@aframe, :columns=>['Name', 'State', 'Time'], :show=>[:headings], :padding=>0).pack(:side=>:left, :fill=>:both, :expand=>true)
		@agentlist.yscrollbar(@ascroll)

		@cframe = Tk::Tile::TFrame.new(@hpane)
		@cscroll = Tk::Tile::TScrollbar.new(@cframe).pack(:side=>:right, :fill=>:y)
		@callerlist = Tk::Tile::Treeview.new(@cframe, :columns=>[:callerid, :calltype, :hold, :brand], :show=>[:headings], :padding=>0).pack(:side=>:left, :fill=>:both, :expand=>true)
		@callerlist.yscrollbar(@cscroll)

		@queuegrouplist = {}
		@queuegroups = {}

		@hpane.add(@aframe, :weight=>0)
		@hpane.add(@cframe, :weight=>0)
		@mutex = Mutex.new

		@queuelist.headingconfigure(:name, :text=>'Queue')
		@queuelist.headingconfigure(:calls, :text=>'Calls')
		@queuelist.headingconfigure(:agents, :text=>'Agents')
		@queuelist.headingconfigure(:avagents, :text=>'Avail. Agents')
		@queuelist.headingconfigure(:completed, :text=>'Completed')
		@queuelist.headingconfigure(:abandoned, :text=>'Abandoned')
		@queuelist.headingconfigure(:avg_queue, :text=>'Avg. Queue')
		@queuelist.headingconfigure(:max_queue, :text=>'Max Queue')

		@queuelist.columnconfigure('#0', :width=>20)
		@queuelist.columnconfigure(:name, :width=>150)
		@queuelist.columnconfigure(:calls, :width=>60)
		@queuelist.columnconfigure(:agents, :width=>60)
		@queuelist.columnconfigure(:avagents, :width=>100)
		@queuelist.columnconfigure(:completed, :width=>100)
		@queuelist.columnconfigure(:abandoned, :width=>100)
		@queuelist.columnconfigure(:avg_queue, :width=>100)
		@queuelist.columnconfigure(:max_queue, :width=>100)

		@callerlist.headingconfigure(:callerid, :text=>'Caller ID')
		@callerlist.headingconfigure(:calltype, :text=>'Type')
		@callerlist.headingconfigure(:hold, :text=>'Hold Time')
		@callerlist.headingconfigure(:brand, :text=>'Brand')

		@callerlist.columnconfigure(:callerid, :width=>120)
		@callerlist.columnconfigure(:calltype, :width=>70)
		@callerlist.columnconfigure(:hold, :width=>80)
		@callerlist.columnconfigure(:brand, :width=>60)

		$rightclick.each do |bind|
			@callerlist.tag_bind('caller', bind){ create_caller_menu(get_selected_caller).post(*@tabbar.winfo_pointerxy) if get_selected_caller }
		end

		@callerlist.tag_bind('caller', 'Button 1'){@callermenu.unpost if @callermenu; @callermenu = nil}

		@agentlist.headingconfigure('Name', :text=>'Name')
		@agentlist.headingconfigure('State', :text=>'State')
		@agentlist.headingconfigure('Time', :text=>'Time')

		@agentlist.columnconfigure('Name', :width=>100)
		@agentlist.columnconfigure('State', :width=>100)
		@agentlist.columnconfigure('Time', :width=>150)
		
		@queuelist.bind('<TreeviewSelect>') do
			if queue = (@queues.values + @queuegroups.values).detect{|q| q.rowid == @queuelist.selection[-1]}
				unless @currentqueue == queue
					@currentqueue = queue
					update_row(queue)
				end
			end
		end

		@queuelist.bind('<TreeviewOpen>') do |x|
			if group = @queuegroups.values.detect{|q| q.rowid == @queuelist.selection[-1]}
				group.open
				group.queues.each do |q|
					update_row(q)
				end
			end
		end
	
		@queuelist.bind('<TreeviewClose>') do |x|
			if group = @queuegroups.values.detect{|q| q.rowid == @queuelist.selection[-1]}
				group.close
			end
		end

		@currentqueue = nil

		@queues = {}
		@agents = {}
	end

	def self.bootstrap
		send('QUEUEGROUPS') do |result, data, event|
			if result
				data.split(' ').each do |x|
					id, name = x.split(':', 2)
					@queuegrouplist.store(id.to_i, name)
				end
			end

			send('QUEUES', 'ALL') do |a, b, c| #get the list of current logged on agents
				send('QUEUECALLERS', 'ALL') do |a, b, c|
					send('QUEUEMEMBERS', 'ALL') do |a, b, c|
						@ready = true
						sort_queues
						@queues.each do |name, queue|
							update_row(queue)
						end
						@queuegroups.each do |k, group|
							update_row(group)
						end
					end
				end
			end
		end
	end

	def self.get_selected_caller
		@currentqueue.callers.detect{|x| x.rowid == @callerlist.selection[-1] }
	end
	
	def self.create_caller_menu(call)
		@callermenu.unpost if @callermenu
		@callermenu = TkMenu.new(@frame, :tearoff=>false)
		
		agenttransfermenu = TkMenu.new(@callermenu, :tearoff=>false)
		@queues.values.inject([]){|sum, n| sum + n.agents.values.select{|a| [Agent::IDLE, Agent::RELEASED].include? a.state}.map{|a| [a.id, a.name, a.state]}}.uniq.each do |agent|
			agenttransfermenu.add(:command, :label=>"#{agent[1]}(#{Agent::STATENAMES[agent[2]]})", :command=>proc{send('ATRANSFER', 'agent', call.uniqueid, agent[0])})
		end
		@callermenu.add(:cascade, :menu=>agenttransfermenu, :label=>'Transfer to Agent')
		if call.type == 'email'
			@callermenu.add(:command, :label=>'View Email', :command=> proc do
				send('CALLINFO', call.uniqueid) do |result, data, event|
					p result
					if result
						$urlpop.call(data.split(' ')[-1])
					end
				end
			end
			)
			@callermenu.add(:command, :label=>'Treat as Spam', :command => proc{ send('AHANGUP', call.uniqueid, 'Marked as spam')})
		elsif call.type == 'call' or call.type == 'outgoing'
			@callermenu.add(:command, :label=>'Redirect to Voicemail', :command => proc{ 
				dialog = Tk::Tile::Dialog.new(
					:type=>'yesno',
					:message=>'Really send to voicemail?',
					:title=>'Voicemail Confirmation',
					:command=>Proc.new{|x| 
						if x == 'yes'
							puts 'doing vm transfer'
							send('ATRANSFER', 'voicemail', call.uniqueid)
						end
					})
				dialog.show
			})
		end
		@callermenu
	end

	def self.sort_queues
		return unless self.ready?
		@queuegroups.values.sort_by{|x| [x.id]}.each_with_index do |group, i|
			@queuelist.move(group.rowid, '', i)
			@queues.values.select{|x| x.group == group.id}.sort_by{|x| [x.name.downcase]}.each_with_index do |q, i|
				@queuelist.move(q.rowid, group.rowid, i)
			end
		end
	end

	def self.update_row(queue)
		return unless self.ready?
		return unless queue.group? or ( @queuegroups[queue.group] and (@queuegroups[queue.group].open?) )
		@queuelist.set(queue.rowid, :name, queue.name)
		@queuelist.set(queue.rowid, :calls, queue.calls)
		@queuelist.set(queue.rowid, :agents, queue.agent_count)
		@queuelist.set(queue.rowid, :avagents, queue.avail_agent_count)
		@queuelist.set(queue.rowid, :completed, queue.completed)
		@queuelist.set(queue.rowid, :abandoned, queue.abandoned)
		@queuelist.set(queue.rowid, :avg_queue, queue.avg_queuetime)
		@queuelist.set(queue.rowid, :max_queue, queue.max_queuetime)

		if queue == @currentqueue
			update_callers(queue)
			update_agents(queue)
		end
	end

	def self.update_callers(queue)
		rows = @callerlist.children('')
		orphans = rows - queue.callers.map{|c| c.rowid} #find any orphaned rows
		orphans.each do |c|
			@callerlist.delete c #remove the orphans
		end
		queue.callers.each do |c|
			c.rowid = @callerlist.insert('', :end, :open=>false, :values=>[c.callerid, c.type, Agent.calculate_duration(c.enteredqueue), @main.brandlist[c.brandid]], :tags=>['caller']) unless rows.include? c.rowid 
			@callerlist.set(c.rowid, :hold, Agent.calculate_duration(c.enteredqueue))
			@callerlist.move(c.rowid, '', queue.callers.index(c)) 
		end
	end

	def self.update_agents(queue)
		rows = @agentlist.children('')
		orphans = rows - queue.agents.values.map{|a| a.rowid} #find any orphaned rows
		orphans.each do |a|
			@agentlist.delete a #remove the orphans
		end

		queue.agents.each do |k,a|
			a.rowid = @agentlist.insert('', :end, :open=>false, :values=>[a.name, 0, 0]) unless rows.include? a.rowid #add any new rows
			@agentlist.set(a.rowid, 'State', @main.get_agent_state_name(a.state, a.statedata))
			@agentlist.set(a.rowid, 'Time', Agent.calculate_duration(a.time))
		end
	end

	def self.handle_event(event)
		@mutex.synchronize do
			cmd, num, data = event.split(' ', 3)
			case cmd
			when 'QUEUE'
				name, queuegroup, weight, calls, completed, abandoned, avg_queuetime, max_queuetime = data.split(' ')

				if group = @queuegroups[queuegroup.to_i]
				else
					group = QueueGroup.new(queuegroup.to_i, @queuegrouplist[queuegroup.to_i])
					@queuegroups[queuegroup.to_i] = group
					group.rowid = @queuelist.insert('', :end, :open=>false, :values=>group.summarize)
				end

				if queue = @queues[name] and queue.rowid
				else
					queue = WatchedQueue.new(name)
					queue.rowid = @queuelist.insert(group.rowid, :end, :open=>false, :values=>[name])
					@queues[name] = queue
					@currentqueue ||= queue
					group.add_queue(queue)
				end

				queue.calls = calls.to_i
				queue.group = queuegroup.to_i
				queue.weight = weight.to_i
				queue.completed = completed.to_i
				queue.abandoned = abandoned.to_i
				queue.avg_queuetime = avg_queuetime.to_i
				queue.max_queuetime = max_queuetime.to_i
				update_row(queue)
				update_row(group)

			when 'QUEUECALLER'
				time, name, uniqueid, type, position, brandid, callerid = data.split(' ')
				if queue = @queues[name] and queue.rowid
					if call = queue.callers.detect {|c| c.uniqueid == uniqueid}
					else
						call = WatchedCaller.new(uniqueid)
						call.enteredqueue = time.to_i
						call.type = type
						call.brandid = brandid
						call.callerid = URI.unescape(callerid)
						queue.calls += 1
						queue.add_caller(call, position.to_i)
					end

					update_row(queue)
					update_row(@queuegroups[queue.group])
				end

			when 'QUEUECALLERREM'
				name, uniqueid, completed, abandoned, avg_queuetime, max_queuetime = data.split(' ')
				if queue = @queues[name] and queue.rowid
					if call = queue.callers.detect {|c| c.uniqueid == uniqueid}
						queue.del_caller(call)
						queue.calls -= 1
						queue.completed = completed.to_i
						queue.abandoned = abandoned.to_i
						queue.avg_queuetime = avg_queuetime.to_i
						queue.max_queuetime = max_queuetime.to_i
					end
					update_row(queue)
					update_row(@queuegroups[queue.group])
				end

			when 'QUEUEMEMBER'
				agentid, agentname, statetime, state, queues = data.split(' ')
				unless agent = @agents[agentid.to_i]
					agent = WatchedAgent.new(agentid.to_i, agentname)
					@agents[agentid.to_i] = agent
					agent.state = state.to_i
					agent.time = statetime.to_i
				end
				groups = []

				if queues.nil? or queues.empty?
					# WTF
					@queues.each do |name, queue|
						if wagent = queue.agents[agentid.to_i]
							queue.del_agent(wagent.id)
							update_row(queue)
							groups << @queuegroups[queue.group]
						end
					end
				else
					queues = queues.split(':')
					@queues.each do |name, queue|

						if queues.include? name
							unless queue.agents[agentid.to_i]
								wagent = WatchedAgentOnQueue.new(agent)
								queue.add_agent(wagent)
							end
						else
							if wagent = queue.agents[agentid.to_i]
								queue.del_agent(wagent.id)
							end
						end
						update_row(queue)
						groups << @queuegroups[queue.group]
					end
				end

				groups.compact.uniq.each do |g|
					update_row(g)
				end

			when 'STATE'
				Tk.after_idle do
					groups = []
					time, agentid, name, state, state_data, profile, callsecs, wrapupsecs, idlesecs = data.split(' ')
					if agent = @agents[agentid.to_i]
						agent.state = state.to_i
						agent.statedata = state_data
						agent.time = time.to_i
						if state.to_i < Agent::IDLE
							@queues.each do |k, q|
								if wagent = q.agents[agentid.to_i]
									q.del_agent(wagent.id)
									update_row(q)
									groups << @queuegroups[q.group]
								end
							end
							groups.compact.uniq.each do |g|
								update_row(g)
							end
						end
					end
				end
			end
		end
	end

	def self.tick
		if @mutex.locked?
			puts 'avoiding deadlock when ticking queues tab'
			return
		end
		@mutex.synchronize do
			return unless @currentqueue
			@currentqueue.agents.each do |k, a|
				@agentlist.set(a.rowid, 'Time', Agent.calculate_duration(a.time))
				@agentlist.set(a.rowid, 'State', @main.get_agent_state_name(a.state, a.statedata))
			end
			@currentqueue.callers.each do |c|
				@callerlist.set(c.rowid, :hold, Agent.calculate_duration(c.enteredqueue))
			end
		end
	end
end

