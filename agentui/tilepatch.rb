# monkey-patch some bugs/missing methods in Tk/Tile

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

# many thanks to Hidetoshi NAGAI (nagai@ai.kyutech.ac.jp)

puts "Loading custom Tk/Tile overrides"
module TileWidget
	def identify(x, y)
		ret = tk_send_without_enc('identify', x, y)
		(ret.empty?)? nil: ret
	end
end

module Tk
	module Tile
		class SizeGrip < TkWindow
		end
	end
end

class Tk::Tile::SizeGrip < TkWindow
	include Tk::Tile::TileWidget

	TkCommandNames = ['::ttk::sizegrip'.freeze].freeze
	WidgetClassName = 'TSizegrip'.freeze
	WidgetClassNames[WidgetClassName] = self

	def self.style(*args)
		[self::WidgetClassName, *(args.map!{|a| _get_eval_string(a)})].join('.')
	end
end

class << Tk::Tile::Style
	def lookup(style, opt, state=None, fallback_value=None)
		tk_call('style', 'lookup', style, '-' << opt.to_s, state, fallback_value)
	end
end

class Tk::Tile::TCombobox
	def current
		number(tk_send_without_enc('current'))
	end

	remove_method :identify if self.methods(false).include? 'identify'
end

class Tk::Tile::SizeGrip < TkWindow
	include Tk::Tile::TileWidget

	TkCommandNames = ['::ttk::sizegrip'.freeze].freeze
	WidgetClassName = 'TSizegrip'.freeze
	WidgetClassNames[WidgetClassName] = self

	def self.style(*args)
		[self::WidgetClassName, *(args.map!{|a| _get_eval_string(a)})].join('.')
	end
end

class Tk::Tile::TNotebook
	def __item_listval_optkeys(id)
		[]
	end

	def __item_methodcall_optkeys(id)
		{}
	end

	private :__item_methodcall_optkeys

	def selected
		window(tk_send_without_end('select'))
	end
end

class Tk::Tile::Treeview
	def bbox(item, column=None)
		list(tk_send('item', 'bbox', item, column))
	end

	def row_identify(x, y)
		tk_send('identify', 'row', x, y)
	end

	def column_identify(x, y)
		tk_send('identify', 'column', x, y)
	end


	def insert(parent, idx, keys={})
		keys = _symbolkey2str(keys)
		id = keys.delete('id')
		if id
			num_or_str(tk_send('insert', parent, idx, '-id', id, *hash_kv(keys)))
		else
			num_or_str(tk_send('insert', parent, idx, *hash_kv(keys)))
		end
	end

	alias get_dictionary get_directory

	def tag_bind(tag, seq, *args)
		if TkComm._callback_entry?(args[0]) || !block_given?
			cmd = args.shift
		else
			cmd = Proc.new
		end
		_bind([@path, 'tag', 'bind', tag], seq, cmd, *args)
		self
	end
	alias tagbind tag_bind

	def tag_bind_append(tag, seq, *args)
		if TkComm._callback_entry?(args[0]) || !block_given?
			cmd = args.shift
		else
			cmd = Proc.new
		end
		_bind_append([@path, 'tag', 'bind', tag], seq, cmd, *args)
		self
	end
	alias tagbind_append tag_bind_append

	def tag_bind_remove(tag, seq)
		_bind_remove([@path, 'tag', 'bind', tag], seq)
		self
	end
	alias tagbind_remove tag_bind_remove

	def tag_bindinfo(tag, context=nil)
		_bindinfo([@path, 'tag', 'bind', tag], context)
	end
	alias tagbindinfo tag_bindinfo
end
