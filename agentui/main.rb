#!/usr/bin/env ruby

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'socket'
require 'thread'
require 'monitor'
require 'yaml'
require 'digest/md5'
require 'tk'
require 'uri'

$LOAD_PATH << File.dirname(__FILE__)

case RUBY_PLATFORM
when /(win32|mingw32)/
	$platform = :windows
	$home = ENV['APPDATA']
	$configfile = File.join(ENV['APPDATA'], 'agentuirc')
	$rightclick = ['Button-3']

	# I have sacrificed elegance on the bloodstained altar of expediency.
	# Credit goes to William Gates, Esquire.
	require 'contrib/winpop'
	require 'win32ole'
	$urlpop = proc do |url|

		found = false

		if url.include? 'comm_1' or url.include? 'email.php' or url.include? 'telephonypop'
			shell = WIN32OLE.new('Shell.Application')

			shell.windows.each do |window|
				begin
					if (window.Document.title.include? 'comm_1' and
							url.include? 'comm_1') or
							(window.Document.title.include? 'Email Responder' and
							 url.include? 'email.php') or 
							 (window.Document.url.include?('telephonypop') and
							  url.include? 'telephonypop')
							 window.Navigate(url)
							 found = true
					end
				rescue
				end
			end
		end

		unless found
#			ie = WIN32OLE.new('InternetExplorer.Application')
#			ie.Visible = true
#			ie.navigate(url)
		    WIN32OLE.new('Shell.Application').open(url)
		end
	end
when /darwin/
	$platform = :macosx
	$home = ENV['HOME']
	$configfile = File.join($home, 'Library/Preferences/agentuirc')
	$rightclick = ['Control-Button-1', 'Button-2']
	$urlpop = proc{|url| `open #{url}`}
else
	$platform = :unix
	$home = ENV['HOME']
	$configfile = File.join($home, '.config/.agentuirc')
	$rightclick = ['Button-3']

	if ENV['CPXBROWSER'].nil? or ENV['CPXBROWSER'].empty?
		puts "CPXBROWSER: Using firefox as fallback"
		$urlpop = proc do |url|
			`firefox -remote "ping()"`

			if $?.exitstatus.zero?
				x = `firefox -remote "openURL(#{url})"`
			else
				# XXX:BUG: 2014-07-08 -- jontow@
				#   This never completes.. firefox doesn't relinquish job control?
				#   agentui hangs in this case.  Have attempted forking, using system(), exec():
				#   symptom is the same.  Agent remains 'logged in' but dead until firefox is closed.
				#x = `firefox "#{url}"`
				x = `echo ""`
				STDERR.puts "Please start firefox for URL pop to work."
			end

			unless $?.exitstatus.zero?
				Tk::Tile::Dialog.new(:icon=>:warning, :type=>'ok', :message=>x, :title=>'Error opening URL').show
			end
		end
	else
		puts "CPXBROWSER: Using #{ENV['CPXBROWSER']}"
		$urlpop = proc do |url|
			`#{ENV['CPXBROWSER']} "#{url}"`
		end
	end
end

begin
	$stdout.stat
rescue Errno::EBADF
	$stdout = File.open(File.join($home, 'agentui.log'), 'a')
	$stderr = $stdout
end

# add contrib/tile_extras to tcl package path so that we can have ttk::dialog in 8.5

begin
	require 'tkextlib/tile'
rescue RuntimeError => e
	# try to fix the paths if we're on Micah's laptop
	unless Tk::AUTO_PATH.to_a.include?('/usr/lib')
		# fix the path and try again
		TkPackage.add_path('/usr/lib')
		retry
	else
		# can't fix it, throw the error again
		raise e
	end
end

unless TkPackage.names().include? 'ttk::dialog'
	puts 'ttk::dialog not present, trying to load from contrib'
	# force loading of ttk::dialog
	TkPackage.add_path(File.expand_path(File.join(File.dirname(__FILE__), 'contrib', 'tile-extras')))
	TkPackage.require('ttk::dialog')
end


require 'contrib/remotesyslog/remotesyslog'

require 'constants/agentconstants'
require 'constants/protocol'
require 'tilepatch' if RUBY_VERSION.gsub('.', '').to_i < 186 # will this work?
require 'stylepatch' if RUBY_VERSION.gsub('.', '').to_i < 187 and eval("begin
								       	  Tk::Tile::Style::theme_names
								       	  false
								       	  rescue => e
									    if e.message.match(/invalid command name/)
										true
									    else
										false
									    end
									end")

require 'module'

require 'version.rb'

require 'defaults.rb'

#Thread.abort_on_exception = true



class InvalidEvent < StandardError
end

class Agent
	include AgentConstants

	def self.calculate_duration(i, precision=2)
		seconds = (Time.now.to_i - (i + $main.timeskew)).to_i
		if seconds > 60
			minutes = seconds / 60
			seconds %= 60
		end

		if minutes and minutes > 60
			hours = minutes / 60
			minutes %= 60
		end

		if hours and hours > 24
			days = hours / 24
			hours %= 24
		end
		#and so on, if we ever needed it...

		# Beware the ternary operator, for it is subtle and quick to error
		x = []
		x << "#{seconds} second#{seconds != 1 ? 's' : ''}"
		x << "#{minutes} minute#{minutes != 1 ? 's' : ''}" if minutes
		x << "#{hours} hour#{hours != 1 ? 's' : ''}" if hours
		x << "#{days} day#{days != 1 ? 's' : ''}" if days

		if precision > x.length
			x.reverse.join(' ')
		else
			x[precision*-1..-1].reverse.join(' ')
		end
	end
end

#wrap this all in a nice class...
class Main
	attr_reader :agent_name, :agent_seclevel, :agent_state, :brandlist,
		:agent_profile, :profiles, :config, :defaultconfig, :queuelist,
		:root, :tabs, :mainmenu, :editmenu, :phone
	attr_accessor :timeskew, :releaseoptions, :agent_statedata
	def initialize
		@root = TkRoot.new
		@root.withdraw #hide the root window until we've populated it
		wipe_slate_clean
		Tk::Tile::Style.theme_use(@config[:theme]) if @config[:theme]
		prompt_login
	end

	def wipe_slate_clean
		@counter = 0
		@outgoing = Queue.new
		@incoming = Queue.new

		@sent_events = {}
		@resent_events = {}

		@reply_callbacks = {}
		
		@agent_name = ''
		@agent_seclevel = 0
		@agent_state = 0
		@agent_statedata = nil

		@agent_state_var = TkVariable.new(Agent::STATENAMES[0])
		@agent_profile = 0
		@agent_profile_var = TkVariable.new('Unknown')

		@state_clock_var = TkVariable.new('')
		@call_clock_var = TkVariable.new('')
		@callstarttime = nil

		@brandlist = {}
		@queuelist = {}
		@profiles = {}

		@timeskew = 0
		@defaultconfig = AUIDEFAULTS
		if File.file? $configfile
			settings = YAML.load(File.read($configfile))
			@config = @defaultconfig.merge(settings) if settings.kind_of? Hash
		end
		@config ||= @defaultconfig.dup

		@nagged = 0
	end

	#show the login window
	def prompt_login
		@root.configure(:title=>'Login')
		@mainframe = Tk::Tile::TFrame.new(@root).pack(:fill=>:both,
														:expand=>true, :padx=>10)
		inputframe = Tk::Tile::TFrame.new(@mainframe).pack(:side=>:top, :pady=>10)
		labelframe = Tk::Tile::TFrame.new(inputframe).pack(:side=>:left)
		Tk::Tile::TLabel.new(labelframe, :text=>'Username').pack(:side=>:top)
		Tk::Tile::TLabel.new(labelframe, :text=>'Password').pack(:side=>:top)
		Tk::Tile::TLabel.new(labelframe, :text=>'Host:Port').pack(:side=>:top)
		Tk::Tile::TLabel.new(labelframe, :text=>'Remote #').pack(:side=>:top)
		entryframe = Tk::Tile::TFrame.new(inputframe).pack(:side=>:right)


		@username = Tk::Tile::TEntry.new(entryframe).pack(:side=>:top)
		@password = Tk::Tile::TEntry.new(entryframe, :show=>'*').pack(:side=>:top)
		@host = Tk::Tile::TEntry.new(entryframe).pack(:side=>:top)
		@remotenumber = Tk::Tile::TEntry.new(entryframe, :validate=>:all, :validatecommand=>proc{|x| valid_phone_number? x}).pack(:side=>:top)
		u = ARGV[0] || @config[:username]
		h = ARGV[2] || @config[:host]
		p = ARGV[3] || @config[:port]
		@username.value = u
		@password.value = ARGV[1] || ''
		@host.value = "#{h}:#{p}"

		@config[:username] ? @password.focus : @username.focus


		@usebuiltinphone = TkVariable.new(@config[:usebuiltinphone] ? 1 : 0)
    Tk::Tile::CheckButton.new(@mainframe, :text=>'Use builtin phone', :variable=>@usebuiltinphone).pack()

		buttonframe = Tk::Tile::TFrame.new(@mainframe).pack(:side=>:bottom,
						:after=>inputframe, :fill=>:x, :expand=>true, :padx=>10)
		Tk::Tile::TButton.new(buttonframe, :text=>'Login',
							:command=>proc{login(@username.value, @password.value, @host.value, @remotenumber.value)}).
							pack(:side=>:left)
		Tk::Tile::TButton.new(buttonframe, :text=>'Cancel',
												:command=>proc{@root.destroy}).
												pack(:side=>:right)
		#Bind the Enter Key for login window.
		@root.bind("Return"){login(@username.value, @password.value, @host.value, @remotenumber.value)}

		#Bind the Esc Key to cancel login.
		@root.bind("Escape"){exit}
		TkRoot.new.protocol('WM_DELETE_WINDOW', proc{die})
		#@root.bind('Configure'){center_window(@root)}
		@root.deiconify
		Thread.new do
			sleep 0.01 until TkWinfo.mapped?(@root)
			Tk.after_idle{center_window(@root)}
		end
	end

	def valid_phone_number?(event)
		begin
			Integer(event.string)
			true
		rescue ArgumentError
			return false
		end
	end

	def center_window(win, width=win.winfo_reqwidth, height=win.winfo_reqheight)
		# translated from http://wiki.tcl.tk/1254
		#width = win.winfo_reqwidth
		#height = win.winfo_reqheight
		x = (win.winfo_vrootwidth - width) / 2
		y = (win.winfo_vrootheight - height) / 2
		win.geometry "#{width}x#{height}+#{x}+#{y}"
	end
	
	#wrapper for incrementing the counter
	def next_count
		@counter += 1
		@counter %= 65535 #make the counter wrap at (2**16)-1
	end

	def errdialog(message)
		Tk::Tile::Dialog.new(:icon=>:warning, :type=>'ok', :message=>message, :title=>'Error').show
	end

	#handle a login attempt
	def login(username, password, host, remotenumber)
		host, port = host.split(':', 2)

		if port.to_i == 0
			#errdialog "Invalid port, assuming the default"
			port = 1337
		end
		
		begin
			@connection = TCPSocket.new(host, port.to_i)
			RemoteSyslog.open(host, 'Agentui') unless RemoteSyslog.opened?
		rescue Errno::ECONNREFUSED, Errno::EBADF, Errno::EINVAL
			errdialog 'no server listening'
			return
		rescue SocketError => e

			errdialog "Socket Error: #{e.message}"
			return
		end

		if result = select([@connection], nil, nil, 1)
			retries = 0
			begin
				line = @connection.gets
			rescue Errno::EINVAL => e # stupid windows...
				retries += 1
				if retries < 2
					sleep(0.2)
					retry
				else
					errdialog "Received EINVAL twice on the socket handle while reading server banner, please try again"
					@connection.close unless @connection.closed?
					return
				end
			end

			if line =~ /^Agent Server/
				puts "Connected to agent server, sending protocol version"
				@connection.puts "Protocol: #{AgentProtocol::MAJOR}.#{AgentProtocol::MINOR}"
				resp = @connection.gets
				code, message = resp.chomp.split(" ",2)
				case code.to_i
				when 0
					url = message.match(/OK (.*)/)
					$urlpop.call(url[1]) if url and url[1] and @config[:crmpop]
				when 1
					errdialog message
				when 2
					errdialog message
					return
				else

				end
			else
				@connection.close
				errdialog "Unexpected server response: #{line}"
				return
			end
		else
			@connection.close
			errdialog "timed out waiting for server response"
			return
		end
		
		@connection.puts("GETSALT 1")
		line = @connection.gets
		event, counter, data = line.strip.split(' ', 3)
		begin
			salt = Integer(data)
		rescue ArgumentError
			errdialog "Server returned an invalid salt: #{data}"
			return
		end
		
		if @usebuiltinphone.value == "1"
			remotenumber = Socket.gethostname + ":" + 6080.to_s
		end

		saltedpass = Digest::MD5.hexdigest("#{salt}#{Digest::MD5.hexdigest(password)}")
		@connection.puts("LOGIN 2 #{username}:#{saltedpass} #{remotenumber}")
		line = @connection.gets
		return unless line
		event, counter, data = line.strip.split(' ', 3)
		if event == 'ACK' and counter == '2'
			@config[:username] = username
			@config[:host] = host
			@config[:port] = port
			@agent_name = username
			seclevel, profile, timestamp = data.split(' ')
			@agent_seclevel = seclevel.to_i
			@agent_profile = profile.to_i
			@root.withdraw
			@root.bind("Escape"){}
			@root.bind("Return"){}

			@mainframe.unpack
			@timeskew = Time.now.to_i - timestamp.to_i
			RemoteSyslog.info("Agent #{username} logged in with seclevel #{seclevel} and profile #{profile} and version #{VERSIONSTRING} to server #{host}")
			unless @usebuiltinphone.value == "0"
				puts "loading builtin phone"
				begin
					require 'contrib/rbphone/phone'
					#@phone = PhoneGUI.new(:username=>username, :password=>password, :proxy=>host, :dialbox=>false)
					@phone = PhoneGUI.new(:useConfigServer => false, :dialbox=>false)
				rescue LoadError
					errdialog "Builtin phone is not available, sorry."
				rescue 
					errdialog "Builtin phone could not be initialized, sorry"
				end
			end
			@config[:usebuiltinphone] = @usebuiltinphone.value == "1"
			create_main
			listen
#			route_incoming
		elsif event == 'ERR'
			@connection.close
			errdialog "Received error when logging in #{data}"
			return
		else
			@connection.close
			errdialog "Received unexpected event #{line}, disconnecting"
			return
		end
	end

	#are we already connected?
	# TODO - should the connection be killed after each login failure?
	def listening?
		return @listen || false
	end

	# send an event to the server
	def send(command, *data, &callback)
		count = next_count
		str = "#{command} #{count} #{data.join(' ')}"
		@sent_events[count] = {:time=>(Time.now.to_i)+300, :event=>str}
		@reply_callbacks[count] = callback
		@outgoing.enq str
	end

	#block current thread until result is returned
	#this is kinda a hack, Fibers might make this easier. 
	def send_blocking(command, *data)
		count = next_count
		str = "#{command} #{count} #{data.join(' ')}"
		@sent_events[count] = {:time=>(Time.now.to_i)+300, :event=>str}
		thread = Thread.current
		thread[:result] = thread[:data] = thread[:event] = nil
		@reply_callbacks[count] = proc do |x,y,z|
			thread[:result] = x
			thread[:data]= y
			thread[:event] = z
		end
		@outgoing.enq str
		loop do
			break if thread[:result]
			Thread.pass
		end
		return [Thread.current[:result], Thread.current[:data], Thread.current[:event]]
	end

	# acknowledge an event
	def ack(counter, data)
		@outgoing.enq "ACK #{counter} #{data}"
	end

	# error in response to a bad event
	def err(counter, error)
		@outgoing.enq "ACK #{counter} #{error}"
	end

	#route an incoming event/reply/error
	#pulls items off the incoming queue
	def route_incoming(event)
		command, counter, data = event.split(' ', 3)

		if ['ACK', 'ERR'].include? command
			@sent_events.delete(counter.to_i)
			@resent_events.delete(counter.to_i)
			result = (command == 'ACK')
			if @reply_callbacks[counter.to_i]
				@reply_callbacks[counter.to_i].call(result, data, event)
				@reply_callbacks.delete(counter.to_i)# remove the callback
			end
		else
			#ask who wants to accept this event and pass it to all of them
			#if any of the modules raises an InvalidEvent exception abort
			#all subsequent processing and return an error to the server
			targets = TabbedModule.tabindices.select{|x| x.accepts? command}
			res = [] # store all the responses
			begin
				targets.each do |t|
					res << t.handle_event(event)
				end
			rescue StandardError => e
				puts e.message
				puts e.backtrace
				RemoteSyslog.err("#{e.message}\n#{e.backtrace}")
				err(counter, e.message)
			else
				res = res.select{|x| [String, Fixnum].include? x.class}
				if res.empty?
					res = command
				else
					res = res.join('::')
				end
				ack(counter, res)
			end
		end
	end

	#listen on the connection
	def listen

		@listen = true

		@listenthread = Thread.new do
			Thread.current.abort_on_exception = true
			catch :disconnect do
				loop do
					if result = select([@connection], nil, nil, 0.25)
						if socket = result[0][0]
							begin
								string = socket.gets
							rescue Errno::ECONNRESET
								throw :disconnect
							end
							if string
								route_incoming(string.chomp)
							else
								throw :disconnect
							end
						end
					end

					while !@outgoing.empty?
						string = @outgoing.shift
						begin
							@connection.puts string
						rescue Errno::EPIPE
							throw :disconnect
						end
					end

					# TODO - is this the best place for this? I'd prefer not to
					# have *another thread for this

					#check dead events and resend them and put them in the resent
					#events list
					check_dead_events(@sent_events) do |e|
						puts "resending event \"#{e}\" because it's older than 10 seconds"
						command, counter, data = e.split(' ', 3)
						newcount = next_count
						str = "#{command} #{newcount} #{data}"
						@outgoing.enq str
						@resent_events[newcount] = {:time=>(Time.now.to_i)+600, :event=>str}
					end

					#check resent events for dead ones
					check_dead_events(@resent_events) do |e|
						#a resent event has failed to return an ack in 60 seconds.
						#this is very bad and indicates something shit itself
						#somewhere along the line.
						puts "Event \"#{e}\" was resent and failed to return after 60 seconds"
						# TODO - freak out
					end
				end
			end
			errdialog 'connection died!'
	
			TabbedModule.destroy_all

			#Bind the Enter Key for login window.
			@root.bind("Return"){login(@username.value, @password.value, @host.value, @remotenumber.value)}
	
			#Bind the Esc Key to cancel login.
			@root.bind("Escape"){exit}


			@root.withdraw
			@menuframe.unpack
			@tabs.unpack
			@mainframe.pack
			wipe_slate_clean
			@root.configure(:title=>'Login')
			@root.deiconify
			#Tk.after_idle{center_window(@root)}
			#prompt_login
			Thread.new do
				sleep 0.01 until TkWinfo.mapped?(@root)
				Tk.after_idle{center_window(@root)}
			end
		end

	end

	#abstraction for checking for dead events
	def check_dead_events(events, &resendcallback)
		if events.length > 0 #only bother to check if we've got events
			#select any expired events
			events.select{|k,v| v[:time] < Time.now.to_i}.each do |e|
				events.delete(e[0]) #delete them from this queue
				resendcallback.call(e[1][:event]) #execute the callback
			end
		end
	end
	
	#create the main window
	def create_main

		@menuframe = Tk::Tile::TFrame.new(@root).pack(:side=>:top, :fill=>:x)
		create_menu

		TabbedModule.load_all
		
		@root.configure(:title=>"AgentUI #{VERSIONSTRING}", :width=>350, :height=>200)
		@root.deiconify
		Tk.after_idle{center_window(@root, @config[:width], @config[:height])}

		state_clock_reset


		############################################################
		# State Label

		@statelabel = Tk::Tile::TLabel.new(@menuframe, :textvariable=>@agent_state_var, :font=>'Helvetica').pack(:side=>:right, :padx=>2)
		Tk::Tile::TLabel.new(@menuframe, :text=>'   State:', :font=>'Helvetica').pack(:side=>:right, :padx=>1)

		@profilelabel = Tk::Tile::TLabel.new(@menuframe, :textvariable=>@agent_profile_var, :font=>'Helvetica').pack(:side=>:right, :padx=>2)
		Tk::Tile::TLabel.new(@menuframe, :text=>'Profile:', :font=>'Helvetica').pack(:side=>:right, :padx=>1)
	
		@stateclocklabel = Tk::Tile::TLabel.new(@menuframe, :textvariable=>@state_clock_var, :font=>'Helvetica').pack(:side=>:right, :padx=>2)
#		Tk::Tile::TLabel.new(@menuframe, :text=>'StateClock:', :font=>'Helvetica').pack(:side=>:right, :padx=>1)
	
		@callclocklabel = Tk::Tile::TLabel.new(@menuframe, :textvariable=>@call_clock_var, :font=>'Helvetica').pack(:side=>:right, :padx=>2)
#		Tk::Tile::TLabel.new(@menuframe, :text=>'CallClock:', :font=>'Helvetica').pack(:side=>:right, :padx=>1)

		@tabs = Tk::Tile::TNotebook.new(@root).pack(:anchor=>:nw,
													:fill=>:both, :expand=>true)
		TabbedModule.attach_all(self)
		@tabs.select(0) #activate the first tab
		
		@tabs.bind('<NotebookTabChanged>'){ @tabs.tabconfigure(@tabs.index('current'),
													  :compound=>:text)}
	end

	#create the menus
	def create_menu

		############################################################
		# Main Menu

		@mainmenubutton = Tk::Tile::Menubutton.new(@menuframe, :text=>'Main', :style=>'TLabel').pack(:side=>:left, :padx=>5)
		@mainmenu = TkMenu.new(@mainmenubutton, :tearoff=>false)
		@mainmenu.add(:command, :label=>'Quit', :command=>proc{die})
		@mainmenubutton.menu(@mainmenu)

		############################################################
		# Edit Menu

		@editmenubutton = Tk::Tile::Menubutton.new(@menuframe, :text=>'Edit', :style=>'TLabel').pack(:side=>:left, :padx=>5)
		@editmenu = TkMenu.new(@editmenubutton, :tearoff=>false)
		@editmenu.add(:command, :label=>'Config', :command=>proc do
			index = TabbedModule.get_tab_index('ConfigModule')
			if index
				@tabs.tabconfigure(index, :state=>:normal)
				@tabs.select(index)
			end
		end)
		@editmenubutton.menu(@editmenu)
		
		############################################################
		# View Menu

		#define the view menu
		@viewmenubutton = Tk::Tile::Menubutton.new(@menuframe, :text=>'View', :style=>'TLabel').pack(:side=>:left, :padx=>5)
		@viewmenu = TkMenu.new(@viewmenubutton, :tearoff=>false)
		@viewmenubutton.menu(@viewmenu)

		############################################################
		# View Menu --> Theme Submenu

		@thememenu = TkMenu.new(@viewmenu, :tearoff=>false)

		Tk::Tile::Style.theme_names.each do |th|
			@thememenu.add(:command, :label=>th,
					   :command=>proc{Tk::Tile::Style.theme_use(th); @config[:theme] = th})
		end

		@viewmenu.add(:cascade, :menu=>@thememenu, :label=>'Themes')

		############################################################
		# Tabs Menu

		@tabmenubutton = Tk::Tile::Menubutton.new(@menuframe, :text=>'Tabs', :style=>'TLabel').pack(:side=>:left, :padx=>5)
		@tabmenu = TkMenu.new(@tabmenubutton, :tearoff=>false, :postcommand=>proc{create_tab_menu})
		@tabmenubutton.menu(@tabmenu)

	end

	def create_tab_menu
		menuend = @tabmenu.index(:end)
		@tabmenu.delete(0, menuend)
	
		filemenu = TkMenu.new(@tabmenu, :tearoff=>false)
		thisdir = File.dirname(__FILE__)
		glob = File.join(thisdir, 'modules/*.rb')
		glob = './'+glob unless glob[0..1] == './' or glob[0,1] == '/' or glob =~ /^[A-Z]:\// # force a valid relative path...
		Dir[glob].each do |f|
			if f[0,thisdir.length] == thisdir # We're running with an absolute path
				relf = f.sub(thisdir, '.')
			elsif f[0,thisdir.length+2] == './'+thisdir #we're running with a crazy relative path (windows)
				relf = f.sub('./'+thisdir, '.')
			else
				relf = f
			end
			next if TabbedModule.subclasses.detect{|s| relf == s.file}
			filemenu.add(:command, :label=>File.basename(f), :command=>proc{
				TabbedModule.load_file(f)
				if mod = TabbedModule.subclasses.detect{|s| s.file == relf}
					if TabbedModule.attach(self, mod)
						begin
							# If tab isn't ready yet, bootstrap it.
							mod.bootstrap unless mod.ready?
						rescue Exception => e
							RemoteSyslog.warning("Error while bootstrapping #{mod.title}: #{e.message}\n#{e.backtrace}")
						end
					end
				else
					puts "Couldn't find module: #{mod}"
				end
			})
		end
		@tabmenu.add(:cascade, :menu=>filemenu, :label=>'Load File')

		TabbedModule.subclasses.each do |s|
			menu =  TkMenu.new(@tabmenu, :tearoff=>false)
			@tabmenu.add(:cascade, :menu=>menu, :label=>s.title)			
			menu.add(:command, :label=>'Reload', :command=>proc{s.reload})
			menu.add(:command, :label=>'Unload', :command=>proc{s.detach})
		end
	end
	
	def tick
		@state_clock_var.value = format_clock(Time.now.to_i - @statestarttime)
		@call_clock_var.value = format_clock(Time.now.to_i - @callstarttime) if @callstarttime
		if ((Time.now.to_i - @statestarttime) / 180) == (@nagged + 1) and @agent_state == Agent::WRAPUP
			@nagged += 1
			# Flash window!  (non-nude version)
			@tabs.winfo_toplevel.iconify
			@tabs.winfo_toplevel.deiconify
			Thread.new{
				Tk::Tile::Dialog.new(:type=>'ok', :message=>"You have been in wrapup for 3 minutes, did you forget?", :title=>'Reminder').show
			}
		end
	end

	def call_clock_stop
		@callstarttime = nil
		@call_clock_var.value = ''
	end

	def call_clock_start
		@callstarttime = Time.now.to_i
	end

	def state_clock_reset
		@statestarttime = Time.now.to_i
	end

	def format_clock(seconds)
		minutes = 0
		hours = 0
		days = 0
		if seconds >= 60
			minutes = seconds / 60
			seconds %= 60
		end

		if minutes and minutes >= 60
			hours = minutes / 60
			minutes %= 60
		end

		if hours and hours > 24
			days = hours / 24
			hours %= 24
		end

		if days > 0
			"#{days}d:#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
		else
			"#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
		end
	end

	def get_agent_state_name(state, statedata)
		if state.to_i == Agent::RELEASED and @releaseoptions and
			opt = @releaseoptions.detect{|x| x[:id] == statedata.to_i} 
			Agent::STATENAMES[state.to_i]+"("+opt[:label]+")"
		else
			Agent::STATENAMES[state.to_i].to_s
		end
	end

	def agent_profile=(profile)
		@agent_profile = profile
		@agent_profile_var.value = @profiles[profile]
	end

	def agent_state=(astate)
		@nagged = 0
		return if @agent_state == astate
		# going to idle/released from wrapup
		if @agent_state == Agent::WRAPUP and
			[Agent::IDLE, Agent::RELEASED].include? astate
			#puts 'stopping call clock'
			call_clock_stop
		# going from idle/released/ringing to oncall/outgoingcall
		elsif [Agent::ONCALL, Agent::OUTGOINGCALL].include? astate and
			[Agent::IDLE, Agent::RELEASED, Agent::RINGING].include? @agent_state
			#puts 'starting call clock'
			call_clock_start
		end

		state_clock_reset

		@agent_state = astate
		@agent_state_var.value = get_agent_state_name(@agent_state, @agent_statedata)

		if astate == Agent::WRAPUP
			@statelabel.configure(:foreground=>:red)
		else
			@statelabel.configure(:foreground=>:darkgreen)
		end
	end

	def die
		puts 'dying!'
		@config[:autoloadfiles] = TabbedModule.subclasses.map{|x| x.file} if @config[:autoload]
		TabbedModule.destroy_all
		#if @phone
		#	@phone.rbphone.configserver.shutdown
		#end
		@root.destroy
	end
end

begin
	#get the show on the road...
	$main = Main.new
	Tk.mainloop
rescue SystemExit
	# NOOP
rescue Exception => e # rescue *everything*
	puts "Encountered unhandled exception of type: #{e.class}"
	puts e.message
	puts e.backtrace
	RemoteSyslog.err("#{e.message}\n#{e.backtrace}")
	exit 1
else
	begin
		RemoteSyslog.info("Agent #{$main.agent_name} has logged off") unless $main.agent_name.empty?
	rescue RuntimeError # remotesyslog not connected
		# NOOP
	end
end

