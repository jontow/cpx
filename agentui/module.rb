# Defines the parent module class as well as the tracking of subclasses

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


class TabbedModule
	
	# yeah you read right, I'm storing an image in a constant
	FLASHIMG = TkPhotoImage.new(:file=>File.join(File.dirname(__FILE__), 'images/exclaim.gif'));

	def self.frame
		@frame
	end

	def self.tabbar
		@tabbar
	end

	def self.main
		@main
	end

	# Stubbed out for reference
	def self.create(main)
		@main = main
		@accepts = nil unless defined? @accepts
		@title = 'Untitled' unless defined? @title
		@index = 65535 unless defined? @index
		@seclevel = 0 unless defined? @seclevel
		@noautoload = false unless defined? @noautoload
		@hidden = false unless defined? @hidden
		@ready = false
	end

	def self.bootstrap
		@ready = true
	end

	@subclasses = [] # Note: this is a class instance variable...
	@tabindices = []

	def self.accepts(*args)
		@accepts = args.map{|x| x.to_sym}
	end

	def self.accepts?(sym)
		return false unless @accepts
		sym = sym.to_sym
		@accepts.include? sym or @accepts.include? :ALL
	end

	# extract the class name from the anonymous module
	def self.get_name
		self.name.split('::')[-1]
	end

	# When classes derive from this class, store them in the array
	def self.inherited(subclass)
		#evil hack to get filename of loaded module
		file = caller[0]
		if (RUBY_VERSION.split('.').map{|x| x.to_i} <=> [1, 9, 0]) == -1
			file = file[0...file.rindex(':')]
		else
			# 1.9 version
			file = file[0...file.rindex(/:\d+:/)]
			# obtain a relative path
			file.sub!(/^#{File.dirname(__FILE__)}/, '.')
		end
		#undefine some evil methods for subclasses
		class << subclass
			undef_method(:load_all)
			undef_method(:attach_all)
		end
		
		thisdir = File.dirname(__FILE__)
		if file[0,thisdir.length] == thisdir
			file.sub!(thisdir, '.')
		end

		subclass.file = file
		# we can't filter by seclevel here
		@subclasses << subclass
	end

	# Accessor for the subclass array
	def self.subclasses
		@subclasses
	end

	def self.file=(file)
		@file = file
	end

	def self.file
		@file
	end

	#allows the definiiton of the tab title
	def self.title(title=nil)
		if title
			@title = title
		else
			@title || 'Unknown'
		end
	end

	def self.index(index=nil)
		if index
			@index = index
		else
			@index || 65535
		end
	end

	def self.seclevel(seclevel=nil)
		if seclevel
			@seclevel = seclevel
		else
			defined?(@seclevel) ? @seclevel : 0
		end
	end

	def self.noautoload(noautoload=nil)
		# Defaults to auto-load if not set in the tab.
		if !noautoload.nil?
			@noautoload = noautoload
		else
			@noautoload || false
		end
	end

	def self.hidden?
		@hidden || false
	end

	def self.ready?
		@ready || false
	end

	# Load all the ruby files in a directory
	def self.load_all(glob = File.join(File.dirname(__FILE__),'modules/*.rb'))
		# clear all old tabs
		@subclasses = []
		@tabindices = []

		Dir[glob].each do |f|
			#load into an anonymous module, #inherited seems to catch the class
			load_file(f)
		end
	end

	def self.load_file(file)
		begin
			load(file, true)
		rescue SyntaxError => e
			puts "SyntaxError loading #{file}:"
			puts e.message
			puts e.backtrace
			RemoteSyslog.warning("SyntaxError when loading module #{file}: #{e.message}\n#{e.backtrace}")
		rescue LoadError => e
			puts "LoadError when loading #{file}"
			puts e.message
			puts e.backtrace
			RemoteSyslog.warning("LoadError when loading module #{file}: #{e.message}\n#{e.backtrace}")
		end
		# clean up modules we don't have the permission to load
		@subclasses.delete_if{|s| $main.agent_seclevel < s.seclevel }
	end

	def self.attach_all(tabbar)
		@subclasses.delete_if{|s| $main.agent_seclevel < s.seclevel}
		if $main.config[:autoload] and $main.config[:autoloadfiles]
			@subclasses.delete_if{|s| !$main.config[:autoloadfiles].include?(s.file)}
		else
			@subclasses.delete_if{|s| s.noautoload}
		end
		@subclasses.sort_by{|x| x.index}.each do |sub|
			attach(tabbar, sub) unless @tabindices.include? sub
		end
		Thread.new do
			@subclasses.sort_by{|x| x.index}.each do |sub|
				begin
					# If tab isn't ready yet, bootstrap it.
					sub.bootstrap unless sub.ready?
				rescue Exception => e
					RemoteSyslog.warning("Error while bootstrapping #{sub.title}: #{e.message}\n#{e.backtrace}")
				end
				sleep 1 until sub.ready?
			end
			run_timer
		end
	end

	def self.attach(main, sub)
		return nil unless @subclasses.include? sub
	
		begin
			sub.create(main)
		rescue => e
			puts "Encountered a #{e.class} when  creating tab #{sub.title}: #{e.message}\n#{e.backtrace}"
			RemoteSyslog.warning("Encountered a #{e.class} when creating tab #{sub.title}: #{e.message}\n#{e.backtrace}")
			tab = TabbedModule.get_tab_index(sub.get_name)
			@tabbar.forget(tab) rescue RuntimeError
			sub.destroy
			TabbedModule.subclasses.delete(sub)
			TabbedModule.tabindices.delete(sub)
			return nil
		end
	
		unless sub.frame
			puts "Module in '#{sub.file}' with title '#{sub.title}' does not include a frame, not loading"
			TabbedModule.subclasses.delete(sub)
			TabbedModule.tabindices.delete(sub)
			return nil
		end

		state = sub.hidden? ? :hidden : :normal
		main.tabs.add(sub.frame, :text=>sub.title, :image=>TabbedModule::FLASHIMG, :compound=>:text, :state=>state)

		@tabindices << sub
		@tabindices = @tabindices.sort_by{|x| [x.index, x.title]}

		main.tabs.insert(@tabindices.index(sub), sub.frame)
#		nindex = @tabindices.length-1 if nindex == :end
	end

	def self.detach
		puts "detaching tab #{title}"
		tab = TabbedModule.get_tab_index(get_name)
		@tabbar.forget(tab) rescue RuntimeError
		self.destroy
		TabbedModule.subclasses.delete(self)
		TabbedModule.tabindices.delete(self)
		true
	end

	def self.reload
		filename = File.join(File.dirname(__FILE__), self.file).gsub('/./', '/') #preserve this so it doesn't get eaten by detach
		file = self.file
		tabbar = self.tabbar
#		index = TabbedModule.tabindices.index(self)
		return unless detach
		puts "reloading source file #{filename}"
		begin
			load(filename, true)
		rescue SyntaxError => e
			puts "SyntaxError reloading #{filename}:"
			puts e.message
			puts e.backtrace
			RemoteSyslog.warning("SyntaxError when loading module #{file}: #{e.message}\n#{e.backtrace}")
		rescue LoadError => e
			puts "LoadError when reloading #{filename}"
			puts e.message
			puts e.backtrace
			RemoteSyslog.warning("LoadError when loading module #{file}: #{e.message}\n#{e.backtrace}")
		end
		if sub = TabbedModule.subclasses.detect{|s| s.file == file}
#			sub.index index
			TabbedModule.attach(main, sub)
			tabbar.select(TabbedModule.tabindices.index(sub))
			begin
				sub.bootstrap
			rescue Exception => e
				RemoteSyslog.warning("Error while bootstrapping #{sub.title}: #{e.message}\n#{e.backtrace}")
			end
		else 
			puts "oh dear, couldn't find the reloaded class..."
		end
	end

	#lookup tab by name
	def self.get_tab_index(ident)
		target = nil

		if ident.kind_of? String
			target = tabindices.index(tabindices.detect{|x| x.get_name == ident})
		elsif ident.kind_of? Fixnum # ???
			target = ident if tabindices[ident]
		end

		target
	end

	def self.destroy
		puts "destroying #{self.title}"
	end

	def self.destroy_all
		@subclasses.each do |s|
			begin
				s.instance_variable_set(:@ready, false)
				s.destroy
			rescue Exception
				# TODO - bother logging this?
			end
		end
	end

	def self.tabindices
		@tabindices
	end

	def self.send(command, *data, &callback)
		$main.send(command, *data, &callback)
	end

	def self.send_blocking(command, *data)
		$main.send_blocking(command, *data)
	end
	
	def self.run_timer
		# Create a tk timer that fires every 1 second
		@timer = TkTimer.new(900, -1, proc do
				@subclasses.each do |s|
					begin
						s.tick if @tabindices.include? s and s.ready?
					rescue Exception => e
						puts "#{e.class} encountered when ticking tab #{s.title}: #{e.message}\n#{e.backtrace}"
						RemoteSyslog.warning("#{e.class} encountered when ticking tab #{s.title}: #{e.message}\n#{e.backtrace}")
					end
				end
				begin
					$main.tick
				rescue Exception => e
					puts "#{e.class} encountered when ticking Main: #{e.message}\n#{e.backtrace}"
					RemoteSyslog.warning("#{e.class} encountered when ticking Main: #{e.message}\n#{e.backtrace}")
				end
		end)
		@timer.start
	end

	def self.tick
	end

	def self.flashwin(target=self.class.get_name)
		target = TabbedModule.get_tab_index(target)

		return if target == @tabbar.index('current')
	#	@tabbar.winfo_toplevel.iconify
	#	@tabbar.winfo_toplevel.deiconify
		@tabbar.tabconfigure(target, :compound=>:right)
	end

	def self.handle_event(event)
	end
end
