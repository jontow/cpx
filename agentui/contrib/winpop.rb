
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'Win32API'

module Win32Bullshit

	@get_desktop = Win32API.new('user32', 'GetDesktopWindow', ['V'], 'L')
	@get_window = Win32API.new('user32', 'GetWindow', ['L', 'I'], 'L')
	@get_window_text = Win32API.new('user32', 'GetWindowText', ['L', 'P'], 'L')
	@set_foreground_window = Win32API.new('user32', 'SetForegroundWindow', ['L'], 'L')
	@bring_window_to_top = Win32API.new('user32', 'BringWindowToTop', ['L'], 'L')
	@get_parent = Win32API.new('user32', 'GetParent', ['L'], 'L')
	@get_client_rect = Win32API.new('user32', 'GetClientRect', ['L', 'P'], 'L')
	@set_window_pos = Win32API.new('user32', 'SetWindowPos', ['L', 'L', 'L', 'L', 'L', 'L', 'L'], 'L')
	@find_window = Win32API.new('user32', 'FindWindow', [ 'P', 'P' ], 'L' )
	@message_box= Win32API.new('user32', 'MessageBox', [ 'L', 'P', 'P', 'l' ], 'l')
	@get_foreground_window = Win32API.new('user32', 'GetForegroundWindow', [ 'V' ], 'L')

	def self.find_window(titlestring)
		return @find_window.call(nil,titlestring)
	end

	def self.get_foreground_window()
		return @get_foreground_window.call()
	end
	
	def self.find_window_by_title(titlestring)
		desktop = @get_window.call(@get_desktop.call, 5)
		child = @get_window.call(desktop, 0)

		while(child!=0) do
			str = " " * 100
			length = @get_window_text.call(child, str)
			title = str[0..length]
			if title.include?(titlestring)
				return child
			end
			child=@get_window.call(child, 2)
		end
		return 0
	end
	
	def self.foreground_window(handle)
		@set_foreground_window.call(handle)
		@bring_window_to_top.call(handle)
	end
	
	def self.center_window(handle)
		rect = " " * 16
		@get_client_rect.call(@get_desktop.call, rect)
		x1, y1, x2, y2 = rect.unpack('L*')
		x = x2/2
		y = y2/2
		p x, y
		@set_window_pos.call(handle, 0, x, y, 0, 0, (0x1|0x4|0x40))
	end
	
	def self.get_parent(handle)
		@get_parent.call(handle)
	end
	
	def self.message_box(message, title)
		@message_box.call(0, message, title, 0x200000)
	end
end
