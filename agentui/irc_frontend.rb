
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'thread'
require 'tk'
require 'tkextlib/tile'

require 'defaults'

Thread.abort_on_exception = true

class TabbedModule
	FLASHIMG = TkPhotoImage.new(:file=>File.join(File.dirname(__FILE__), 'images/exclaim.gif'));
	class << self

		def frame
			@frame
		end

		def tabbar
			@tabbar
		end

		def main
			@main
		end

		# Stubbed out for reference
		def create(main)
			@main = main
		end

		def bootstrap
			@ready = true
		end
		def file=(file)
			@file = file
		end

		def file
			@file
		end

		#allows the definiiton of the tab title
		def title(title=nil)
			if title
				@title = title
			else
				@title || 'Unknown'
			end
		end

		def index(index=nil)
			if index
				@index = index
			else
				@index || 65535
			end
		end

		def seclevel(seclevel=nil)
			if seclevel
				@seclevel = seclevel
			else
				@seclevel || 0
			end
		end

		def noautoload(noautoload=nil)
			# Defaults to auto-load if not set in the tab.
			if !noautoload.nil?
				@noautoload = noautoload
			else
				@noautoload || false
			end
		end

		def hidden?
			@hidden || false
		end

		def ready?
			@ready || false
		end

		def get_name
			self.name.split('::')[-1]
		end

		def flashwin(*args)
		end
	end
end

load 'modules/chat.rb'

class Main
	attr_reader :tabs, :config, :agent_name
	def initialize
		@tabs = TkRoot.new
		@config = AUIDEFAULTS
		@agent_name = `whoami`.chomp
	end
end

ChatModule.create(Main.new)

Thread.new do
	loop do
		ChatModule.tick
		sleep 1
	end
end

ChatModule.frame.pack(:expand=>true, :fill=>:both)

Tk.mainloop
