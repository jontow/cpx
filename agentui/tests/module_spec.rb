
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

class TkPhotoImage
	def self.new(*args)
	end
end

require File.dirname(__FILE__) + '/../module.rb'

#Dir.chdir(File.dirname(__FILE__))

context 'The Tab framework working with module files' do
	setup do
		$main = mock('Main')
	end

	specify 'should load all files in the modules directory that are less than tier 4' do
		$main.should_receive(:agent_tier).at_least(:once).and_return 1
		TabbedModule.load_all(File.dirname(__FILE__)+'/modules/*.rb')
		names = TabbedModule.subclasses.map{|x| x.get_name}
		names.should_include 'TestModule'
		names.length.should_eql 1
	end

	specify 'should load all files in the modules directory that are less than or equal to tier 4' do
		$main.should_receive(:agent_tier).at_least(:once).and_return 4
		TabbedModule.load_all(File.dirname(__FILE__)+'/modules/*.rb')
		names = TabbedModule.subclasses.map{|x| x.get_name}
		names.should_include 'TestModule'
		names.should_include 'TestAdminModule'
		names.length.should_eql 2
	end

	specify 'should attach modules loaded from files' do
		$main.should_receive(:agent_tier).at_least(:once).and_return 1
		tabbar = mock('TabBar')
		tabbar.should_receive(:add).with(nil, :text=>'Unknown', :image=>nil, :compound=>:text, :state=>:normal)
		tabbar.should_receive(:insert)
		TabbedModule.load_all(File.dirname(__FILE__)+'/modules/*.rb')
		TabbedModule.attach_all(tabbar)
	end

	specify 'should set the filename of the loaded file' do
		$main.should_receive(:agent_tier).at_least(:once).and_return 1
		tabbar = mock('TabBar')
#		tabbar.should_receive(:add).with(nil, :text=>'Unknown', :image=>nil, :compound=>:text, :state=>:normal)
#		tabbar.should_receive(:insert)
		TabbedModule.load_all(File.dirname(__FILE__)+'/modules/*.rb')
		TabbedModule.subclasses[0].get_name.should_eql 'TestModule'
	end

	specify 'should allow lookups by classname' do
		$main.should_receive(:agent_tier).at_least(:once).and_return 1
		tabbar = mock('TabBar')
		tabbar.should_receive(:add).with(nil, :text=>'Unknown', :image=>nil, :compound=>:text, :state=>:normal)
		tabbar.should_receive(:insert)
		TabbedModule.load_all(File.dirname(__FILE__)+'/modules/*.rb')
		TabbedModule.get_tab_index('TestModule').should_eql nil
#		puts "Tab index: #{TabbedModule.get_tab_index('TestModule')}"
		TabbedModule.attach_all(tabbar)
		TabbedModule.get_tab_index('TestModule').should_eql 0
	end

	specify 'should allow lookups by index?' do
		$main.should_receive(:agent_tier).at_least(:once).and_return 1
		tabbar = mock('TabBar')
		tabbar.should_receive(:add).with(nil, :text=>'Unknown', :image=>nil, :compound=>:text, :state=>:normal)
		tabbar.should_receive(:insert)
		TabbedModule.load_all(File.dirname(__FILE__)+'/modules/*.rb')
		TabbedModule.get_tab_index(0).should_eql nil
#		puts "Tab index: #{TabbedModule.get_tab_index('TestModule')}"
		TabbedModule.attach_all(tabbar)
		TabbedModule.get_tab_index(0).should_eql 0
	end

	teardown do
		TabbedModule.instance_variable_set(:@subclasses, [])
		TabbedModule.instance_variable_set(:@tabindices, [])
	end
end

context 'The Tab Framework dealing with invalid modules' do
	setup do
		$main = mock('Main')
	end

	specify 'should rescue LoadErrors' do
		proc{TabbedModule.load_file('invalid.rb')}.should_not_raise LoadError
	end

	specify 'should rescue SyntaxErrors' do
		proc{TabbedModule.load_file('tests/broken_modules/broken.rb')}.should_not_raise SyntaxError
	end
	
	teardown do
		TabbedModule.instance_variable_set(:@subclasses, [])
		TabbedModule.instance_variable_set(:@tabindices, [])
	end
end

context 'The Tab Framework' do
	setup do
		$main = mock('Main')
	end

	specify 'should try to honor tab indices' do
		tab1 = mock('Tab1')
		tab2 = mock('Tab2')
		tab3 = mock('Tab3')
		tab4 = mock('Tab4')
		tabbar = mock('TabBar')
	
		tabbar.should_receive(:add).at_least(:once)
		tabbar.should_receive(:insert).at_least(:once)

		TabbedModule.instance_variable_set(:@subclasses, [tab1, tab2, tab3, tab4])

		tab1.should_receive(:create).with(tabbar)
		tab1.should_receive(:hidden?).at_least(:once).and_return false
		tab1.should_receive(:frame).at_least(:once).and_return nil
		tab1.should_receive(:title).and_return 'tab1'
		tab1.should_receive(:index).at_least(:once).and_return 3
		TabbedModule.attach(tabbar, tab1)

		tab2.should_receive(:create).with(tabbar)
		tab2.should_receive(:hidden?).at_least(:once).and_return false
		tab2.should_receive(:frame).at_least(:once).and_return nil
		tab2.should_receive(:title).and_return 'tab2'
		tab2.should_receive(:index).at_least(:once).and_return 1
		TabbedModule.attach(tabbar, tab2)
	
		tab3.should_receive(:create).with(tabbar)
		tab3.should_receive(:hidden?).at_least(:once).and_return false
		tab3.should_receive(:frame).at_least(:once).and_return nil
		tab3.should_receive(:title).and_return 'tab3'
		tab3.should_receive(:index).at_least(:once).and_return 2
		TabbedModule.attach(tabbar, tab3)

		tab4.should_receive(:create).with(tabbar)
		tab4.should_receive(:hidden?).at_least(:once).and_return false
		tab4.should_receive(:frame).at_least(:once).and_return nil
		tab4.should_receive(:title).and_return 'tab4'
		tab4.should_receive(:index).at_least(:once).and_return 10
		TabbedModule.attach(tabbar, tab4)

		TabbedModule.tabindices.should == [tab2, tab3, tab1, tab4]
	end

	teardown do
		TabbedModule.instance_variable_set(:@subclasses, [])
		TabbedModule.instance_variable_set(:@tabindices, [])
	end
end
