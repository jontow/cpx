#!/usr/bin/env ruby -w

=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 }* modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'find'

unless ARGV.length >= 2
	STDERR.puts "Usage: #{$0} <branch|'trunk'> <tag>"
	exit(-1)
end

MODULES = %w{constants auiserver agentui cbx cpxmon deploy mailcatcher}

branch = ARGV[0] == 'trunk' ? 'trunk' : "branches/#{ARGV[0]}"
tag = ARGV[0] == 'trunk'? "branches/#{ARGV[1]}" : "tags/#{ARGV[1]}"

MODULES.each do |mod|
	puts "tagging #{mod}"
	`svn copy #{mod}/#{branch} #{mod}/#{tag}`
	
	Find.find(File.join(mod, branch)) do |x|
		if File.directory? x and !x.include? '.svn'
			if `svn proplist #{x}`.include?('svn:externals')
				trimmed = x.sub(/#{branch}*/, tag)

				puts 'found externals'
				externals =  `svn propget svn:externals #{x}`
				new_externals = []
				externals.split("\n").each do |external|
					puts "Fixing external #{external}"
					new_externals << external.sub(branch, tag)
					#`svn propset svn:externals "#{external.sub(branch, tag)}" #{mod}/#{tag}`
				end
				`svn propset svn:externals \"#{new_externals.join("\n")}\" #{trimmed}`
			end
		end
	end
end

