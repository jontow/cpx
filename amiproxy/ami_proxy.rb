
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'thread'
require 'socket'

Thread.abort_on_exception = true

begin
	load 'config/ami_proxy.conf'
rescue LoadError => e
	STDERR.puts "Error when loading server config file, does config/ami_proxy.conf exist?"
	STDERR.puts "Error message: #{e.message}"
	exit(1)
rescue SyntaxError => e
	STDERR.puts "Syntax error encountered when loading config/ami_proxy.conf"
	STDERR.puts "Error message: #{e.message}"
	exit(2)
end

class AMIDisconnectError < StandardError
end

class AMIProxy
	def initialize
		@server = TCPServer.new(AMIProxyConfig::LISTEN_HOST, AMIProxyConfig::LISTEN_PORT)
		@client_session = nil
		@retrywait = 5
		@reconnectmutex = Mutex.new
		get_events
		accept_connections
	end

	def ami_connect
		@reconnectmutex.synchronize do
			puts "trying to connect to #{AMIProxyConfig::AMI_HOST}:#{AMIProxyConfig::AMI_PORT}"
			begin
				@ami_socket = TCPSocket.new(AMIProxyConfig::AMI_HOST, AMIProxyConfig::AMI_PORT)
				p @ami_socket
				@retrywait = 5 # reset the retry wait on successful connect
				if /^Asterisk Call Manager/ =~ @ami_socket.gets
					puts 'connected to the AMI'
				else
					puts 'unexpected output from AMI received'
				end

				@ami_socket.puts "Action: Login\r\n"
				@ami_socket.puts "Username: #{AMIProxyConfig::AMI_USERNAME}\r\n"
				@ami_socket.puts "Secret: #{AMIProxyConfig::AMI_PASSWORD}\r\n"
				@ami_socket.puts "\r\n"

				resp = @ami_socket.gets
				if resp.include? 'Success'
					puts "logged into the AMI successfully"
				else
					print 'Failed to login into the AMI with error:'
					puts @ami_socket.gets
					exit
				end

			rescue Errno::ECONNREFUSED
				puts "connection to ami failed, sleeping for #{@retrywait} seconds before retrying"
				sleep @retrywait
				@retrywait *= 2 if @retrywait < 20 #double the retry wait for next time if it fails agaon
				retry
			end
		end
	end

	def get_event
		event = ''
		loop do
			if result = select([@ami_socket], nil, nil, 1.5)
				if result
					if line = result[0][0].gets
						line.chomp!
						p line
						key, value = line.chomp.split(':').map{|x| x.strip}
						if key == 'Event'
							if AMIProxyConfig::ALLOWED_EVENTS.include? value
								puts "new action: #{value}"
								event << line+"\r\n"
							else
								next #don't care, just keep going
							end
						elsif key == 'Response'
							event << line+"\r\n"
						elsif event.empty?
							next #we'ne not interested in this event
						elsif line.chomp.empty? #got the end
							puts event
							return event+"\r\n"
						else # just another brick in the wall
							event << line+"\r\n"
						end
					else
						puts "AMI connection died"
						raise AMIDisconnectError
					end
				end
			end
		end
	end

	def get_events
		Thread.new do
			ami_connect
			loop do
				begin
					event = get_event #get the next useful event
					begin
						@client_session.puts event if @client_session
					rescue IOError, Errno::EPIPE
						@client_session.close
						@client_session = nil
					end
				rescue AMIDisconnectError
					puts "attempting reconnect"
					if @reconnectmutex.locked?
						puts "connection to ami died but I'm already trying to reconnect, sleeping"
						sleep 2 while @reconnectmutex.locked?
					else	
						ami_connect
					end
					retry
				end
			end
		end
	end


	def accept_connections
		while @client_session = @server.accept #listen for connections, one at a time
			puts 'got client connection'

			loop do
				# TODO Make input events atomic
				if result = select([@client_session], nil, nil, 1.5)
					if line = result[0][0].gets
						# fake a login as the server is already logged in
						if line.chomp == 'Action: Login' 
							aid = nil
							while l = result[0][0].gets and !l.chomp.empty?
								if /ActionID: (.+)\r\n/ =~ l
									aid = $1
								end
								#do nothing
							end
							result[0][0].puts "Response: Success\r\n"
							result[0][0].puts "Message: Right, fine whatever\r\n"
							result[0][0].puts "ActionID: #{aid}\r\n" if aid
							result[0][0].puts "\r\n"
							next
						end
						puts ">> #{line}"

						begin
							@ami_socket.puts line
						rescue Errno::EPIPE
							if @reconnectmutex.locked?
								puts "connection to ami died but I'm already trying to reconnect, sleeping"
								sleep 2 while @reconnectmutex.locked?
							else
								ami_reconnect
							end
							retry
						end
					else
						puts 'session died'
						@ami_socket.puts "\r\n" #terminate any potentially incomplete events
#						@ami_socket.puts "Action: Logoff\r\n\r\n"
						@client_session.close
						@client_session = nil
						break
					end
				end
			end
		end
	end
end

AMIProxy.new
