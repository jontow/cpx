/*
 * Asterisk -- An open source telephony toolkit.
 *
 * Copyright (C) 1999 - 2005, Digium, Inc.
 *
 * Matthew Fredrickson <creslin@digium.com>
 *
 * See http://www.asterisk.org for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 *
 * 2007-05-10 -- MODIFIED! (Jonathan Towne)
 */

/*! \file
 *
 * \brief Trivial application to record a {original:file,modified:voicemail}
 *
 * \author Matthew Fredrickson <creslin@digium.com>
 * \contributor Jonathan Towne <jontow@mototowne.com>
 * \contributor Andrew Thompson <andrew@hijacked.us>
 *
 * \ingroup applications
 */
 
#include "cbx.h"

ASTERISK_FILE_VERSION(__FILE__, "$Revision: 56839 $")

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>

#include "cbx/lock.h"
#include "cbx/file.h"
#include "cbx/logger.h"
#include "cbx/channel.h"
#include "cbx/pbx.h"
#include "cbx/module.h"
#include "cbx/translate.h"
#include "cbx/dsp.h"
#include "cbx/utils.h"
#include "cbx/options.h"
#include "cbx/app.h"
#include "cbx/manager.h"

static char *config = "vmrecord.conf";
static char *dsn = NULL, *username = NULL, *password = NULL, *table = NULL;

static int connected = 0;

static char *app = "VMRecord";

static char *synopsis = "Record voicemail to a file";

static char sqlcmd[512];

static char *descrip = 
"  Record(filename.format|silence[|maxduration][|options])\n\n"
"Records from the channel into a given filename. If the file exists it will\n"
"be overwritten.\n"
"- 'format' is the format of the file type to be recorded (wav, gsm, etc).\n"
"- 'silence' is the number of seconds of silence to allow before returning.\n"
"- 'maxduration' is the maximum recording duration in seconds. If missing\n"
"or 0 there is no maximum.\n"
"- 'options' may contain any of the following letters:\n"
"     'a' : append to existing recording rather than replacing\n"
"     'n' : do not answer, but record anyway if line not yet answered\n"
"     'q' : quiet (do not play a beep tone)\n"
"     's' : skip recording if the line is not yet answered\n"
"     't' : use alternate '*' terminator key (DTMF) instead of default '#'\n"
"     'x' : ignore all terminator keys (DTMF) and keep recording until hangup\n"
"\n"
"If filename contains '%d', these characters will be replaced with a number\n"
"incremented by one each time the file is recorded. A channel variable\n"
"named RECORDED_FILE will also be set, which contains the final filemname.\n\n"
"Use 'core show file formats' to see the available formats on your system\n\n"
"User can press '#' to terminate the recording and continue to the next priority.\n\n"
"If the user should hangup during a recording, all data will be lost and the\n"
"application will terminate. \n";

AST_MUTEX_DEFINE_STATIC(vmrecord_lock);

static int odbc_init(void);
static int odbc_do_query(const char *brandid, struct ast_channel *chan, const char *filename);
static void odbc_disconnect(void);

static SQLHENV	ODBC_env = SQL_NULL_HANDLE;
static SQLHDBC	ODBC_con;
static SQLHSTMT	ODBC_stmt;


static int vmrecord_prepare_stmt(void)
{
	SQLRETURN ODBC_res;

	/* clear sqlcmd to be safe. */
	memset(sqlcmd, 0, sizeof(sqlcmd));

	snprintf(sqlcmd, sizeof(sqlcmd), "INSERT INTO %s "
		"(uniqueid,cidname,cidnum,brandid,filename) VALUES (?,?,?,?,?)", table);

	ODBC_res = SQLAllocHandle(SQL_HANDLE_STMT, ODBC_con, &ODBC_stmt);

	if ((ODBC_res != SQL_SUCCESS) && (ODBC_res != SQL_SUCCESS_WITH_INFO)) {
		ast_log(LOG_ERROR, "app_vmrecord: Failure in AllocStatement %d\n", ODBC_res);
		SQLFreeHandle(SQL_HANDLE_STMT, ODBC_stmt);
		odbc_disconnect();
		return -1;
	}

	ODBC_res = SQLPrepare(ODBC_stmt, (unsigned char *)sqlcmd, SQL_NTS);

	if ((ODBC_res != SQL_SUCCESS) && (ODBC_res != SQL_SUCCESS_WITH_INFO)) {
		ast_log(LOG_ERROR, "app_vmrecord: Failure in SQLPrepare %d\n", ODBC_res);
		SQLFreeHandle(SQL_HANDLE_STMT, ODBC_stmt);
		odbc_disconnect();
		return -1;
	}

	return 0;
}

static int record_exec(struct ast_channel *chan, void *data)
{
	int res = 0;
	int count = 0;
	int percentflag = 0;
	int vmduration = 0;
	time_t starttime, endtime = 0; /* initialize timer endpoints */
	char *filename, *ext = NULL, *silstr, *maxstr, *options;
	char *vdata, *p;
	int i = 0;
	char tmp[256];
	const char *brandid;

	struct ast_filestream *s = '\0';
	struct ast_module_user *u;
	struct ast_frame *f = NULL;
	
	struct ast_dsp *sildet = NULL;   	/* silence detector dsp */
	int totalsilence = 0;
	int dspsilence = 0;
	int silence = 0;		/* amount of silence to allow */
	int gotsilence = 0;		/* did we timeout for silence? */
	int maxduration = 0;		/* max duration of recording in milliseconds */
	int gottimeout = 0;		/* did we timeout for maxduration exceeded? */
	int option_skip = 0;
	int option_noanswer = 0;
	int option_append = 0;
	int terminator = '#';
	int option_quiet = 0;
	int rfmt = 0;
	int flags;
	int waitres;
	struct ast_silence_generator *silgen = NULL;

	/* The next few lines of code parse out the filename and header from the input string */
	if (ast_strlen_zero(data)) { /* no data implies no filename or anything is present */
		ast_log(LOG_WARNING, "Record requires an argument (filename)\n");
		return -1;
	}

	u = ast_module_user_add(chan);

	/* Get the BRANDID channel variable, used in manager_event below -- JMT */
	brandid = pbx_builtin_getvar_helper(chan, "BRANDID");

	/* Yay for strsep being easy */
	vdata = ast_strdupa(data);

	p = vdata;
	filename = strsep(&p, "|");
	silstr = strsep(&p, "|");
	maxstr = strsep(&p, "|");	
	options = strsep(&p, "|");
	
	if (filename) {
		if (strstr(filename, "%d"))
			percentflag = 1;
		ext = strrchr(filename, '.'); /* to support filename with a . in the filename, not format */
		if (!ext)
			ext = strchr(filename, ':');
		if (ext) {
			*ext = '\0';
			ext++;
		}
	}
	if (!ext) {
		ast_log(LOG_WARNING, "No extension specified to filename!\n");
		ast_module_user_remove(u);
		return -1;
	}
	if (silstr) {
		if ((sscanf(silstr, "%d", &i) == 1) && (i > -1)) {
			silence = i * 1000;
		} else if (!ast_strlen_zero(silstr)) {
			ast_log(LOG_WARNING, "'%s' is not a valid silence duration\n", silstr);
		}
	}
	
	if (maxstr) {
		if ((sscanf(maxstr, "%d", &i) == 1) && (i > -1))
			/* Convert duration to milliseconds */
			maxduration = i * 1000;
		else if (!ast_strlen_zero(maxstr))
			ast_log(LOG_WARNING, "'%s' is not a valid maximum duration\n", maxstr);
	}
	if (options) {
		/* Retain backwards compatibility with old style options */
		if (!strcasecmp(options, "skip"))
			option_skip = 1;
		else if (!strcasecmp(options, "noanswer"))
			option_noanswer = 1;
		else {
			if (strchr(options, 's'))
				option_skip = 1;
			if (strchr(options, 'n'))
				option_noanswer = 1;
			if (strchr(options, 'a'))
				option_append = 1;
			if (strchr(options, 't'))
				terminator = '*';
			if (strchr(options, 'x'))
				terminator = 0;
			if (strchr(options, 'q'))
				option_quiet = 1;
		}
	}
	
	/* done parsing */
	
	/* these are to allow the use of the %d in the config file for a wild card of sort to
	  create a new file with the inputed name scheme */
	if (percentflag) {
		AST_DECLARE_APP_ARGS(fname,
			AST_APP_ARG(piece)[100];
		);
		char *tmp2 = ast_strdupa(filename);
		char countstring[15];
		int i;

		/* Separate each piece out by the format specifier */
		AST_NONSTANDARD_APP_ARGS(fname, tmp2, '%');
		do {
			int tmplen;
			/* First piece has no leading percent, so it's copied verbatim */
			ast_copy_string(tmp, fname.piece[0], sizeof(tmp));
			tmplen = strlen(tmp);
			for (i = 1; i < fname.argc; i++) {
				if (fname.piece[i][0] == 'd') {
					/* Substitute the count */
					snprintf(countstring, sizeof(countstring), "%d", count);
					ast_copy_string(tmp + tmplen, countstring, sizeof(tmp) - tmplen);
					tmplen += strlen(countstring);
				} else if (tmplen + 2 < sizeof(tmp)) {
					/* Unknown format specifier - just copy it verbatim */
					tmp[tmplen++] = '%';
					tmp[tmplen++] = fname.piece[i][0];
				}
				/* Copy the remaining portion of the piece */
				ast_copy_string(tmp + tmplen, &(fname.piece[i][1]), sizeof(tmp) - tmplen);
			}
			count++;
		} while (ast_fileexists(tmp, ext, chan->language) > 0);
		pbx_builtin_setvar_helper(chan, "RECORDED_FILE", tmp);
	} else
		ast_copy_string(tmp, filename, sizeof(tmp));
	/* end of routine mentioned */
	
	if (chan->_state != AST_STATE_UP) {
		if (option_skip) {
			/* At the user's option, skip if the line is not up */
			ast_module_user_remove(u);
			return 0;
		} else if (!option_noanswer) {
			/* Otherwise answer unless we're supposed to record while on-hook */
			res = ast_answer(chan);
		}
	}
	
	if (res) {
		ast_log(LOG_WARNING, "Could not answer channel '%s'\n", chan->name);
		goto out;
	}
	
	if (!option_quiet) {
		/* Some code to play a nice little beep to signify the start of the record operation */
		res = ast_streamfile(chan, "beep", chan->language);
		if (!res) {
			res = ast_waitstream(chan, "");
		} else {
			ast_log(LOG_WARNING, "ast_streamfile failed on %s\n", chan->name);
		}
		ast_stopstream(chan);
	}
		
	/* The end of beep code.  Now the recording starts */
		
	if (silence > 0) {
		rfmt = chan->readformat;
		res = ast_set_read_format(chan, AST_FORMAT_SLINEAR);
		if (res < 0) {
			ast_log(LOG_WARNING, "Unable to set to linear mode, giving up\n");
			ast_module_user_remove(u);
			return -1;
		}
		sildet = ast_dsp_new();
		if (!sildet) {
			ast_log(LOG_WARNING, "Unable to create silence detector :(\n");
			ast_module_user_remove(u);
			return -1;
		}
		ast_dsp_set_threshold(sildet, 256);
	} 
		
	flags = option_append ? O_CREAT|O_APPEND|O_WRONLY : O_CREAT|O_TRUNC|O_WRONLY;
	s = ast_writefile( tmp, ext, NULL, flags , 0, 0644);
		
	if (!s) {
		ast_log(LOG_WARNING, "Could not create file %s\n", filename);
		goto out;
	}

	starttime = time(NULL);

	if (ast_opt_transmit_silence)
		silgen = ast_channel_start_silence_generator(chan);
	
	/* Request a video update */
	ast_indicate(chan, AST_CONTROL_VIDUPDATE);
	
	if (maxduration <= 0)
		maxduration = -1;
	
	while ((waitres = ast_waitfor(chan, maxduration)) > -1) {
		if (maxduration > 0) {
			if (waitres == 0) {
				gottimeout = 1;
				break;
			}
			maxduration = waitres;
		}
		
		f = ast_read(chan);
		if (!f) {
			res = -1;
			break;
		}
		if (f->frametype == AST_FRAME_VOICE) {
			res = ast_writestream(s, f);
			
			if (res) {
				ast_log(LOG_WARNING, "Problem writing frame\n");
				ast_frfree(f);
				break;
			}
			
			if (silence > 0) {
				dspsilence = 0;
				ast_dsp_silence(sildet, f, &dspsilence);
				if (dspsilence) {
					totalsilence = dspsilence;
				} else {
					totalsilence = 0;
				}
				if (totalsilence > silence) {
					/* Ended happily with silence */
					ast_frfree(f);
					gotsilence = 1;
					break;
				}
			}
		} else if (f->frametype == AST_FRAME_VIDEO) {
			res = ast_writestream(s, f);
			
			if (res) {
				ast_log(LOG_WARNING, "Problem writing frame\n");
				ast_frfree(f);
				break;
			}
		} else if ((f->frametype == AST_FRAME_DTMF) &&
		    (f->subclass == terminator)) {
			ast_frfree(f);
			break;
		}
		ast_frfree(f);
	}
	if (!f) {
		ast_log(LOG_DEBUG, "Got hangup\n");
		res = -1;
	}
			
	if (gotsilence) {
		ast_stream_rewind(s, silence-1000);
		ast_truncstream(s);
	} else if (!gottimeout) {
		/* Strip off the last 1/4 second of it */
		ast_stream_rewind(s, 250);
		ast_truncstream(s);
	}
	ast_closestream(s);

	if (silgen)
		ast_channel_stop_silence_generator(chan, silgen);

	endtime = time(NULL);
	vmduration = (int) endtime - (int) starttime;

	/* Entering ODBC-foo */
	ast_mutex_lock(&vmrecord_lock);

	if (!connected){
		res = odbc_init();
		if (res < 0){
			ast_log(LOG_WARNING, "%s: cannot reconnect!\n", dsn);
			odbc_disconnect();
		} else {
			res = vmrecord_prepare_stmt();
			if (res < 0) {
				if (option_verbose > 10)
					ast_verbose( VERBOSE_PREFIX_4 "app_vmrecord: Couldn't prepare statement!\n");
			}
		}
	}

	if (connected) {
		res = odbc_do_query(brandid, chan, filename);
		if (res < 0) {
			ast_log(LOG_ERROR, "Query FAILED! Voicemail not recorded!\n");
			if (option_verbose > 10)
				ast_verbose( VERBOSE_PREFIX_4 "app_vmrecord: Reconnecting to dsn %s\n", dsn);

			odbc_disconnect();
			res = odbc_init();
			if (res < 0) {
				ast_log(LOG_WARNING, "%s: cannot reconnect!\n", dsn);
				odbc_disconnect();
			} else {
				if (option_verbose > 10)
					ast_verbose( VERBOSE_PREFIX_4 "app_vmrecord: Trying Query again\n");
				res = vmrecord_prepare_stmt();
				if (res < 0) {
					if (option_verbose > 10)
						ast_verbose( VERBOSE_PREFIX_4 "app_vmrecord: Couldn't prepare statement!\n");
				} else {
					res = odbc_do_query(brandid, chan, filename);
					if (res < 0)
						ast_log(LOG_ERROR, "Query FAILED again! Voicemail not recorded (#2)!\n");
				}
			}
		}
	} else {
		ast_log(LOG_ERROR, "Query FAILED! Voicemail not recorded (not connected)!\n");
	}

	/* Exiting ODBC-foo */
	ast_mutex_unlock(&vmrecord_lock);

	manager_event(EVENT_FLAG_CALL, "MessageWaiting", "Uniqueid: %s\r\nFilename: %s.%s\r\nBrandID: %s\r\nDuration: %d\r\n", chan->uniqueid, filename, ext, brandid, vmduration);
	
 out:
	if ((silence > 0) && rfmt) {
		res = ast_set_read_format(chan, rfmt);
		if (res)
			ast_log(LOG_WARNING, "Unable to restore read format on '%s'\n", chan->name);
		if (sildet)
			ast_dsp_free(sildet);
	}

	ast_module_user_remove(u);

	return res;
}

static int odbc_init(void)
{
	SQLRETURN ODBC_res;

	if (ODBC_env == SQL_NULL_HANDLE || connected == 0) {
		ODBC_res = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &ODBC_env);
		if ((ODBC_res != SQL_SUCCESS) && (ODBC_res != SQL_SUCCESS_WITH_INFO)) {
			ast_log(LOG_ERROR, "app_vmrecord: Error AllocHandle\n");
			connected = 0;
			return -1;
		}

		ODBC_res = SQLSetEnvAttr(ODBC_env, SQL_ATTR_ODBC_VERSION, (void *)SQL_OV_ODBC3, 0);
		if ((ODBC_res != SQL_SUCCESS) && (ODBC_res != SQL_SUCCESS_WITH_INFO)) {
			ast_log(LOG_ERROR, "app_vmrecord: Error SetEnv\n");
			connected = 0;
			return -1;
		}

		ODBC_res = SQLAllocHandle(SQL_HANDLE_DBC, ODBC_env, &ODBC_con);
		if ((ODBC_res != SQL_SUCCESS) && (ODBC_res != SQL_SUCCESS_WITH_INFO)) {
			SQLFreeHandle(SQL_HANDLE_ENV, ODBC_env);
			connected = 0;
			return -1;
		}

		SQLSetConnectAttr(ODBC_con, SQL_LOGIN_TIMEOUT, (SQLPOINTER *)10, 0);
	}

	ODBC_res = SQLConnect(ODBC_con, (SQLCHAR *)dsn, SQL_NTS, (SQLCHAR *)username, SQL_NTS, (SQLCHAR *)password, SQL_NTS);

	if ((ODBC_res == SQL_SUCCESS) || (ODBC_res == SQL_SUCCESS_WITH_INFO)) {
		ast_log(LOG_NOTICE, "app_vmrecord: Connected to %s\n", dsn);
		connected = 1;
	} else {
		SQLFreeHandle(SQL_HANDLE_DBC, ODBC_con);
		SQLFreeHandle(SQL_HANDLE_ENV, ODBC_env);
		connected = 0;
		return -1;
	}

	return 0;
}

static void odbc_disconnect(void)
{
	SQLDisconnect(ODBC_con);
	SQLFreeHandle(SQL_HANDLE_DBC, ODBC_con);
	SQLFreeHandle(SQL_HANDLE_ENV, ODBC_env);
	connected = 0;
}

static int odbc_do_query(const char *brandid, struct ast_channel *chan, const char *filename)
{
	SQLRETURN ODBC_res;
	SQLINTEGER ODBC_err;
	SQLINTEGER i;
	SQLSMALLINT ODBC_mlen;
	SQLCHAR ODBC_msg[200], ODBC_stat[10];

	ast_log(LOG_NOTICE, "app_vmrecord: brandid %s\n", brandid);

	SQLBindParameter(ODBC_stmt, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_CHAR, sizeof(chan->uniqueid), 0, (char *)chan->uniqueid, 0, NULL);
	SQLBindParameter(ODBC_stmt, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_CHAR, sizeof(chan->cid.cid_name), 0, chan->cid.cid_name, 0, NULL);
	SQLBindParameter(ODBC_stmt, 3, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_CHAR, sizeof(chan->cid.cid_num), 0, chan->cid.cid_num, 0, NULL);
	SQLBindParameter(ODBC_stmt, 4, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_CHAR, sizeof(brandid), 0, (char *)brandid, 0, NULL);
	SQLBindParameter(ODBC_stmt, 5, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_CHAR, sizeof(filename), 0, (char *)filename, 0, NULL);
    
	ODBC_res = SQLExecute(ODBC_stmt);
    
	if ((ODBC_res != SQL_SUCCESS) && (ODBC_res != SQL_SUCCESS_WITH_INFO)) {
		ast_log(LOG_ERROR, "app_vmrecord: Error in Query %d\n", ODBC_res);

		/* small hack. */
		i = 1;
		ODBC_res = SQL_SUCCESS;
		while (ODBC_res == SQL_SUCCESS) {
			ODBC_res = SQLGetDiagRec(SQL_HANDLE_STMT, ODBC_stmt, ++i, (unsigned char *)ODBC_stat, &ODBC_err, (unsigned char *)ODBC_msg, sizeof(ODBC_msg), &ODBC_mlen);
			if (SQL_SUCCEEDED(ODBC_res)) {
				ast_log(LOG_WARNING, "app_vmrecord: (STMT) %s:%ld:%s\n", ODBC_stat, ODBC_err, ODBC_msg);
			}
		}

		/* small hack. */
		i = 1;
		ODBC_res = SQL_SUCCESS;
		while (ODBC_res == SQL_SUCCESS) {
			ODBC_res = SQLGetDiagRec(SQL_HANDLE_DBC, ODBC_con, ++i, (unsigned char *)ODBC_stat, &ODBC_err, (unsigned char *)ODBC_msg, sizeof(ODBC_msg), &ODBC_mlen);
			if (SQL_SUCCEEDED(ODBC_res)) {
				ast_log(LOG_WARNING, "app_vmrecord: (DBC) %s:%ld:%s\n", ODBC_stat, ODBC_err, ODBC_msg);
			}
		}

		odbc_disconnect();
		return -1;
	}

	ast_log(LOG_NOTICE, "app_vmrecord: Query Successful!\n");
	return 0;
}

static int unload_module(void)
{
	int res;

	ast_mutex_lock(&vmrecord_lock);
	if (connected) {
		odbc_disconnect();
	}

	if (dsn)
		free(dsn);
	if (table)
		free(table);
	if (username)
		free(username);
	if (password)
		free(password);

	res = ast_unregister_application(app);
	ast_mutex_unlock(&vmrecord_lock);
	
	ast_module_user_hangup_all();

	return res;	
}

static int load_module(void)
{
	int res = 0;
	struct ast_config *cfg;
	struct ast_variable *var;
	const char *tmp;

	ast_mutex_lock(&vmrecord_lock);
	
	cfg = ast_config_load(config);
	if (!cfg) {
		ast_log(LOG_WARNING, "app_vmrecord: Unable to load config: %s\n", config);
		res = AST_MODULE_LOAD_DECLINE;
		ast_mutex_unlock(&vmrecord_lock);
		return res;
	}

	var = ast_variable_browse(cfg, "global");
	if (!var) {
		ast_mutex_unlock(&vmrecord_lock);
		return 0;
	}

	tmp = ast_variable_retrieve(cfg, "global", "dsn");
	if (tmp == NULL) {
		ast_log(LOG_WARNING, "app_vmrecord: dsn not defined; assuming 'asterisk'\n");
		tmp = "asterisk";
	}

	dsn = strdup(tmp);
	if (dsn == NULL) {
		ast_log(LOG_ERROR, "app_vmrecord.c: Out of memory error.\n");
		ast_mutex_unlock(&vmrecord_lock);
		return -1;
	}

	tmp = ast_variable_retrieve(cfg, "global", "table");
	if (tmp == NULL) {
		ast_log(LOG_WARNING, "app_vmrecord: table not defined; assuming 'vmrecord'\n");
		tmp = "vmrecord";
	}

	table = strdup(tmp);
	if (table == NULL) {
		ast_log(LOG_ERROR, "app_vmrecord.c: Out of memory error.\n");
		ast_mutex_unlock(&vmrecord_lock);
		return -1;
	}

	tmp = ast_variable_retrieve(cfg, "global", "username");
	if (tmp) {
		username = strdup(tmp);
		if (username == NULL) {
			ast_log(LOG_ERROR, "Out of memory error.\n");
			ast_mutex_unlock(&vmrecord_lock);
			return -1;
		}
	}

	tmp = ast_variable_retrieve(cfg, "global", "password");
	if (tmp) {
		password = strdup(tmp);
		if (password == NULL) {
			ast_log(LOG_ERROR, "Out of memory error.\n");
			ast_mutex_unlock(&vmrecord_lock);
			return -1;
		}
	}

	ast_log(LOG_NOTICE, "app_vmrecord: dsn is %s, table is %s\n", dsn, table);
	if (username)
		ast_log(LOG_NOTICE, "app_vmrecord: username is %s\n", username);
	else
		ast_log(LOG_NOTICE, "app_vmrecord: retrieving username/password from odbc\n");

	res = odbc_init();
	if (res < 0) {
		ast_log(LOG_ERROR, "app_vmrecord: Unable to connect to dsn: %s\n", dsn);
		ast_mutex_unlock(&vmrecord_lock);
		return 0;
	}

	res =  vmrecord_prepare_stmt();
	if ( res < 0 ) {
		ast_log(LOG_ERROR, "app_vmrecord: Unable to prepare statement\n");
		ast_mutex_unlock(&vmrecord_lock);
		return 0;
	}

	ast_mutex_unlock(&vmrecord_lock);
	return ast_register_application(app, record_exec, synopsis, descrip);
}

AST_MODULE_INFO_STANDARD(ASTERISK_GPL_KEY, "Trivial Voicemail Record Application");
