/*
 * Asterisk -- An open source telephony toolkit.
 *
 * Copyright (C) 2008, Andrew Thompson
 *
 * Andrew Thompson <andrew@hijacked.us>
 *
 * See http://www.asterisk.org for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/*! \file
 *
 * \brief Application to check for various 'floating' vacations
 *
 * \author Andrew Thompson <andrew@hijacked.us>
 * 
 * \ingroup applications
 */

/*** MODULEINFO
	<defaultenabled>yes</defaultenabled>
 ***/

#include "cbx.h"

ASTERISK_FILE_VERSION(__FILE__, "$Revision: 40722 $")

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include "cbx/file.h"
#include "cbx/logger.h"
#include "cbx/channel.h"
#include "cbx/pbx.h"
#include "cbx/module.h"
#include "cbx/lock.h"
#include "cbx/app.h"
#include "cbx/astdb.h"

static int generic_holiday_goto(struct ast_channel *chan, void *data, struct tm *holiday, struct tm *now)
{
	int ret;
	char *stringp, *branch1, *branch2;

	stringp = ast_strdupa(data);
	branch1 = strsep(&stringp,":");
	branch2 = strsep(&stringp,"");

	if (holiday->tm_mday == now->tm_mday && holiday->tm_mon == now->tm_mon) {
		if (ast_strlen_zero(branch1))
			return 0;
		ret = ast_parseable_goto(chan, branch1);
	} else {
		if (ast_strlen_zero(branch2))
			return 0;
		ret = ast_parseable_goto(chan, branch2);
	}

	ast_verbose( VERBOSE_PREFIX_3 "Goto (%s,%s,%d)\n", chan->context,chan->exten, chan->priority+1);
	return ret;
}

static void calculate_easter(struct tm *easter)
{
	int a, b, c, d, e, f, g, h, i, k, l, m, month, day, year;

	year = easter->tm_year + 1900;

	a = year % 19;
	b = year / 100;
	c = year % 100;
	d = b / 4;
	e = b % 4;
	f = (b + 8) / 25;
	g = (b - f + 1) / 3;
	h = (19 * a + b - d - g + 15) % 30;
	i = c / 4;
	k = c % 4;
	l = (32 + 2 * e + 2 * i - h - k) % 7;
	m = (a + 11 * h + 22 * l) / 451;

	month = (h + l - 7 * m + 114) / 31;
	day = ((h + l - 7 * m + 114) % 31) + 1;

	easter->tm_mon = month - 1;
	easter->tm_mday = day;
}

static char *app1 = "GotoIfEaster";
static char *synopsis1 = "Conditional goto if today is easter.";
static char *descrip1 = "This application uses the Meeus/Jones/Butcher algorithm\n"
	"to determine whether the current date is Easter. Hopefully it will cache\n"
	"results in the astdb to improve performance.";

static int goto_if_easter(struct ast_channel *chan, void *data)
{
	struct tm *now;
	struct tm easter;
	time_t tsec;

	if (ast_strlen_zero(data)) {
		ast_log(LOG_WARNING, "Not taking any branch\n");
		return 0;
	}

	tsec = time(NULL);
	now = localtime(&tsec);
	memcpy(&easter, now, sizeof easter);

	calculate_easter(&easter);

	return generic_holiday_goto(chan, data, &easter, now);
}

static void calculate_laborday(struct tm *laborday)
{
	int dow;

	laborday->tm_mon = 8; /* september */
	laborday->tm_mday = 1;

	mktime(laborday);
	dow = laborday->tm_wday; /* figure out what day of the week the 1st is */
	
	/* find the first monday in the month */
	/* if I were a smarter man, I'd use math for this */
	switch (dow) {
		case 0:
			laborday->tm_mday = 2;
			break;
		case 1:
			laborday->tm_mday = 1;
			break;
		case 2:
			laborday->tm_mday = 7;
			break;
		case 3:
			laborday->tm_mday = 6;
			break;
		case 4:
			laborday->tm_mday = 5;
			break;
		case 5:
			laborday->tm_mday = 4;
			break;
		case 6:
			laborday->tm_mday = 3;
			break;
		default:
			break;
	}
}

static char *app2 = "GotoIfLaborDay";
static char *synopsis2 = "Conditional goto if today is labor day.";
static char *descrip2 = "\n";

static int goto_if_laborday(struct ast_channel *chan, void *data)
{
	struct tm *now;
	struct tm laborday;
	time_t tsec;

	if (ast_strlen_zero(data)) {
		ast_log(LOG_WARNING, "Not taking any branch\n");
		return 0;
	}

	tsec = time(NULL);
	now = localtime(&tsec);
	memcpy(&laborday, now, sizeof laborday);

	calculate_laborday(&laborday);

	return generic_holiday_goto(chan, data, &laborday, now);
}

static void calculate_thanksgiving(struct tm *thanksgiving)
{
	int dow;

	thanksgiving->tm_mon = 10; /* november */
	thanksgiving->tm_mday = 1;

	mktime(thanksgiving);
	dow = thanksgiving->tm_wday; /* figure out what day of the week the 1st is */
	
	/* find the third thursday in the month */
	/* if I were a smarter man, I'd use math for this */
	switch (dow) {
		case 0:
			thanksgiving->tm_mday = 19;
			break;
		case 1:
			thanksgiving->tm_mday = 18;
			break;
		case 2:
			thanksgiving->tm_mday = 17;
			break;
		case 3:
			thanksgiving->tm_mday = 16;
			break;
		case 4:
			thanksgiving->tm_mday = 15;
			break;
		case 5:
			thanksgiving->tm_mday = 21;
			break;
		case 6:
			thanksgiving->tm_mday = 20;
			break;
		default:
			break;
	}
}

static char *app3 = "GotoIfThanksgiving";
static char *synopsis3 = "Conditional goto if today is Thanksgiving.";
static char *descrip3 = "\n";

static int goto_if_thanksgiving(struct ast_channel *chan, void *data)
{
	struct tm *now;
	struct tm thanksgiving;
	time_t tsec;

	if (ast_strlen_zero(data)) {
		ast_log(LOG_WARNING, "Not taking any branch\n");
		return 0;
	}

	tsec = time(NULL);
	now = localtime(&tsec);
	memcpy(&thanksgiving, now, sizeof thanksgiving);

	calculate_thanksgiving(&thanksgiving);

	return generic_holiday_goto(chan, data, &thanksgiving, now);
}

static void calculate_presidentsday(struct tm *presidentsday)
{
	int dow;

	presidentsday->tm_mon = 1; /* february */
	presidentsday->tm_mday = 1;

	mktime(presidentsday);
	dow = presidentsday->tm_wday; /* figure out what day of the week the 1st is */
	
	/* find the third monday in the month */
	/* if I were a smarter man, I'd use math for this */
	switch (dow) {
		case 0:
			presidentsday->tm_mday = 16;
			break;
		case 1:
			presidentsday->tm_mday = 15;
			break;
		case 2:
			presidentsday->tm_mday = 21;
			break;
		case 3:
			presidentsday->tm_mday = 20;
			break;
		case 4:
			presidentsday->tm_mday = 19;
			break;
		case 5:
			presidentsday->tm_mday = 18;
			break;
		case 6:
			presidentsday->tm_mday = 17;
			break;
		default:
			break;
	}
}

static char *app4 = "GotoIfPresidentsDay";
static char *synopsis4 = "Conditional goto if today is Presidents Day.";
static char *descrip4 = "\n";

static int goto_if_presidentsday(struct ast_channel *chan, void *data)
{
	struct tm *now;
	struct tm presidentsday;
	time_t tsec;

	if (ast_strlen_zero(data)) {
		ast_log(LOG_WARNING, "Not taking any branch\n");
		return 0;
	}

	tsec = time(NULL);
	now = localtime(&tsec);
	memcpy(&presidentsday, now, sizeof presidentsday);

	calculate_presidentsday(&presidentsday);

	return generic_holiday_goto(chan, data, &presidentsday, now);
}

static void calculate_mlkday(struct tm *mlkday)
{
	int dow;

	mlkday->tm_mon = 0; /* january */
	mlkday->tm_mday = 1;

	mktime(mlkday);
	dow = mlkday->tm_wday; /* figure out what day of the week the 1st is */
	
	/* find the third monday in the month */
	/* if I were a smarter man, I'd use math for this */
	switch (dow) {
		case 0:
			mlkday->tm_mday = 16;
			break;
		case 1:
			mlkday->tm_mday = 15;
			break;
		case 2:
			mlkday->tm_mday = 21;
			break;
		case 3:
			mlkday->tm_mday = 20;
			break;
		case 4:
			mlkday->tm_mday = 19;
			break;
		case 5:
			mlkday->tm_mday = 18;
			break;
		case 6:
			mlkday->tm_mday = 17;
			break;
		default:
			break;
	}
}

static char *app5 = "GotoIfMLKDay";
static char *synopsis5 = "Conditional goto if today is Martin Luther King Jr. Day.";
static char *descrip5 = "\n";

static int goto_if_mlkday(struct ast_channel *chan, void *data)
{
	struct tm *now;
	struct tm mlkday;
	time_t tsec;

	if (ast_strlen_zero(data)) {
		ast_log(LOG_WARNING, "Not taking any branch\n");
		return 0;
	}

	tsec = time(NULL);
	now = localtime(&tsec);
	memcpy(&mlkday, now, sizeof mlkday);

	calculate_mlkday(&mlkday);

	return generic_holiday_goto(chan, data, &mlkday, now);
}

static void calculate_columbusday(struct tm *columbusday)
{
	int dow;

	columbusday->tm_mon = 9; /* october */
	columbusday->tm_mday = 1;

	mktime(columbusday);
	dow = columbusday->tm_wday; /* figure out what day of the week the 1st is */
	
	/* find the second monday in the month */
	/* if I were a smarter man, I'd use math for this */
	switch (dow) {
		case 0:
			columbusday->tm_mday = 9;
			break;
		case 1:
			columbusday->tm_mday = 8;
			break;
		case 2:
			columbusday->tm_mday = 14;
			break;
		case 3:
			columbusday->tm_mday = 13;
			break;
		case 4:
			columbusday->tm_mday = 12;
			break;
		case 5:
			columbusday->tm_mday = 11;
			break;
		case 6:
			columbusday->tm_mday = 10;
			break;
		default:
			break;
	}
}

static char *app6 = "GotoIfColumbusDay";
static char *synopsis6 = "Conditional goto if today is Columbus Day.";
static char *descrip6 = "\n";

static int goto_if_columbusday(struct ast_channel *chan, void *data)
{
	struct tm *now;
	struct tm columbusday;
	time_t tsec;

	if (ast_strlen_zero(data)) {
		ast_log(LOG_WARNING, "Not taking any branch\n");
		return 0;
	}

	tsec = time(NULL);
	now = localtime(&tsec);
	memcpy(&columbusday, now, sizeof columbusday);

	calculate_columbusday(&columbusday);

	return generic_holiday_goto(chan, data, &columbusday, now);
}

static void calculate_memorialday(struct tm *memorialday)
{
	int dow;

	memorialday->tm_mon = 4; /* may */
	memorialday->tm_mday = 1;

	mktime(memorialday);
	dow = memorialday->tm_wday; /* figure out what day of the week the 1st is */

	/* find the last monday in the month */
	/* if I were a smarter man, I'd use math for this */
	switch (dow) {
		case 0:
			memorialday->tm_mday = 30;
			break;
		case 1:
			memorialday->tm_mday = 29;
			break;
		case 2:
			memorialday->tm_mday = 28;
			break;
		case 3:
			memorialday->tm_mday = 27;
			break;
		case 4:
			memorialday->tm_mday = 26;
			break;
		case 5:
			memorialday->tm_mday = 25;
			break;
		case 6:
			memorialday->tm_mday = 31;
			break;
		default:
			break;
	}
}

static char *app7 = "GotoIfMemorialDay";
static char *synopsis7 = "Conditional goto if today is Memorial Day.";
static char *descrip7 = "\n";

static int goto_if_memorialday(struct ast_channel *chan, void *data)
{
	struct tm *now;
	struct tm memorialday;
	time_t tsec;

	if (ast_strlen_zero(data)) {
		ast_log(LOG_WARNING, "Not taking any branch\n");
		return 0;
	}

	tsec = time(NULL);
	now = localtime(&tsec);
	memcpy(&memorialday, now, sizeof memorialday);

	calculate_memorialday(&memorialday);

	return generic_holiday_goto(chan, data, &memorialday, now);
}

static int unload_module(void)
{
	int res;
	res = ast_unregister_application(app1);
	res = ast_unregister_application(app2);
	res = ast_unregister_application(app3);
	res = ast_unregister_application(app4);
	res = ast_unregister_application(app5);
	res = ast_unregister_application(app6);
	res = ast_unregister_application(app7);
	return res;	
}

static int load_module(void)
{
	int res;
	res = ast_register_application(app1, goto_if_easter, synopsis1, descrip1);
	res = ast_register_application(app2, goto_if_laborday, synopsis2, descrip2);
	res = ast_register_application(app3, goto_if_thanksgiving, synopsis3, descrip3);
	res = ast_register_application(app4, goto_if_presidentsday, synopsis4, descrip4);
	res = ast_register_application(app5, goto_if_mlkday, synopsis5, descrip5);
	res = ast_register_application(app6, goto_if_columbusday, synopsis6, descrip6);
	res = ast_register_application(app7, goto_if_memorialday, synopsis7, descrip7);
	return res;
}

AST_MODULE_INFO_STANDARD(ASTERISK_GPL_KEY, "Is Holiday Application");
