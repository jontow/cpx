/*
 * Asterisk -- An open source telephony toolkit.
 *
 * Copyright (C) 1999 - 2006, Digium, Inc.
 *
 * Mark Spencer <markster@digium.com>
 *
 * See http://www.asterisk.org for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

#ifndef _ASTERISK_AGENT_H
#define _ASTERISK_AGENT_H

#define AST_MAX_AGENT  80                          /*!< Agent ID or Password max length */

/*! \brief Structure representing an agent.  */
struct agent_pvt {
	ast_mutex_t lock;              /*!< Channel private lock */
	int dead;                      /*!< Poised for destruction? */
	int pending;                   /*!< Not a real agent -- just pending a match */
	int abouttograb;               /*!< About to grab */
	int autologoff;                /*!< Auto timeout time */
	int ackcall;                   /*!< ackcall */
	int deferlogoff;               /*!< Defer logoff to hangup */
	time_t loginstart;             /*!< When agent first logged in (0 when logged off) */
	time_t start;                  /*!< When call started */
	struct timeval lastdisc;       /*!< When last disconnected */
	int wrapuptime;                /*!< Wrapup time in ms */
	ast_group_t group;             /*!< Group memberships */
	int acknowledged;              /*!< Acknowledged */
	char moh[80];                  /*!< Which music on hold */
	char agent[AST_MAX_AGENT];     /*!< Agent ID */
	char password[AST_MAX_AGENT];  /*!< Password for Agent login */
	char name[AST_MAX_AGENT];
	ast_mutex_t app_lock;          /**< Synchronization between owning applications */
	volatile pthread_t owning_app; /**< Owning application thread id */
	volatile int app_sleep_cond;   /**< Sleep condition for the login app */
	struct ast_channel *owner;     /**< Agent */
	char loginchan[80];            /**< channel they logged in from */
	char logincallerid[80];        /**< Caller ID they had when they logged in */
	struct ast_channel *chan;      /**< Channel we use */
	int realtime;			/*!< Realtime backed agent? */
	int paused;				/*!< Is this shit paused? */
	AST_LIST_ENTRY(agent_pvt) list;	/**< Next Agent in the linked list. */
};

#endif /* _ASTERISK_AGENT_H */
