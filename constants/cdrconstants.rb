
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

module CDRConstants
	# this module holds information about the various states a CDR can be in
	
	CDRINIT = 0
	INIVR = 1
	DIALOUTGOING = 2
	INQUEUE = 3
	RINGING = 4
	PRECALL = 5
	INCALL = 6
	INOUTGOING = 7
	FAILEDOUTGOING = 8
	TRANSFER = 9
	WARMXFER = 10
	WARMXFERCOMPLETE = 11
	WARMXFERFAILED = 12
	WARMXFERLEG = 13
	INWRAPUP = 14
	ENDWRAPUP = 15
	ABANDONQUEUE = 16
	ABANDONIVR = 17
	LEFTVOICEMAIL = 18
	ENDCALL = 19
	UNKNOWNTERMINATION = 20
	CDREND = 21

	TRANSACTIONNAMES = {
		CDRINIT => 'CDR Init',
		INIVR => 'In IVR',
		DIALOUTGOING => 'Dial Outgoing',
		INQUEUE => 'In Queue',
		RINGING => 'Ringing',
		PRECALL => 'Pre-Call',
		INCALL => 'In Call',
		INOUTGOING => 'In Outgoing Call',
		FAILEDOUTGOING => 'Failed Outgoing Call',
		TRANSFER => 'Transfer',
		WARMXFER => 'Warm Transfer',
		WARMXFERCOMPLETE => 'Warm Transfer Complete',
		WARMXFERFAILED => 'Warm Transfer Failed',
		WARMXFERLEG => 'Warm Transfer 2nd Leg',
		INWRAPUP => 'In Wrapup',
		ENDWRAPUP => 'End Wrapup',
		ABANDONQUEUE => 'Abandon in Queue',
		ABANDONIVR => 'Abandon in IVR',
		LEFTVOICEMAIL => 'Left a Voicemail',
		ENDCALL => 'End Call',
		UNKNOWNTERMINATION => 'Unknown Termination',
		CDREND => 'CDR End'
	}
end
