
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

module BrandSetConst
  HLIMIT = {'name' => 'Hard Call Limit in seconds (agent & supervisor)', 'value' => 'hlimit', 'match' => /[0-9]*/}
  HLMSG = {'name' => 'Hard Limit Reached Message (agent & supervisor)', 'value' => 'hlmsg', 'match' => /.*/}
  SUPLIMIT = {'name' => 'Supervisor Call Limit in seconds (supervisor only)', 'value' => 'suplimit', 'match' => /[0-9]*/}
  SUPLMSG = {'name' => 'Supservisor Limit Reached Message (supervisor only)', 'value' => 'suplmsg', 'match' => /.*/}
  SLIMIT = {'name' => 'Soft Call Limit in seconds (agent only)', 'value' => 'slimit', 'match' => /[0-9]*/}
  SLMSG = {'name' => 'Soft Limit Reached Message (agent only)', 'value' => 'slmsg', 'match' => /.*/}
  EMAILS = {'name' => 'Email Start Time (HHMM 24 hr format)', 'value' => 'emailstart', 'match' => /[0-9][0-5][0-9]|[01][0-9][0-5][0-9]|2[0-3][0-5][0-9]|2400/}
  EMAILE = {'name' => 'Email End Time (HHMM 24 hr format)', 'value' => 'emailend', 'match' => /[0-9][0-5][0-9]|[01][0-9][0-5][0-9]|2[0-3][0-5][0-9]|2400/}
  TLKHI = { 'name' =>'Talk Time High Threshold in seconds', 'value' => 'tlkhi', 'match' => /[0-9]*/}
  TLKLO = { 'name' =>'Talk Time Low Threshold in seconds', 'value' => 'tlklo', 'match' => /[0-9]*/}
  WRPHI = { 'name' =>'Wrapup Time High Threshold in seconds', 'value' => 'wrphi', 'match' => /[0-9]*/}
  WRPLO = { 'name' =>'Wrapup Time Low Threshold in seconds', 'value' => 'wrplo', 'match' => /[0-9]*/}
  QUEHI = { 'name' =>'Queue Time High Threshold in seconds', 'value' => 'quehi', 'match' => /[0-9]*/}
  QUELO = { 'name' =>'Queue Time Low Threshold in seconds', 'value' => 'quelo', 'match' => /[0-9]*/}
  ABNHI = { 'name' =>'Abandonment High Threshold (percent)', 'value' => 'abnhi', 'match' => /[0-9]*/}
  ABNLO = { 'name' =>'Abandonment Low Threshold (percent)', 'value' => 'abnlo', 'match' => /[0-9]*/}
  BRANDSETTINGS = [ HLIMIT, HLMSG, SUPLIMIT, SUPLMSG, SLIMIT, SLMSG, EMAILS, EMAILE,QUEHI,QUELO,TLKHI,TLKLO,WRPHI,WRPLO,ABNHI,ABNLO ]
end

