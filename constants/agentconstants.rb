
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

module AgentConstants
	# This module holds the possible states an agent can be in
	# as well as some information about those states.

	# the following states indicate that the agent is not logged in
	UNKNOWN = 0
	OFFLINE = 1

	# the following states are 'normal'
	IDLE = 2
	RINGING = 3

	# the following states are paused
	PRECALL = 4
	ONCALL = 5
	OUTGOINGCALL= 6
	RELEASED = 7
	WARMXFER = 8
	WRAPUP = 9
	UNKNOWNPAUSE = 10

	NSTATES = 10

	PAUSEDSTATES = [PRECALL,ONCALL,OUTGOINGCALL,RELEASED,WARMXFER,WRAPUP,UNKNOWNPAUSE]

	STATENAMES = {
		UNKNOWN => 'Unknown', OFFLINE => 'Offline', IDLE => 'Idle',
		RINGING => 'Ringing', PRECALL => 'Pre-Call', ONCALL => 'OnCall', 
		OUTGOINGCALL => 'Outgoingcall', RELEASED => 'Released', 
		WARMXFER => 'Warm Transfer', WRAPUP => 'Wrapup', 
		UNKNOWNPAUSE => 'UnknownPause'
	}
end
