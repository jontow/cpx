#!/usr/bin/env ruby

Dir[File.join(File.dirname(__FILE__),'**', '*_spec.rb')].each do |n|

	contents = File.read(n)
	contents = contents.gsub(/^(\s*)context/, "\\1describe").gsub(/^(\s*)specify/, "\\1it").gsub(/^(\s*)setup/, "\\1before").gsub(/(^\s*)teardown/, '\1after').gsub('should_receive', 'should.receive').gsub('should_raise', 'should.raise').gsub('should_eql', 'should.equal')
	
	unless contents.include? 'facon'
		contents.sub!("=end\n\n", "=end\n\nrequire File.join(File.dirname(__FILE__), '..', 'faconhack')\n")
	end

	File.open(n, 'w') do |f|
		f.puts contents
	end
end
