
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.join(File.dirname(__FILE__), '..', 'faconhack')
require File.dirname(__FILE__) + '/../mock_helpers'
require File.dirname(__FILE__) + '/../../server_events'

load File.dirname(__FILE__) + '/../../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../../agents'
require File.dirname(__FILE__) + '/../../cdr'

describe 'the QueueParams event' do
	before do
#		if Object.const_defined?(:CallQueue)
#			@tmpqueue = CallQueue
#			Object.send(:remove_const, :CallQueue)
#		end
#		CallQueue = mock('QueueClass')

#		if Object.const_defined?(:Agent)
#			@tmpagent = Agent
#			Object.send(:remove_const, :Agent)
#		end

#		Agent = mock('AgentClass')
		ClassBackup.replace_class_with_mock(:CallQueue, mock('CallQueueClass'))
#		ClassBackup.replace_class_with_mock(:Caller, mock('CallerClass'))
		ClassBackup.replace_class_with_mock(:Agent, mock('AgentClass'))

	end

	it 'should initialize as the correct event type' do
		ServerEventFactory.eventfactory(nil, "Event: QueueParams\r\n").should be_an_instance_of(QueueParamsServerEvent)
	end


	it 'should parse the event variables into instance variables correctly' do
		string = <<EVENT
Event: QueueParams\r
Queue: testq\r
Calls: 0\r
Holdtime: 3\r
Completed: 6\r
Abandoned: 2\r
Servicelevel: 60\r
ServicelevelPerf: 0.4\r
Weight: 2\r
\r
EVENT
		event = ServerEventFactory.eventfactory(nil, string)
		event.instance_variable_get(:@queue).should.equal 'testq'
		event.instance_variable_get(:@calls).should.equal '0'
		event.instance_variable_get(:@holdtime).should.equal '3'
		event.instance_variable_get(:@completed).should.equal '6'
		event.instance_variable_get(:@abandoned).should.equal '2'
		event.instance_variable_get(:@servicelevel).should.equal '60'
		event.instance_variable_get(:@servicelevelperf).should.equal '0.4'
		event.instance_variable_get(:@weight).should.equal '2'
	end

	it 'should dispatch the event correctly' do
		string = <<EVENT
Event: QueueParams\r
Queue: testq\r
Calls: 0\r
Holdtime: 3\r
Completed: 6\r
Abandoned: 2\r
Servicelevel: 60\r
ServicelevelPerf: 0.4\r
Weight: 2\r
\r
EVENT
		queue = mock('Queue')
		CallQueue.should.receive(:[]).with('testq').and_return(queue)
		agent = mock('Agent')
		queue.should.receive(:name).and_return('testq')
		queue.should.receive(:calls).and_return(0)
		queue.should.receive(:completed).and_return 1
		queue.should.receive(:abandoned).and_return 2
		queue.should.receive(:avg_queuetime).and_return 3
		queue.should.receive(:max_queuetime).and_return 4
		agent.should.receive(:send_event).with('QUEUE', 'testq', 0, 1, 2, 3, 4)
		Agent.should.receive(:match_agents).with(:tier=>4).and_yield(agent)
		ServerEventFactory.eventfactory(nil, string).dispatch
	end
	
	it 'should create the queue if it does not exist and dispatch the event correctly' do
		string = <<EVENT
Event: QueueParams\r
Queue: testq\r
Calls: 0\r
Holdtime: 3\r
Completed: 6\r
Abandoned: 2\r
Servicelevel: 60\r
ServicelevelPerf: 0.4\r
Weight: 2\r
\r
EVENT
		queue = mock('Queue')
		CallQueue.should.receive(:[]).with('testq').and_return(nil)
		CallQueue.should.receive(:new).and_return(queue)
		agent = mock('Agent')
		queue.should.receive(:name).and_return('testq')
		queue.should.receive(:calls).and_return(0)
		queue.should.receive(:completed).and_return 1
		queue.should.receive(:abandoned).and_return 2
		queue.should.receive(:avg_queuetime).and_return 3
		queue.should.receive(:max_queuetime).and_return 4
		agent.should.receive(:send_event).with('QUEUE', 'testq', 0, 1, 2, 3, 4)
		Agent.should.receive(:match_agents).with(:tier=>4).and_yield(agent)
		ServerEventFactory.eventfactory(nil, string).dispatch
	end

	after do
#		Object.send(:remove_const, :CallQueue)
#		Object.send(:remove_const, :Agent)
#		CallQueue = @tmpqueue
#		Agent = @tmpagent
		ClassBackup.restore_all
	end
end

