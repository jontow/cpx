
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.join(File.dirname(__FILE__), '..', 'faconhack')
require File.dirname(__FILE__) + '/../mock_helpers'
require File.dirname(__FILE__) + '/../../server_events'

load File.dirname(__FILE__) + '/../../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../../agents'
require File.dirname(__FILE__) + '/../../cdr'

describe 'The MessageWaiting Event' do
	it 'should initialize as the correct event type' do
		ServerEventFactory.eventfactory(nil, "Event: MessageWaiting\r\n").should be_an_instance_of(MessageWaitingServerEvent)
	end

	it 'should raise InvalidEventError on an unparseable mailbox' do
		string = <<EVENT
Event: MessageWaiting\r
Mailbox: 6000@default\r
Waiting: 99\r
New: 56\r
Old: 43\r
Msgnum: 99\r
\r
EVENT
		eventserver = mock('EventServer')
		reply = mock('Reply')
		lambda{ServerEventFactory.eventfactory(eventserver, string).dispatch}.should.raise ServerEvent::InvalidEventError
	end

	it 'should raise InvalidEventError when a mailbox cannot be matched to a queue' do
		string = <<EVENT
Event: MessageWaiting\r
Mailbox: 00310002@default\r
Waiting: 99\r
New: 56\r
Old: 43\r
Msgnum: 99\r
\r
EVENT
		eventserver = mock('EventServer')
		reply = mock('Reply')
		dbh = mock('SQLHandle')
		result = mock('SQLResult')
		eventserver.should.receive(:dbh).and_return dbh
		dbh.should.receive(:safe_query).and_return result
		result.should.receive(:num_rows).and_return 0
#		result.should.receive(:fetch_row).and_return ['queuename']
		result.should.receive(:free)
#		eventserver.should.receive(:send_event).with('Originate', :anything)
		#{'Channel'=>'Local@1/ppxtestq', 'Context'=>'vm-listen', 'Exten'=>1, 'Priority'=>1, 'Variable'=>"TYPE=voicemail\r\nVariable: DATE=#{Time.now.to_i}\r\nVariable: VMFILE=/var/spool/asterisk/voicemail/default/6000/INBOX/msg0099", 'Async'=>1})
		lambda{ServerEventFactory.eventfactory(eventserver, string).dispatch}.should.raise ServerEvent::InvalidEventError
	end


	it 'should emit an event to all relevant agents' do
		string = <<EVENT
Event: MessageWaiting\r
Mailbox: 00310002@default\r
Waiting: 99\r
New: 56\r
Old: 43\r
Msgnum: 99\r
\r
EVENT
		eventserver = mock('EventServer')
		reply = mock('Reply')
		dbh = mock('SQLHandle')
		result = mock('SQLResult')
		eventserver.should.receive(:dbh).and_return dbh
		dbh.should.receive(:safe_query).and_return result
		result.should.receive(:num_rows).and_return 1
		result.should.receive(:fetch_row).and_return ['queuename']
		result.should.receive(:free)
		eventserver.should.receive(:send_event).with('Originate', :anything)
		#{'Channel'=>'Local@1/ppxtestq', 'Context'=>'vm-listen', 'Exten'=>1, 'Priority'=>1, 'Variable'=>"TYPE=voicemail\r\nVariable: DATE=#{Time.now.to_i}\r\nVariable: VMFILE=/var/spool/asterisk/voicemail/default/6000/INBOX/msg0099", 'Async'=>1})
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	it 'should warn if Msgnum is not set in the event' do
		string = <<EVENT
Event: MessageWaiting\r
Mailbox: 6000@default\r
Waiting: 99\r
New: 56\r
Old: 43\r
Msgnum: \r
\r
EVENT
		eventserver = mock('EventServer')

		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end
end
