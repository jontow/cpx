
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.join(File.dirname(__FILE__), '..', 'faconhack')
require File.dirname(__FILE__) + '/../mock_helpers'
require File.dirname(__FILE__) + '/../../server_events'

load File.dirname(__FILE__) + '/../../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../../agents'
require File.dirname(__FILE__) + '/../../cdr'


describe 'The Leave Event' do
	before do
		ClassBackup.replace_class_with_mock(:CallQueue, mock('CallQueueClass'))
		ClassBackup.replace_class_with_mock(:Caller, mock('CallerClass'))
		ClassBackup.replace_class_with_mock(:Agent, mock('AgentClass'))

	end

	it 'should be instanciated correctly' do
		ServerEventFactory.eventfactory(nil, "Event: Leave\r\n").should be_an_instance_of(LeaveServerEvent)
	end

	it 'should emit events to agents' do
		string = <<EVENT
Event: Leave\r
Channel: SIP/1234\r
UniqueID: 98765\r
Queue: testq\r
Count: 5\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		queue = mock('Queue')
		agent = mock('Agent')
		Caller.should.receive(:[]).with('98765').and_return call
		CallQueue.should.receive(:[]).with('testq').and_return queue
		queue.should.receive(:remove_caller).with(call)
		Agent.should.receive(:match_agents).and_yield agent
		queue.should.receive(:name).and_return('testq')
		call.should.receive(:uniqueid).and_return('98765')
		call.should.receive(:enteredqueue).and_return(Time.now.to_i - 10)
		queue.should.receive(:add_queuetime).with(10)
		queue.should.receive(:add_completed)
		queue.should.receive(:completed).and_return 0
		queue.should.receive(:abandoned).and_return 1
		queue.should.receive(:avg_queuetime).and_return 2
		queue.should.receive(:max_queuetime).and_return 3
		agent.should.receive(:send_event).with('QUEUECALLERREM', 'testq', '98765', 0, 1, 2, 3)
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	it 'should error on unknown queue' do
		string = <<EVENT
Event: Leave\r
Channel: SIP/1234\r
CallerID: 123456\r
CallerIDName: Grand Poohbah\r
UniqueID: 98765\r
Queue: testq\r
Count: 5\r
Position: 5\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		Caller.should.receive(:[]).with('98765').and_return call
		CallQueue.should.receive(:[]).with('testq').and_return nil
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	it 'should error on unknown call' do
		string = <<EVENT
Event: Leave\r
Channel: SIP/1234\r
CallerID: 123456\r
CallerIDName: Grand Poohbah\r
UniqueID: 98765\r
Queue: testq\r
Count: 5\r
Position: 5\r
\r
EVENT
		eventserver = mock('EventServer')
		Caller.should.receive(:[]).with('98765').and_return nil
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	
	after do
#		Object.send(:remove_const, :CallQueue)
#		CallQueue = @tmpcallqueue
#		Object.send(:remove_const, :Caller)
#		Caller = @tmpcaller
#		Object.send(:remove_const, :Agent)
#		Agent = @tmpagent
		ClassBackup.restore_all
	end
end

