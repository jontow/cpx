
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.join(File.dirname(__FILE__), '..', 'faconhack')
require File.dirname(__FILE__) + '/../mock_helpers'
require File.dirname(__FILE__) + '/../../client_events'

#included purely for the constants they contain... (I hope)
load File.dirname(__FILE__) + '/../../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../../agents'
require File.dirname(__FILE__) + '/../../cdr'
describe 'The ABLAB event' do
	before do
		@agent = mock('Agent')
		@eventserver = mock('EventServer')
	end
	it 'should be be restricted to seclevel 2 and above agents' do
		@agent.should.receive(:seclevel).and_return(1)
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'ABLAB 1 1')}.should.raise ClientEvent::InsufficientPermissionError
	end
	it 'should be instanciated correctly from valid events' do
		@agent.should.receive(:seclevel).and_return(2)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'ABLAB 1 1').class.should.equal ABlabClientEvent
	end
	it 'should raise InvalidEventError on missing arguments' do
		@agent.should.receive(:seclevel).at_least(2).and_return(4)
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'ABLAB 1').dispatch}.should.raise ClientEvent::InvalidEventError
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'ABLAB 1 1').dispatch}.should.raise ClientEvent::InvalidEventError
	end
	it 'should raise InvalidEventError if Agent.match_agents raises ArgumentError' do
		@agent.should.receive(:seclevel).and_return(4)
		@agent.should.receive(:class).and_return(@agent)
		@agent.should.receive(:match_agents).and_raise ArgumentError
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'ABLAB 1 1 1').dispatch}.should.raise ClientEvent::InvalidEventError
	end
	it 'should send BLAB to affected agents' do
		@agent.should.receive(:seclevel).and_return(4)
		@agent.should.receive(:class).and_return(@agent)
		agent2 = mock('Agent2')
		#		agent2.should.receive(:id).and_return(1)
		#		agent2.should.receive(:state=)
		@agent.should.receive(:match_agents).and_yield(agent2)
		agent2.should.receive(:send_event).with('BLAB', '1')
		ClientEventFactory.eventfactory(@agent, @eventserver, 'ABLAB 1 1 1').dispatch
	end
end

