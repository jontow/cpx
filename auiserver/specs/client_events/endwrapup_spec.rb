
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.join(File.dirname(__FILE__), '..', 'faconhack')
require File.dirname(__FILE__) + '/../mock_helpers'
require File.dirname(__FILE__) + '/../../client_events'

#included purely for the constants they contain... (I hope)
load File.dirname(__FILE__) + '/../../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../../agents'
require File.dirname(__FILE__) + '/../../cdr'

describe 'The EndWrapup event' do
	before do
		@agent = mock('Agent')
		@eventserver = mock('EventServer')
	end

	it 'should be instanciated correctly from valid events' do
		@agent.should.receive(:tier).once.and_return(1)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'ENDWRAPUP 1 1').class.should.equal EndWrapupClientEvent
	end

	it 'should raise an InvalidEventError if the agent isn\'t in WRAPUP' do
		@agent.should.receive(:state).and_return(Agent::IDLE)
		@agent.should.receive(:paused?).and_return(false)
		lambda{EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch}.should.raise ClientEvent::InvalidEventError
	end

	it 'should log agent off if the QueuePause fails' do
		@agent.should.receive(:state).and_return(Agent::WRAPUP)
		@agent.should.receive(:id).and_return(1)
		@agent.should.receive(:queuerelease).and_return false
		call = mock('Call')
		reply = mock('Reply')
		reply.should.receive(:success?).and_return false
		reply.should.receive(:message).and_return 'foo'
		@agent.should.receive(:logoff)
		@agent.should.receive(:talkingto).and_return nil
		@eventserver.should.receive(:send_event).once.and_yield(reply)
		@agent.should.receive(:dialoutid=).with(nil)
		@agent.should.receive(:dialoutnum=).with(nil)
		EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end

	it 'should log agent off if the QueuePause fails and clean up any associated calls' do
		@agent.should.receive(:state).and_return(Agent::WRAPUP)
		@agent.should.receive(:id).and_return(1)
		@agent.should.receive(:queuerelease).and_return false
		call = mock('Call')
		reply = mock('Reply')
		cdr = mock('CDR')
		reply.should.receive(:success?).and_return false
		reply.should.receive(:message).and_return 'foo'
		@agent.should.receive(:logoff)
		@agent.should.receive(:talkingto).at_least(:twice).and_return call
		call.should.receive(:cdr).and_return cdr
		cdr.should.receive(:add_transaction).with(CDR::ENDCALL)
		@agent.should.receive(:talkingto=).with nil
		@eventserver.should.receive(:send_event).once.and_yield(reply)
		@agent.should.receive(:dialoutid=).with(nil)
		@agent.should.receive(:dialoutnum=).with(nil)
		EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end


	it 'should send an event to the event server if the agent is in WRAPUP' do
		@agent.should.receive(:state).at_least(:once).and_return(Agent::WRAPUP)
		@agent.should.receive(:paused=).with(false)
		@agent.should.receive(:queuerelease).and_return false
		@agent.should.receive(:id).and_return(1)
		@agent.should.receive(:state=).with(Agent::IDLE)
		call = mock('Call')
		cdr = mock('CDR')
		call.should.receive(:cdr).and_return cdr
		cdr.should.receive(:add_transaction).with(CDR::ENDCALL)
		call.should.receive(:get).with('VMFILE').and_return(nil)
		@agent.should.receive(:talkingto).at_least(3).times.and_return(call)
		reply = mock('Reply')
		@agent.should.receive(:talkingto=).with(nil)
		reply.should.receive(:success?).and_return true
		@eventserver.should.receive(:send_event).once.and_yield(reply)
		@agent.should.receive(:dialoutid=).with(nil)
		@agent.should.receive(:dialoutnum=).with(nil)

		EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end

	it 'should send an event to the event server if the agent is in WRAPUP' do
		@agent.should.receive(:state).and_return(Agent::WRAPUP)
		@agent.should.receive(:paused=).with(false)
		@agent.should.receive(:queuerelease).and_return false
		reply = mock('Reply')
		reply.should.receive(:success?).and_return true
		@agent.should.receive(:id).and_return(1)
		@agent.should.receive(:state=).with(Agent::IDLE)
		call = mock('Call')
		cdr = mock('CDR')
		call.should.receive(:cdr).and_return cdr
		cdr.should.receive(:add_transaction).with(CDR::ENDCALL)
		@agent.should.receive(:talkingto).at_least(3).times.and_return(call)
		call.should.receive(:get).with('VMFILE').at_least(2).times.and_return('/dev/null')
		@agent.should.receive(:send_event).with('BLAB', :anything)
		@agent.should.receive(:talkingto=).with(nil)
		@agent.should.receive(:dialoutid=).with(nil)
		@agent.should.receive(:dialoutnum=).with(nil)
		@eventserver.should.receive(:send_event).once.and_yield(reply)
		EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end
	
	it 'should put the agent in released if they queued release' do
		@agent.should.receive(:state).once.and_return(Agent::WRAPUP)
		@agent.should.receive(:queuerelease).and_return true
		@agent.should.receive(:state=).with(Agent::RELEASED)
		@agent.should.receive(:queuerelease=).with false
		call = mock('Call')
		cdr = mock('CDR')
		call.should.receive(:cdr).and_return cdr
		cdr.should.receive(:add_transaction).with(CDR::ENDCALL)
		@agent.should.receive(:talkingto).exactly(2).times.and_return(call)
		reply = mock('Reply')
		@agent.should.receive(:talkingto=).with(nil)
		@agent.should.receive(:dialoutid=).with(nil)
		@agent.should.receive(:dialoutnum=).with(nil)
		EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end

	after do
	end
end

