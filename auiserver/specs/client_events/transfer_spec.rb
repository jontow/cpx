
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.join(File.dirname(__FILE__), '..', 'faconhack')
describe 'The Transfer event' do
	before do
		@agent = mock('Agent')
		@eventserver = mock('EventServer')
	end

	it 'should be instanciated correctly from valid events' do
		@agent.should.receive(:tier).once.and_return(1)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'TRANSFER 1 1').class.should.equal TransferClientEvent
	end

	it "should raise InvalidEventError if the agent isn't in call" do
		@agent.should.receive(:talkingto).once.and_return(nil)
		lambda{TransferClientEvent.new(@agent, @eventserver, '1', '1').dispatch}.should.raise ClientEvent::InvalidEventError
	end

	it 'should raise an InvalidEventError if no extension is specified' do
		call = mock('Call')
		@agent.should.receive(:talkingto).twice.and_return(call)
		lambda{TransferClientEvent.new(@agent, @eventserver, '1', '').dispatch}.should.raise ClientEvent::InvalidEventError
		lambda{TransferClientEvent.new(@agent, @eventserver, '1', nil).dispatch}.should.raise ClientEvent::InvalidEventError
	end

	it 'should send a event to the eventserver on dispatch' do
		call = mock("Call")
		call.should.receive(:channel).once.and_return('Local/1')
		cdr = mock('CDR')
		cdr.should.receive(:add_transaction)
		call.should.receive(:cdr).and_return(cdr)
		reply = mock('Reply')
		reply.should.receive(:success?).and_return true
		@agent.should.receive(:talkingto).at_least(:once).and_return(call)
		@agent.should.receive(:talkingto=).once.with(nil)
		@agent.should.receive(:state=)
		@eventserver.should.receive(:send_event).once.and_yield(reply)
		TransferClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end

	it 'should send a event to the eventserver on dispatch' do
		call = mock("Call")
		@agent.should.receive(:talkingto).at_least(:once).and_return call
		call.should.receive(:channel).once.and_return('Local/1')
		reply = mock('Reply')
		reply.should.receive(:message)
		@eventserver.should.receive(:send_event).once.and_yield(reply)
		reply.should.receive(:success?).and_return false
		lambda{TransferClientEvent.new(@agent, @eventserver, '1', '1').dispatch}.should.raise ClientEvent::InvalidEventError
	end

end

