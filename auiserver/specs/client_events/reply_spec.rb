
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.join(File.dirname(__FILE__), '..', 'faconhack')
require File.dirname(__FILE__) + '/../mock_helpers'
require File.dirname(__FILE__) + '/../../client_events'

#included purely for the constants they contain... (I hope)
load File.dirname(__FILE__) + '/../../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../../agents'
require File.dirname(__FILE__) + '/../../cdr'

describe 'An ACK response' do
	it 'should be created as such' do
		agent = mock('Agent')
		ClientEventFactory.eventfactory(agent, nil, 'ACK 1 something').class.should.equal ClientReply
	end

	it 'should dispatch correctly' do
		agent = mock('Agent')
		agent.should.receive(:mark_replied).with(1)
		ClientEventFactory.eventfactory(agent, nil, 'ACK 1 something').dispatch
	end

	it 'should indicate it is not required to be acknowledged' do
		agent = mock('Agent')
		ClientEventFactory.eventfactory(agent, nil, 'ACK 1 something').ack?.should.equal false
	end

end

describe 'An ERR response' do
	it 'should be created as such' do
		agent = mock('Agent')
		ClientEventFactory.eventfactory(agent, nil, 'ERR 1 something').class.should.equal ClientReply
	end

	it 'should dispatch correctly' do
		agent = mock('Agent')
		agent.should.receive(:mark_replied).with(1)
		ClientEventFactory.eventfactory(agent, nil, 'ERR 1 something').dispatch
	end

	it 'should indicate it is not required to be acknowledged' do
		agent = mock('Agent')
		ClientEventFactory.eventfactory(agent, nil, 'ERR 1 something').ack?.should.equal false
	end

end
