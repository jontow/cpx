
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.join(File.dirname(__FILE__), '..', 'faconhack')
require File.dirname(__FILE__) + '/../mock_helpers'
require File.dirname(__FILE__) + '/../../client_events'

#included purely for the constants they contain... (I hope)
load File.dirname(__FILE__) + '/../../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../../agents'
require File.dirname(__FILE__) + '/../../cdr'

describe 'The State event' do
	before do
		@agent = mock('Agent')
		@eventserver = mock('EventServer')
	end

	it 'should be instanciated correctly from valid events' do
		@agent.should.receive(:tier).once.and_return(4)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'STATE 1 1').class.should.equal StateClientEvent
	end

	it 'should raise an InvalidEventError on an invalid state' do
		@agent.should.receive(:state=).once.and_raise(Agent::InvalidStateError)
#		@agent.should.receive(:state).at_least(:once).and_return Agent::IDLE
		lambda{StateClientEvent.new(@agent, @eventserver, '1', 'g').dispatch}.should.raise ClientEvent::InvalidEventError
	end
	
	it 'should attempt to set the state' do
		@agent.should.receive(:state=).once.with('6').and_return(6)
#		@agent.should.receive(:state).at_least(:once).and_return Agent::IDLE
		StateClientEvent.new(@agent, @eventserver, '1', '6').dispatch
	end

	it 'should set the state to released and pause the agent if requested and the current state is idle' do
		reply = mock('Reply')
		@agent.should.receive(:state=).once.with('5')
		@agent.should.receive(:paused=).with(true)
		@agent.should.receive(:state).at_least(:once).and_return Agent::IDLE
		@agent.should.receive(:id).and_return 6000
		reply.should.receive(:success?).and_return true
		@eventserver.should.receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
		StateClientEvent.new(@agent, @eventserver, '1', '5').dispatch

	end

	it 'should log off the agent if pausing them fails while going released' do
		reply = mock('Reply')
		#@agent.should.receive(:state=).once.with(Agent::RELEASED.to_s)
		#@agent.should.receive(:paused=).with(true)
		@agent.should.receive(:state).at_least(:once).and_return Agent::IDLE
		@agent.should.receive(:id).and_return 6000
		@eventserver.should.receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
		reply.should.receive(:success?).and_return false
		reply.should.receive(:message).and_return 'foo'
		@agent.should.receive(:logoff)
		StateClientEvent.new(@agent, @eventserver, '1', '5').dispatch
	end

	it 'should set the state to idle and unpaused the agent if requeested and the current state is released' do
		reply = mock('Reply')
		@agent.should.receive(:state=).once.with('2')
		@agent.should.receive(:paused=).with(false)
		@agent.should.receive(:state).at_least(:once).and_return Agent::RELEASED
		@agent.should.receive(:id).and_return 6000
		reply.should.receive(:success?).and_return true
		@eventserver.should.receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>0).and_yield reply
		StateClientEvent.new(@agent, @eventserver, '1', '2').dispatch

	end
end

