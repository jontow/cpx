
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.join(File.dirname(__FILE__), '..', 'faconhack')
require File.dirname(__FILE__) + '/..//mock_helpers'
require File.dirname(__FILE__) + '/../../client_events'

#included purely for the constants they contain... (I hope)
load File.dirname(__FILE__) + '/../../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../../agents'
require File.dirname(__FILE__) + '/../../cdr'

describe 'The BRIDGEDCALLERS event' do
	before do
		@agent = mock('Agent')
		@eventserver = mock('EventServer')
	end

	it 'should be be restricted to tier 4 agents' do
		@agent.should.receive(:tier).once.and_return(3)
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'BRIDGEDCALLERS 1 1')}.should.raise ClientEvent::InsufficientPermissionError
	end
		
	it 'should be instanciated correctly from valid events' do
		@agent.should.receive(:tier).once.and_return(4)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'BRIDGEDCALLERS 1 1').class.should.equal BridgedCallersClientEvent
	end

	it 'should raise InvalidEventError if Agent.match_agents raises ArgumentError' do
		@agent.should.receive(:tier).once.and_return(4)
		@agent.should.receive(:class).and_return(@agent)
		@agent.should.receive(:match_agents).and_raise ArgumentError
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'BRIDGEDCALLERS 1 1').dispatch}.should.raise ClientEvent::InvalidEventError
	end

	it 'should yield matched agents to the block' do
		@agent.should.receive(:tier).once.and_return(4)
		@agent.should.receive(:class).and_return(@agent)
		agent2 = mock('Agent2')
		call = mock('Call')
		agent2.should.receive(:talkingto).exactly(4).times.and_return(call)
		call.should.receive(:uniqueid).and_return('98765')
		call.should.receive(:get).with('BRANDID').and_return '7'
		call.should.receive(:get).with('CALLERID').and_return '1234'
		@agent.should.receive(:match_agents).and_yield(agent2)
		@agent.should.receive(:send_event).with('BRIDGECALLER', '98765', '7', '1234')
		ClientEventFactory.eventfactory(@agent, @eventserver, 'BRIDGEDCALLERS 1 1').dispatch
	end
end
