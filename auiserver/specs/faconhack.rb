require 'facon'

# Hack support for #at_least, #at_most and #exactly into facon

module Facon
	class Expectation
		attr_reader :min_expected_received_count, :max_expected_received_count

		def met?
			raise_exception_error(self) if (
				@min_expected_receive_count &&
				@min_expected_receive_count <= @actual_received_count)

			raise_exception_error(self) if (
				@max_expected_receive_count &&
				@max_expected_receive_count >= @actual_received_count)

			return true if @expected_received_count == :any ||
				@expected_received_count == @actual_received_count ||
				@min_expected_received_count || @max_expected_received_count

			raise_expectation_error(self)
		end

		def at_least(count)
			@min_expected_received_count = Integer(count)
			self
		end

		def at_most(count)
			@max_expected_received_count = Integer(count)
			self
		end

		def exactly(count)
			@expected_received_count = Integer(count)
			self
		end
		
		def times
			self
		end

		alias :time :times

		def once
			@expected_received_count = 1
			self
		end

		def twice
			@expected_received_count = 2
			self
		end
	end

	class	ErrorGenerator
		def raise_expectation_error(expectation)
			if expectation.min_expected_received_count &&
				expectation.min_expected_received_count > expectation.actual_received_count
				message = "#{target_name} expected :#{expectation.method} with #{format_args(*expectation.argument_expectation)} at least #{format_count(expectation.min_expected_received_count)}, but received it #{format_count(expectation.actual_received_count)}"
			elsif expectation.max_expected_received_count &&
				expectation.max_expected_received_count < expectation.actual_received_count
				message = "#{target_name} expected :#{expectation.method} with #{format_args(*expectation.argument_expectation)} at most #{format_count(expectation.max_expected_received_count)}, but received it #{format_count(expectation.actual_received_count)}"
			else
				message = "#{target_name} expected :#{expectation.method} with #{format_args(*expectation.argument_expectation)} #{format_count(expectation.expected_received_count)}, but received it #{format_count(expectation.actual_received_count)}"
			end
			raise(Facon::MockExpectationError, message)
		end
	end
end
