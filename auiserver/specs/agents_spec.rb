
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.join(File.dirname(__FILE__), 'faconhack')
require File.dirname(__FILE__) + '/../agents'

describe 'The Agent list with one offline agent' do
	before do
		@agent = Agent.new(0)
	end

	it 'should contain one agent' do
		Agent.match_agents(:online=>false).length.should.equal 1
	end

	it 'should contain no online agents' do
		Agent.match_agents.length.should.equal 0
	end

	it 'should raise an ArgumentError on invalid filter' do
		lambda{Agent.match_agents(6)}.should.raise ArgumentError
	end

	it 'should raise an ArgumentError on invalid id filter parameter' do
		lambda{Agent.match_agents(:ids=>'h')}.should.raise ArgumentError
	end

	it 'should filter agents by id correctly' do
		Agent.match_agents(:ids=>[0], :online=>false).length.should.equal 1
	end

	it 'should yield data to a passed in block' do
		Agent.match_agents() do |x|
			x.class.should.equal Agent
		end
	end

	it 'should raise a DuplicateAgentIDError when an new agent is specified with an existing id' do
		lambda{Agent.new(0)}.should.raise Agent::DuplicateAgentIDError
	end

	after do
		Agent.instance_variable_set(:@agents, [])
		Agent.class_eval "@@counter = 0"
	end
end

describe 'The Agent list with one offline agent and one online agent' do
	before do
		@agent1 = Agent.new(0)
		@agent2 = Agent.new(1, mock('Socket'))
		@agent2.tier = 4
		@sql = mock('SQLHandle')
		Agent.sql = @sql
	end

	it 'should contain 2 agents' do
		Agent.match_agents(:online=>false).length.should.equal 2
	end

	it 'should contain one online agent' do
		Agent.match_agents.length.should.equal 1
	end

	it 'should return the correct agent for id 0' do
		Agent.match_agents(:ids=>[0], :online=>false).length.should.equal 1
		Agent.match_agents(:ids=>[0], :online=>false)[0].should.equal @agent1
	end

	it 'should return both agents with the correct id list' do
		Agent.match_agents(:ids=>[0,1], :online=>false).length.should.equal 2
	end

	it 'should return nothing for incorrect id list' do
		Agent.match_agents(:ids=>[3,4]).length.should.equal 0
	end

	it 'should match tier and online' do
		Agent.match_agents(:tier=>4, :online=>true)[0].should.equal @agent2
	end

	it 'should accept ALL as a filter' do
		Agent.match_agents(:ids=>'ALL', :online=>false).length.should.equal 2
	end

	it 'should accept the string "0:1" as a filter' do
		Agent.match_agents(:ids=>'0:1', :online=>false).length.should.equal 2
	end

	it 'should not accept an invalid string as a filter' do
		lambda{Agent.match_agents(:ids=>'6/7')}.should.raise ArgumentError
		lambda{Agent.match_agents(:ids=>'')}.should.raise ArgumentError
	end

	it 'should raise ArgumentError if passed anything other than a String, an Array or :all' do
		lambda{Agent.match_agents(:ids=>{})}.should.raise ArgumentError
		lambda{Agent.match_agents(:ids=>:die)}.should.raise ArgumentError
	end

	it 'should notify an online tier4 agent of a state change' do
		result = mock('Result')
		@sql.should.receive(:safe_query).twice.and_return result
		result.should.receive(:fetch_row).and_return(Array.new)
		@agent1.set_socket(true)
		@agent1.paused = true
		@agent1.state = Agent::RELEASED
		@agent2.outputqueue.pop.should_match(/^STATE/)
	end

	it 'should support looking up agents by id and by socket' do
		require 'socket'
		sock = UDPSocket.new
		result = mock('Result')
		@sql.should.receive(:safe_query).and_return result
		result.should.receive(:fetch_row).and_return Array.new
		@agent1.set_socket(sock)
		Agent[sock].should.equal @agent1
		Agent[1].should.equal @agent2
	end
	
	it 'should allow agents to be detected on arbitary constraints' do
		Agent.detect {|x| x.id == 1}.should.equal @agent2
	end

	it 'should provide a list of all defined agents' do
		Agent.agents.should.equal([@agent1, @agent2])
	end
	after do
		Agent.instance_variable_set(:@agents, [])
		Agent.class_eval "@@counter = 0"
	end
end


describe 'An Agent' do
	before do
		@agent = Agent.new(0)
		@sql = mock('SQLHandle')
		Agent.sql = @sql
	end

	it 'should allow its socket to be set' do
		socket = mock('Socket')
		result = mock('Result')
		@sql.should.receive(:safe_query).and_return result
		result.should.receive(:fetch_row).and_return Array.new
		@agent.set_socket(socket)
		@agent.socket.should.equal(socket)
	end

	it 'should be allowed to be paused and unpaused' do
		@agent.paused?.should.equal false
		@agent.paused = true
		@agent.paused?.should.equal true
	end

#	specify 'should allow its state to be set' do

#		@agent.state = Agent::IDLE
#		@agent.state.should.equal Agent::IDLE
#		@agent.state = Agent::UNKNOWNPAUSE
#		@agent.state.should.equal Agent::UNKNOWNPAUSE
#		@agent.state = Agent::OFFLINE
#		@agent.state.should.equal Agent::OFFLINE
#	end


	it 'should have state set to Offline and  socket set to nil on logout, talkingto should be preserved' do
		socket = mock('Socket')
		call = mock('Caller')
		result = mock('Result')
		@sql.should.receive(:safe_query).at_least(5).times.and_return result
		result.should.receive(:fetch_row).and_return Array.new([1, 2, 3])
		@agent.set_socket(socket)
		@agent.paused = true
		@agent.state = Agent::RELEASED
		@agent.paused = false
		@agent.state = Agent::IDLE
		@agent.state = Agent::ONCALL
		@agent.talkingto = call
		@agent.logoff
		@agent.state.should.equal Agent::OFFLINE
		@agent.socket.should.equal nil
		@agent.talkingto.should.equal call
	end

	it 'should set its statetime correctly, and return it as an integer' do
		@sql.should.receive(:safe_query)
		@agent.state = Agent::OFFLINE
		(@agent.statetime - Time.now.to_i).should_satisfy {|x| x < 5}
	end

	it 'should not change its statetime if the new state is the same as the old state' do
		ts = @agent.statetime
		@agent.state = @agent.state
		@agent.statetime.should.equal ts
	end

	it 'should append messages onto its queue if the agent is online' do
		result = mock('Result')
		@sql.should.receive(:safe_query).and_return result
		result.should.receive(:fetch_row).and_return Array.new
		@agent.set_socket(true)
		@agent.send_event('Testing')
		@agent.outputqueue.pop.should.equal('Testing 1')
	end

	it 'should queue errors on send_error' do
		outputqueue = mock('Queue')
		@agent.instance_variable_set(:@outputqueue, outputqueue)
		outputqueue.should.receive(:<<).with 'ERR 1 foo'
		@agent.send_err(1, 'foo')
	end

	it 'should queue errors on send_ack' do
		outputqueue = mock('Queue')
		@agent.instance_variable_set(:@outputqueue, outputqueue)
		outputqueue.should.receive(:<<).with 'ACK 1 foo'
		@agent.send_ack(1, 'foo')
	end
	
	it 'should mark replied to events' do
		unrepliedevents = mock('Unreplied')
		event = mock('Event')
		callback = mock('Callback')
		@agent.instance_variable_set(:@unreplied, unrepliedevents)
		unrepliedevents.should.receive(:[]).with(1).and_return event
		event.should.receive(:[]).with(:callback).twice.and_return callback
		callback.should.receive(:call).with @agent
		unrepliedevents.should.receive(:delete).with 1
		@agent.mark_replied(1)
	end

	it 'must receive a valid counter' do
		unrepliedevents = mock('Unreplied')
		event = mock('Event')
		callback = mock('Callback')
		@agent.instance_variable_set(:@unreplied, unrepliedevents)
		unrepliedevents.should.receive(:[]).with(2).and_return nil
		#event.should.receive(:[]).with(:callback).twice.and_return callback
		#callback.should.receive(:call).with @agent
		unrepliedevents.should_not_receive(:delete).with 1
		@agent.mark_replied(2)
	end
	

	it 'should yield unreplied-to events to a passed in block' do
#		unrepliedevents = mock('Unreplied')
		event = mock('Event')
		@agent.instance_variable_set(:@unreplied, {1=>event})
#		unrepliedevents.should.receive(:select).and_yield(1, event)
		event.should.receive(:[]).with(:time).and_return Time.now
		@agent.handle_unreplied(30) {|x| x.should.equal event}
		
	end

	it 'should expire events correctly' do
		unrepliedevents = mock('Unreplied')
		event = mock('Event')
		@agent.instance_variable_set(:@unreplied, unrepliedevents)
#		unrepliedevents.should.receive(:select).and_yield(1, event)
#		event.should.receive(:[]).with(:time).and_return Time.now
#		@agent.handle_unreplied(30) {|x| x.should.equal event}
		event.should.receive(:[]).with(:counter).and_return 1
		unrepliedevents.should.receive(:delete).with 1
		@agent.expire_event(event)
	end

	after do
		Agent.instance_variable_set(:@agents, [])
		Agent.class_eval "@@counter = 0"
	end
end

describe 'An Agent changing their state' do
	before do
		@agent = Agent.new(0)
		@sql = mock('SQLHandle')
		Agent.sql = @sql
		result = mock('Result')
		@sql.should.receive(:safe_query).at_least(1).times.and_return result
		result.should.receive(:fetch_row).and_return Array.new([1, 2, 3])
		@agent.set_socket(true)
	end

	it 'should raise InvalidStateError on invalid state' do
		lambda{@agent.state = 99}.should.raise Agent::InvalidStateError
		lambda{@agent.state = -4}.should.raise Agent::InvalidStateError
		lambda{@agent.state = 'a'}.should.raise Agent::InvalidStateError
	end

#	specify 'should raise InvalidChangeStateError if agent is paused and state is set to Idle or OnCall' do
#		@agent.paused = true
#		lambda{@agent.state = Agent::IDLE}.should.raise Agent::InvalidStateChangeError
#		lambda{@agent.state = Agent::ONCALL}.should.raise Agent::InvalidStateChangeError
#	end

### TODO
#	specify 'should never be able to explicitly change to UNKNOWN state' do
#		#@agent.instance_variable_set(:@state, Agent::OFFLINE)
#		1.upto(7) do |x|
#			@agent.instance_variable_set(:@state, x)
#			lambda{@agent.state=(Agent::UNKNOWN)}.should.raise Agent::InvalidStateChangeError
#		end
#	end
	
	it 'should not be able to change to any state besides OFFLINE if not logged in' do
		@agent.logoff
		0.upto(7) do |x|
			next if x == @agent.state
			lambda{@agent.state=(x)}.should.raise Agent::InvalidStateChangeError
		end

		@agent.instance_variable_set(:@state, Agent::UNKNOWN)
		lambda{@agent.state=Agent::OFFLINE}.should_not_raise Agent::InvalidStateChangeError

	end

	it 'should only be able to change from OFFLINE to RELEASED' do
		@agent.instance_variable_set(:@state, Agent::OFFLINE)
		0.upto(7) do |x|
			next if x == @agent.state
			Agent::PAUSEDSTATES.include?(x) ? @agent.paused = true : @agent.paused = false
			if x == Agent::RELEASED
				lambda{@agent.state=(x)}.should_not_raise Agent::InvalidStateChangeError
			else
				lambda{@agent.state=(x)}.should.raise Agent::InvalidStateChangeError
			end
		end
	end

	it 'should only be able to change from IDLE to OFFLINE,ONCALL,OUTGOINGCALL,RELEASED' do
		0.upto(7) do |x|
			@agent.instance_variable_set(:@state, Agent::IDLE)
			next if x == @agent.state
			Agent::PAUSEDSTATES.include?(x) ? @agent.paused = true : @agent.paused = false
			if [Agent::UNKNOWN,Agent::WRAPUP].include? x
				lambda{@agent.state=(x)}.should.raise Agent::InvalidStateChangeError
			else
				lambda{@agent.state=(x)}.should_not_raise Agent::InvalidStateChangeError
			end
		end
	end

	it 'should only be able to change from ONCALL to WRAPUP or queue RELEASED' do
		0.upto(7) do |x|
			@agent.instance_variable_set(:@state, Agent::ONCALL)
			next if x == @agent.state
			Agent::PAUSEDSTATES.include?(x) ? @agent.paused = true : @agent.paused = false
			if Agent::WRAPUP == x
				lambda{@agent.state=(x)}.should_not_raise Agent::InvalidStateChangeError
			elsif Agent::RELEASED == x
				lambda{@agent.state=(x)}.should_not_raise Agent::InvalidStateChangeError
				@agent.state.should_equal Agent::ONCALL
				@agent.queuerelease.should_equal true
			else
				lambda{@agent.state=(x)}.should.raise Agent::InvalidStateChangeError
			end
		end
	end

	it 'should only be able to change from OUTGOINGCALL to WRAPUP or queue RELEASED' do
		0.upto(7) do |x|
			@agent.instance_variable_set(:@state, Agent::OUTGOINGCALL)
			next if x == @agent.state
			Agent::PAUSEDSTATES.include?(x) ? @agent.paused = true : @agent.paused = false
			if Agent::WRAPUP == x
				lambda{@agent.state=(x)}.should_not_raise Agent::InvalidStateChangeError
			elsif Agent::RELEASED == x
				lambda{@agent.state=(x)}.should_not_raise Agent::InvalidStateChangeError
				@agent.state.should_equal Agent::OUTGOINGCALL
				@agent.queuerelease.should_equal true
			else
				lambda{@agent.state=(x)}.should.raise Agent::InvalidStateChangeError
			end
		end
	end

	it 'should only be able to change from RELEASED to IDLE,OUTGOINGCALL' do
		0.upto(7) do |x|
			@agent.paused = true
			@agent.instance_variable_set(:@state, Agent::RELEASED)
			next if x == @agent.state
			Agent::PAUSEDSTATES.include?(x) ? @agent.paused = true : @agent.paused = false
			if [Agent::IDLE,Agent::OUTGOINGCALL].include? x
				lambda{@agent.state=(x)}.should_not_raise Agent::InvalidStateChangeError
			else
				lambda{@agent.state=(x)}.should.raise Agent::InvalidStateChangeError
			end
		end
	end

#	WTF DO WE DO WITH UNKNOWN TO ANY OTHER? -- TODO

	it 'should only be able to change from WRAPUP to RELEASED,IDLE' do
		0.upto(7) do |x|
			@agent.paused = true
			@agent.instance_variable_set(:@state, Agent::WRAPUP)
			next if x == @agent.state
			Agent::PAUSEDSTATES.include?(x) ? @agent.paused = true : @agent.paused = false
			if [Agent::IDLE,Agent::RELEASED].include? x
				lambda{@agent.state=(x)}.should_not_raise Agent::InvalidStateChangeError
			else
				lambda{@agent.state=(x)}.should.raise Agent::InvalidStateChangeError
			end
		end
	end

	it 'should raise InvalidStateError when changing from an invalid state' do
		@agent.instance_variable_set(:@state, 66)
		lambda{@agent.state = Agent::IDLE}.should.raise Agent::InvalidStateError
	end

	it 'should raise InvalidStateChangeError if changing to a paused state without being paused' do
		@agent.paused = false
		2.upto(7) do |s|
			@agent.instance_variable_set(:@state, Agent::IDLE)
			if Agent::PAUSEDSTATES.include? s
				lambda{@agent.state = s}.should.raise Agent::InvalidStateChangeError,  /annot switch to a paused state when not paused/
			else
				lambda{@agent.state = s}.should_not_raise Agent::InvalidStateChangeError,  /annot switch to a paused state when not paused/
			end
		end
	end

	it 'should raise InvalidStateChangeError if changing to an unpaused state while being paused' do
		@agent.paused = true
		2.upto(7) do |s|
			@agent.instance_variable_set(:@state, Agent::RELEASED)
			if Agent::PAUSEDSTATES.include? s
				lambda{@agent.state = s}.should_not_raise Agent::InvalidStateChangeError, /annot switch to an unpaused state when paused/
			else
				lambda{@agent.state = s}.should.raise Agent::InvalidStateChangeError, /annot switch to an unpaused state when paused/
			end
		end
	end

	it 'should be able to change from UNKNOWN to any state except WRAPUP' do
		2.upto 7 do |x|
			@agent.instance_variable_set(:@state, Agent::UNKNOWN)
			Agent::PAUSEDSTATES.include?(x) ? @agent.paused = true : @agent.paused = false
			if x == Agent::WRAPUP
				lambda{@agent.state = x}.should.raise Agent::InvalidStateChangeError
			else
				lambda{@agent.state = x}.should_not_raise Agent::InvalidStateChangeError
			end
		end
	end

	it 'should raise InvalidStateChangeError when attempting to change to UNKNOWN' do
		1.upto 7 do |x|
			@agent.instance_variable_set(:@state, x)
			Agent::PAUSEDSTATES.include?(x) ? @agent.paused = true : @agent.paused = false
			lambda{@agent.state = Agent::UNKNOWN}.should.raise Agent::InvalidStateChangeError
		end
	end


#	specify 'should not be able to set themselves to any state other than UNKNOWN, OFFLINE or ONCALL without being logged in' do
#		lambda{@agent.state=(Agent::IDLE)}.should.raise Agent::InvalidStateError
#		lambda{@agent.state=(Agent::RELEASED)}.should.raise Agent::InvalidStateError
#		lambda{@agent.state=(Agent::WRAPUP)}.should.raise Agent::InvalidStateError
#		lambda{@agent.state=(Agent::UNKNOWNPAUSE)}.should.raise Agent::InvalidStateError
#	end
#
#	specify 'should not be able to set themselves as offline or unknown while logged in' do
#		result = mock('Result')
#		@sql.should.receive(:safe_query).and_return result
#		result.should.receive(:fetch_row).and_return Array.new
#		@agent.set_socket(true)
#		lambda{@agent.state=(Agent::UNKNOWN)}.should.raise Agent::InvalidStateError
#		lambda{@agent.state=(Agent::OFFLINE)}.should.raise Agent::InvalidStateError
#	end
#
#	specify 'should not be able to set state to IDLE, ONCALL or OUTGOINGCALL if paused' do
#		result = mock('Result')
#		@sql.should.receive(:safe_query).and_return result
#		result.should.receive(:fetch_row).and_return Array.new
#		@agent.set_socket(true)
#		@agent.paused = true
#		lambda{@agent.state=(Agent::IDLE)}.should.raise Agent::InvalidStateError
#		lambda{@agent.state=(Agent::ONCALL)}.should.raise Agent::InvalidStateError
#		lambda{@agent.state=(Agent::OUTGOINGCALL)}.should.raise Agent::InvalidStateError
#	end
#
#	specify 'should not be able to go into WRAPUP unless paused' do
#		result = mock('Result')
#		@sql.should.receive(:safe_query).and_return result
#		result.should.receive(:fetch_row).and_return Array.new
#		@agent.set_socket(true)
#		lambda{@agent.state=(Agent::WRAPUP)}.should.raise Agent::InvalidStateError
#	end
#
#	specify 'should not be able to go released if not marked as logged in' do
#		result = mock('Result')
#		@sql.should.receive(:safe_query).and_return result
#		result.should.receive(:fetch_row).and_return Array.new
#		@agent.set_socket(true)
#		lambda{@agent.state=(Agent::RELEASED)}.should.raise Agent::InvalidStateError
#	end
#
#	specify 'should not be able to go released from idle unless paused' do
#		result = mock('Result')
#		@sql.should.receive(:safe_query).twice.and_return result
#		result.should.receive(:fetch_row).and_return Array.new
#		@agent.set_socket(true)
#		@agent.state = Agent::IDLE
#		lambda{@agent.state=(Agent::RELEASED)}.should.raise Agent::InvalidStateError
#	end
#
	it 'should queue the released state if the agent is oncall' do
		#result = mock('Result')
		#@sql.should.receive(:safe_query).twice.and_return result
		#result.should.receive(:fetch_row).and_return Array.new
		#@agent.set_socket(true)
		@agent.state = Agent::ONCALL
		@agent.state = Agent::RELEASED
		@agent.queuerelease.should.equal true
		@agent.state.should.equal Agent::ONCALL
	end

	it 'should queue the released state if the agent is on an outgoingcall' do
		#result = mock('Result')
		#@sql.should.receive(:safe_query).twice.and_return result
		#result.should.receive(:fetch_row).and_return Array.new
		#@agent.set_socket(true)
		@agent.paused = true
		@agent.state = Agent::OUTGOINGCALL
		@agent.state = Agent::RELEASED
		@agent.queuerelease.should.equal true
		@agent.state.should.equal Agent::OUTGOINGCALL
	end
	
	it 'should queue the released state if the agent is in wrapup' do
		#result = mock('Result')
		#@sql.should.receive(:safe_query).twice.and_return result
		#result.should.receive(:fetch_row).and_return [1, 2, 3]
		#@agent.set_socket(true)
		@agent.instance_variable_set(:@state, Agent::ONCALL)
		@agent.paused = true
		@agent.state = Agent::WRAPUP
		@agent.state = Agent::RELEASED
		@agent.queuerelease.should == true
		@agent.state.should.equal Agent::WRAPUP
	end

	it 'should set the agents state to released if requested and the agent is idle and paused' do
		#result = mock('Result')
		#@sql.should.receive(:safe_query).exactly(3).times.and_return result
		#result.should.receive(:fetch_row).and_return [1, 2, 3]
		#@agent.set_socket(true)
		@agent.state= Agent::IDLE
		@agent.paused = true
		@agent.state = Agent::RELEASED
		@agent.state.should.equal Agent::RELEASED
	end
#
#	specify 'should set the agent to released if they are in unknown pause' do
#		result = mock('Result')
#		@sql.should.receive(:safe_query).exactly(3).times.and_return result
#		result.should.receive(:fetch_row).and_return Array.new
#		@agent.set_socket(true)
#		@agent.paused = true
#		@agent.state = Agent::UNKNOWNPAUSE
#		@agent.state = Agent::RELEASED
#		@agent.state.should.equal Agent::RELEASED
#	end

	after do
		Agent.instance_variable_set(:@agents, [])
		Agent.class_eval "@@counter = 0"
	end
end
