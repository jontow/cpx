
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'thread'

require 'constants/agentconstants'

class Agent

	include AgentConstants
	
	@agents ||= []
	@@counter ||= 0

	class DuplicateAgentIDError < StandardError; end
	class InvalidStateError < StandardError; end
	class InvalidStateChangeError < StandardError; end
	class InvalidProfileError < StandardError; end

	def self.releaseoptions
		if @releaseoptions
			@releaseoptions
		else
			res = []
			result = @@dbh.safe_query('SELECT id, label, utilbias FROM release_options WHERE enabled=1 ORDER BY sortorder DESC')
			result.each_hash do |row|
				res << {:id => row['id'].to_i, :name => row['label'], :utilbias => row['utilbias'].to_i}
			end
			@releaseoptions = res
		end
	end

	def self.sql=(dbh)
		@@dbh = dbh
	end

	def self.new(eventserver, id, socket=nil)
		if @agents.detect{|x| x.id == id}
			raise DuplicateAgentIDError, "This agentid is already in use", caller
		end
		val = super
		@agents << val
		val
	end

	def self.profiles
		unless @profiles
			@profiles= {}
			result = @@dbh.safe_query("SELECT profile, name FROM profiles")
			result.each do |row|
				@profiles[row[0].to_i] = row[1]
			end
		end
		@profiles
	end

	def self.profiles=(hash)
		@profiles=hash
	end

	def self.match_agents(arg=nil)
		if arg.kind_of? Hash or arg.nil?
			filter = {:ids=>:all, :seclevel=>0, :iseclevel=>5, :online=>true, :hidden=>true}
			filter.merge!(arg) if arg
		else
			raise ArgumentError, "Pass a parameter hash or nothing", caller
		end

		if filter[:ids].kind_of? String
			if filter[:ids] == 'ALL'
				filter[:ids] = :all
			elsif !filter[:ids].empty?
				filter[:ids] = filter[:ids].split(':').map do |x|
					begin
						Integer(x)
					rescue ArgumentError
						x.downcase
					end
				end
			else
				raise ArgumentError,
					"Must pass ALL or a colon seperated list of agent names or IDs", caller
			end
		end

		if filter[:ids] == :all
			agentlist = @agents
		elsif filter[:ids].kind_of? Array
			agentlist = @agents.select do |a|
				filter[:ids].include?(self.profiles[a.profile].to_s.downcase) or
				filter[:ids].include?(a.id) or
				filter[:ids].include?(a.name.downcase)
			end
		else
			raise ArgumentError, ":ids must be an Array, :all or a String", caller
		end
	
		return [] unless agentlist

		agentlist = agentlist.select do |a|
			a.seclevel >= filter[:seclevel] and
			a.seclevel <= filter[:iseclevel] and
			(a.socket or !filter[:online]) and
			(!a.hidden or (a.hidden and filter[:hidden]) or
				(filter[:agent] and (filter[:agent].profile == a.profile)))
		end

		agentlist.each do |a|
			yield a
		end if block_given? #heh, tricky
		agentlist
	end

	# This method looks up an agent by socket or agent id, either is valid.
	def self.[](id)
		case id
		when BasicSocket
			@agents.detect{|a| a.socket == id}
		when Integer
			@agents.detect{|a| a.id == id}
		when String
			@agents.detect{|a| a.name == id}
		else
			raise ArgumentError
		end
	end

	def self.detect(&block)
		@agents.detect(&block)
	end

	def self.agents
		@agents
	end

	attr_reader :id, :socket, :outputqueue, :state, :profile, :hidden
	attr_accessor :seclevel, :name, :talkingto, :queuerelease, :queuereleasedata, :dialoutid, :dialoutnum, :dialedclient, :warmxfernum, :warmxferid, :warmxferchan, :askxfer, :lastpop, :completing_transfer, :warmxferlinked

	def initialize(eventserver, id, socket=nil)
		@eventserver = eventserver
		@id = id
		@socket = socket
		@state = UNKNOWN
		@statetime = Time.now
		@state_data = nil
		@seclevel = 0
		@outputqueue = Queue.new
		@talkingto = nil
		@paused = true
		@unreplied = {}
		@queuerelease = false		# TODO: rename for clarity?  actually queues a release state
		@queuereleasedata = nil
		@callsecs = 0
		@wrapupsecs = 0
		@idlesecs = 0
		@profile = 0
		@hidden = false
		@logintime = nil
	end

	def to_s
		"#{@name}(#{@id})"
	end

	def next_counter
		@@counter += 1
		@@counter %= 65535
	end

	def stats
		# check if we should bootstrap
#		if @idlesecs == 0 and @callsecs == 0
#			now = Time.now
#			@callsecs, @idlesecs, @wrapupsecs = bootstrap_agent_stats(now - Time.mktime(now.year, now.month, now.day))
#		end
		[@callsecs, @wrapupsecs, @idlesecs]
	end

	def bootstrap_agent_stats(interval)
		# Build a list of release states that negatively affect utilization
		negrelease = self.class.releaseoptions.select{|x| x[:utilbias] < 0}.map{|x| x[:id]}.join(',')
		if negrelease.nil? or negrelease.empty?
			negrelease = "0"
		end
		# Build a list of release states that positively affect utilization
		posrelease = self.class.releaseoptions.select{|x| x[:utilbias] > 0}.map{|x| x[:id]}.join(',')
		if posrelease.nil? or posrelease.empty?
			posrelease = "0"
		end
		result = @@dbh.safe_query("SELECT
								(SELECT SUM(end-start)
									FROM agent_states
									WHERE start > #{interval}
									AND agent = '#{@id}'
									AND profile = '#{@profile}'
									AND (oldstate = #{ONCALL}
										OR oldstate = #{OUTGOINGCALL}
										OR oldstate = #{WARMXFER}
										OR (oldstate = #{RELEASED} AND data IN (#{posrelease})))) AS incall,
								(SELECT SUM(end-start)
									FROM agent_states
									WHERE start > #{interval}
									AND agent = '#{@id}'
									AND profile = '#{@profile}'
									AND oldstate = #{WRAPUP}) AS wrapup,
								(SELECT SUM(end-start)
									FROM agent_states
									WHERE start > #{interval}
									AND agent = '#{@id}'
									AND profile = '#{@profile}'
									AND (oldstate = #{IDLE}
									OR (oldstate = #{RELEASED} AND data IN (#{negrelease})))) as idle")
		result.fetch_row.map{|x| x.to_i} # returns a 3 element array -- same as Agent.stats() from above
	end
	
	def statetime
		@statetime.to_i
	end

	def set_profile(profile)
		return if @profile == profile # Don't bother
		queues = profile_change(profile)
		unless [RINGING, PRECALL, ONCALL, OUTGOINGCALL, WARMXFER, WRAPUP].include? @state
			@oldprofile = @profile
		end
		@profile = profile
		send_event('PROFILE', profile)
		queues
	end

	def set_defaultprofile(profile)
		queues = profile_change(profile)
		@profile = profile
		@oldprofile = profile
		@defaultprofile = profile
		queues
	end

	#
	# Change agent profile: ...
	#
	def profile_change(profile)
		result = @@dbh.safe_query("SELECT name, hidden FROM profiles WHERE profile=#{@@dbh.quote(profile)}")
		if result.num_rows != 1
			result.free
			raise InvalidProfileError,
				"Tried to set agent #{@id} to invalid profile #{profile}", caller
		end
		row = result.fetch_row
		name = row[0]
		@hidden = !row[1].to_i.zero?
		result.free

		result = @@dbh.safe_query("SELECT queue, penalty
						FROM profile_lookup
						WHERE profile=#{profile}
						AND queue IN
					  (SELECT name
						FROM queues
						WHERE skill IN
						  (SELECT skill
							FROM skill_lookup
							WHERE agent=#{@id})
							OR skill IS NULL)")
		queues = []
		result.each do |row|
			queues << row
		end
		result.free

		insertstring = '(' + queues.map{|x| "'#{x[0]}', 'Agent/#{@id}', #{x[1]}"}.join('),(') + ')'
		@@dbh.safe_query("DELETE FROM queue_members WHERE interface='Agent/#{@id}'")
		@@dbh.safe_query("INSERT INTO queue_members(queue_name, interface, penalty) VALUES #{insertstring}") unless queues.empty?
		@eventserver.send_event('QueuesSync', 'Interface'=>"Agent/#{id}")
		
		# recalculate stats
		now = Time.now
		@callsecs, @wrapupsecs, @idlesecs = bootstrap_agent_stats(Time.mktime(now.year, now.month, now.day).to_i)

		queues.map{|x| x[0]}
	end

	private :profile_change

	def state=(state)
		set_state(state)
	end

	def set_state(state, data=nil)
		begin
			state = Integer(state).abs
		rescue ArgumentError
			raise InvalidStateError,
				"Agent #{@name} tried to set state to invalid state #{state} (#{STATENAMES[@state]} -> #{state})", caller
		else
			if state < 0 or state > NSTATES
				raise InvalidStateError,
					"Agent #{@name} tried to set state to out of bounds state (#{STATENAMES[@state]} -> #{state})", caller
			end
		end

		if state == @state
			# Setting state to current state, this should be fine (if somewhat redundant)
		elsif !@socket
			# Agent not logged in
			if state != OFFLINE
				# Agent isn't being set to offline
				raise InvalidStateChangeError,
						"Agent #{@name} cannot set state to anything but OFFLINE if not logged in (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
			end

		else

			if state == RELEASED and [ONCALL, PRECALL, OUTGOINGCALL, WRAPUP].include? @state
				@queuerelease = true
				Cpxlog.debug "#{self} queued release state with data #{data}"
				@queuereleasedata = data.to_i if data
				return
			end

			if PAUSEDSTATES.include? state
				unless self.paused?
					raise InvalidStateChangeError,
						"Agent #{@name} cannot switch to a paused state when not paused (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				end
			else
				if state != RINGING and self.paused?
					raise InvalidStateChangeError,
						"Agent #{@name} cannot switch to an unpaused state when paused (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				end
			end

			if @socket and [UNKNOWN, OFFLINE].include? state
					raise InvalidStateChangeError,
						"Agent #{@name} cannot set state to UNKNOWN or OFFLINE while logged in (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
			end

			case @state
			when UNKNOWN
				if state == WRAPUP
					# Agent cannot go into WRAPUP if currently in UNKNOWN state
					raise InvalidStateChangeError,
							"Agent #{@name} cannot set state to WRAPUP from UNKNOWN (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				end

			when OFFLINE
				unless [RELEASED, ONCALL, OUTGOINGCALL, RINGING].include? state
					# Agent *MUST* go RELEASED when logging in (changing state away from OFFLINE implies a login)
					raise InvalidStateChangeError,
							"Agent #{@name} cannot set state to anything but RELEASED, ONCALL, OUTGOINGCALL or RINGING when logging in (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				end

			when IDLE
				if [UNKNOWN,WRAPUP].include? state
					# Agent can't go into UNKNOWN or RELEASED state from IDLE
					raise InvalidStateChangeError,
							"Agent #{@name} cannot set state to UNKNOWN OR WRAPUP from IDLE (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				
				end

			when RINGING
				unless [IDLE,ONCALL,RELEASED,PRECALL].include? state
					# Agent *MUST* go IDLE or ONCALL from RINGING
					raise InvalidStateChangeError,
							"Agent #{@name} cannot set state to anything but IDLE, ONCALL, PRECALL or RELEASED from RINGING (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				end

			when PRECALL

			when ONCALL
				unless [WRAPUP,WARMXFER].include? state
					# Agent must go into WRAPUP after a call
					raise InvalidStateChangeError,
							"Agent #{@name} cannot set state to anything but WRAPUP or WARMXFER from ONCALL (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				end

			when OUTGOINGCALL
				unless [WRAPUP,WARMXFER].include? state
					# Agent must go into WRAPUP after an outbound call
					raise InvalidStateChangeError,
							"Agent #{@name} cannot set state to anything but WRAPUP or WARMXFER from OUTGOINGCALL (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				end

			when RELEASED
				unless [RINGING,PRECALL,IDLE,OUTGOINGCALL,ONCALL].include? state
					# Agent can only go IDLE or initiate an OUTGOINGCALL from RELEASED
					raise InvalidStateChangeError,
							"Agent #{@name} cannot set state to anything but RINGING,IDLE,OUTGOINGCALL,ONCALL from RELEASED (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				end

			when WARMXFER
				unless [WRAPUP, ONCALL].include? state
					raise InvalidStateChangeError,
						"Agent #{@name} cannot set state to anything but WRAPUP from WARMXFER (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				end

			when WRAPUP
				unless [RELEASED,IDLE].include? state
					# Agent can only go RELEASED or IDLE after WRAPUP
					raise InvalidStateChangeError,
							"Agent #{@name} cannot set state to anything but IDLE,RELEASED from WRAPUP (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
				end

			when UNKNOWNPAUSE
				# TODO -- can we get rid of this soon?

			else
				raise InvalidStateError,
						"What the hell?  Current invalid state: #{@state} for Agent #{@name}", caller
			end
		end

		if [ONCALL, WARMXFER, WRAPUP].include? state and !@talkingto
			raise InvalidStateChangeError,
				"Agent #{id} cannot change to this state without being in call (#{STATENAMES[@state]} -> #{STATENAMES[state]})", caller
		end

		stchange(state, data)

	end

	def stchange(state, data)
		oldstate = @state
		@state = state
		send_event('ASTATE', @state)

		if oldstate != state
			oldtime = @statetime
			@statetime = Time.now

			case oldstate
			when ONCALL, OUTGOINGCALL
				@callsecs += (@statetime - oldtime).to_i
			when IDLE
				@idlesecs += (@statetime - oldtime).to_i
			when RELEASED
				opt = self.class.releaseoptions.detect{|x| x[:id] == @state_data}
				if opt and opt[:utilbias] > 0
					@callsecs += (@statetime - oldtime).to_i
				elsif opt and opt[:utilbias] < 0
					@idlesecs += (@statetime - oldtime).to_i
				end
			when WRAPUP
				@wrapupsecs += (@statetime - oldtime).to_i
			end
	
			@state_data = nil

			if @talkingto
				@state_data = @talkingto.uniqueid
			elsif @state == RINGING
				if call = Caller.callers.detect{|x| x.ringingto == @id}
					@state_data = call.uniqueid
				end
			elsif data
				@state_data = data
			end

			self.class.match_agents(:seclevel=>2) do |a|
				a.send_event('STATE', statetime, @id, @name, @state, self.state_data, @profile, *stats)
			end
			
			# write the old profile to the state table for consistant metrics
			if [RINGING, ONCALL, OUTGOINGCALL, WARMXFER, WRAPUP].include? oldstate
				profile = @oldprofile
			else
				profile = @profile
				@oldprofile = @profile
			end

			@@dbh.safe_query("INSERT INTO agent_states VALUES('#{@id}', #{oldstate}, #{@state}, #{oldtime.to_i}, #{@statetime.to_i}, #{profile}, '#{@state_data}')")
		end
	end

	private :stchange

	def pause(bool)
		if @paused == bool or !@socket
			# what the hell; let's execute the block anyway, it might help
			if block_given?
				yield
			end
			return
		end


		@eventserver.send_event('AgentPause', {"Agent"=>id, 'Paused'=>bool ? 'true' : 'false'}) do |reply|
			if reply.success?
				@paused = bool
				if block_given?
					yield
				end
			else
				# Logoff agent -- only way this case can be hit is if 
				# CBX thinks the interface doesn't exist.
				# NOTE: this also gets hit if the agent is a member of a nonexistant queue?
				Cpxlog.err "Error #{bool ? 'pausing' : 'true'} #{@name}: #{reply.message}"
				logoff
			end
		end
	end

	def paused?
		@paused
	end

	def state_data
		if @state_data
			@state_data
		else
			'none'
		end
	end

	def send_event(event, *args, &block)
		return unless @socket
		counter = next_counter
		ev = ([event, counter]+args).join(' ')
		@unreplied[counter] = {:time=>Time.now, :callback=>block, :event=>ev, :counter=>counter, :retries=>0}
		@outputqueue << ev
	end

	def send_err(counter, message)
		@outputqueue << "ERR #{counter} #{message}"
	end

	def send_ack(counter, message)
		@outputqueue << "ACK #{counter} #{message}"
	end

	def set_socket(socket, dologin=true)
		if socket and dologin
			now = Time.now
			@callsecs, @wrapupsecs, @idlesecs = bootstrap_agent_stats(Time.mktime(now.year, now.month, now.day).to_i)
			@logintime = now
		end

		@socket = socket
	end

	def logoff
		if @socket
			@socket.close unless @socket.closed?
		end
		@socket = nil
		stchange(OFFLINE, nil)
		@unreplied = {}
		@queuerelease = false
		@queuereleasedata = nil
		@askxfer = false
		@paused = true
		@warmxferlinked = false
		@completing_transfer = false
		@@dbh.safe_query("DELETE FROM queue_members WHERE interface='Agent/#{@id}'")
		Cpxlog.info "Agent #{self} logged off (logged in since #{@logintime})"
	end

	def mark_replied(counter, reply)
		if e = @unreplied[counter]
			catch :return do
				e[:callback].call(reply) if e[:callback]
			end
			@unreplied.delete counter
		else
			Cpxlog.warning("Got reply '#{reply.data}' for unexpected event #{counter} for Agent #{self}")
		end
	end

	def handle_unreplied(timeout)
		events = @unreplied.select{|key, value| Time.now - value[:time] > timeout}
		if block_given?
			events.each {|counter, event| yield event}
		end
		events
	end

	def expire_event(event)
		Cpxlog.notice "Expiring unsent event '#{event[:event]} to #{self} after #{event[:retries]} retries"
		@unreplied.delete(event[:counter])
	end
end

