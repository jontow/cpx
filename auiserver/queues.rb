
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end


class CallQueue

	class DuplicateQueueError < StandardError; end
	class DuplicateMemberError < StandardError; end

	@queues ||= []

	def self.new(*args)
		if self[args[0]]
			raise DuplicateQueueError, "Queue name #{args[0]} already exists", caller
		end

		val = super
		@queues << val
		val
	end

	def self.match_queues(filter)
		if filter == 'ALL' or filter == :all
			queues = @queues
		elsif filter.kind_of? String or filter.kind_of? Array
			if filter.kind_of? String
				filter = filter.split(':')
			end
			queues = @queues.select{|q| filter.include? q.name}

		else
			raise ArgumentError,
				"Must be passed the string 'ALL', the symbol :all, a : seperated list in string form or an array",
				caller
		end

		return [] if queues.empty?

		if block_given?
			queues.each do |q|
				yield q
			end
		end
		queues
	end

	def self.[](name)
		@queues.detect{|q| q.name == name}
	end

	def self.queues
		@queues
	end

	def self.update_queue_membership(agent, queues)
		@queues.each do |queue|
			if queues.include? queue.name
				unless queue.members.detect{|x| x.agent == agent}
					queue.add_member(agent) 
				end
			else
				queue.remove_member(agent)
			end
		end
	end

	def self.recover_unknown_queue(serverevent, queuename)
		if serverevent.recursing
			Cpxlog.err("Recursion detected while trying to instanciate unknown queue #{queuename}")
			return
		end

		Cpxlog.notice("Attempting to recover queue #{queuename}")

		serverevent.eventserver.send_event('QueueInfo', 'Queue'=> queuename) do |reply|
			if reply.success?
				serverevent.recursing = true
				if CallQueue[queuename]
					serverevent.dispatch
				else
					Cpxlog.err("queue #{queuename} populated from QueueInfo but somehow not instanciated?!")
				end
			else
				Cpxlog.err("Unknown queue: #{queuename}")
			end
		end
	end

	attr_reader :name, :callers, :members
	attr_accessor :queuegroup, :commonname, :weight

	def initialize(name)
		@name = name
		@callers = []
		@members = []
		@queuetimes = {}
		@queuegroup = 1
		@commonname = ''
		@weight = 0
		bootstrap_queue_times
		bootstrap_queue_calls
	end

	def to_s
		"#{@commonname}(#{@name})"
	end

	def add_caller(call, position)
		# NOTE - cbx's queue positions are 1 indexed, deduct 1 from it to correct for this
		# (ruby and the tile treeview widget are 0 indexed)
		@callers.insert(position - 1, call)
	end

	def remove_caller(call)
		@callers.delete call
	end

	def add_member(agent)
		if @members.detect{|x| x.agent == agent}
			raise DuplicateMemberError,
				"This Agent is already a member of this queue", caller
		end
		member = QueueMember.new(agent)
		@members << member
		member
	end

	def remove_member(agent)
		@members.delete(@members.detect{|x| x.agent == agent})
	end

	def add_queuetime(duration, time = Time.now.to_f)
		@queuetimes[time] = duration
	end

	def trim_queuetimes
		now = Time.now
		# Grab start(00:00) of current year,month,day and store as midnight
		midnight = Time.mktime(now.year, now.month, now.day).to_i
		# Replace queuetimes hash with a new one containing queue times since
		# midnight of today.
		@queuetimes = Hash[*@queuetimes.select{|x, y| x > midnight}.flatten]
	end

	def avg_queuetime
		trim_queuetimes
		return 0 if @queuetimes.empty?
		(@queuetimes.values.inject{|sum, n| sum + n})/@queuetimes.length
	end

	def max_queuetime
		trim_queuetimes
		return 0 if @queuetimes.empty?
		@queuetimes.values.max
	end

	def bootstrap_queue_times
		now = Time.now
		midnight = Time.mktime(now.year, now.month, now.day).to_i
		@queuetimes = CDR.queuetimes_since(midnight, @name)
	end

	def bootstrap_queue_calls
		now = Time.now
		midnight = Time.mktime(now.year, now.month, now.day).to_i
		@completed = CDR.calls_since(midnight, "AND LastQueue = '#{@name}' AND ( LastState = #{CDR::ENDCALL} OR LastState = #{CDR::ENDWRAPUP})")
		@abandoned = CDR.calls_since(midnight, "AND LastQueue = '#{@name}' AND LastState = #{CDR::ABANDONQUEUE}")
	end

	def add_abandoned
		@abandoned << Time.now.to_i
	end

	def add_completed
		@completed << Time.now.to_i
	end

	def trim_calls(list)
		now = Time.now
		midnight = Time.mktime(now.year, now.month, now.day).to_i
		list.select{|x| x > midnight}
	end

	def abandoned
		@abandoned = trim_calls(@abandoned)
		@abandoned.length
	end

	def completed
		@completed = trim_calls(@completed)
		@completed.length
	end
end

class Caller
	@callers ||= []

	def self.new(*args)
		val = super
		@callers << val
		val
	end

	def self.callers
		@callers
	end

	def self.remove(call)
		@callers.delete(call)
	end

	def self.[](uid)
		return nil if uid.nil?
		# NOTE - fix this in 2038!
		if uid.match(/^[0-9]{10}\.[0-9]+$/)
			@callers.detect{|c| c.uniqueid == uid}
		else
			@callers.detect{|c| c.channel == uid}
		end
	end

	attr_accessor :enteredqueue, :ringingto, :sip_callid, :sip_fromdomain, :unlinkedby
	attr_reader :uniqueid, :channel, :vars, :cdr, :type, :origuniqueid, :uniqueids, :hangupreason

	def initialize(uniqueid, channel, defercdr=false)
		@uniqueid = uniqueid
		@origuniqueid = uniqueid
		@uniqueids = [uniqueid]
		@channel = channel
		@vars = {}
		@type = :call
		@cdr = CDR.new(self, defercdr)
		@enteredqueue = 0
		@ringingto = nil
		@hungup = false
		@pickedup = false
		@transfered = false
		@abandoned = false
		@parked = false
		@unlinkedby = :unknown
	end

	def to_s
		"#{@type.to_s.capitalize}(#{@origuniqueid})"
	end

	def uniqueid=(id)
		@uniqueid = id
		@uniqueids << id
		if @cdr.defercdr?
			@cdr.uniqueid = id
		end
	end

	def type=(type)
		@type = type
		# If this is a call being recovered from the DB, we might not
		# have a CDR object yet. Even if we did we don't want to change
		# the type on it.
		@cdr.type = type if @cdr
	end

	def set(var, value)
		@vars[var] = value
		case var
		when 'BRANDID'
			tenant, brand = CDR.parse_brandid(value)
			@cdr.set_info(:TenantID, tenant)
			@cdr.set_info(:BrandID, brand)
		when 'MAILID'
			@cdr.set_info(:EmailID, value)
		when 'VMID'
			@cdr.set_info(:VoicemailID, value)
		when 'CALLERIDNUM'
			@cdr.set_info(:CallerIDNum, value)
		when 'CALLERIDNAME'
			@cdr.set_info(:CallerIDName, value)
		when 'DNIS'
			@cdr.set_info(:DNIS, value)
		when 'CASEID'
			@cdr.set_info(:CaseID, value)
		end
	end

	def get(var)
		case var
		when 'CALLERIDNUM', 'CALLERIDNAME'
			if @vars[var].nil?
				return ''
			else
				return @vars[var]
			end
		else
			return @vars[var]
		end
	end

	def hangup
		@hungup = true
		self.class.remove(self)
	end

	def hungup?
		@hungup
	end

	def pickup
		@pickedup = true
	end

	def pickedup?
		@pickedup
	end

	def transfered=(arg)
		@transfered = arg ? true : false
	end

	def transfered?
		@transfered
	end

	def abandon
		@abandoned = true
	end

	def abandon=(arg)
		@abandoned = arg ? true : false
	end

	def abandoned?
		@abandoned
	end

	def park
		@parked = true
	end

	def unpark
		@parked = false
	end

	def parked?
		@parked
	end

	def hangupreason=(reason)
		@hangupreason = reason
	end
end

#wrapper for agent objects
class QueueMember
	attr_reader :agent
	attr_accessor :callstaken, :lastcall, :state
	def initialize(agent)
		@agent = agent
	end
end

