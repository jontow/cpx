
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

module ClientEventFactory
	@events ||= {}

	def self.add_event(event, klass)
		@events[event] = klass
	end

	def self.eventfactory(agent, eventserver, event)
		type, counter, data = event.strip.split(' ', 3)
		unless counter
			raise ClientEvent::IllegalEventError,
				"Invalid event; missing counter", caller
		end
		begin
			counter = Integer(counter)
		rescue ArgumentError
			raise ClientEvent::IllegalEventError,
				"Invalid event; non-integer or missing counter. Found: #{counter}", caller
		end
		if type == 'ACK' or type == 'ERR'
			ClientReply.new(agent, eventserver, counter, data)

		elsif klass = @events[type]
			if klass.seclevel > agent.seclevel
				raise ClientEvent::InsufficientPermissionError, 
					"You lack the permissions to send a #{type} event", caller
			end
			klass.new(agent, eventserver, counter, data)
		else
			raise ClientEvent::UnknownEventError,
				"Unknown event #{type}", caller
		end
	end
end

class ClientReply
	attr_reader :agent, :eventserver, :counter, :reply, :data
	def initialize(agent, eventserver, counter, data)
		@agent = agent
		@eventserver = eventserver
		@counter = counter
		@data = data
	end

	def dispatch
		@agent.mark_replied(@counter, self)
	end

	def ack?
		false
	end
end

class ClientEvent

	class IllegalEventError < StandardError; end
	class InvalidEventError < StandardError; end
	class InsufficientPermissionError < StandardError; end
	class UnknownEventError < StandardError; end
	class InvalidEventSpecificationError < StandardError; end

	@seclevel = 0

	def self.seclevel
		if defined? @seclevel
			@seclevel
		else
			superclass.seclevel
		end
	end

	def self.inherited(klass)
		if md = /^([A-Za-z]+)ClientEvent/.match(klass.name)
			ClientEventFactory.add_event(md[1].upcase, klass)
		else
			raise InvalidEventSpecificationError,
				"The class #{klass.name} is not named according to the <EventName>ClientEvent style", caller
		end
	end

	attr_reader :agent, :counter, :data, :seclevel, :retdata
	def initialize(agent, eventserver, counter, data)
		@agent = agent
		@eventserver = eventserver
		@counter = counter
		@data = data
		@retdata = data
	end

	def ack?
		true
	end

	def dispatch
		nil
	end
end

# seclevel 0 events
class StateClientEvent < ClientEvent
	def dispatch
		if !@data or @data.empty?
			raise InvalidEventError, "STATE must be passed a state to change to", caller
		end
		newstate, data = @data.split(' ', 2)
		# special case for the RELEASE state
		if ( [Agent::PRECALL, Agent::RELEASED].include?(newstate.to_i) and [Agent::IDLE, Agent::RINGING].include?(@agent.state) ) or 
		   ( newstate.to_i == Agent::IDLE and [Agent::PRECALL, Agent::RELEASED].include?(@agent.state) )

			# There are only two possibilities here, (precall | released)->idle or idle-->(precall | released)
			# once we've made it inside this clause, this just simplifies the code
			# ever so slightly.
			[Agent::PRECALL, Agent::RELEASED].include?(newstate.to_i) ? paused = true : paused = false
			begin
			@agent.pause(paused) { @agent.set_state(newstate.to_i, newstate.to_i == Agent::RELEASED ? data.to_i : nil) }
			rescue Agent::InvalidStateError => e 
			  raise InvalidEventError, "Invalid state: #{e.message}", caller
			rescue Agent::InvalidStateChangeError => e
			  raise InvalidEventError, "Invalid state change: #{e.message}", caller
			end
			return
		end

		begin
			@agent.set_state(newstate.to_i, newstate.to_i == Agent::RELEASED ? data.to_i : nil)
		rescue Agent::InvalidStateError => e 
			raise InvalidEventError, "Invalid state: #{e.message}", caller
		rescue Agent::InvalidStateChangeError => e
			raise InvalidEventError, "Invalid state change: #{e.message}", caller
		end
	end
end

class TransferClientEvent < ClientEvent
	def dispatch
		if @agent.talkingto
			if @data.nil? or @data.strip.empty?
				raise InvalidEventError, "Missing type of transfer / extension", caller
			end

			xfertype, extension = @data.strip.split(' ', 2)

			case xfertype
			when 'agent'
				if extension.nil?
					raise InvalidEventError, "Missing extension to transfer to", caller
				else
					if xferagent = (Agent[extension] || Agent[extension.to_i]) # allow agent name or ID
						raise InvalidEventError,
							"Agent #{xferagent.name} is currently being asked to accept a transfer", caller if xferagent.askxfer
						if [Agent::IDLE, Agent::RELEASED].include? xferagent.state
							xferagent.askxfer = true
							xferagent.send_event('ASK', 'yesno', "Do you want to accept a transfer from #{@agent.name}?") do |reply|
								xferagent.askxfer = false

								# ensure the agent is still in call with someone
								if @agent.talkingto.nil? or @agent.talkingto.hungup?
									Cpxlog.warning "Caller hungup before #{@agent} to #{xferagent} transfer could succeed"
									throw :return
								end

								if reply.data == 'yes'
									# TODO - add waiting for transfer state, this is a hack
									xferagent.pause(false) { xferagent.state = Agent::IDLE } if xferagent.state == Agent::RELEASED
									@eventserver.send_event('SetVar', 'Variable'=>'DESTAGENT', 'Value'=>"Agent/#{xferagent.id}", 'Channel'=>@agent.talkingto.channel) do |reply|
										if reply.success?
											@eventserver.send_event("Redirect", {'Channel'=>@agent.talkingto.channel, 'Exten'=>'s', 'Context'=>'trycalling', 'Priority'=>1}) do |reply|
												if reply.success?
													cid = URI.escape(@agent.talkingto.get('CALLERIDNAME') + " <" + @agent.talkingto.get('CALLERIDNUM') + ">")
													xferagent.send_event('CALLINFO', @agent.talkingto.get('BRANDID'), @agent.talkingto.type, cid)
													tenantid = @agent.talkingto.get('BRANDID')[0..3].to_i.to_s
													brandid = @agent.talkingto.get('BRANDID')[4..-1].to_i.to_s
													crmpopurl = ServerConfig::CRMPOPURL.sub('_TENANTID_', tenantid).
														sub('_BRANDID_', brandid).
														sub('_CALLID_', @agent.talkingto.origuniqueid).
														sub('_ITXT_', @agent.talkingto.get('IVROPT').to_s)

													crmpopurl << "&case=#{agent.talkingto.get('CASEID')}"if agent.talkingto.get('CASEID')
													crmpopurl << "&ani=#{agent.talkingto.get('CALLERIDNUM')}"if agent.talkingto.get('CALLERIDNUM')

													xferagent.send_event('URL', crmpopurl)

													@agent.lastpop = nil

													@agent.talkingto.cdr.add_transaction(CDR::TRANSFER, data)
													@agent.pause(true) do
														@agent.talkingto.cdr.add_transaction(CDR::INWRAPUP, @agent.id)
														begin
														  @agent.state = Agent::WRAPUP
														rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
														  Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{@agent.name}"
														  Server.instance.terminate_agent(@agent)
														end
													end
												else
													raise InvalidEventError, "Failed to transfer: #{reply.message}", caller
												end
											end
										else
											raise InvalidEventError, "Failed to set DESTAGENT to Agent/#{xferagent.id}", caller
										end
									end
								else
									@agent.send_event('BLAB', "Agent #{xferagent.name} declined your transfer")
								end
							end
						else
							raise InvalidEventError,
								"Agent #{xferagent.name} is not available to accept a transfer (in state #{Agent::STATENAMES[xferagent.state]})", caller
						end
					else
						raise InvalidEventError, "No Agent with ID #{extension} found", caller
					end
				end
			when 'queue'
				if queue = CallQueue[extension]
					Cpxlog.info "Agent #{@agent} is transferring #{@agent.talkingto} to queue #{queue}"
					cdr = @agent.talkingto.cdr
					@eventserver.send_event('SetVar', 'Variable'=>'QUEUE', 'Value'=>extension, 'Channel'=>@agent.talkingto.channel) do |reply|
						if reply.success?
							@agent.pause(true) do # TODO - why are we doing this?!
							    begin
							  	  @agent.state = Agent::WRAPUP
							    rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
								  Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{@agent.name}"
								  Server.instance.terminate_agent(@agent)
							    end
								@agent.lastpop = nil

								@eventserver.send_event('Redirect', 'Channel'=>@agent.talkingto.channel, 'Exten'=>'qinject',
									'Context'=>'queue-inject', 'Priority'=>1) do |reply|
									if reply.success?
										cdr.add_transaction(CDR::INWRAPUP, @agent.id)
										cdr.add_transaction(CDR::TRANSFER, "queue #{extension}")
									end
								end
							end
						else
							Cpxlog.warning "Failed to SetVar QUEUE to #{extension} for channel #{@agent.talkingto.channel} with error: #{reply.message}"
						end
					end
				else
					raise InvalidEventError, "Invalid Queue #{extension}, will not attempt transfer", caller
				end
			when 'blind'
				Cpxlog.info "Agent #{@agent} is blind transferring #{@agent.talkingto} to #{extension}"
				@eventserver.send_event('Redirect', 'Channel'=>@agent.talkingto.channel, 'Context'=>'blindxfer',
					'Exten'=>extension, 'Priority'=>1) do |reply|
					if reply.success?
						cdr = @agent.talkingto.cdr
						cdr.add_transaction(CDR::INWRAPUP, @agent.id)
						cdr.add_transaction(CDR::TRANSFER, "blind #{extension}")
						begin
						  @agent.state = Agent::WRAPUP
						rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
						  Cpxlog.warning "#{e.class.name}: #{e.message}, terminating agent #{@agent.name}"
						  Server.instance.terminate_agent(@agent)
						end
					else
						Cpxlog.warning "Failed trying to blind transfer #{@agent.talkingto} to #{extension} with error: #{reply.message}"
					end
				end
			else
				raise InvalidEventError, "Invalid transfer type", caller
			end
		else
			raise InvalidEventError, "Agent is not in call", caller
		end
	end
end

class WarmXferClientEvent < ClientEvent
	def dispatch
		if @data.nil? or @data.strip.empty?
			raise InvalidEventError,
				"WARMXFER must be passed a valid action and optional parameter", caller
		else
			xferaction, param = @data.strip.split(' ', 2)
			case xferaction
			when 'START'
				#puts "WARMXFER START hit: #{param}"
				# store agent.talkingto in agent.parkedcalls array
				# park agent.talkingto
				# dial #{param} outbound
				
				unless @agent.talkingto
					raise InvalidEventError,
						"Must be in call to start a warm transfer", caller
				end

				if @agent.warmxferchan
					raise InvalidEventError,
						"You can't start a warm transfer when you're already in one", caller
				end

				unless [:call, :outgoing].include? @agent.talkingto.type
					raise InvalidEventError,
						"Cannot warm transfer a #{@agent.talkingto.type}", caller
				end

				if param.empty?
					raise InvalidEventError,
						"WARMXFER START must be passed a number to dial", caller
				end
	
				unless param =~ /^[0-9]+$/
					raise InvalidEventError, "Number to dial must be fully numeric", caller
				end

				@agent.warmxferlinked = false

				#unless ([4, 7, 10, 11, 14].include? param.length or param[0..2] == '011') and param =~ /^[0-9]+$/
					#raise InvalidEventError,
						#"Invalid number to dial, must be 4, 7, 10, 11 or 14 digits long or begin with 011 and be fully numeric", caller
				#end

				@agent.pause(true) do
					begin
					  @agent.state = Agent::WARMXFER
					rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
					  Cpxlog.warning "#{e.class.name}: #{e.message}, terminating agent #{@agent.name}"
					  Server.instance.terminate_agent(@agent)
					end
					@agent.talkingto.cdr.add_transaction(CDR::WARMXFER, param)

					@agent.warmxfernum = param

					if @agent.talkingto.parked?
						# don't double park the call, just originate a new call to the 3rd party
						dial_3rd_party
					else
						@eventserver.send_event("Park", {'Channel' => @agent.talkingto.channel, 'Channel2' => @agent.talkingto.channel }) do |reply|
							if reply.success?
								dial_3rd_party
							else
								# TODO
							end
						end
					end
				end
			when 'COMPLETE'
				# store agent.talkingto in agent.parkedcalls array
				# park agent.talkingto
				# pop first two calls off agent.parkedcalls array, bridge them.
				# enter wrapup
				
				unless @agent.warmxferchan and @agent.talkingto and !@agent.talkingto.hungup?
					raise InvalidEventError,
						"Must be in call and have a 3rd party on the line to warmxfer", caller
				end

				unless @agent.warmxferlinked
					raise InvalidEventError,
						"You cannot complete a warm transfer until you've connected to the 3rd party", caller
				end

				if @agent.completing_transfer
					raise InvalidEventError,
						"You are already trying to complete a warm transfer", caller
				end

				@agent.completing_transfer = true # flag to prevent multiple attempts
				
				@eventserver.send_event('Bridge', {'Channel1' => @agent.talkingto.channel, 'Channel2' => @agent.warmxferchan}) do |reply|
					@agent.completing_transfer = false # clear this flag ASAP
					if reply.success?
						Cpxlog.info("Bridged 2 pieces of warm transfer call")
		
						# Ensure we don't record the caller<->third party part of the call
						@eventserver.send_event('StopMixMonitor', 'Channel'=>@agent.talkingto.channel)
						@eventserver.send_event('StopMixMonitor', 'Channel'=>@agent.warmxferchan)

						@agent.warmxferchan, @agent.warmxferid = nil
						@agent.talkingto.cdr.add_transaction(CDR::WARMXFERCOMPLETE, @agent.id)
						@agent.talkingto.cdr.add_transaction(CDR::INWRAPUP, @agent.id)
						@agent.talkingto.cdr.add_transaction(CDR::WARMXFERLEG)
						begin
						  @agent.state = Agent::WRAPUP
						rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
						  Cpxlog.warning "#{e.class.name}: #{e.message}, terminating agent #{@agent.name}"
						  Server.instance.terminate_agent(@agent)
						end
						Cpxlog.info "Set cdr state WARMXFERLEG"
					else
						if reply.message.split(' ', 2)[0] == 'Channel2'
							#puts 'Caller hung up before being bridged to 3rd party'
							@agent.warmxferchan, @agent.warmxferid = nil
							@agent.talkingto.cdr.add_transaction(CDR::WARMXFERFAILED, "3rd party hungup in bridge stage")
							@agent.send_event('ASK', 'yesno', '3rd party was disconnected before they were connected to the caller, would you like to be reconnected with the caller?') do |reply|
								if reply.data == 'yes'
									#puts 'reconnecting to caller'
									@eventserver.send_event("Redirect", 'Channel'=>@agent.talkingto.channel,
													'Exten'=>@agent.id, 'Context'=>'sip-agents', 'Priority'=>1)
								else
									@agent.send_event('WARMXFERFAILED')
									#puts 'not reconnecting to caller'
								end
							end
						elsif reply.message.split(' ', 2)[0] == 'Channel1'
							@agent.talkingto.hangup # TODO - is this right
							@agent.talkingto.cdr.add_transaction(CDR::WARMXFERFAILED, "Caller hungup in bridge stage")
							@agent.send_event('ASK', 'ok', 'Caller was disconnected before they were connected to the 3rd party, reconnecting you to the 3rd party') do |reply|
								@eventserver.send_event("Redirect", 'Channel'=>@agent.warmxferchan,
												'Exten'=>@agent.id, 'Context'=>'sip-agents', 'Priority'=>1)
								#puts 'reconnecting to 3rd party'
							end
						else
							Cpxlog.warning "Failed to bridge #{@agent.talkingto.channel} and #{@agent.warmxferchan} with error: #{reply.message}"
						end
					end
				end
				

			when 'CANCEL'
				#puts 'WARMXFER CANCEL hit'
				# check to see if agent.parkedcalls has > 1 entry
				#   if so: ?
				#   else:
				#     unpark call, bridge back to agent
				# somehow note this in the CDR and continue on as if this were
				# a regular live inbound call for the moment.
				#
				# NOTE: keep in mind there could be more than one WARMXFER attempt
				#   in this particular CDR.  If it fails, agent cancels, explains to
				#   the user the new scenario and tries again to $next-3rd-party.

				@agent.talkingto.cdr.add_transaction(CDR::WARMXFERFAILED, "Agent cancel")
				if @agent.warmxferchan
					@agent.send_event('ASK', 'yesno', 'Call to 3rd party still up, do you want to terminate it?') do |reply|
						if reply.data == 'yes'
							@eventserver.send_event('Hangup', 'Channel'=>@agent.warmxferchan) do |reply|
								unless reply.success?
									Cpxlog.warning "Failed to hang up channel #{@agent.warmxferchan}: #{reply.message}"
								end
							end
						end
					end
				else
					@eventserver.send_event("Redirect", {'Channel'=>@agent.talkingto.channel, 'Exten'=>@agent.id, 'Context'=>'sip-agents', 'Priority'=>1}) do |reply|
						if reply.success?
							Cpxlog.info "Warm transfer of #{@agent.talkingto} sucessfully cancelled by #{@agent}"
						else
							Cpxlog.warning "Agent #{@agent} failed to cancel transfer of #{@agent.talkingto}: #{reply.message}"
						end
					end
				end
			else
				raise InvalidEventError,
					"WARMXFER must be passed a valid action and optional parameter", caller
			end
		end
	end

	def dial_3rd_party
		tenant, brand = CDR.parse_brandid(@agent.talkingto.get('BRANDID'))
		result = @eventserver.dbh.safe_query("SELECT callerid FROM brandlist WHERE brand=#{brand} AND tenant=#{tenant} AND callerid IS NOT NULL")
		if result.num_rows == 1
			callerid = result.fetch_row[0]
		else
			callerid = ServerConfig::DEFAULTCALLERID
		end
		result.free

		parameters = {
			"Channel"=>"Agent/#{@agent.id}",
			'Context'=>'outbound',
			'Exten'=>@agent.warmxfernum,
			'Priority'=>1,
			'Async' => 1,
			'Variable' => 'CALLTYPE=warmxfer',
			'CallerID' => callerid
		}

		Cpxlog.info("#{@agent} dialing #{@agent.warmxfernum} for warm xfer")
		Thread.new do
			sleep(8)
			@eventserver.send_event("Originate", parameters) do |reply|
				if reply.success?
					Cpxlog.info "#{@agent} dialed #{@agent.warmxfernum} for warm xfer"
					# TODO ???
				else
					Cpxlog.info("Agent #{@agent}'s attempt to dial 3rd party at #{@agent.warmxfernum} failed with error: #{e.message}")
					@agent.warmxfernum = nil
					# TODO - bridge the parked call back to the agent
				end
			end
		end
	end
end


class ParkClientEvent < ClientEvent
	def dispatch
		if @agent.talkingto
			@eventserver.send_event("Park", {'Channel' => @agent.talkingto.channel, 'Channel2' => @agent.talkingto.channel }) do |reply|
				if reply.success?
					
				else
					raise InvalidEventError, "Failed to park: #{reply.message}"
				end
			end
		else
			raise InvalidEventError, "Agent is not in call", caller
		end
	end
end

class DialClientEvent < ClientEvent
	def dispatch
		if @agent.state != Agent::IDLE and @agent.state != Agent::RELEASED and @agent.state != Agent::PRECALL
			raise InvalidEventError,
				"Agent must be idle or released when attempting to dial", caller
		else
			if @data.nil? or @data.strip.empty?
				raise InvalidEventError,
					"Must pass context, extension, priority", caller
			else
				clientid, context, extension, priority, channelvars = data.split(' ', 5)
				unless clientid and context and extension and priority
					raise InvalidEventError,
						"Must pass clientid, context, extension, priority", caller
				end

				unless extension =~ /^[0-9]+$/
					raise InvalidEventError, "Number to dial must be fully numeric", caller if @agent.seclevel < 2
				end
				#unless [4, 7, 10, 11, 14].include? extension.length and extension =~ /^[0-9]+$/
					#raise InvalidEventError,
						#"Invalid number to dial, must be 4, 7, 10, 11 or 14 digits long and fully numeric", caller if @agent.seclevel < 2
				#end
				
				tenant, brand = CDR.parse_brandid(clientid)
				result = @eventserver.dbh.safe_query("SELECT callerid FROM brandlist WHERE brand=#{brand} AND tenant=#{tenant} AND callerid IS NOT NULL")
				if result.num_rows == 1
					callerid = result.fetch_row[0]
				else
					callerid = ServerConfig::DEFAULTCALLERID
				end
				result.free

				parameters = {
					"Channel"=>"Agent/#{@agent.id}",
					'Context'=>context,
					'Exten'=>extension,
					'Priority'=>priority,
					'CallerID'=>callerid,
					'Async' => 1
				}

				parameters['Variable'] = channelvars.split(' ').join("\r\nVariable: ") if channelvars

				@eventserver.send_event("Originate", parameters) do |reply|

					if reply.success?
						@agent.dialoutnum = extension
						@agent.dialedclient = clientid
					else
						raise InvalidEventError,
							"Originate failed: #{reply.message}", caller
					end
				end
			end
		end
	end
end

class EndWrapupClientEvent < ClientEvent
	def dispatch
		if @agent.state != Agent::WRAPUP
			raise InvalidEventError, "Agent must be in wrapup to send an ENDWRAPUP",caller
		else
			@agent.dialoutid = nil
			@agent.dialoutnum = nil
			@agent.dialedclient = nil

			if @agent.talkingto
				@agent.talkingto.cdr.add_transaction(CDR::ENDWRAPUP, @agent.id)
				# if the other end never pickedup, or if the person we were talking to hungup
				@agent.talkingto.cdr.add_transaction(CDR::CDREND) if !@agent.talkingto.pickedup? or @agent.talkingto.hungup?
				@agent.talkingto = nil

				if @agent.queuerelease
					@agent.send(:stchange, Agent::RELEASED, @agent.queuereleasedata)
					@agent.queuerelease = false
					@agent.queuereleasedata = nil
				else
					@agent.pause(false) do
						begin
						  @agent.state = Agent::IDLE
						rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
						  Cpxlog.warning "#{e.class.name}: #{e.message}, terminating agent #{@agent.name}"
						  Server.instance.terminate_agent(@agent)
						end
					end
				end

			end
		end
	end
end

class BrandListClientEvent < ClientEvent
	def dispatch
		brands = []
		result = @eventserver.dbh.safe_query("SELECT * FROM profile_brand WHERE profile=#{@agent.profile}")
		limitquery = ""
		limitquery = "INNER JOIN profile_brand p on b.tenant = p.tenant and b.brand = p.brand WHERE p.profile = #{@agent.profile}" if result.num_rows > 0

		result = @eventserver.dbh.safe_query("SELECT b.tenant as tenant, b.brand as brand, b.brandname as brandname FROM brandlist b #{limitquery}")
		result.each do |x|
			brands << x[0].to_s.rjust(4, '0') + x[1].to_s.rjust(4, '0') + '|' + x[2]
		end
		@retdata = '(' + brands.join('),(') + ')' 
	end
end

class ProfilesClientEvent < ClientEvent
	def dispatch
		profiles = []
		result = @eventserver.dbh.safe_query("SELECT profile, name FROM profiles order by (sortorder IS NULL), sortorder, name")
		result.each do |x|
			profiles << x
		end

		Agent.profiles= Hash[*profiles.map{|x| [x[0].to_i, x[1]]}.flatten]

		@retdata = profiles.map{|x| "#{x[0]}:#{x[1]}"}.join(' ')
	end
end

class QueueNamesClientEvent < ClientEvent
	def dispatch
		result = @eventserver.dbh.safe_query("SELECT * FROM profile_brand WHERE profile=#{@agent.profile}")
		filter=""
		if result.num_rows == 0
		  filter="ALL"
		else
		  filter=[]
		  result = @eventserver.dbh.safe_query("SELECT queue FROM profile_lookup WHERE profile=#{@agent.profile}")
		  result.each_hash { |q| filter << q['queue'] }
		end
		queues = []
		CallQueue.match_queues(filter).each do |queue|
			queues << "#{queue.name}|#{queue.commonname}"
		end

		@retdata = '(' + queues.join('),(') + ')'
	end
end

class QueueGroupsClientEvent < ClientEvent
	def dispatch
		profiles = []
		result = @eventserver.dbh.safe_query("SELECT * FROM queuegroups")
		result.each do |x|
			profiles << x
		end

		@retdata = profiles.map{|x| "#{x[0]}:#{x[1]}"}.join(' ')
	end
end

class PingClientEvent < ClientEvent
	def dispatch
		@retdata = Time.now.to_i
	end
end

class AvailAgentsClientEvent < ClientEvent
	def dispatch
		agents = []
		Agent.match_agents(:hidden => false, :agent=>@agent) do |a|
			next if a == @agent
			agents << [a.name, a.state, a.profile] if [Agent::IDLE, Agent::RELEASED].include? a.state
		end

		@retdata = agents.map{|x| x.join(':')}.join(' ')
	end
end

class UptimeClientEvent < ClientEvent
	def dispatch
		@retdata = "#{Server.instance.startuptime.to_i} #{@eventserver.startuptime.to_i}"
	end
end

class HangupClientEvent < ClientEvent
	def dispatch
		raise InvalidEventError, "You must be in-call to issue a HANGUP command", caller unless @agent.talkingto
		# TODO - check for 3rd party leg and other stuff...
		@agent.send_event('ASK', 'yesno', "Are you sure you want to hangup on this #{@agent.talkingto.type} from #{@agent.talkingto.get('CALLERIDNUM')}?") do |reply|
			if reply.data == 'yes'
				@eventserver.send_event('Hangup', 'Channel'=>"Agent/#{@agent.id}") do |reply|
					if reply.success?
						Cpxlog.notice "Agent #{@agent} forced a hangup for #{@agent.talkingto}"
					else
						Cpxlog.warning "Agent #{@agent} failed to force a hangup on #{@agent.talkingto} on channel #{@agent.talkingto.channel}"
						call.hangupreason = nil # clear this, just in case
					end
				end
			end
		end
	end
end

class ReleaseOptionsClientEvent < ClientEvent
	def dispatch
		@retdata = Agent.releaseoptions.map{|x| "#{x[:id]}:#{x[:name]}:#{x[:utilbias]}"}.join(',')
	end
end

# seclevel 4 events
class AgentsClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		begin
			@agent.class.match_agents(:ids=>@data, :hidden=>false, :agent=>agent) do |a|
				@agent.send_event('STATE', a.statetime, a.id, a.name, a.state, a.state_data, a.profile, *a.stats)
			end
		rescue ArgumentError => e
			raise InvalidEventError,
				"Invalid agent list: #{e.message}", caller
		end
	end
end

class AStateClientEvent < ClientEvent

	# This event allows you to administratively set agent state. In theory you can
	# set it to whatever you want but in practice you're limited to the following
	# by the invalid state change guards:
	#  Idle -> Released
	#  Released -> Idle
	#  Idle/Released -> Ringing
	
	# To administratively end wrapup, see AENDWRAPUP
	
	@seclevel = 2

	def dispatch

		if @data.nil? or @data.strip.empty?
			raise InvalidEventError,
				"ASTATE must be passed an agent list and a state", caller
		else
			agents, state = @data.strip.split(' ', 2)
			if state.nil?
				raise InvalidEventError,
					"ASTATE must be passed an agent list and a state", caller
			end
		end

		failed = []

		begin
			@agent.class.match_agents(:ids=>agents) do |agent|
				# this block was commented originally, minus the ASTATE send_event?
				begin
					#puts 'attempt #1'
					agent.state = state
				rescue Agent::InvalidStateError => e
					raise InvalidEventError,
						"Invalid state requested", caller
					#@agent.send_event('BLAB', e.message)

				rescue Agent::InvalidStateChangeError => e
					# okay, so this went wrong. Let's try to toggle the pause state and
					# see if that makes it work. We only do this if they're not in call.
					unless agent.talkingto
						agent.pause(!agent.paused?) do
							begin
								#puts 'attempt #2'
								agent.state = state
							rescue Agent::InvalidStateChangeError => e
								# undo the pause toggle
								agent.pause(!agent.paused?)
								#@agent.send_event('BLAB', e.message)
								failed << agent.name
							else
								agent.send_event('ASTATE', state)
							end
						end
					else
						failed << agent.name
						#@agent.send_event('BLAB', e.message)
					end
				else
					agent.send_event('ASTATE', state)
				end
			end
		rescue ArgumentError => e
			raise InvalidEventError,
				"Invalid agent list: #{e.message}", caller
		end

		unless failed.empty?
			raise InvalidEventError,
				"Failed to change state for agents: #{failed.join(' ')}", caller
		end
	end
end

class AEndWrapupClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		if @data.nil? or @data.strip.empty?
			raise InvalidEventError,
				"AENDWRAPUP must be passed an agent list", caller
		end

		failed = []

		@agent.class.match_agents(:ids=>@data) do |agent|
			if agent.state != Agent::WRAPUP
				failed << agent.name
				#@agent.send_event('BLAB', "Agent #{agent.name} is not in Wrapup")
			else
				if agent.queuerelease
					# this probably isn't appropiate to honor
					#@agent.send(:stchange, Agent::RELEASED)
					agent.queuerelease = false
				else
				  agent.pause(false) do
					begin
					  agent.state = Agent::IDLE
					rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
					  Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{agent.name}"
					  Server.instance.terminate_agent(agent)
					end
				  end
				end

				agent.dialoutid = nil
				agent.dialoutnum = nil
				agent.dialedclient = nil

				if agent.talkingto
					agent.talkingto.cdr.add_transaction(CDR::ENDWRAPUP, agent.id)
					# if the other end never pickedup, or if the person we were talking to hungup
					agent.talkingto.cdr.add_transaction(CDR::CDREND) if !agent.talkingto.pickedup? or agent.talkingto.hungup?
					agent.talkingto = nil
				end
			end
		end
		
		unless failed.empty?
			raise InvalidEventError,
				"Failed to end wrapup for agents: #{failed.join(' ')}", caller
		end
	end
end

=begin DONOTUSE 
class ADialClientEvent < ClientEvent
	@seclevel = 4
	def dispatch
		if @data.nil? or @data.strip.empty?
			raise InvalidEventError,
				"ADIAL must be passed a brand, a number, a destination type (queue|agent) and a destination", caller
		end

		brandid, number, desttype, dest = @data.split(' ', 4)

		if dest.nil?
			raise InvalidEventError,
				"ADIAL must be passed a brand, a number, a destination type (queue|agent) and a destination", caller
		end
		
		tenant, brand = CDR.parse_brandid(brandid)

		if tenant.zero? and brand.zero?
			raise InvalidEventError,
				"Invalid Tenant/Brand ID: #{brandid}", caller
		end

		result = @eventserver.dbh.safe_query("SELECT callerid FROM brandlist WHERE brand=#{brand} AND tenant=#{tenant} AND callerid IS NOT NULL")

		if result.num_rows == 1
			callerid = result.fetch_row[0]
		else
			callerid = ServerConfig::DEFAULTCALLERID
		end

		parameters = {
			#"Channel"=>"Agent/#{@agent.id}",
			'Context'=>'outbound-acked',
			'Exten'=>number,
			'Priority'=>1,
			'Async' => 1,
			'Variable' => {'CALLTYPE' => 'outcall', 'BRANDID' => brandid}.map{|x| x.join('=')}.join("\r\nVariable: "),
			'CallerID' => callerid
		}

		case desttype
		when 'agent'
			matches = @agent.class.match_agents(:ids=>dest)
			if matches.empty?
				raise InvalidEventError,
					"No such agent #{dest} found", caller
			elsif matches.length > 1
				raise InvalidEventError,
					"Cannot originate call to multiple agents: #{matches.map{|x| x.name}.join(' ')}", caller
			end

			unless [Agent::IDLE, Agent::RELEASED].include? agent.state
				raise InvalidEventError,
					"Cannot originate a call to #{agent.name} because they are in #{Agent::STATENAMES[agent.state]} state", caller
			end

			agent = matches[0]

			parameters['Channel'] = "Agent/#{agent.id}"
			agent.dialedclient = brandid
			@eventserver.send_event('Originate', parameters) do |reply|
				if reply.success?
					agent.send_event('CALLINFO', agent.dialedclient, 'outcall', number)
				end
			end
				#agent.pause(false) do 
					#agent.state = Agent::RINGING
				#end
			#end

		when 'queue'
			unless queue = CallQueue[dest]
				raise InvalidEventError,
					"Cannot find queue #{dest}", caller
			end
			parameters['Channel'] = "Local/qinject@queue-inject"
			parameters['Variable'] = {'TYPE' => 'call', 'BRANDID' => brandid,
				'QUEUE' => dest}.map{|x| x.join('=')}.join("\r\nVariable: ")

			@eventserver.send_event('Originate', parameters)
		else
			raise InvalidEventError,
				'Destination type must be either queue or agent', caller
		end

	end
end
=end

class AProfileClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		if @data.nil?
			raise InvalidEventError,
				"Must be passed an agent list and a profile to assign them to", caller
		end

		agents, profile = @data.split(' ', 2)
		if profile.nil? or profile.empty?
			raise InvalidEventError,
					"Must be passed an agent list and a profile to assign them to", caller
		end

		begin
			profile = Integer(profile)
		rescue ArgumentError
			raise InvalidEventError,
				"Profile must be a integer, got #{profile}", caller
		end

		result = @eventserver.dbh.safe_query("SELECT profile FROM profiles WHERE profile=#{@eventserver.dbh.quote(profile)}")
		if result.num_rows == 1
			result.free

		else
			result.free
			raise InvalidEventError, "Invalid profile #{profile}", caller
		end
		
		begin
			Agent.match_agents(:ids=>agents) do |a|
				oprofile = a.profile
				washidden = a.hidden
				queuelist = a.set_profile(profile.to_i)
				return unless queuelist
				CallQueue.update_queue_membership(a, queuelist)
				queues = CallQueue.queues.select{|q| q.members.detect{|y| y.agent==a}}.map{|x| x.name}.join(':')
				if a.seclevel > 1 and a.hidden
					Agent.match_agents(:ids=>Agent.profiles[a.profile]) do |ag|
						# show this agent all the members of the hidden profile they just joined
						a.send_event('STATE', ag.statetime, ag.id, ag.name, ag.state, 'none', ag.profile, *ag.stats)
					end
				elsif a.seclevel > 1 and a.hidden != washidden
					Agent.match_agents(:ids=>Agent.profiles[oprofile]) do |ag|
						# make agents in the agent's old hidden profile disappear
						a.send_event('STATE', ag.statetime, ag.id, ag.name, Agent::OFFLINE, 'none', ag.profile, *ag.stats)
					end
				end

				Agent.match_agents(:seclevel=>2) do |ag|
					if a.hidden and a.profile != ag.profile
						# make this agent look offline to non profile members
						ag.send_event('STATE', a.statetime, a.id, a.name, Agent::OFFLINE, 'none', oprofile, *a.stats)
					else
						ag.send_event('STATE', a.statetime, a.id, a.name, a.state, a.state_data, a.profile, *a.stats)
						ag.send_event('QUEUEMEMBER', a.id, a.name, a.statetime, a.state, queues)
					end
				end
			end
			
		rescue ArgumentError => e
			raise InvalidEventError, 
				"Invalid agent list: #{e.message}", caller
		rescue Agent::InvalidProfileError
			raise InvalidEventError,
				"Invalid profile #{profile}", caller
		end
	end
end

class ABlabClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		if @data.nil? or @data.strip.empty?
			raise InvalidEventError,
				"Must be passed an agent list and a message", caller
		else
			agents, message = @data.strip.split(' ', 2)
			if message.nil?
				raise InvalidEventError,
					"Must pass a valid message to ABLAB", caller
			end
		end

		if agents == 'ALL'
			profiles = []
			result = @eventserver.dbh.safe_query("SELECT profile, name FROM profiles order by (sortorder IS NULL), sortorder, name")
			result.each_hash do |x|
				profiles << x['name']
			end

			if @agent.seclevel < 4
				raise InvalidEventError,
					"ABlab to 'ALL' not allowed, try one of #{profiles.join(',')}", caller
			end
		end
		
		begin
			Cpxlog.info "#{@agent} BLAB to #{agents}: #{message}"
			@agent.class.match_agents(:ids=>agents) do |a|
				a.send_event('BLAB', message)
			end
		rescue ArgumentError => e
			raise InvalidEventError,
				"Invalid agent list: #{e.message}", caller
		end
	end
end

class AURLClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		if @data.nil? or @data.strip.empty?
			raise InvalidEventError,
				"Must be passed an agent list and a URL", caller
		else
			agents, url = @data.strip.split(' ', 2)
			if url.nil?
				raise InvalidEventError,
					"Must pass a valid message to AURL", caller
			end
		end

		begin
			@agent.class.match_agents(:ids=>agents) do |a|
				a.send_event('URL',  url)
			end
		rescue ArgumentError => e
			raise InvalidEventError,
				"Invalid agent list: #{e.message}", caller
		end
	end
end

# Administrative transfer, redirect arbitrary channel to arbitrary location
class ATransferClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		raise InvalidEventError,
			"You must pass the type of transfer and the id of the call to ATRANSFER" if @data.nil? or @data.strip.empty?
		xfertype, callid, options = @data.split(' ', 3)
		raise InvalidEventError,
			"You must pass the type of transfer and the id of the call to ATRANSFER" if callid.nil?
		case xfertype
		when 'agent'
			if call = Caller[callid]
				begin
					agentid = Integer(options)
				rescue ArgumentError
					raise InvalidEventError, "Invalid Agent ID: #{options}", caller
				end
				
				if agent = Agent[agentid]
					Cpxlog.info "#{@agent} is trying to transfer #{call} to #{agent}"
					unless [Agent::IDLE, Agent::RELEASED].include? agent.state
						raise InvalidEventError, "#{agent} is not available to accept a transfer", caller
					end
					call.transfered = true
					@eventserver.send_event('SetVar', 'Variable'=>'DESTAGENT', 'Value'=>"Agent/#{agent.id}", 'Channel'=>call.channel) do |reply|
						if reply.success?
							@eventserver.send_event("Redirect", 'Channel'=>call.channel, 'Exten'=>'s', 'Context'=>'trycalling', 'Priority'=>1) do |reply|
								if reply.success?
									begin
										agent.state = Agent::RINGING
									rescue Agent::InvalidStateChangeError => e
										# TODO - put call back in queue?!
										Cpxlog.error "#{@agent}'s attempt to redirect #{call} to #{agent} failed due to an invalid state change: #{Agent::STATENAMES[agent.state]} => Ringing"
									else

										call.ringingto = agent.id
										cid = URI.escape(call.get('CALLERIDNAME') + " <" + call.get('CALLERIDNUM') + ">")
										Agent[call.ringingto].send_event('CALLINFO', call.get('BRANDID'), call.type, cid)

										tenantid = call.get('BRANDID')[0..3].to_i.to_s
										brandid = call.get('BRANDID')[4..-1].to_i.to_s
										crmpopurl = ServerConfig::CRMPOPURL.sub('_TENANTID_', tenantid).sub('_BRANDID_', brandid).sub('_CALLID_', call.origuniqueid).sub('_ITXT_', call.get('IVROPT').to_s)

										crmpopurl << "&case=#{call.get('CASEID')}"if call.get('CASEID')
										crmpopurl << "&ani=#{call.get('CALLERIDNUM')}"if call.get('CALLERIDNUM')

										agent.send_event('URL', crmpopurl)

										call.cdr.add_transaction(CDR::TRANSFER, "#{xfertype} #{options}")
									end
								else
									Cpxlog.warning "Failed to redirect #{call.channel} to #{agent} with error: #{reply.message}"
								end
							end
						end
					end
				else
					raise InvalidEventError, "Cannot find an agent of id #{options}", caller
				end

			else
				raise InvalidEventError,
					"Cannot find call with channel or uniqueid of #{callid}", caller
			end

		when 'queue'
			raise InvalidEventError, "Not supported yet", caller

		when 'voicemail'
			if call = Caller[callid]
				call.transfered = true
				@eventserver.send_event('Redirect', 'Channel'=>call.channel, 'Exten'=>'s', 'Context'=>'macro-voicemail', 'Priority'=>1) do |reply|
					if reply.success?
						Cpxlog.notice "#{@agent} redirected #{call} to voicemail"
						call.cdr.add_transaction(CDR::TRANSFER, "voicemail #{@agent}")
						# NOOP?
					else
						Cpxlog.warning "Failed to redirect #{call.channel} to voicemail with error: #{reply.message}"
					end
				end
			else
				raise InvalidEventError,
					"Cannot find call #{callid}", caller
			end

		else
			raise InvalidEventError, "Invalid transfer type: #{xfertype}", caller
		end
	end
end

class KickClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		begin
			list = @agent.class.match_agents(:ids=>@data, :iseclevel=>@agent.seclevel-1)
			raise InvalidEventError,
				"No eligible agents to kick matching filter: #{@data}", caller if list.empty?
			list.each do |a|
				a.logoff
			end
		rescue ArgumentError => e
			raise InvalidEventError,
				"Invalid agent list: #{e.message}", caller
		end
	end
end

class ReloadClientEvent < ClientEvent
	# this is a stub event to reload the code.
	@seclevel = 4
end

class QueuesClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		begin
			CallQueue.match_queues(@data) do |q|
				#@agent.send_event('QUEUE', q.name, q.queuegroup, q.weight, q.callers.length, q.completed, q.abandoned, q.avg_queuetime, q.max_queuetime)
				@agent.send_event('QUEUE', q.name, q.queuegroup, q.weight, 0, q.completed, q.abandoned, q.avg_queuetime, q.max_queuetime)
			end
		rescue ArgumentError => e
			raise InvalidEventError,
				"Invalid queue list: #{e.message}", caller
		end
	end
end

class QueueMembersClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		agents = {}
		begin
			CallQueue.match_queues(@data) do |q|
				q.members.each do |m|
					next if m.agent.state <= Agent::OFFLINE
					agents[m.agent] ||= []
					agents[m.agent] << q.name
				end
			end
			agents.each do |a, queues|
				@agent.send_event('QUEUEMEMBER', a.id, a.name, a.statetime, a.state, queues.join(':'))
			end
		rescue ArgumentError => e
			raise InvalidEventError,
				"Invalid queue list: #{e.message}", caller
		end
	end
end

class QueueCallersClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		begin
			CallQueue.match_queues(@data) do |q|
				q.callers.each do |c|
					if c
						cid = URI.escape(c.get('CALLERIDNAME') + " <" + c.get('CALLERIDNUM') + ">")
						@agent.send_event('QUEUECALLER', c.enteredqueue, q.name, c.uniqueid, c.type, q.callers.index(c), c.get('BRANDID'), cid)
					else
						Cpxlog.error "There's a null call in queue #{q}"
					end
				end
			end
		rescue ArgumentError => e
			raise InvalidEventError,
				"Invalid queue list: #{e.message}", caller
		end
	end
end

class BridgedCallersClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		begin
			@agent.class.match_agents(:ids=>@data) do |a|
				if a.talkingto
					@agent.send_event('BRIDGECALLER', a.id, a.talkingto.uniqueid, a.talkingto.get('BRANDID'), a.talkingto.get('CALLERID'))
				end
			end
		rescue ArgumentError => e
			raise InvalidEventError,
				"Invalid agent list: #{e.message}", caller
		end
	end
end

class ReloadQueuesClientEvent < ClientEvent
	@seclevel = 4
	def dispatch
		@eventserver.send_event('QueueStatus')
	end
end

# Return the info for a particular call
class CallInfoClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		if call = Caller[@data]
			extra_info = ''
			case call.type
			when :email
				extra_info = ServerConfig::EMAILURL.sub('_ID_', call.get('MAILID').to_s)
			when :voicemail
				extra_info = call.get('VMID')
			end

			#cid = URI.escape(call.get('CALLERIDNAME') + " <" + call.get('CALLERIDNUM') + ">")
			#@retdata = [call.get('BRANDID'), call.type, cid, extra_info.to_s].join(' ')
			@retdata = [call.get('BRANDID'), call.type, call.get('CALLERIDNUM'), extra_info.to_s].join(' ')

		else
			raise InvalidEventError,
				"Cannot find call #{@data}", caller
		end

	end
end

class StatusClientEvent < ClientEvent
	@seclevel = 4
	def dispatch
		@agent.send_event('INFO', 'Spewing system information, as requested')
		@agent.send_event('INFO', 'CALLID   TYPE   UNLINKEDBY   ISHUNGUP   AGENT   QUEUE   CDRSTATE   CDRDATA') unless Caller.callers.empty?
		Caller.callers.each do |call|
			ag = Agent.agents.detect{|x| x.talkingto == call}
			qu = CallQueue.queues.detect{|x| x.callers.include?(call)}
			unless tr = call.cdr.get_last_transaction(CDR::CDREND)
				tr = call.cdr.get_last_transaction
			end
			@agent.send_event('INFO', call.uniqueid, call.type, call.unlinkedby, call.hungup?, ag ? ag.name : 'nil', qu ? qu.name : 'nil', CDR::TRANSACTIONNAMES[tr[:transaction]], tr[:data].inspect)
		end
		# TODO - Agents
	end
end

class AHangupClientEvent < ClientEvent
	@seclevel = 2
	def dispatch
		raise InvalidEventError, 'Must pass a callid and a reason to AHANGUP', caller if @data.nil?
		callid, reason = @data.split(' ', 2)
		if call = Caller[callid]
			@agent.send_event('ASK', 'yesno', "Are you sure you want to hangup on this #{call.type} from #{call.get('CALLERIDNUM')}?") do |reply|
				if reply.data == 'yes'
					call.hangupreason = "by #{@agent} reason: #{reason}"
					@eventserver.send_event('Hangup', 'Channel'=>call.channel) do |reply|
						if reply.success?
							Cpxlog.notice "Agent #{@agent} forced a hangup for #{call} with reason: #{reason}"
						else
							Cpxlog.warning "Agent #{@agent} failed to force a hangup on #{call} on channel #{call.channel} with error: #{reply.message}"
							call.hangupreason = nil # clear this, just in case
						end
					end
				end
			end
		else
			raise InvalidEventError,
				"Cannot find call #{callid}", caller
		end
	end
end

=begin DONOTUSE
# Execute an event with a lower security level. Possibly useful for 
# the planned Web API.
class ExecSecClientEvent < ClientEvent
	@seclevel = 4

	class MasqAgent
		attr_reader :id, :name, :seclevel, :profile, :retdata
		def initialize(id, name, seclevel, profile, origagent)
			@id = id
			@name = name
			@seclevel = seclevel
			@profile = profile
			@origagent = origagent
			@retdata = []
		end

		def send_event(event, *args, &block)
			#@origagent.send_event(event, *args, &block)
			@retdata << ([event]+args).join(' ')
		end

		class << self
			def method_missing(symbol, *args, &block)
				Agent.__send__(symbol, *args, &block)
			end
		end
	end

	def dispatch
		seclevel, profile, eventtype, args  = @data.split(' ', 4)
		seclevel = seclevel.to_i
		if seclevel > @agent.seclevel
			raise InvalidEventError,
				"You cannot execute events at a higher security level then you yourself possess", caller
		end

		agent = MasqAgent.new(@agent.id, @agent.name+'-masq', seclevel.to_i, profile.to_i, @agent)
		begin
			event = ClientEventFactory.eventfactory(agent, @eventserver, [eventtype, @counter, args].join(' '))

		rescue InsufficientPermissionError, UnknownEventError, IllegalEventError => e
			raise InvalidEventError, e, caller
		else
			event.dispatch
			if agent.retdata.empty?
				@agent.send_ack(event.counter, event.retdata) if event.ack?
			else
				# TODO come up with a real way to seperate the events
				@agent.send_ack(event.counter, agent.retdata.join('~~')) if event.ack?
			end
		end
	end
end
=end
