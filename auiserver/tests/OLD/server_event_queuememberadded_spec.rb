
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../server_events'

load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'The QueueMemberAdded Event' do
	setup do
#		if Object.const_defined?(:Agent)
#			@tmp = Agent
#			Object.send(:remove_const, :Agent)
#		end
#		Agent = mock('AgentClass')
#		ClassBackup.replace_class_with_mock(:CallQueue, mock('CallQueueClass'))
#		ClassBackup.replace_class_with_mock(:Caller, mock('CallerClass'))
		ClassBackup.replace_class_with_mock(:Agent, mock('AgentClass'))

	end

	specify 'should initialize as the correct event type' do
		ServerEventFactory.eventfactory(nil, "Event: QueueMemberAdded\r\n").should.an_instance_of QueueMemberAddedServerEvent
	end

	specify 'should emit an event to all relevant agents' do
		string = <<EVENT
Event: QueueMemberAdded\r
Queue: testq\r
Location: SIP/6000\r
Membership = Static\r
Penalty: 0\r
Callstaken: 0\r
LastCall \r
Status: ???\r
Paused: 0\r
\r
EVENT
		agent = mock('Agent')
		agent.should_receive(:send_event).with("QUEUEMEMBER", 'testq', '6000')
		Agent.should_receive(:match_agents).with(:tier=>4, :online=>true).and_yield(agent)
		ServerEventFactory.eventfactory(nil, string).dispatch
	end

	teardown do
#		Object.send(:remove_const, :Agent)
#		Agent = @tmp
		ClassBackup.restore_all
	end
end

