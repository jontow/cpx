
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../client_events'

#included purely for the constants they contain... (I hope)
load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

########################################
# General tests for the ClientEvent System

context 'The ClientEvent class' do
	specify 'should have a tier of 0' do
		ClientEvent.tier.should_eql(0)
	end

	specify 'should populate the ClientEventFactory events table correctly' do
		events = ClientEventFactory.instance_variable_get(:@events)
		klasses = events.values
		ObjectSpace.each_object(Class) do |obj|
			if obj.ancestors.include? ClientEvent and obj != ClientEvent
				klasses.should_include obj
			end
		end
	end

	specify 'should raise InvalidEventSpecificationError when a non-standard named class is inherited from it' do
		lambda{class NotStandard < ClientEvent;end}.should_raise ClientEvent::InvalidEventSpecificationError
	end

end

context 'The ClientEventFactory' do
	specify 'should raise IllegalClientEventError on an event with a missing counter' do
		lambda{ClientEventFactory.eventfactory(nil, nil, 'FOO')}.should_raise ClientEvent::IllegalEventError
	end

	specify 'should raise IllegalClientEventError on an event with a non-integer counter' do
		lambda{ClientEventFactory.eventfactory(nil, nil, 'FOO bar')}.should_raise ClientEvent::IllegalEventError
	end

	specify 'should raise UnknownClientEventError on an unknown event' do
		lambda{ClientEventFactory.eventfactory(nil, nil, 'FOO 1')}.should_raise ClientEvent::UnknownEventError
	end

	specify 'should raise InsufficentPermissionError on a tier 4 event sent from a tier 1 agent' do
		agent = mock('Agent')
		agent.should_receive(:tier).once.and_return(1)
		lambda{ClientEventFactory.eventfactory(agent, nil, 'AGENTS 1')}.should_raise ClientEvent::InsufficientPermissionError
	end

	specify 'should return an event object for a tier4 agent on a tier 4 event' do
		agent = mock('Agent')
		agent.should_receive(:tier).once.and_return(4)
		ClientEventFactory.eventfactory(agent, nil, 'AGENTS 1').class.should_eql AgentsClientEvent
	end

	specify 'should return a ClientReply object for any replies received' do
		agent = mock('Agent')
#		agent.should_receive(:tier).twice.and_return(1)
		ClientEventFactory.eventfactory(agent, nil, 'ACK 1 something').class.should_eql ClientReply
		ClientEventFactory.eventfactory(agent, nil, 'ERR 1 something').class.should_eql ClientReply
	end

end

context 'ClientEvent Subclasses' do
	specify 'should return the tier of ClientEvent if they don\'t have one themselves' do
		StateClientEvent.tier.should_eql 0
	end

	specify 'should return their own tier if they do have one' do
		AgentsClientEvent.tier.should_eql 4
	end

	specify 'should indicate they require to be acknowledged' do
		StateClientEvent.new(nil, nil, nil, nil).ack?.should_eql true
	end

end
