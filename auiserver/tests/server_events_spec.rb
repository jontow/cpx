
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../server_events'

load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'The ServerEvent class' do
	specify 'should populate the ServerEventFactory events table correctly' do
		events = ServerEventFactory.instance_variable_get(:@events)
		klasses = events.values
		ObjectSpace.each_object(Class) do |obj|
			if obj.ancestors.include? ServerEvent and obj != ServerEvent
				klasses.should_include obj
			end
		end
	end
	
	specify 'should raise InvalidEventSpecificationError when a non-standard named class is inherited from it' do
		lambda{class NotStandard2 < ServerEvent;end}.should_raise ServerEvent::InvalidEventSpecificationError
	end
end

context 'The ServerEventFactory' do
	specify 'should raise UnknownEventError on an unknown event' do
		lambda{ServerEventFactory.eventfactory(nil, 'Action: die')}.should_raise ServerEvent::UnknownEventError
		lambda{ServerEventFactory.eventfactory(nil, 'Event: die')}.should_raise ServerEvent::UnknownEventError
	end
	
	specify 'should return a valid event instance on valid input' do
		ServerEventFactory.eventfactory(nil, "Event: Agents\r\nAgent: 6000\r\n").should be_an_instance_of(AgentsServerEvent)
	end

	specify 'should set instance vars from parsing event' do
		ServerEventFactory.eventfactory(nil, "Event: Agents\r\nAgent: 6000\r\n").instance_variable_get(:@agent).should_eql '6000'
	end
end

context 'The Event Server' do
	specify 'should initialize correctly' do
		server = EventServer.new('localhost', 5038)
		server.instance_variable_get(:@host).should_eql 'localhost'
		server.instance_variable_get(:@port).should_eql 5038
		server.instance_variable_get(:@socket).should_eql nil
		server.instance_variable_get(:@outputqueue).class.should_eql Queue
		server.instance_variable_get(:@counter).should_eql 0
		server.instance_variable_get(:@callbacks).should == Hash.new
	end

	specify 'should increment the counter correctly' do
		server = EventServer.new(nil, nil)
		server.next_counter.should_eql 1
		server.next_counter.should_eql 2
	end

	specify 'should wrap the counter at 65535' do
		server = EventServer.new(nil, nil)
		server.instance_variable_set(:@counter, 65534)

#		65534.times do |i|
#			server.next_counter.should_eql i+1
#		end
		server.next_counter.should_eql 0
	end

	specify 'should send an event correctly' do
		outputqueue = mock('outputqueue')
		server = EventServer.new(nil, nil)
		args = {'Interface'=>'Agent/6000', 'Paused'=>0}
		args2 = {'Interface'=>'Agent/6000', 'Paused'=>0, 'ActionID'=>1}
		server.instance_variable_set(:@outputqueue, outputqueue)
		# pain in the ass, damn unpredictable hash ordering
		outputqueue.should_receive(:<<).with("Action: QueuePause\r\n"+args2.to_a.map{|a| a.join(': ')}.join("\r\n")+"\r\n\r\n")
		server.send_event('QueuePause', args)
	end

	specify 'should set a callback correctly if send_event is passed a block' do
		outputqueue = mock('outputqueue')
		server = EventServer.new(nil, nil)
		args = {'Interface'=>'Agent/6000', 'Paused'=>0}
		server.instance_variable_set(:@outputqueue, outputqueue)
		outputqueue.should_receive(:<<)
		block = lambda{|reply| p reply}
		server.send_event('QueuePause', args, &block)
		hash = {1=>block}
		server.instance_variable_get(:@callbacks).should == hash
	end
end

