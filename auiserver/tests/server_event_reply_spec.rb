
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../server_events'

load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'ServerReply objects' do
	specify 'should be created by the event factory correctly' do
		ServerEventFactory.eventfactory(nil, "Response: Success\r\n").should be_an_instance_of(ServerReply)
	end

	specify 'should report success/failure correctly' do
		ServerEventFactory.eventfactory(nil, "Response: Success\r\n").success?.should be_an_instance_of(TrueClass)
		ServerEventFactory.eventfactory(nil, "Response: Error\r\n").success?.should be_an_instance_of(FalseClass)
	end

	specify 'should provide accessors for field data' do
		reply = ServerEventFactory.eventfactory(nil, "Response: Success\r\nActionID: 2\r\nMessage: hello world\r\n\r\n")
		reply.success?.should be_an_instance_of(TrueClass)
		reply.message.should_eql 'hello world'
		reply.actionid.should_eql '2'
	end

	specify 'should raise NoMethodError on invalid accessor' do
		reply = ServerEventFactory.eventfactory(nil, "Response: Success\r\nActionID: 2\r\nMessage: hello world\r\n\r\n")
		lambda{reply.magical}.should_raise NoMethodError
		lambda{reply.flavor}.should_raise NoMethodError
	end

	specify 'should dispatch correctly' do
		eventserver = mock('EventServer')
		callbacks = mock('CallbackList')
		callback = mock('Callback')
		reply = ServerEventFactory.eventfactory(eventserver, "Response: Success\r\nActionID: 2\r\nMessage: hello world\r\n\r\n")
		eventserver.should_receive(:callbacks).and_return callbacks
		callbacks.should_receive(:[]).with(2).and_return callback
		callback.should_receive(:call).with reply
		reply.dispatch
	end
end

