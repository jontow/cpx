
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../client_events'

#included purely for the constants they contain... (I hope)
load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'The QUEUEMEMBERS event' do
	setup do
		@agent = mock('Agent')
		@eventserver = mock('EventServer')
		#back up the class and undefine its constant
#		if Object.const_defined?(:CallQueue)
#			@tmp = CallQueue
#			Object.send(:remove_const, :CallQueue)
#		end
#		define the class as a mock
#		CallQueue = mock('CallQueue')
		@callqueueclass = mock('CallQueueClass')
		ClassBackup.replace_class_with_mock(:CallQueue, @callqueueclass)
	end

	specify 'should be be restricted to tier 4 agents' do
		@agent.should_receive(:tier).once.and_return(3)
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'QUEUEMEMBERS 1 1')}.should_raise ClientEvent::InsufficientPermissionError
	end
		
	specify 'should be instanciated correctly from valid events' do
		@agent.should_receive(:tier).once.and_return(4)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'QUEUEMEMBERS 1 1').class.should_eql QueueMembersClientEvent
	end

	specify 'should raise InvalidEventError if CallQueue.match_agents raises ArgumentError' do
		@agent.should_receive(:tier).once.and_return(4)
		CallQueue.should_receive(:match_queues).and_raise ArgumentError
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'QUEUEMEMBERS 1 1').dispatch}.should_raise ClientEvent::InvalidEventError
	end

	specify 'should yield the queues to the block' do
		@agent.should_receive(:tier).once.and_return(4)
		queue = mock('Queue')
		member = mock('QueueMember')
		agent2 = mock('Agent2')
		@callqueueclass.should_receive(:match_queues).with('1').and_yield(queue)
		queue.should_receive(:name).and_return('testq')
		# FIXME - this is a hack
		queue.should_receive(:members).and_return(queue)
		queue.should_receive(:each).and_yield(member)

		member.should_receive(:agent).at_least(:once).and_return(agent2)
		agent2.should_receive(:state).twice.and_return Agent::IDLE
		agent2.should_receive(:id).and_return(1)
		agent2.should_receive(:name).and_return('name')
		agent2.should_receive(:statetime).and_return(0)
		@agent.should_receive(:send_event).with('QUEUEMEMBER', 'testq', 1, 'name', 0, 2)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'QUEUEMEMBERS 1 1').dispatch
	end

	teardown do
		#remove the defined mock and reinstate the class
#		Object.send(:remove_const, :CallQueue)
#		CallQueue = @tmp if @tmp
		ClassBackup.restore_all
	end
end

