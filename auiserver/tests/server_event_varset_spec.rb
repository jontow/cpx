
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../server_events'

load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'


context 'The VarSet event' do
	setup do
		@agentclass = mock('AgentClass')
		ClassBackup.replace_class_with_mock(Agent, @agentclass)
		@callerclass = mock('CallerClass')
		ClassBackup.replace_class_with_mock(:Caller, @callerclass)
	end

	specify 'should be instanciated correctly' do
		ServerEventFactory.eventfactory(nil, "Event: VarSet\r\n").should be_an_instance_of(VarSetServerEvent)
	end

	specify 'should call "set" on a call for "normal" varsets' do
		string = <<EVENT
Event: VarSet\r
Channel: SIP/1234\r
Variable: FooBar\r
Value: 6\r
UniqueID: 98765\r
\r
EVENT
		call = mock('Call')
		Caller.should_receive(:[]).with('98765').and_return(call)
		call.should_receive(:set).with('FooBar', '6')
		ServerEventFactory.eventfactory(nil, string).dispatch
	end

	specify 'should initialize a new call correctly' do
		string = <<EVENT
Event: VarSet\r
Channel: SIP/1234\r
Variable: CALLTYPE\r
Value: call\r
UniqueID: 98765\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		array = mock('Array')
		reply = mock('Reply')
		cdr = mock('Cdr')
		Caller.should_receive(:[]).with('98765').twice.and_return nil
		Caller.should_receive(:new).with('98765', 'SIP/1234').and_return call
#		eventserver.should_receive(:send_event).with('GetVar', 'Channel'=>'SIP/1234', 'Variable'=>'TYPE').and_yield(reply)
#		reply.should_receive(:success?).and_return true
#		reply.should_receive(:value).and_return('call')
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::INIVR)	
		call.should_receive(:type=).with(:call)
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should initialize a new email correctly' do
		string = <<EVENT
Event: VarSet\r
Channel: SIP/1234\r
Variable: CALLTYPE\r
Value: email\r
UniqueID: 98765\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		array = mock('Array')
		reply = mock('Reply')
		reply2 = mock('Reply2')
		cdr = mock('Cdr')
		Caller.should_receive(:[]).with('98765').twice.and_return nil
		Caller.should_receive(:new).with('98765', 'SIP/1234').and_return call
#		eventserver.should_receive(:send_event).with('GetVar', 'Channel'=>'SIP/1234', 'Variable'=>'TYPE').and_yield(reply)
#		reply.should_receive(:success?).and_return true
#		reply.should_receive(:value).and_return('email')
		call.should_receive(:type=).with(:email)
		eventserver.should_receive(:send_event).with('GetVar', 'Channel'=>'SIP/1234', 'Variable'=>'MAILID').and_yield(reply2)
		reply2.should_receive(:success?).and_return true
		reply2.should_receive(:variable).and_return 'MAILID'
		reply2.should_receive(:value).and_return '6'
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::INIVR)
		call.should_receive(:set).with('MAILID', '6')
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should warn on new email with missing MAILID' do
		string = <<EVENT
Event: VarSet\r
Channel: SIP/1234\r
Variable: CALLTYPE\r
Value: email\r
UniqueID: 98765\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		array = mock('Array')
		reply = mock('Reply')
		reply2 = mock('Reply2')
		cdr = mock('Cdr')
		Caller.should_receive(:[]).with('98765').twice.and_return nil
		Caller.should_receive(:new).with('98765', 'SIP/1234').and_return call
		call.should_receive(:type=).with(:email)
		eventserver.should_receive(:send_event).with('GetVar', 'Channel'=>'SIP/1234', 'Variable'=>'MAILID').and_yield(reply2)
		reply2.should_receive(:success?).and_return false
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::INIVR)
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	
	specify 'should initialize a new voicemail correctly' do
		string = <<EVENT
Event: VarSet\r
Channel: SIP/1234\r
Variable: CALLTYPE\r
Value: voicemail\r
UniqueID: 98765\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		array = mock('Array')
		reply = mock('Reply')
		reply2 = mock('Reply2')
		cdr = mock('Cdr')
		Caller.should_receive(:[]).with('98765').twice.and_return nil
		Caller.should_receive(:new).with('98765', 'SIP/1234').and_return call
#		eventserver.should_receive(:send_event).with('GetVar', 'Channel'=>'SIP/1234', 'Variable'=>'TYPE').and_yield(reply)
#		reply.should_receive(:success?).and_return true
#		reply.should_receive(:value).and_return('voicemail')
		call.should_receive(:type=).with(:voicemail)
		eventserver.should_receive(:send_event).with('GetVar', 'Channel'=>'SIP/1234', 'Variable'=>'VMFILE').and_yield(reply2)
		reply2.should_receive(:success?).and_return true
		reply2.should_receive(:variable).and_return 'VMFILE'
		reply2.should_receive(:value).and_return '/tmp/foo'
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::INIVR)
		call.should_receive(:set).with('VMFILE', '/tmp/foo')
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should warn on new voicemail with missing VMFILE' do
		string = <<EVENT
Event: VarSet\r
Channel: SIP/1234\r
Variable: CALLTYPE\r
Value: voicemail\r
UniqueID: 98765\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		array = mock('Array')
		reply = mock('Reply')
		reply2 = mock('Reply2')
		cdr = mock('Cdr')
		Caller.should_receive(:[]).with('98765').twice.and_return nil
		Caller.should_receive(:new).with('98765', 'SIP/1234').and_return call
#		eventserver.should_receive(:send_event).with('GetVar', 'Channel'=>'SIP/1234', 'Variable'=>'TYPE').and_yield(reply)
#		reply.should_receive(:success?).and_return true
#		reply.should_receive(:value).and_return('voicemail')
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::INIVR)
		call.should_receive(:type=).with(:voicemail)
		eventserver.should_receive(:send_event).with('GetVar', 'Channel'=>'SIP/1234', 'Variable'=>'VMFILE').and_yield(reply2)
		reply2.should_receive(:success?).and_return false
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end


	specify 'should initialize an outgoing call correctly' do
		string = <<EVENT
Event: VarSet\r
Channel: Agent/6000\r
Variable: CALLTYPE\r
Value: outcall\r
UniqueID: 98765\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		agent = mock('Agent')
		reply = mock('Reply')
		Caller.should_receive(:[]).with('98765').and_return nil
		Agent.should_receive(:[]).with(6000).and_return agent
		agent.should_receive(:state=).with 4
		agent.should_receive(:id).twice.and_return(6000)
		eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
		reply.should_receive(:success?).and_return true
		agent.should_receive(:paused=).with true
#		Caller.should_receive(:new).with('98765', 'Agent/6000').and_return call
#		agent.should_receive(:talkingto=).with call
		agent.should_receive(:dialoutid=).with("98765")
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should error on outgoing call if agent channel is not set correctly' do
		string = <<EVENT
Event: VarSet\r
Channel: Agent/q6000\r
Variable: CALLTYPE\r
Value: outcall\r
UniqueID: 98765\r
\r
EVENT
		eventserver = mock('EventServer')
		Caller.should_receive(:[]).with('98765').and_return nil
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should logoff an agent that cannot be paused correctly' do
		string = <<EVENT
Event: VarSet\r
Channel: Agent/6000\r
Variable: CALLTYPE\r
Value: outcall\r
UniqueID: 98765\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		agent = mock('Agent')
		reply = mock('Reply')
		Caller.should_receive(:[]).with('98765').and_return nil
		Agent.should_receive(:[]).with(6000).and_return agent
#		agent.should_receive(:state=).with 4
		agent.should_receive(:id).and_return(6000)
		eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
		reply.should_receive(:success?).and_return false
		reply.should_receive(:message)
		agent.should_receive(:logoff)
		agent.should_receive(:dialoutid=).with("98765")
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should error on invalid agent' do
		string = <<EVENT
Event: VarSet\r
Channel: Agent/6000\r
Variable: CALLTYPE\r
Value: outcall\r
UniqueID: 98765\r
\r
EVENT
		eventserver = mock('EventServer')
		Caller.should_receive(:[]).with('98765').and_return nil
		Agent.should_receive(:[]).with(6000).and_return nil
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should ignore a varset for a call that isnt tagged correctly' do
		string = <<EVENT
Event: VarSet\r
Channel: Agent/6000\r
Variable: OK\r
Value: whatever\r
UniqueID: 98765\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		agent = mock('Agent')
		reply = mock('Reply')
		Caller.should_receive(:[]).with('98765').and_return nil
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end


	teardown do
#		Object.send(:remove_const, :Caller)
#		Caller = @tmpcaller
#		Object.send(:remove_const, :Agent)
#		Agent = @tmpagent
		ClassBackup.restore_all
	end
end

