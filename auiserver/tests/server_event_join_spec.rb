
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../server_events'

load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'The Join Event' do
	setup do
		ClassBackup.replace_class_with_mock(:CallQueue, mock('CallQueueClass'))
		ClassBackup.replace_class_with_mock(:Caller, mock('CallerClass'))
		ClassBackup.replace_class_with_mock(:Agent, mock('AgentClass'))

	end

	specify 'should be instanciated correctly' do
		ServerEventFactory.eventfactory(nil, "Event: Join\r\n").should be_an_instance_of(JoinServerEvent)
	end

	specify 'should emit events to agents' do
		string = <<EVENT
Event: Join\r
Channel: SIP/1234\r
CallerID: 123456\r
CallerIDName: Grand Poohbah\r
UniqueID: 98765\r
Queue: testq\r
Count: 5\r
Position: 5\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		queue = mock('Queue')
		agent = mock('Agent')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('98765').and_return call
		CallQueue.should_receive(:[]).with('testq').and_return queue
		queue.should_receive(:add_caller).with(call)
		call.should_receive(:enteredqueue=).with(:numeric)
		Agent.should_receive(:match_agents).and_yield agent
		call.should_receive(:enteredqueue).and_return Time.now.to_i
		queue.should_receive(:name).and_return 'testq'
		call.should_receive(:uniqueid).and_return('98765')
		call.should_receive(:type).and_return :call
		call.should_receive(:get).with('BRANDID').and_return 6
		call.should_receive(:get).with('CALLERIDNUM').and_return '123456'
		agent.should_receive(:send_event).with('QUEUECALLER', :numeric, 'testq', '98765', :call, 6, '123456')
		call.should_receive(:cdr).and_return(cdr)
		cdr.should_receive(:add_transaction)#.with(
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should error on unknown queue' do
		string = <<EVENT
Event: Join\r
Channel: SIP/1234\r
CallerID: 123456\r
CallerIDName: Grand Poohbah\r
UniqueID: 98765\r
Queue: testq\r
Count: 5\r
Position: 5\r
\r
EVENT
		eventserver = mock('EventServer')
		call = mock('Call')
		Caller.should_receive(:[]).with('98765').and_return call
		CallQueue.should_receive(:[]).with('testq').and_return nil
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should error on unknown call' do
		string = <<EVENT
Event: Join\r
Channel: SIP/1234\r
CallerID: 123456\r
CallerIDName: Grand Poohbah\r
UniqueID: 98765\r
Queue: testq\r
Count: 5\r
Position: 5\r
\r
EVENT
		eventserver = mock('EventServer')
		Caller.should_receive(:[]).with('98765').and_return nil
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	
	teardown do
#		Object.send(:remove_const, :CallQueue)
#		CallQueue = @tmpcallqueue
#		Object.send(:remove_const, :Caller)
#		Caller = @tmpcaller
#		Object.send(:remove_const, :Agent)
#		Agent = @tmpagent
		ClassBackup.restore_all
	end
end

