
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

module ClassBackup
	# this helper code allows you to remove the constant that references a class and
	# replace it with a new anonymous class that wraps two mocks, the class mock and the instance mock
	# It also mirrors all the constants from the original class over to the new anonymous replacement class
	
	@class_backups = {}

	def self.replace_class_with_mock(klass, mock)
		if klass.kind_of? Module
			name = klass.name.to_sym # extract the name of the class
		elsif klass.kind_of? Symbol
			name = klass
		elsif klass.kind_of? String
			name = klass.to_sym
		end
			
		# undefine the constant for the class we're replacing
		if Object.const_defined? name
			@class_backups[name] = Object.const_get(name)
			Object.send(:remove_const,name)
		else
			@class_backups[name] = :none
		end

		# create a new anonymous class and assign it to the replaced class' constant
		new_klass = Class.new
		Object.const_set(name, new_klass)

		# populate the replacement class with the constants from the old one
		if @class_backups[name].kind_of? Module
			@class_backups[name].constants.each do |const|
				new_klass.const_set(const, @class_backups[name].const_get(const))
			end
		end

		# setup new to return the instance mock, and method_missing to call the class mock
		new_klass.instance_variable_set(:@mock, mock)
		class << new_klass
			def new(*args, &block)
				@mock.new(*args, &block)
			end
			def method_missing(symbol, *args, &block)
				@mock.send(symbol, *args, &block)
			end
		end
	end

	# code to restore the class

	def self.restore_class(name)
		if klass = @class_backups[name]
			if Object.const_defined? name
				Object.send(:remove_const, name)
			end
			if klass != :none
				Object.const_set(name, klass)
			end
		end
	end

	def self.restore_all
		@class_backups.keys.each do |c|
			restore_class c
		end
	end
end
