
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../server_events'

load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'


context 'The Agents Event' do
	setup do
#		if Object.const_defined?(:Agent)
#			@tmpagent = Agent
#			Object.send(:remove_const, :Agent)
#		end
#		if Object.const_defined?(:Caller)
#			@tmpcaller = Caller
#			Object.send(:remove_const, :Caller)
#		end

		#define the class as a mock
#		$agent = mock('Agent') # HACK!
#		class Agent
#			UNKNOWN = 0
#			OFFLINE = 1
#			IDLE = 2
#			ONCALL = 3
#			OUTGOINGCALL= 4
#			RELEASED = 5
#			WRAPUP = 6
#			UNKNOWNPAUSE = 7
#			class InvalidStateError < StandardError; end
#			def self.new(*args)
#				$agent
#			end
#		end

#		Caller = mock('CallerClass')
		@callerclass = mock('CallerClass')
		ClassBackup.replace_class_with_mock(:Caller, @callerclass)
		@agentclass = mock('AgentClass')
		ClassBackup.replace_class_with_mock(Agent, @agentclass)
		@eventserver = mock('EventServer')
	end

	specify 'should initialize as the correct event type' do
		ServerEventFactory.eventfactory(nil, "Event: Agents\r\nAgent: 6000\r\n").should be_an_instance_of(AgentsServerEvent)
	end

	specify 'should parse the event variables into instance variables correctly' do
		string = <<EVENT
Event: Agents\r
Agent: 6000\r
Name: Andrew Thompson\r
Status: AGENT_IDLE\r
Loggedinchan: SIP/6000\r
Loggedintime: 20\r
Talkingto: \r
\r
EVENT
		event = ServerEventFactory.eventfactory(nil, string)
		event.instance_variable_get(:@agent).should_eql '6000'
		event.instance_variable_get(:@name).should_eql 'Andrew Thompson'
		event.instance_variable_get(:@status).should_eql 'AGENT_IDLE'
		event.instance_variable_get(:@loggedinchan).should_eql 'SIP/6000'
		event.instance_variable_get(:@loggedintime).should_eql '20'
		event.instance_variable_get(:@talkingto).should_eql nil
	end

	specify 'should try to log the agent off on AGENT_IDLE' do
		string = <<EVENT
Event: Agents\r
Agent: 6000\r
Name: Andrew Thompson\r
Status: AGENT_IDLE\r
Loggedinchan: SIP/6000\r
Loggedintime: 20\r
Talkingto: \r
\r
EVENT
		agent = mock('Agent')
		reply = mock('Reply')
		Agent.should_receive(:new).and_return agent
		agent.should_receive(:name=).with('Andrew Thompson')
#		@agent.should_receive(:state=).with(Agent::IDLE)
		agent.should_receive(:id).and_return(6000)
		@eventserver.should_receive(:send_event).with('AgentLogoff', {'Agent'=>6000, 'Soft'=>'true'}).and_yield reply
		reply.should_receive(:success?).and_return true
		agent.should_receive(:state=).with Agent::OFFLINE
		event = ServerEventFactory.eventfactory(@eventserver, string).dispatch
	end

	specify 'should handle an AgentLogoff failure locally on AGENT_IDLE' do
		string = <<EVENT
Event: Agents\r
Agent: 6000\r
Name: Andrew Thompson\r
Status: AGENT_IDLE\r
Loggedinchan: SIP/6000\r
Loggedintime: 20\r
Talkingto: \r
\r
EVENT
		agent = mock('Agent')
		reply = mock('Reply')
		Agent.should_receive(:new).and_return agent
		agent.should_receive(:name=).with('Andrew Thompson')
#		@agent.should_receive(:state=).with(Agent::IDLE)
		agent.should_receive(:id).at_least(:once).and_return(6000)
		@eventserver.should_receive(:send_event).with('AgentLogoff', {'Agent'=>6000, 'Soft'=>'true'}).and_yield reply
		reply.should_receive(:success?).and_return false
		reply.should_receive(:message)
		agent.should_receive(:logoff)
		event = ServerEventFactory.eventfactory(@eventserver, string).dispatch
	end


	specify 'should set the agent to offline on AGENT_LOGGEDOFF' do
		string = <<EVENT
Event: Agents\r
Agent: 6000\r
Name: Andrew Thompson\r
Status: AGENT_LOGGEDOFF\r
Loggedinchan: SIP/6000\r
Loggedintime: 20\r
Talkingto: \r
\r
EVENT
		agent = mock('Agent')
		Agent.should_receive(:new).and_return agent
		agent.should_receive(:name=).with('Andrew Thompson')
		agent.should_receive(:state=).with(Agent::OFFLINE)
		event = ServerEventFactory.eventfactory(nil, string).dispatch
	end

	specify 'should set the agent to unknown on AGENT_UNKNOWN' do
		string = <<EVENT
Event: Agents\r
Agent: 6000\r
Name: Andrew Thompson\r
Status: AGENT_UNKNOWN\r
Loggedinchan: SIP/6000\r
Loggedintime: 20\r
Talkingto: \r
\r
EVENT
		agent = mock('Agent')
		Agent.should_receive(:new).with(6000, nil).and_return agent
		agent.should_receive(:name=).with('Andrew Thompson')
		agent.should_receive(:state=).with(Agent::UNKNOWN)
		event = ServerEventFactory.eventfactory(nil, string).dispatch
	end

	specify 'should get the information on the call on AGENT_ONCALL' do
		string = <<EVENT
Event: Agents\r
Agent: 6000\r
Name: Andrew Thompson\r
Status: AGENT_ONCALL\r
Loggedinchan: SIP/6000\r
Loggedintime: 20\r
Talkingto: SIP/1234\r
\r
EVENT
		agent = mock('Agent')
		Agent.should_receive(:new).and_return agent

		eventserver = mock('EventServer')
		reply = mock('ServerReply')
		reply2 = mock('ServerReply2')
		call = mock('Caller')
		agent.should_receive(:name=).with('Andrew Thompson')
		agent.should_receive(:state=).with(Agent::ONCALL)
		eventserver.should_receive(:send_event).with('GetVar', {'Channel'=>'SIP/1234', 'Variable'=>'UNIQUEID'}).and_yield(reply)
		reply.should_receive(:success?).and_return true
#		Caller.should_receive(:callers).and_return({'1234'=>call})
		reply.should_receive(:value).and_return('1234')
		Caller.should_receive(:[]).with('1234').and_return call
		call.should_receive(:get).with('BRANDID').and_return nil
		call.should_receive(:channel).at_least(:once).and_return('SIP/1234')
		eventserver.should_receive(:send_event).with('GetVar', {'Channel'=>'SIP/1234', 'Variable'=>'BRANDID'}).and_yield(reply2)
		reply2.should_receive(:success?).and_return true
		reply2.should_receive(:value).and_return('6')
		call.should_receive(:set).with('BRANDID', '6')
#		call.should_receive(:uniqueid).and_return('1234')
		agent.should_receive(:talkingto=).with(call)

		event = ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should get the information on the call on AGENT_ONCALL and create a new call object if needed' do
		string = <<EVENT
Event: Agents\r
Agent: 6000\r
Name: Andrew Thompson\r
Status: AGENT_ONCALL\r
Loggedinchan: SIP/6000\r
Loggedintime: 20\r
Talkingto: SIP/1234\r
\r
EVENT
		agent = mock('Agent')
		Agent.should_receive(:new).and_return agent

		eventserver = mock('EventServer')
		reply = mock('ServerReply')
		reply2 = mock('ServerReply2')
		call = mock('Caller')
		agent.should_receive(:name=).with('Andrew Thompson')
		agent.should_receive(:state=).with(Agent::ONCALL)
		eventserver.should_receive(:send_event).with('GetVar', {'Channel'=>'SIP/1234', 'Variable'=>'UNIQUEID'}).and_yield(reply)
		reply.should_receive(:success?).and_return true
#		Caller.should_receive(:callers).and_return({'5678'=>nil})
		Caller.should_receive(:[]).with('1234').and_return nil
		Caller.should_receive(:new).and_return(call)
		reply.should_receive(:value).twice.and_return('1234')
		call.should_receive(:get).with('BRANDID').and_return '6'
#		call.should_receive(:channel).and_return('SIP/1234')
#		eventserver.should_receive(:send_event).with('GetVar', {'Channel'=>'SIP/1234', 'Variable'=>'BRANDID'}).and_yield(reply2)
#		reply2.should_receive(:success?).and_return true
#		reply2.should_receive(:value).and_return('6')
#		call.should_receive(:set).with('BRANDID', '6')
#		call.should_receive(:uniqueid).and_return('1234')
		agent.should_receive(:talkingto=).with(call)

		event = ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should log and error on AGENT_ONCALL if BRANDID retrieval fails' do
		string = <<EVENT
Event: Agents\r
Agent: 6000\r
Name: Andrew Thompson\r
Status: AGENT_ONCALL\r
Loggedinchan: SIP/6000\r
Loggedintime: 20\r
Talkingto: SIP/1234\r
\r
EVENT
		agent = mock('Agent')
		Agent.should_receive(:new).and_return agent

		eventserver = mock('EventServer')
		reply = mock('ServerReply')
		reply2 = mock('ServerReply2')
		call = mock('Caller')
		agent.should_receive(:name=).with('Andrew Thompson')
		agent.should_receive(:state=).with(Agent::ONCALL)
		eventserver.should_receive(:send_event).with('GetVar', {'Channel'=>'SIP/1234', 'Variable'=>'UNIQUEID'}).and_yield(reply)
		reply.should_receive(:success?).and_return true
		Caller.should_receive(:[]).with('1234').and_return nil
		Caller.should_receive(:new).and_return(call)
		reply.should_receive(:value).twice.and_return('1234')

		call.should_receive(:get).with('BRANDID').and_return nil
		call.should_receive(:channel).at_least(:once).and_return('SIP/1234')
		eventserver.should_receive(:send_event).with('GetVar', {'Channel'=>'SIP/1234', 'Variable'=>'BRANDID'}).and_yield(reply2)
		reply2.should_receive(:success?).and_return false
		call.should_receive(:uniqueid).at_least(:once)
		agent.should_receive(:talkingto=).with(call)

		event = ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should log and error on AGENT_ONCALL if UNIQUEID retrieval fails' do
		string = <<EVENT
Event: Agents\r
Agent: 6000\r
Name: Andrew Thompson\r
Status: AGENT_ONCALL\r
Loggedinchan: SIP/6000\r
Loggedintime: 20\r
Talkingto: SIP/1234\r
\r
EVENT
		agent = mock('Agent')
		Agent.should_receive(:new).and_return agent

		eventserver = mock('EventServer')
		reply = mock('ServerReply')
		reply2 = mock('ServerReply2')
		call = mock('Caller')
		agent.should_receive(:name=).with('Andrew Thompson')
		agent.should_receive(:state=).with(Agent::ONCALL)
		eventserver.should_receive(:send_event).with('GetVar', {'Channel'=>'SIP/1234', 'Variable'=>'UNIQUEID'}).and_yield(reply)
		reply.should_receive(:success?).and_return false
		agent.should_receive(:id).and_return '6000'
		agent.should_receive(:logoff)

		event = ServerEventFactory.eventfactory(eventserver, string).dispatch
	end



	specify 'should raise InvalidEventError if setting agent state raises InvalidStateError' do
		string = <<EVENT
Event: Agents\r
Agent: 6000\r
Name: Andrew Thompson\r
Status: AGENT_UNKNOWN\r
Loggedinchan: SIP/6000\r
Loggedintime: 20\r
Talkingto: \r
\r
EVENT
		agent = mock('Agent')
		Agent.should_receive(:new).and_return agent
		agent.should_receive(:name=).with('Andrew Thompson')
		agent.should_receive(:state=).with(Agent::UNKNOWN).and_raise(Agent::InvalidStateError)
		lambda{ServerEventFactory.eventfactory(nil, string).dispatch}.should_raise ServerEvent::InvalidEventError

	end
	teardown do
#		Object.send(:remove_const, :Agent)
#		agent = @tmpagent
#		Object.send(:remove_const, :Caller)
#		agent = @tmpcaller
		ClassBackup.restore_all
	end
end
