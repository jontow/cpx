
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../client_events'

#included purely for the constants they contain... (I hope)
load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'The State event' do
	setup do
		@agent = mock('Agent')
		@eventserver = mock('EventServer')
	end

	specify 'should be instanciated correctly from valid events' do
		@agent.should_receive(:tier).once.and_return(4)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'STATE 1 1').class.should_eql StateClientEvent
	end

	specify 'should raise an InvalidEventError on an invalid state' do
		@agent.should_receive(:state=).once.and_raise(Agent::InvalidStateError)
#		@agent.should_receive(:state).at_least(:once).and_return Agent::IDLE
		lambda{StateClientEvent.new(@agent, @eventserver, '1', 'g').dispatch}.should_raise ClientEvent::InvalidEventError
	end
	
	specify 'should attempt to set the state' do
		@agent.should_receive(:state=).once.with('6').and_return(6)
#		@agent.should_receive(:state).at_least(:once).and_return Agent::IDLE
		StateClientEvent.new(@agent, @eventserver, '1', '6').dispatch
	end

	specify 'should set the state to released and pause the agent if requested and the current state is idle' do
		reply = mock('Reply')
		@agent.should_receive(:state=).once.with('5')
		@agent.should_receive(:paused=).with(true)
		@agent.should_receive(:state).at_least(:once).and_return Agent::IDLE
		@agent.should_receive(:id).and_return 6000
		reply.should_receive(:success?).and_return true
		@eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
		StateClientEvent.new(@agent, @eventserver, '1', '5').dispatch

	end

	specify 'should log off the agent if pausing them fails while going released' do
		reply = mock('Reply')
		#@agent.should_receive(:state=).once.with(Agent::RELEASED.to_s)
		#@agent.should_receive(:paused=).with(true)
		@agent.should_receive(:state).at_least(:once).and_return Agent::IDLE
		@agent.should_receive(:id).and_return 6000
		@eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
		reply.should_receive(:success?).and_return false
		reply.should_receive(:message).and_return 'foo'
		@agent.should_receive(:logoff)
		StateClientEvent.new(@agent, @eventserver, '1', '5').dispatch
	end

	specify 'should set the state to idle and unpaused the agent if requeested and the current state is released' do
		reply = mock('Reply')
		@agent.should_receive(:state=).once.with('2')
		@agent.should_receive(:paused=).with(false)
		@agent.should_receive(:state).at_least(:once).and_return Agent::RELEASED
		@agent.should_receive(:id).and_return 6000
		reply.should_receive(:success?).and_return true
		@eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>0).and_yield reply
		StateClientEvent.new(@agent, @eventserver, '1', '2').dispatch

	end
end

