
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../server_events'

load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'The Unlink event' do
	setup do
		@agentclass = mock('AgentClass')
		ClassBackup.replace_class_with_mock(:Agent, @agentclass)
		@callerclass = mock('CallerClass')
		ClassBackup.replace_class_with_mock(:Caller, @callerclass)
	end

	specify 'should be instanciated correctly' do
		ServerEventFactory.eventfactory(nil, "Event: Unlink\r\n").should be_an_instance_of(UnlinkServerEvent)
	end

	specify 'should dispatch correctly for an inbound call' do
		string = <<EVENT
Event: Unlink\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		eventserver = mock('EventServer')
		reply = mock('Reply')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('123456').and_return call
		Agent.should_receive(:detect).and_return agent
		agent.should_receive(:talkingto).and_return call
		agent.should_receive(:socket).and_return true
		agent.should_receive(:state).and_return Agent::ONCALL
		agent.should_receive(:id).twice.and_return 6000
		eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
		reply.should_receive(:success?).and_return true
		agent.should_receive(:paused=).with true
		agent.should_receive(:state=).with Agent::WRAPUP
		call.should_receive(:type).and_return :call
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::INWRAPUP, 6000)
		Agent.should_receive(:match_agents)
		ServerEventFactory.eventfactory(eventserver, string).dispatch

	end

	specify 'should use BRIDGEPEER for an inbound call if an agent is not already assigned' do
		string = <<EVENT
Event: Unlink\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		eventserver = mock('EventServer')
		reply = mock('Reply')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('123456').and_return call
		Agent.should_receive(:detect).and_return nil
		agent.should_receive(:talkingto).and_return call
		call.should_receive(:get).with('BRIDGEPEER').at_least(:twice).and_return 'SIP/6000'
		Agent.should_receive(:[]).with(6000).and_return agent
		agent.should_receive(:socket).and_return true
		agent.should_receive(:state).and_return Agent::ONCALL
		agent.should_receive(:id).twice.and_return 6000
		eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
		reply.should_receive(:success?).and_return true
		agent.should_receive(:paused=).with true
		agent.should_receive(:state=).with Agent::WRAPUP
		call.should_receive(:type).and_return :call
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::INWRAPUP, 6000)
		Agent.should_receive(:match_agents)
		ServerEventFactory.eventfactory(eventserver, string).dispatch

	end

	specify 'should error for an inbound call if an agent cannot be assigned' do
		string = <<EVENT
Event: Unlink\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		eventserver = mock('EventServer')
		reply = mock('Reply')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('123456').and_return call
		Agent.should_receive(:detect).and_return nil
		call.should_receive(:get).with('BRIDGEPEER').and_return nil

		ServerEventFactory.eventfactory(eventserver, string).dispatch

	end

	specify 'should dispatch correctly for an inbound email call' do
		string = <<EVENT
Event: Unlink\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		eventserver = mock('EventServer')
		reply = mock('Reply')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('123456').and_return call
		Agent.should_receive(:detect).and_return agent
		agent.should_receive(:talkingto).and_return call
		agent.should_receive(:socket).and_return true
		agent.should_receive(:state).and_return Agent::ONCALL
		agent.should_receive(:id).twice.and_return 6000
		eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
		reply.should_receive(:success?).and_return true
		agent.should_receive(:paused=).with true
		agent.should_receive(:state=).with Agent::WRAPUP
		call.should_receive(:type).and_return :email
		call.should_receive(:get).with('MAILID')
		agent.should_receive(:send_event).with('URL', :string)
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::INWRAPUP, 6000)
		Agent.should_receive(:match_agents)
		ServerEventFactory.eventfactory(eventserver, string).dispatch

	end

	specify 'should dispatch correctly for an inbound email call' do
		string = <<EVENT
Event: Unlink\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		eventserver = mock('EventServer')
		reply = mock('Reply')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('123456').and_return call
		Agent.should_receive(:detect).and_return agent
		agent.should_receive(:talkingto).and_return call
		agent.should_receive(:socket).and_return true
		agent.should_receive(:state).and_return Agent::ONCALL
		agent.should_receive(:id).and_return 6000
		eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
		reply.should_receive(:success?).and_return false
		agent.should_receive(:logoff)
		reply.should_receive(:message)
		Agent.should_receive(:match_agents)
		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should error on unexpected association' do
		string = <<EVENT
Event: Unlink\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		eventserver = mock('EventServer')
		reply = mock('Reply')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('123456').and_return call
		Agent.should_receive(:detect).and_return nil
		agent.should_receive(:talkingto).and_return nil
		call.should_receive(:get).with('BRIDGEPEER').at_least(:twice).and_return 'SIP/6000'
		Agent.should_receive(:[]).with(6000).and_return agent
		agent.should_receive(:socket).and_return true
		agent.should_receive(:id)
		ServerEventFactory.eventfactory(eventserver, string).dispatch

	end

	specify 'should error on unknown call being unlinked from unknown agent' do
		string = <<EVENT
Event: Unlink\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		eventserver = mock('EventServer')
		reply = mock('Reply')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('123456').and_return nil
		Caller.should_receive(:[]).with('345678').and_return nil
		ServerEventFactory.eventfactory(eventserver, string).dispatch

	end

	specify 'should dispatch correctly for an outbound call' do
		string = <<EVENT
Event: Unlink\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		eventserver = mock('EventServer')
		reply = mock('Reply')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('123456').and_return call
		Agent.should_receive(:detect).and_return agent
		agent.should_receive(:talkingto).and_return call
		agent.should_receive(:socket).and_return true
		agent.should_receive(:state).and_return Agent::OUTGOINGCALL
#		agent.should_receive(:id).and_return 6000
#		eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
#		reply.should_receive(:success?).and_return true
#		agent.should_receive(:paused=).with true
#		agent.should_receive(:state=).with Agent::WRAPUP
#		call.should_receive(:cdr).and_return cdr
#		cdr.should_receive(:add_transaction).with(CDR::INWRAPUP, 6000)
#		agent.should_receive(:send_event) # TODO
		Agent.should_receive(:match_agents)
#		call.should_receive(:get)
		ServerEventFactory.eventfactory(eventserver, string).dispatch

	end

	specify 'should error for an outbound call if agent cannot be paused' do
		string = <<EVENT
Event: Unlink\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		eventserver = mock('EventServer')
		reply = mock('Reply')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('123456').and_return call
		Agent.should_receive(:detect).and_return agent
		agent.should_receive(:talkingto).and_return call
		agent.should_receive(:socket).and_return true
		agent.should_receive(:state).and_return Agent::OUTGOINGCALL
#		agent.should_receive(:id).and_return 6000
#		eventserver.should_receive(:send_event).with('QueuePause', 'Interface'=>'Agent/6000', 'Paused'=>1).and_yield reply
#		reply.should_receive(:success?).and_return false
#		reply.should_receive(:message)
#		agent.should_receive(:logoff)
		Agent.should_receive(:match_agents)
		ServerEventFactory.eventfactory(eventserver, string).dispatch

	end


	specify 'should finish writing the CDR if the agent is not online' do
		string = <<EVENT
Event: Unlink\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		eventserver = mock('EventServer')
		reply = mock('Reply')
		cdr = mock('CDR')
		Caller.should_receive(:[]).with('123456').and_return call
		Agent.should_receive(:detect).and_return agent
		agent.should_receive(:talkingto).and_return call
		agent.should_receive(:socket).twice.and_return false
		call.should_receive(:cdr).twice.and_return cdr
		agent.should_receive(:id).at_least(:once).and_return 6000
		cdr.should_receive(:add_transaction).with(CDR::INWRAPUP, 6000)
		cdr.should_receive(:add_transaction).with(CDR::ENDCALL)

		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	teardown do
		ClassBackup.restore_all
	end
end
