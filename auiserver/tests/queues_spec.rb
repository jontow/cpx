
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../queues'
require File.dirname(__FILE__) + '/../cdr'

context "An empty queue list" do
	setup do
		@cdrclass = mock('CDRClass')
		ClassBackup.replace_class_with_mock(:CDR, @cdrclass)
		@cdrclass.should_receive(:queuetimes_since).and_return Hash.new
		@cdrclass.should_receive(:calls_since).twice
	end

	specify 'should allow a queue to be added' do
		CallQueue.new('queue1')
		CallQueue['queue1'].should_be.kind_of? CallQueue
	end

	specify 'should not allow a queue to be duplicated' do
		lambda{CallQueue.new('queue1')}.should_not_raise CallQueue::DuplicateQueueError
		lambda{CallQueue.new('queue1')}.should_raise CallQueue::DuplicateQueueError
	end

	teardown do
		CallQueue.instance_variable_set(:@queues, [])
		ClassBackup.restore_all
	end
end

context "A queue list with a single queue" do
	setup do
		@cdrclass = mock('CDRClass')
		ClassBackup.replace_class_with_mock(:CDR, @cdrclass)
		@cdrclass.should_receive(:queuetimes_since).with(:numeric, 'testq').and_return Hash.new
		@cdrclass.should_receive(:calls_since).twice
		@queue = CallQueue.new('testq')
	end

	specify 'should report one queue' do
		CallQueue.match_queues('ALL').length.should_eql 1
		CallQueue.match_queues(:all).length.should_eql 1
		CallQueue.match_queues('testq').length.should_eql 1
		CallQueue.match_queues(['testq']).length.should_eql 1
	end

	specify 'should raise ArgumentError on invalid input' do
		lambda{CallQueue.match_queues(nil)}.should_raise ArgumentError
		lambda{CallQueue.match_queues(:foo)}.should_raise ArgumentError
		lambda{CallQueue.match_queues(6)}.should_raise ArgumentError
	end

	specify 'should return an empty array on no matches' do
		CallQueue.match_queues('bleh').should_eql []
		CallQueue.match_queues(['bleh']).should_eql []
	end

	specify 'should pass queues to an associated block' do
		CallQueue.match_queues('ALL') do |q|
			q.class.should_eql CallQueue
		end
	end

	teardown do
		CallQueue.instance_variable_set(:@queues, [])
		ClassBackup.restore_all
	end
end

context "A queue" do
	setup do
		@cdrclass = mock('CDRClass')
		ClassBackup.replace_class_with_mock(:CDR, @cdrclass)
		@cdrclass.should_receive(:queuetimes_since).with(:numeric, 'testq').and_return Hash.new
		@cdrclass.should_receive(:calls_since).twice.and_return Array.new
		@queue = CallQueue.new('testq')
	end
	
#	specify 'should be able to add calls' do
#		call = mock('Call')
#		@queue.add_caller(call)
#		@queue.callers.should_eql [call]
#	end

	specify 'should be able to remove callers' do
		call = mock('Call')
		@queue.add_caller(call)
		@queue.callers.should_eql [call]
		@queue.remove_caller(call).should_eql call
		@queue.callers.should_eql []
	end

	specify 'should be able to add/remove members' do
		agent = mock('Agent')
		member = @queue.add_member(agent)
		member.class.should_eql QueueMember
		member.agent.should_eql agent
		@queue.members.should_eql [member]
		@queue.remove_member(agent).should_eql member
		@queue.callers.should_eql []
	end

	specify 'should not allow the same agent to be in the queue twice' do
		agent = mock('Agent')
		@queue.add_member(agent)
		lambda{@queue.add_member(agent)}.should_raise CallQueue::DuplicateMemberError
	end

	specify 'removing a non-existant caller from the queue should return nil' do
		@queue.remove_caller(mock('Call')).should_eql nil
	end

	specify 'removing a non-existant agent from the queue should return nil' do
		@queue.remove_member(mock('Agent')).should_eql nil
	end

	specify 'should allow queues to be looked up by name' do
		CallQueue['testq'].should_eql @queue
		CallQueue['nonexistant'].should_eql nil
	end

	specify 'should allow a queuetime to be added' do
		time = Time.now.to_i
		@queue.add_queuetime(10, time)
		qt = @queue.instance_variable_get(:@queuetimes)
		qt[time].should_eql 10
	end

	specify 'should trim queuetimes prior to midnight' do
		now = Time.now
		midnight = Time.mktime(now.year, now.month, now.day).to_i

		@queue.add_queuetime(5, midnight - 1)
		@queue.add_queuetime(10, midnight)
		@queue.add_queuetime(15, midnight + 1)

		timehash = @queue.trim_queuetimes
		midnight += 1
		timehash.should == Hash[midnight, 15]
	end

	specify 'should reveal an average queuetime' do
		now = Time.now
		midnight = Time.mktime(now.year, now.month, now.day).to_i + 1

		@queue.add_queuetime(5, midnight + 0.1)
		@queue.add_queuetime(10, midnight + 0.2)
		@queue.add_queuetime(15, midnight + 0.3)

		@queue.avg_queuetime.should_eql 10
	end

	specify 'should reveal a maximum queuetime' do
		now = Time.now
		midnight = Time.mktime(now.year, now.month, now.day).to_i + 1

		@queue.add_queuetime(5, midnight + 0.1)
		@queue.add_queuetime(10, midnight + 0.2)
		@queue.add_queuetime(15, midnight + 0.3)

		@queue.max_queuetime.should_eql 15
	end

	specify 'should return 0 (avg/max_queuetimes) if queuetimes are empty' do
		@queue.avg_queuetime.should_eql 0
		@queue.max_queuetime.should_eql 0

		@queue.add_queuetime(5, 1)
		@queue.add_queuetime(5, 15)
		@queue.avg_queuetime.should_eql 0
	end

	specify 'should add abandoned calls to an array' do
		@queue.add_abandoned
		@queue.instance_variable_get(:@abandoned).length.should_eql 1
	end

	specify 'should add completed calls to an array' do
		@queue.add_completed
		@queue.instance_variable_get(:@completed).length.should_eql 1
	end

	specify 'should trim calls from the list that are older than today' do
		now = Time.now
		midnight = Time.mktime(now.year, now.month, now.day).to_i

		callres = @queue.trim_calls([midnight - 1, midnight, midnight + 1])

		callres.should == [midnight + 1]
	end

	specify 'should trim abandoned/completed calls older than today' do
		now = Time.now
		midnight = Time.mktime(now.year, now.month, now.day).to_i

		@queue.instance_variable_set(:@abandoned, [1,5,midnight,midnight + 1])
		@queue.abandoned.should_eql 1

		@queue.instance_variable_set(:@completed, [1,5,midnight,midnight + 1])
		@queue.completed.should_eql 1
	end

	teardown do
		CallQueue.instance_variable_set(:@queues, [])
		ClassBackup.restore_all
	end
end

context 'A Caller' do
	setup do
		cdr = mock('CDR')
		ClassBackup.replace_class_with_mock(:CDR, cdr)
	end

	specify 'should allow variables to be get and set' do
		CDR.should_receive(:new)#.with(call)
		call = Caller.new('6', 'Local/10')
		call.get('FOO').should_eql nil
		call.set('FOO', 'bar')
		call.get('FOO').should_eql 'bar'
	end

	specify 'should allow a caller to be looked up by uid' do
		CDR.should_receive(:new)#.with(call)
		call = Caller.new('6123123123.1', 'Local/10')
		Caller['6123123123.1'].should eql(call)
		Caller['Local/10'].should eql(call)
	end

	specify 'should provide the list of all callers' do
		CDR.should_receive(:new).twice#.with(call)
		call1 = Caller.new('6', 'Local/10')
		call2 = Caller.new('7', 'Local/11')
		Caller.callers.should_eql [call1, call2]

	end

	specify 'should allow the type to be set correctly' do
		cdr = mock('CDRinstance')
		CDR.should_receive(:new).and_return(cdr)
		call = Caller.new('42', 'Local/8675309')
		call.type.should_eql :call

		cdr.should_receive(:type=).with(:email)
		call.type = :email
		call.type.should_eql :email
	end

	teardown do
		Caller.instance_variable_set(:@callers, [])
		ClassBackup.restore_all
	end
end
