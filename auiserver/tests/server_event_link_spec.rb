
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../server_events'

load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'The Link event' do
	setup do
		@agentclass = mock('AgentClass')
		ClassBackup.replace_class_with_mock(:Agent, @agentclass)
		@callerclass = mock('CallerClass')
		ClassBackup.replace_class_with_mock(:Caller, @callerclass)
	end

	specify 'should be instanciated correctly' do
		ServerEventFactory.eventfactory(nil, "Event: Link\r\n").should be_an_instance_of(LinkServerEvent)
	end

	specify 'should emit events to agents correctly' do
		string = <<EVENT
Event: Link\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		agent2 = mock('Agent2')
		cdr = mock('cdr')
		Caller.should_receive(:[]).with('123456').and_return call
		call.should_receive(:get).with('BRIDGEPEER').twice.and_return 'Agent/6000'
		Agent.should_receive(:[]).with(6000).and_return agent
		agent.should_receive(:talkingto=).with(call)
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::INCALL, 6000)
		agent.should_receive(:state=).with 3
		Agent.should_receive(:match_agents).and_yield(agent2)
		agent.should_receive(:id).twice.and_return(6000)
		call.should_receive(:uniqueid).and_return '345678'
		call.should_receive(:get).with('BRANDID').and_return 6
		call.should_receive(:get).with('CALLERIDNUM').and_return '87654'
		agent2.should_receive(:send_event).with('BRIDGECALLER', 6000, '345678', 6, '87654')

		ServerEventFactory.eventfactory(nil, string).dispatch
	end

	specify 'should error on invalid agent' do
		string = <<EVENT
Event: Link\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		agent2 = mock('Agent2')
		cdr = mock('cdr')
		Caller.should_receive(:[]).with('123456').and_return call
		call.should_receive(:get).with('BRIDGEPEER').twice.and_return 'Agent/6000'
		Agent.should_receive(:[]).with(6000).and_return nil

		ServerEventFactory.eventfactory(nil, string).dispatch
	end

	specify 'should error on missing BRIDGEPEER' do
		string = <<EVENT
Event: Link\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		call = mock('Call')
		agent = mock('Agent')
		agent2 = mock('Agent2')
		cdr = mock('cdr')
		Caller.should_receive(:[]).with('123456').and_return call
		call.should_receive(:get).with('BRIDGEPEER').and_return nil

		ServerEventFactory.eventfactory(nil, string).dispatch
	end

	specify 'should error on unknown call' do
		string = <<EVENT
Event: Link\r
Channel1: SIP/6000\r
Channel2: SIP/1234\r
UniqueID1: 123456\r
UniqueID2: 345678\r
CallerID1: 98765\r
CallerID2: 87654\r
\r
EVENT
		Caller.should_receive(:[]).with('123456').and_return nil
		Agent.should_receive(:detect).and_return nil
		ServerEventFactory.eventfactory(nil, string).dispatch
	end


	teardown do
#		Object.send(:remove_const, :Caller)
#		Caller = @tmpcaller
#		Object.send(:remove_const, :Agent)
#		Agent = @tmpagent
		ClassBackup.restore_all
	end
end

