
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../client_events'

#included purely for the constants they contain... (I hope)
load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'The EndWrapup event' do
	setup do
		@agent = mock('Agent')
		@eventserver = mock('EventServer')
	end

	specify 'should be instanciated correctly from valid events' do
		@agent.should_receive(:tier).once.and_return(1)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'ENDWRAPUP 1 1').class.should_eql EndWrapupClientEvent
	end

	specify 'should raise an InvalidEventError if the agent isn\'t in WRAPUP' do
		@agent.should_receive(:state).and_return(Agent::IDLE)
		@agent.should_receive(:paused?).and_return(false)
		lambda{EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch}.should_raise ClientEvent::InvalidEventError
	end

	specify 'should log agent off if the QueuePause fails' do
		@agent.should_receive(:state).and_return(Agent::WRAPUP)
		@agent.should_receive(:id).and_return(1)
		@agent.should_receive(:queuerelease).and_return false
		call = mock('Call')
		reply = mock('Reply')
		reply.should_receive(:success?).and_return false
		reply.should_receive(:message).and_return 'foo'
		@agent.should_receive(:logoff)
		@agent.should_receive(:talkingto).and_return nil
		@eventserver.should_receive(:send_event).once.and_yield(reply)
		@agent.should_receive(:dialoutid=).with(nil)
		@agent.should_receive(:dialoutnum=).with(nil)
		EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end

	specify 'should log agent off if the QueuePause fails and clean up any associated calls' do
		@agent.should_receive(:state).and_return(Agent::WRAPUP)
		@agent.should_receive(:id).and_return(1)
		@agent.should_receive(:queuerelease).and_return false
		call = mock('Call')
		reply = mock('Reply')
		cdr = mock('CDR')
		reply.should_receive(:success?).and_return false
		reply.should_receive(:message).and_return 'foo'
		@agent.should_receive(:logoff)
		@agent.should_receive(:talkingto).at_least(:twice).and_return call
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::ENDCALL)
		@agent.should_receive(:talkingto=).with nil
		@eventserver.should_receive(:send_event).once.and_yield(reply)
		@agent.should_receive(:dialoutid=).with(nil)
		@agent.should_receive(:dialoutnum=).with(nil)
		EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end


	specify 'should send an event to the event server if the agent is in WRAPUP' do
		@agent.should_receive(:state).at_least(:once).and_return(Agent::WRAPUP)
		@agent.should_receive(:paused=).with(false)
		@agent.should_receive(:queuerelease).and_return false
		@agent.should_receive(:id).and_return(1)
		@agent.should_receive(:state=).with(Agent::IDLE)
		call = mock('Call')
		cdr = mock('CDR')
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::ENDCALL)
		call.should_receive(:get).with('VMFILE').and_return(nil)
		@agent.should_receive(:talkingto).at_least(3).times.and_return(call)
		reply = mock('Reply')
		@agent.should_receive(:talkingto=).with(nil)
		reply.should_receive(:success?).and_return true
		@eventserver.should_receive(:send_event).once.and_yield(reply)
		@agent.should_receive(:dialoutid=).with(nil)
		@agent.should_receive(:dialoutnum=).with(nil)

		EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end

	specify 'should send an event to the event server if the agent is in WRAPUP' do
		@agent.should_receive(:state).and_return(Agent::WRAPUP)
		@agent.should_receive(:paused=).with(false)
		@agent.should_receive(:queuerelease).and_return false
		reply = mock('Reply')
		reply.should_receive(:success?).and_return true
		@agent.should_receive(:id).and_return(1)
		@agent.should_receive(:state=).with(Agent::IDLE)
		call = mock('Call')
		cdr = mock('CDR')
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::ENDCALL)
		@agent.should_receive(:talkingto).at_least(3).times.and_return(call)
		call.should_receive(:get).with('VMFILE').at_least(2).times.and_return('/dev/null')
		@agent.should_receive(:send_event).with('BLAB', :anything)
		@agent.should_receive(:talkingto=).with(nil)
		@agent.should_receive(:dialoutid=).with(nil)
		@agent.should_receive(:dialoutnum=).with(nil)
		@eventserver.should_receive(:send_event).once.and_yield(reply)
		EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end
	
	specify 'should put the agent in released if they queued release' do
		@agent.should_receive(:state).once.and_return(Agent::WRAPUP)
		@agent.should_receive(:queuerelease).and_return true
		@agent.should_receive(:state=).with(Agent::RELEASED)
		@agent.should_receive(:queuerelease=).with false
		call = mock('Call')
		cdr = mock('CDR')
		call.should_receive(:cdr).and_return cdr
		cdr.should_receive(:add_transaction).with(CDR::ENDCALL)
		@agent.should_receive(:talkingto).exactly(2).times.and_return(call)
		reply = mock('Reply')
		@agent.should_receive(:talkingto=).with(nil)
		@agent.should_receive(:dialoutid=).with(nil)
		@agent.should_receive(:dialoutnum=).with(nil)
		EndWrapupClientEvent.new(@agent, @eventserver, '1', '1').dispatch
	end

	teardown do
	end
end

