
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../server_events'

load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'The QueueMember event' do
	setup do
#		if Object.const_defined?(:CallQueue)
#			@tmpqueue = CallQueue
#			Object.send(:remove_const, :CallQueue)
#		end
#		CallQueue = mock('QueueClass')

#		if Object.const_defined?(:Agent)
#			@tmpagent = Agent
#			Object.send(:remove_const, :Agent)
#		end
#		$agent = mock('Agent') # HACK!
#		$agentclass = mock('AgentClass')
#		class Agent
#			UNKNOWN = 0
#			OFFLINE = 1
#			IDLE = 2
#			ONCALL = 3
#			OUTGOINGCALL= 4
#			RELEASED = 5
#			WRAPUP = 6
#			UNKNOWNPAUSE = 7
#			class InvalidStateError < StandardError; end
#			def self.new(*args)
#				$agent
#			end
#			def self.method_missing(*args)
#				$agentclass.send(*args)
#			end
		ClassBackup.replace_class_with_mock(:CallQueue, mock('CallQueueClass'))
#		ClassBackup.replace_class_with_mock(:Caller, mock('CallerClass'))
		ClassBackup.replace_class_with_mock(:Agent, mock('AgentClass'))

#		end

	end

	specify 'should initialize as the correct event type' do
		ServerEventFactory.eventfactory(nil, "Event: QueueMember\r\n").should be_an_instance_of(QueueMemberServerEvent)
	end

	specify 'should dispatch correctly' do
		string = <<EVENT
Event: QueueMember\r
Queue: testq\r
Location: Agent/6000\r
Membership: static\r
Penalty: 0\r
Callstaken: 0\r
LastCall: 0\r
Status: 4\r
Paused: 1\r
\r
EVENT
		eventserver = mock('EventServer')
		queue = mock('Queue')
		agent = mock('Agent')
		member = mock('Member')
		CallQueue.should_receive(:[]).with('testq').and_return(queue)
		Agent.should_receive(:[]).with(6000).and_return(agent)
		queue.should_receive(:members).and_return([])
		queue.should_receive(:add_member).with(agent).and_return(member)
		member.should_receive(:callstaken=).with(0)
		member.should_receive(:lastcall=).with(Time.at(0))
		member.should_receive(:agent).at_least(:once).and_return(agent)
		agent.should_receive(:paused=).with(true)
		agent.should_receive(:state).and_return(2)
		agent.should_receive(:state=).with(7)


		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should warn if queuemember location is invalid' do
		string = <<EVENT
Event: QueueMember\r
Queue: testq\r
Location: Agent/q6000\r
Membership: static\r
Penalty: 0\r
Callstaken: 0\r
LastCall: 0\r
Status: 4\r
Paused: 1\r
\r
EVENT
		eventserver = mock('EventServer')
		queue = mock('Queue')
		CallQueue.should_receive(:[]).with('testq').and_return(queue)

		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end

	specify 'should warn if queue is invalid' do
		string = <<EVENT
Event: QueueMember\r
Queue: testq\r
Location: Agent/6000\r
Membership: static\r
Penalty: 0\r
Callstaken: 0\r
LastCall: 0\r
Status: 4\r
Paused: 1\r
\r
EVENT
		eventserver = mock('EventServer')
		CallQueue.should_receive(:[]).with('testq').and_return(nil)

		ServerEventFactory.eventfactory(eventserver, string).dispatch
	end


	teardown do
#		Object.send(:remove_const, :Agent)
#		Agent = @tmpagent
#		Object.send(:remove_const, :CallQueue)
#		CallQueue = @tmpqueue
		ClassBackup.restore_all
	end
end

