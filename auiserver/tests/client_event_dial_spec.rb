
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require File.dirname(__FILE__) + '/mock_helpers'
require File.dirname(__FILE__) + '/../client_events'

#included purely for the constants they contain... (I hope)
load File.dirname(__FILE__) + '/../config/server.conf.example' unless Object.const_defined? :ServerConfig
require File.dirname(__FILE__) + '/../agents'
require File.dirname(__FILE__) + '/../cdr'

context 'The Dial event' do
	setup do
		@agent = mock('Agent')
		@eventserver = mock('EventServer')
	end

	specify 'should be instanciated correctly from valid events' do
		@agent.should_receive(:tier).once.and_return(1)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'DIAL 1 1').class.should_eql DialClientEvent
	end

	specify 'should raise InvalidEventError is the agent is not idle' do
		@agent.should_receive(:tier).and_return(1)
		@agent.should_receive(:state).twice.and_return(0)
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'DIAL 1 1').dispatch}.should_raise ClientEvent::InvalidEventError
	end

	specify 'should raise InvalidEventError on invalid input' do
		@agent.should_receive(:tier).at_least(:once).and_return(1)
		@agent.should_receive(:state).at_least(:once).and_return(Agent::IDLE)
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'DIAL 1').dispatch}.should_raise ClientEvent::InvalidEventError
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'DIAL 1 1').dispatch}.should_raise ClientEvent::InvalidEventError
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'DIAL 1 1 1').dispatch}.should_raise ClientEvent::InvalidEventError
	end

	specify 'should send an Originate event on valid input' do
		@agent.should_receive(:tier).and_return(1)
		@agent.should_receive(:state).and_return(Agent::IDLE)
		@agent.should_receive(:id).and_return 1
		@eventserver.should_receive(:send_event).with('Originate', :anything)
		ClientEventFactory.eventfactory(@agent, @eventserver, 'DIAL 1 1 1 1').dispatch
	end

	specify 'should raise InvalidEventError on failed Originate' do
		reply = mock('Reply')
		@agent.should_receive(:tier).and_return(1)
		@agent.should_receive(:state).and_return(Agent::IDLE)
		@agent.should_receive(:id).and_return 1
		@eventserver.should_receive(:send_event).with('Originate', :anything).and_yield reply
		reply.should_receive(:success?).and_return false
		reply.should_receive(:message)
		lambda{ClientEventFactory.eventfactory(@agent, @eventserver, 'DIAL 1 1 1 1').dispatch}.should_raise ClientEvent::InvalidEventError
	end
end
