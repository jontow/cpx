
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'socket'
require 'thread'

class EventServerCallback
	def initialize(event, args, counter, context, block)
		@event = event
		@args = args
		@context = context
		@counter = counter
		@block = block
		@start = Time.now
	end

	def call(*args)
		oargs = @args.reject{|k,v| k == 'ActionID'}
		begin
			@block.call(*args)
		rescue => e # rescue all exceptions!
			Cpxlog.err "Callback ##{@counter} for #{@event} (#{oargs.inspect}) raised an unexpected #{e.class} with message \"#{e.message}\" at #{@context[0]} -- #{e.backtrace[0]} after #{(Time.now - @start).to_i} seconds"
		else
			elapsed = Time.now - @start
			if elapsed > 5
				Cpxlog.warning "Callback ##{@counter} for #{@event} (#{oargs.inspect}) succeeded after #{sprintf('%.2f', elapsed)} seconds"
			else
				Cpxlog.debug "Callback ##{@counter} for #{@event} (#{oargs.inspect}) succeeded after #{sprintf('%.2f', elapsed)} seconds"
			end
		end
	end
end

class EventServer
	# Used to assign (set) the database handle
	def self.dbh=(dbh)
		@@dbh = dbh
	end

	attr_accessor :socket, :outputqueue, :callbacks, :startuptime
	def initialize(host, port)
		@host = host
		@port = port
		@socket = nil
		@outputqueue = Queue.new
		@counter = 0
		@callbacks = {}
		@shutdown = false
	end

	def connect
		# TODO: add more rescues (TIMEOUT, etc)
		begin
			@socket = TCPSocket.new(@host, @port)
		rescue Errno::ECONNREFUSED, Errno::EPROTONOSUPPORT
			Cpxlog.err "Connection to AMI (#{@host}:#{@port}) failed"
			exit(1)
		end
	end

	# Used to retrieve (get) the database handle
	def dbh
		@@dbh
	end

	def next_counter
		@counter += 1
		@counter %= 65535
	end

	def send_event(event, args={}, &block)
		counter = next_counter
		args['ActionID'] = counter
		@outputqueue << "Action: #{event}\r\n"+args.to_a.map{|a| a.join(': ')}.join("\r\n")+"\r\n\r\n"
		if block_given?
			@callbacks[counter] = EventServerCallback.new(event, args, counter, caller, block)
		end
	end

	def shutdown
		@shutdown = true
	end

	def shutdown?
		@shutdown
	end
end

module ServerEventFactory
	@events ||= {}

	def self.add_event(event, klass)
		@events[event] = klass
	end

	def self.eventfactory(eventserver,  eventstring)
		category, type = eventstring.split("\n", 2)[0].strip.split(': ')

		if category == 'Event'
			if klass = @events[type]
				klass.new(eventserver, eventstring)
			else
				raise ServerEvent::UnknownEventError,
					"Unknown event #{type}", caller
			end
		elsif category == 'Response'
			ServerReply.new(eventserver, eventstring)
		else
			raise ServerEvent::UnknownEventError,
				"Unknown event category #{category}", caller
		end
	end
end

# Class that represents a response to a previously sent command
class ServerReply
	def initialize(eventserver, event)
		@eventserver = eventserver
		@original_event = event
		event.split("\n").each do |l|
			key, value = l.split(': ').map{|x| x.strip}
			next unless value
	
			value = nil if value == '(null)'

			instance_variable_set("@#{key.downcase.gsub('-', '_')}", value) if key and !key.empty?
		end
	end

	def success?
		@response == 'Success'
	end

	def method_missing(symbol, *args)
		# convert the result of #instance_variables to symbols for 1.9 compatability
		if args.empty? and instance_variables.map{|x| x.to_sym}.include? "@#{symbol}".to_sym
			instance_variable_get "@#{symbol}".to_sym
		else
			raise NoMethodError, "No method #{symbol} found", caller
		end
	end

	def dispatch
		if callback = @eventserver.callbacks[@actionid.to_i]
			catch :return do
				callback.call(self)
			end
		end
	end
end

# Base class that represents an event sent by the server
# You MUST inherit from this class for the event to be parsed and handled.

class ServerEvent

	class InvalidEventSpecificationError < StandardError; end
	class InvalidEventError < StandardError; end
	class UnknownEventError < StandardError; end

	attr_accessor :recursing
	attr_reader :eventserver

	def self.inherited(klass)
		if md = /^([A-Za-z]+)ServerEvent$/.match(klass.name)
			ServerEventFactory.add_event(md[1], klass)
		else
			raise InvalidEventSpecificationError,
				"The class #{klass.name} is not named according to the <EventName>ServerEvent style", caller
		end
	end
	
	def initialize(eventserver, event)
		@eventserver = eventserver
		event.split("\n").each do |l|
			key, value = l.strip.split(': ')
			instance_variable_set("@#{key.downcase.gsub('-', '_')}", value) if key and !key.empty? and value
		end
	end
end

class AgentsServerEvent < ServerEvent
	def dispatch
		agent = Agent.new(@eventserver, @agent.to_i, nil)
		agent.name = @name
		begin
			case @status
			when 'AGENT_LOGGEDOFF'
				agent.state = Agent::OFFLINE

			when 'AGENT_IDLE'
				@eventserver.send_event('AgentLogoff', {'Agent'=>agent.id, 'Soft'=>'true'}) do |reply|
					if reply.success?
						agent.state = Agent::OFFLINE
					else
						Cpxlog.err "CBX thinks agent #{agent} does not exist: logging them off (#{reply.message})"
						agent.logoff
					end
				end

			when 'AGENT_ONCALL'
				# get the info about the person they're talking to..
				@eventserver.send_event('GetVars', {'Channel'=>@talkingto, 'Variables'=>'UNIQUEID,BRANDID,CALLTYPE'}) do |reply|
					if reply.success?
						unless call = Caller[reply.uniqueid]
							call = Caller.new(reply.uniqueid, @talkingto)
						end

						call.pickup

						if reply.calltype
							if reply.calltype == 'outcall'
								call.type = :outgoing
							else
								call.type = reply.calltype.to_s
							end
						else
							call.type = :call
							Cpxlog.warning("#{call.uniqueid}: CALLTYPE is not set for existing call, defaulting to 'call'")
						end

						if reply.brandid
							call.set('BRANDID', reply.brandid)
							Cpxlog.notice "Agent #{agent} is talking to #{call} after bootstrap"
						else
							Cpxlog.warning "Agent #{agent} bootstrapped as talkingto to #{call} without BRANDID set"
							# TODO - Should we also attempt to get DNIS so that we can write proper CDR?
						end

						agent.talkingto = call

						# NOTE- call type cannot be checked here in a valid way --
						#   email and voicemail will not actually be a call that we can deduce
					else
						# NOTE- Channel disappeared before we could get the uniqueid, so the call
						#   went down almost immediately on startup.  log the agent off and continue.
						Cpxlog.warning("Call dropped before full bootstrap for agent #{agent}")
						agent.logoff
					end
				end

			when 'AGENT_UNKNOWN'
				agent.state = Agent::UNKNOWN
			end
		rescue Agent::InvalidStateError => e
			raise InvalidEventError, "Tried to set invalid agent state: #{e.message}", caller
		end
	end
end

class QueueEntryServerEvent < ServerEvent
	def dispatch
		if queue = CallQueue[@queue]
			unless call = Caller[@uniqueid]
				call = Caller.new(@uniqueid, @channel)
				call.cdr.add_transaction(CDR::INQUEUE, @queue, Time.now.to_i - @wait.to_i)
			end

			call.enteredqueue = Time.now.to_i - @wait.to_i

			@eventserver.send_event('GetVars', {'Channel'=>call.channel,
							'Variables'=>'BRANDID,CALLERIDNUM,CALLERIDNAME,CALLTYPE,MAILID,VMID,VMFILE'}) do |reply|
				if reply.success?
					call.set('BRANDID', reply.brandid)
					call.set('CALLERIDNUM', reply.calleridnum)
					call.set('CALLERIDNAME', reply.calleridname)
					call.type = reply.calltype.to_sym unless reply.calltype.nil? or reply.calltype.empty?
					call.set('MAILID', reply.mailid)
					call.set('VMID', reply.vmid)
					call.set('VMFILE', reply.vmfile)


					if reply.brandid.nil?
						Cpxlog.warning "BRANDID is not set for bootstrapped #{call}"
					end

					if reply.calltype.nil?
						call.type = :call
						Cpxlog.warning "CALLTYPE is not set for #{call.uniqueid}, defaulting to 'call'"
					end

					Cpxlog.notice "Bootstrapped #{call} in queue #{queue.name} from CBX"
				end
			end

			queue.add_caller(call, @position.to_i)

			Agent.match_agents(:seclevel=>2) do |agent|
				cid = URI.escape(call.get('CALLERIDNAME') + " <" + call.get('CALLERIDNUM') + ">")
				agent.send_event('QUEUECALLER', call.enteredqueue,
								 queue.name, call.uniqueid, queue.callers.index(call), call.get('BRANDID'), cid)
			end
		else
			CallQueue.recover_unknown_queue(self, @queue)
		end
	end
end

class QueueParamsServerEvent < ServerEvent
	def dispatch
		unless queue = CallQueue[@queue]
			queue = CallQueue.new(@queue)
			queue.weight = @weight.to_i
		end

		# always sync this information from DB, so that RELOADQUEUES can update this
		result = @eventserver.dbh.safe_query("SELECT commonname, queuegroup FROM queues where name='#{@eventserver.dbh.quote(@queue)}'")
		commonname, queuegroup = result.fetch_row
		queue.queuegroup = queuegroup
		queue.commonname = commonname


		Agent.match_agents(:seclevel=>2) do |agent|
			agent.send_event('QUEUE', queue.name, queue.queuegroup, queue.weight, 0,
							 queue.completed, queue.abandoned, queue.avg_queuetime, queue.max_queuetime)
		end
	end
end

class MessageWaitingServerEvent < ServerEvent
	def dispatch
		if @brandid
			tenantid = @brandid[0..3].to_i
			brandid = @brandid[4..7].to_i
			raise InvalidEventError, "Voicemail mailbox #{mailbox} can't be parsed into tenant/brand id (#{@brandid[0..3]} #{@brandid[4..7]})", caller if tenantid == 0 or brandid == 0

			result = @eventserver.dbh.safe_query("SELECT dnis FROM brandlist WHERE tenant=#{tenantid} and brand=#{brandid}")
			if result.num_rows == 1
				dnis = result.fetch_row[0]
				result.free
			else
				result.free
				raise InvalidEventError, "Unable to determine DNIS for tenant #{tenantid} with brand #{brandid} (more/less than 1 row returned)", caller
			end

			if call = Caller[@uniqueid]
				queue = (call.cdr.get_last_queue || call.get('QUEUE'))
			else
				Cpxlog.warning "Got MessageWaiting event for unknown call #{@uniqueid}"
				# TODO recover call
			end

			raise InvalidEventError, "Cannot find queue to inject this voicemail into!", caller if queue.nil? or queue.empty?

			call.cdr.add_transaction(CDR::LEFTVOICEMAIL, queue, (Time.now.to_i - @duration.to_i))

			file = File.join(File.dirname(@filename), File.basename(@filename, '.wav'))

			# Sublime hack to allow multiple Variable: lines, ugh
			# This requires that each queue context have the 'queue-inject' context included in it.
			@eventserver.send_event('Originate', {'Channel' => "Local/qinject@IVR-#{dnis}", 'Context' => 'vm-listen',
								  'Exten' => 1, 'Priority' => 1, 'Timeout'=>604800000,
									'Variable' => {
										'TYPE' => 'voicemail',
										'DATE' => Time.now.to_i,
										'VMFILE' => file,
										'BRANDID' => @brandid,
										'QUEUE' => queue,
										'VMID' => @uniqueid,
										'QTIMEOUT' => 604800,
										'IVROPT' => call.get('IVROPT')
										}.map{|x| x.join('=')}.join("\r\nVariable: "),
									'Async' => 1})
		else
			Cpxlog.warning("No brandid passed for #{@brandid}")
		end
	end
end

class QueueMemberServerEvent < ServerEvent
	def dispatch
		if queue = CallQueue[@queue]
			if md = /Agent\/([0-9]{4})/.match(@location)
				if agent = Agent[md[1].to_i]
					unless member = queue.members.detect{|x| x.agent.id == agent.id}
						member = queue.add_member(agent)
					end

					member.callstaken = @callstaken.to_i
					member.lastcall = Time.at(@lastcall.to_i)

				end
			else
				Cpxlog.warning "Invalid queue member location: #{@location}"
			end
		else
			CallQueue.recover_unknown_queue(self, @queue)
		end
	end
end

class VarSetServerEvent < ServerEvent
	def dispatch
		if @variable == 'CALLTYPE' and @value == 'ignore' and call = Caller[@uniqueid]
				Cpxlog.notice("asked to ignore further progress of #{@uniqueid}")
				Caller.remove(call)
				#call.cdr.delete
		elsif call = Caller[@uniqueid]
		  call.set(@variable, @value) unless @value.nil?
		  if @variable == 'BLINDXFERNUMBER'
			Cpxlog.info("@variable == 'BLINDXFERNUMBER'") if @variable == 'BLINDXFERNUMBER'
			Cpxlog.info "Blind Transfer (#{@uniqueid}) to (#{@value})"
			call.cdr.add_transaction(CDR::TRANSFER,"IVRxfer #{@value}")
		  end
		elsif @variable == 'CALLTYPE'
			if ['call', 'email', 'voicemail'].include? @value
				new_call(@value)
			elsif @value == 'outcall'
				new_outgoing_call
			elsif @value == 'warmxfer'
				if md = /^Agent\/([0-9]{4})$/.match(@channel)
					if agent = Agent[md[1].to_i]
						Cpxlog.info("Warm xfer initiated by #{agent.name}")
						agent.warmxferid = @uniqueid
						agent.warmxferchan = @channel
					end
				end
			end
		elsif @variable == 'CHANLOCALSTATUS' and ['CHANUNAVAIL', 'CONGESTION'].include?(@value)
			if md = /([0-9]{4})@sip-agents/.match(@channel)
				if agent = Agent[md[1].to_i] and agent.socket

					if agent.state > Agent::OFFLINE
						# XXX - this is where the Warm Transfer -> Released bug happens
						if agent.talkingto
							if agent.state == Agent::WARMXFER
								Cpxlog.notice "Agent #{agent}'s phone crashed during a warm transfer"
								agent.send_event('BLAB', "Hey, it looks like your phone crashed while you were dialing a 3rd party. You might want to try restarting it.")
							else
								Cpxlog.error "Agent #{agent}'s phone reported as being in state #{@value} when they were offered a call in #{Agent::STATENAMES[agent.state]} state when talking to #{agent.talkingto}"
							end
						else
							Cpxlog.notice "Agent #{agent}'s phone is returning #{@value}; setting them released and notifying supervisor"
							agent.send_event('BLAB', "Your phone is detected as being in #{@value} state. The supervisor is being notified and you are being set as released.")
							agent.pause(true) do
							  begin
								agent.state = Agent::RELEASED
							  rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
								Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{agent.name}"
								Server.instance.terminate_agent(agent)
							  end
							end
						end
					end
					Agent.match_agents(:seclevel=>2) do |a|
						if agent.talkingto and agent.state == Agent::WARMXFER
							# NOOP
						elsif agent.talkingto
							a.send_event('BLAB', "Agent #{agent.name}'s phone is not receiving calls correctly (in #{@value} state). However, this agent is in state #{Agent::STATENAMES[agent.state]} and in call with #{agent.talkingto}. Please alert your friendly neighbourhood system administrator.")
						else
							a.send_event('BLAB', "Agent #{agent.name}'s phone is not receiving calls correctly (in #{@value} state). The agent has been advised and set to released")
						end
					end
				end
			end
		elsif @variable == 'BLINDXFERNUMBER'
			Cpxlog.info "Blind Transfer (#{@uniqueid}) to (#{@value})"
			call.cdr.add_transaction(CDR::TRANSFER,"IVRxfer #{@value}")
		else
			# NOTE- we ignore anything that hits this, we're not watching this channel.
			return
		end
	end

	private

	def new_call(type)
		unless call = Caller[@uniqueid]
			call = Caller.new(@uniqueid, @channel)
			call.cdr.add_transaction(CDR::INIVR)
		end
=begin
		@eventserver.send_event('SIPgetcallid', 'Channel' => @channel) do |reply|
			if reply.success?
				call.sip_callid = reply.callid
				call.sip_fromdomain = reply.fromdomain
				#puts "CALLID: #{reply.message}"
			end
		end
=end
		case type
		when 'email'
			call.type = :email
			@eventserver.send_event('GetVar', {'Channel' => @channel, 'Variable' => 'MAILID'}) do |reply|
				if reply.success?
					call.set(reply.variable, reply.value)
					Cpxlog.info "#{call} with ID #{reply.value} created"
				else
					Cpxlog.warning "Missing MAILID for #{call}"
				end
			end
		when 'voicemail'
			call.type = :voicemail
			@eventserver.send_event('GetVars', {'Channel' => @channel, 'Variables' => 'VMFILE,VMID'}) do |reply|
				if reply.success?
					if reply.vmfile
						call.set('VMFILE', reply.vmfile)
						Cpxlog.info("#{call} with ID #{reply.vmid} created")
					else
						Cpxlog.warning("#{call} does not have VMFILE set")
					end

					if reply.vmid
						call.set('VMID', reply.vmid)
						result = @eventserver.dbh.safe_query("SELECT brandid, cidname, cidnum FROM vmrecord WHERE uniqueid='#{reply.vmid}'")

						if result.num_rows == 1
							bid, cidname, cidnum = result.fetch_row
							#puts "setting call details for voicemail #{bid}, #{cidname}, #{cidnum}"
							call.set('BRANDID', bid) unless call.get('BRANDID') or bid.empty?
							call.set('CALLERIDNAME', cidname)
							call.set('CALLERIDNUM', cidnum)
						end
						result.free

						# NOTE: we're lazy, deal with it
						if call.ringingto
							cid = URI.escape(call.get('CALLERIDNAME') + " <" + call.get('CALLERIDNUM') + ">")
							Agent[call.ringingto].send_event('CALLINFO', call.get('BRANDID'), call.type, cid)
						end
					end
				else
						Cpxlog.warning "#{call} does not have VMID set"
				end
			end
		else
			call.type = :call
		end
	end

	def new_outgoing_call
		if md = /^Agent\/([0-9]{4})$/.match(@channel)
			if agent = Agent[md[1].to_i]
				agent.dialoutid = @uniqueid


				Cpxlog.info "Agent #{agent} dialing outbound to #{agent.dialoutnum} on #{@uniqueid}"
			else
				Cpxlog.err "Invalid Agent ID: #{md[1]}"
				return
			end
		else
			Cpxlog.err "Invalid Agent Channel: #{@channel}"
			return
		end
	end
end

class JoinServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid]
			call.unlinkedby = :unknown

			if call.ringingto
			  begin
				Agent[call.ringingto].state = Agent[call.ringingto].paused? ? Agent::RELEASED : Agent::IDLE if Agent[call.ringingto].state == Agent::RINGING
			  rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
				Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{Agent[call.ringingto].name}"
				Server.instance.terminate_agent(Agent[call.ringingto])
			  end
			  Cpxlog.debug "Agent #{call.ringingto} is no longer ringing with #{call}, it returned to queue"
			  call.ringingto = nil
			end

			call.abandon = false
			if queue = CallQueue[@queue]
				queue.add_caller(call, @position.to_i)
				call.transfered = false
				call.enteredqueue = Time.now.to_i
				Cpxlog.info "#{call} joined queue #{queue}"
				cid = URI.escape(call.get('CALLERIDNAME') + " <" + call.get('CALLERIDNUM') + ">")
				Agent.match_agents(:seclevel=>2){|a| a.send_event('QUEUECALLER', call.enteredqueue, queue.name, call.uniqueid, call.type, queue.callers.index(call), call.get('BRANDID'), cid)}
				call.cdr.add_transaction(CDR::INQUEUE, @queue)
			else
				CallQueue.recover_unknown_queue(self, @queue)
			end
		else
			# TODO Maybe we should be watching this one?
			Cpxlog.err "Unknown call #{@uniqueid} joined queue: #{@queue}"
		end
	end
end

class LeaveServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid]
	
			#if call.ringingto
#				Agent[call.ringingto].state = Agent::IDLE
				#call.ringingto = nil
			#end

			if queue = CallQueue[@queue]
				queue.remove_caller call
				queuetime = Time.now.to_i - call.enteredqueue
				queue.add_queuetime(queuetime)
				queue.add_completed unless call.abandoned?
				Cpxlog.info "#{call} #{call.abandoned? ? 'abandoned from' : 'left'} queue #{queue} after #{queuetime} seconds"
				Agent.match_agents(:seclevel=>2){|a| a.send_event('QUEUECALLERREM', queue.name, call.uniqueid, queue.completed, queue.abandoned, queue.avg_queuetime, queue.max_queuetime)}
			else
				CallQueue.recover_unknown_queue(self, @queue)
			end
		else
			# TODO Maybe we should (still) be watching this one?
			Cpxlog.err "Unknown call #{@uniqueid} left queue: #{@queue}"
		end
	end
end

class LinkServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid1]
			call.unlinkedby = :unknown
			call.pickup
			call.transfered = false

			unless call.get('MIXMONITOR_FILENAME')
				Cpxlog.info("Mixmonitoring #{call.uniqueid} (#{@channel1})")
				@eventserver.send_event('MixMonitor', 'Filename'=>"#{call.uniqueid}.wav", 'Channel'=>@channel1)
			end


			# Clear call.ringingto -- someone got it.
			if call.ringingto
				call.ringingto = nil
			end

			if bridgepeer = (call.get("REALBRIDGEPEER") || call.get("BRIDGEPEER"))
				id = bridgepeer.split('/')[1].to_i
				if agent = Agent[id]
					agent.talkingto = call

					agent.pause(true) do
						# only set the state if we're logged in
						# If we don't set the state, we still set the talkingto association so 
						# the agent's state can be recovered when they log back in
						if agent.state > Agent::OFFLINE
							Cpxlog.info "Agent #{agent} linked with #{call}"
							begin
							  agent.state = Agent::ONCALL
							rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
							  Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{agent.name}"
							  Server.instance.terminate_agent(agent)
							end
						else
							Cpxlog.err "Agent #{agent} linked with #{call} while logged off"
						end
					end

					if call.type == :email
						emurl = ServerConfig::EMAILURL.sub('_ID_', call.get('MAILID').to_s)
						agent.send_event('URL', emurl)
					end

					call.cdr.add_transaction(CDR::INCALL, agent.id)
					cid = URI.escape(call.get('CALLERIDNAME') + " <" + call.get('CALLERIDNUM') + ">")
					Agent.match_agents(:seclevel=>2){|a| a.send_event('BRIDGECALLER', agent.id, call.uniqueid, call.get('BRANDID'), cid)}
				elsif t = call.cdr.get_last_transaction(CDR::TRANSFER) and t[:data].include?('blind')
					Cpxlog.info "#{call} linked with blind transfer(#{t[:data].split(' ')[1]})"
				else
					Cpxlog.err "#{call} bridged to invalid agent: #{id}"
				end
			else
				# TODO Use a VarGet here to attempt to re-retrieve BRIDGEPEER ?
				Cpxlog.err "#{call} has no BRIDGEPEER or REALBRIDGEPEER set, no agent to tag?"
			end

		elsif agent = Agent.detect{|x| x.dialoutid == @uniqueid1}
			Cpxlog.info "Creating new call on #{@uniqueid2} dialed by #{agent} on channel #{@uniqueid1}"
			@eventserver.send_event('MixMonitor', 'Filename'=>"#{@uniqueid2}.wav", 'Channel'=>@channel2)

			call = Caller.new(@uniqueid2, @channel2, agent.state == Agent::PRECALL)

			if agent.state == Agent::PRECALL
				call.cdr.add_transaction(CDR::CDRINIT, nil, agent.statetime)
				call.cdr.add_transaction(CDR::PRECALL, nil, agent.statetime)
				call.cdr.uniqueid = call.uniqueid
			end

			call.type = :outgoing
			call.pickup
			agent.dialoutid = nil
=begin
			@eventserver.send_event('SIPgetcallid', 'Channel' => @channel2) do |reply|
				if reply.success?
					call.sip_callid = reply.callid
					call.sip_fromdomain = reply.fromdomain
				end
			end
=end
			@eventserver.send_event('SetVar', 'Variable'=>'CALLTYPE', 'Value'=>'outcall', 'Channel'=>@channel2)

			agent.pause(true) do
			  begin
				agent.state = Agent::OUTGOINGCALL
			  rescue Agent::InvalidStateChangeError => e
				Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{agent.name}"
				Server.instance.terminate_agent(agent)
			  end
			end
			call.set('BRANDID', agent.dialedclient)
			call.cdr.add_transaction(CDR::DIALOUTGOING, agent.dialedclient)
			agent.talkingto = call
			call.cdr.add_transaction(CDR::INOUTGOING, @callerid2)
			call.cdr.set_info(:DialedNumber, @callerid2)
			Agent.match_agents(:seclevel=>2){|a| a.send_event('BRIDGECALLER', agent.id, call.uniqueid, call.get('BRANDID'), agent.dialoutnum)}

		elsif agent = Agent.detect{|x| x.warmxferid == @uniqueid1}
			agent.talkingto.unlinkedby = :unknown
			Cpxlog.info "Linked 3rd party callout for #{agent.talkingto} to #{agent.warmxfernum} by  Agent #{agent}"
			@eventserver.send_event('MixMonitor', 'Filename'=>"#{agent.talkingto.origuniqueid}-warmxfer-#{@uniqueid2.split('.', 2)[1]}.wav", 'Channel'=>@channel2)
			agent.warmxferchan = @channel2
			agent.warmxferid = @uniqueid2
			agent.warmxferlinked = true

		else
			# TODO Maybe we should (still) be watching this one?
			#Cpxlog.err("Unknown call (#{@uniqueid1}) bridged to agent?")
		end
	end
end

class UnlinkServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid1] 
			if agent = Agent.detect{|a| a.talkingto == call and a.state != Agent::WRAPUP} or bridgepeer = (call.get("REALBRIDGEPEER") || call.get("BRIDGEPEER"))
				if !agent
					id = bridgepeer.split('/')[1].to_i
					agent = Agent[id]
				end

				if agent
					if defined? @unlinkedby
						if @unlinkedby == call.uniqueid or @unlinkedby == call.origuniqueid
							call.unlinkedby = :caller
							Cpxlog.info "#{call} unlinked itself from Agent #{agent}"
						else
							call.unlinkedby = :agent
							Cpxlog.info "Agent #{agent} unlinked itself from #{call}"
						end
					end

					if agent.talkingto == call and agent.socket
						if agent.state == Agent::ONCALL
							agent.pause(true) do
							  begin
								agent.state = Agent::WRAPUP
							  rescue Agent::InvalidStateChangeError => e
								Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{agent.name}"
								Server.instance.terminate_agent(agent)
							  end
							  call.cdr.add_transaction(CDR::INWRAPUP, agent.id)
							end
						end

						Agent.match_agents(:seclevel=>2){|a| a.send_event('UNBRIDGECALLER', agent.id, call.uniqueid)}
					elsif !agent.socket
						# NOTE - attempt to recover the end of the call to the best of our knowledge
						Cpxlog.warning("Agent #{agent} hungup in the middle of talking to #{call} when not logged in, writing 0 length wrapup time")
						call.cdr.add_transaction(CDR::INWRAPUP, agent.id)
						call.cdr.add_transaction(CDR::ENDWRAPUP, agent.id)
						call.cdr.add_transaction(CDR::CDREND)
					else
						# NOTE - what the hell?
						Cpxlog.err("Unexpected call association between #{agent} and #{call}")
					end
				elsif t = call.cdr.get_last_transaction and t[:transaction] == CDR::TRANSFER and t[:data].include?('blind')
					if @unlinkedby == call.uniqueid or @unlinkedby == call.origuniqueid
						call.unlinkedby = :caller
						Cpxlog.info "#{call} unlinked itself from blind transfer(#{t[:data].split(' ')[1]})"
					else
						call.unlinkedby = :thirdparty
						Cpxlog.info "Blind transfer(#{t[:data].split(' ')[1]}) unlinked itself from #{call}"
					end
				else
					Cpxlog.info "Hello Mr. EdgeCase #{call}"
				end
			else
				Cpxlog.err("#{call} has no BRIDGEPEER or REALBRIDGEPEER set, no agent to tag?)")
			end
		elsif call = Caller[@uniqueid2] and call.type == :outgoing
			if agent = Agent.detect{|x| x.talkingto == call} and agent.state == Agent::OUTGOINGCALL
				if defined? @unlinkedby
					if @unlinkedby == call.uniqueid or @unlinkedby == call.origuniqueid
						call.unlinkedby = :caller
						Cpxlog.info "#{call} unlinked itself from Agent #{agent}"
					else
						call.unlinkedby = :agent
						Cpxlog.info "Agent #{agent} unlinked itself from #{call}"
					end
				end
				agent.pause(true) do
				  begin
					agent.state = Agent::WRAPUP
				  rescue Agent::InvalidStateChangeError => e
					Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{agent.name}"
					Server.instance.terminate_agent(agent)
				  end
				  call.cdr.add_transaction(CDR::INWRAPUP, agent.id)
				  # TODO - do we need to do anything else here?
				end
				Agent.match_agents(:seclevel=>2){|a| a.send_event('UNBRIDGECALLER', agent.id, call.uniqueid)}
			end
		elsif call = Caller[@uniqueid2]
			Cpxlog.info "WARMXFER #{call} unlinked?"
			if call.uniqueids.include? @unlinkedby
				Cpxlog.info "#{call} unlinked itself from 3rd party"
				call.unlinkedby = :caller
			else
				Cpxlog.info "3rd party unlinked itself from #{call}"
				call.unlinkedby = :thirdparty
			end

		else
			# TODO Maybe we should have been watching this one?
			#Cpxlog.err("Unknown call (#{@uniqueid1}) unlinked from unknown agent")
		end
	end
end

class QueueCallerAbandonServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid]

			if @eventserver.shutdown? and [:email, :voicemail].include? call.type
				# don't mark email/voicemail as abandoned so we can re-inject it later
				Cpxlog.notice "Not marking #{call} as abandoned from queue because CBX is shutting down"
				return
			end

			return if call.hangupreason # don't treat this as abandoned
			if queue = CallQueue[@queue]
				
				if call.transfered?

					call.transfered = false
					queuetime = Time.now.to_i - call.enteredqueue
					queue.add_queuetime(queuetime)
					queue.add_completed
					Cpxlog.info "#{call} was transfered out of queue #{queue}"
				else
					if call.ringingto
					  begin
						Agent[call.ringingto].state = Agent[call.ringingto].paused? ? Agent::RELEASED : Agent::IDLE if Agent[call.ringingto].state == Agent::RINGING
					  rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
						Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{Agent[call.ringingto].name}"
						Server.instance.terminate_agent(Agent[call.ringingto])
					  end
					  Cpxlog.debug "Agent #{call.ringingto} is no longer ringing with #{call}, it abandoned from queue #{queue}"
					  call.ringingto = nil
					end

					call.cdr.add_transaction(CDR::ABANDONQUEUE, @queue)
					call.abandon
					queue.add_abandoned

					Cpxlog.info "#{call} abandoned from queue #{queue} after #{Time.now.to_i - call.enteredqueue} seconds"
				end
				queue.remove_caller(call)
				Agent.match_agents(:seclevel=>2) do |a|
					a.send_event('QUEUECALLERREM', @queue, @uniqueid, queue.completed, queue.abandoned, queue.avg_queuetime, queue.max_queuetime)
				end
			else
				CallQueue.recover_unknown_queue(self, @queue)
			end

		else
			# TODO Some information would have been nice here..
			Cpxlog.err("Unknown call (#{@uniqueid}) dropped from queue #{@queue}")
		end
	end
end

class RenameServerEvent < ServerEvent
	def dispatch
		if call = Caller[@newname]
			Cpxlog.info("#{call} was renamed and replaced by a new channel with the uniqueid of #{@uniqueid}")
			call.uniqueid = @uniqueid
		elsif agent = Agent.detect{|x| x.warmxferchan == @newname}
			Cpxlog.info("Warm XFER call #{agent.warmxferid} was renamed and replaced by a new channel with the uniqueid of #{@uniqueid}")
			agent.warmxferid = @uniqueid
		end
	end
end

class ParkedCallServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid]
			call.park
		end
	end
end

class ParkedCallGiveUpServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid]
			call.hangup
			if agent = Agent.detect{|x| x.talkingto == call}
				Cpxlog.info("#{call} on channel #{@channel} parked by #{agent} hung up while parked")
				agent.send_event('BLAB', 'Caller hung-up, sorry')
				agent.pause(true) do
				  begin
					agent.state = Agent::WRAPUP
				  rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
					Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{agent.name}"
					Server.instance.terminate_agent(agent)
				  end
				  # TODO - park abandon state?
				  call.cdr.add_transaction(CDR::WARMXFERFAILED, 'caller hangup')
				  call.cdr.add_transaction(CDR::INWRAPUP, agent.id)
				end
			end
		elsif call = Caller.callers.detect{|x| x.uniqueids.include? @uniqueid}
			# this is probably a call that got un-parked manually, hopefully by app_bridge
			# we assume this to be the case because the current uniqueid isn't the same as the
			# one the parked call reports, but it's one of its previous ids
			call.unpark
		end
	end
end

class HangupServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid]
			
			if @eventserver.shutdown? and [:email, :voicemail].include? call.type
				# don't mark email/voicemail as abandoned so we can re-inject it later
				Cpxlog.notice "Not marking #{call} as hungup because CBX is shutting down"
				return
			end

			Cpxlog.debug "#{call} hungup"

			if call.type == :voicemail and call.get('VMFILE')
				Dir["#{call.get('VMFILE')}.*"].each do |f|
					fdest = f.sub('queued', 'processed')
					if File.file?(f) and File.directory?(File.dirname(fdest))
						File.rename(f, f.sub('queued', 'processed'))
					else
						Cpxlog.error("File #{f} or directory #{File.dirname(fdest)}")
					end
				end
			end
			call.hangup # store the fact that the call is hungup
			# Clear call.ringingto -- caller hung up?
			if call.ringingto
			    begin
				  Agent[call.ringingto].state = Agent[call.ringingto].paused? ? Agent::RELEASED : Agent::IDLE if Agent[call.ringingto].state == Agent::RINGING
				rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
				  Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{Agent[call.ringingto].name}"
				  Server.instance.terminate_agent(Agent[call.ringingto])
				end
				Cpxlog.debug "Agent #{call.ringingto} is no longer ringing with #{call}, it hungup from queue"
				call.ringingto = nil
			end

			# someone hung up on it with a vengence
			if call.hangupreason
				call.cdr.add_transaction(CDR::ENDCALL, call.hangupreason)
				call.cdr.add_transaction(CDR::CDREND)

			# make sure this is not a call now in wrapup
			elsif agent = Agent.detect{|a| a.talkingto == call}
				if agent.state == Agent::WRAPUP
					call.cdr.add_transaction(CDR::ENDCALL)
				elsif agent.state == Agent::WARMXFER
					agent.pause(true) do
					  begin
						agent.state = Agent::WRAPUP
					  rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
						Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{agent.name}"
						Server.instance.terminate_agent(agent)
					  end
					  call.cdr.add_transaction(CDR::INWRAPUP, agent.id)
					end
					call.cdr.add_transaction(CDR::ENDCALL)
				end

				if call.unlinkedby == :agent
					Cpxlog.info "Agent #{agent} hung up on #{call}"
				elsif call.unlinkedby == :caller
					# TODO - caller hang up on 3rd party
					Cpxlog.info "#{call} hung up on Agent #{agent}"
				else
					Cpxlog.info "#{call} <-> #{agent} hangup"
				end
			else
				# Possibly the agent has ended wrapup...

				# if the call is not complete, make it so now
				if !call.pickedup? and !call.cdr.complete? and
					!([CDR::ABANDONQUEUE, CDR::LEFTVOICEMAIL].include?(call.cdr.get_last_transaction[:transaction]))
					call.cdr.add_transaction(CDR::ABANDONIVR, 'Abandoned in IVR')
					call.cdr.add_transaction(CDR::CDREND)
					Cpxlog.info "#{call} abandoned in IVR"
				elsif !call.cdr.complete?
					unless ([CDR::ABANDONQUEUE, CDR::LEFTVOICEMAIL].include?(call.cdr.get_last_transaction[:transaction]))
						call.cdr.add_transaction(CDR::ENDCALL) 
					end
					call.cdr.add_transaction(CDR::CDREND)
				end
			end

		# outgoing call failed (we hungup with no link or unlink)
		elsif agent = Agent.detect{|x| x.dialoutid == @uniqueid}
			if agent.state == Agent::OUTGOINGCALL
				call = Caller.new(@uniqueid, @channel)
				call.set('BRANDID', agent.dialedclient)
				call.cdr.add_transaction(CDR::DIALOUTGOING, agent.dialedclient)
				call.type = :outgoing
				call.cdr.add_transaction(CDR::FAILEDOUTGOING, @cause_txt)
				call.cdr.add_transaction(CDR::INWRAPUP, agent.id)
				call.hangup
				agent.talkingto = call
				begin
				  agent.state = Agent::WRAPUP
				rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
				  Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{agent.name}"
				  Server.instance.terminate_agent(agent)
				end
				agent.send_event('BLAB', "Call failed: #{@cause_txt}")
				Cpxlog.info "Outbound call to #{agent.dialoutnum} by #{agent} failed with error #{@cause_txt}"
			end
		elsif agent = Agent.detect{|x| x.warmxferchan == @channel}
			agent.warmxferchan = nil
			agent.warmxferid = nil
			agent.warmxferlinked = false

			agent.talkingto.cdr.add_transaction(CDR::WARMXFERFAILED, "3rd party hungup while talking to agent") if agent.talkingto

			if agent.state == Agent::WARMXFER
				Cpxlog.info "Warmxfer to 3rd party at #{agent.warmxfernum} by Agent #{agent} hung up before being bridged to #{agent.talkingto}"
				agent.send_event('ASK', 'yesno', '3rd party was disconnected, do you want to re-connect to the caller?') do |reply|
					if reply.data == 'yes'
						@eventserver.send_event("Redirect", 'Channel'=>agent.talkingto.channel,
							'Exten'=>agent.id, 'Context'=>'sip-agents', 'Priority'=>1) do |reply|
							#agent.pause(false)
						end
					else
						agent.send_event('WARMXFERFAILED')
					end
				end
			end
		elsif agent = Agent.detect{|x| x.warmxferid == @uniqueid}
			agent.warmxferchan = nil
			agent.warmxferid = nil

			agent.talkingto.cdr.add_transaction(CDR::WARMXFERFAILED, "Failed to dial 3rd party")

			if agent.state == Agent::WARMXFER
				Cpxlog.info "Agent #{agent} calling 3rd party at #{agent.warmxfernum} for warm xfer to #{agent.talkingto} failed with error #{@cause_txt}"
				agent.send_event('BLAB', "Failed to call 3rd party: #{@cause_txt}")
			end
		end
	end
end

class AgentCalledServerEvent < ServerEvent
	def dispatch
		if call = Caller[@peeruniqueid]
			if md = /Agent\/([0-9]{4})$/.match(@agentcalled)
				if agent = Agent[md[1].to_i] and agent.socket and agent.state > Agent::OFFLINE
					# clear ringing state
					if call.ringingto and call.ringingto != agent.id
					    begin
						  Agent[call.ringingto].state = Agent[call.ringingto].paused? ? Agent::RELEASED : Agent::IDLE if Agent[call.ringingto].state == Agent::RINGING
						rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
						  Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{Agent[call.ringingto].name}"
						  Server.instance.terminate_agent(Agent[call.ringingto])
						end
					end

					call.ringingto = agent.id
					Cpxlog.debug "Agent #{agent} is ringing with #{call}"
					begin
						agent.state = Agent::RINGING
						call.cdr.add_transaction(CDR::RINGING, agent.id)
					rescue Agent::InvalidStateChangeError => e
						Cpxlog.err "GORTHAG SMASH #{agent} TO SAVE REST OF PEEPLES."
						Agent.match_agents(:seclevel=>2) do |a|
							a.send_event('BLAB', "Agent #{agent.name}'s state inappropiately changed from #{Agent::STATENAMES[agent.state]} to Ringing. This isn't their fault, but they had to be disconnected to preserve consistancy")
						end
						agent.logoff
					end

					cid = URI.escape(call.get('CALLERIDNAME') + " <" + call.get('CALLERIDNUM') + ">")
					agent.send_event('CALLINFO', call.get('BRANDID'), call.type, cid)

					if !ServerConfig::ACKCALL
						tenantid = call.get('BRANDID')[0..3].to_i.to_s
						brandid = call.get('BRANDID')[4..-1].to_i.to_s
						unless agent.lastpop == call.origuniqueid
							crmpopurl = ServerConfig::CRMPOPURL.sub('_TENANTID_', tenantid).sub('_BRANDID_', brandid).sub('_CALLID_', call.origuniqueid).sub('_ITXT_', call.get('IVROPT').to_s)

							crmpopurl << "&case=#{call.get('CASEID')}"if call.get('CASEID')
							crmpopurl << "&ani=#{call.get('CALLERIDNUM')}"if call.get('CALLERIDNUM')

							agent.send_event('URL', crmpopurl)
							agent.lastpop = call.origuniqueid
						end
					end
				elsif agent = Agent[md[1].to_i]
					Cpxlog.warning "Call offer: peeruniqueid #{@peeruniqueid} for #{@agentcalled} is for a logged-off agent, trying to pause them"
					agent.pause(true)
				else
					# TODO -- eh?
					Cpxlog.warning "Call offer: peeruniqueid #{@peeruniqueid} for #{@agentcalled} doesn't match an agent?"
				end
			else
				Cpxlog.warning "Call offer: no match for #{@agentcalled}"
			end
		else
			# TODO
			Cpxlog.warning "Call offer, but not tracking it correctly?"
		end
	end
end

class AgentPickupServerEvent < ServerEvent
	def dispatch
		if agent = Agent.detect{|x| x.id == @agent.to_i}
			if ServerConfig::ACKCALL
				if call = Caller.callers.detect{|x| x.ringingto == agent.id}
					tenantid = call.get('BRANDID').to_s[0..3].to_i.to_s
					brandid = call.get('BRANDID').to_s[4..-1].to_i.to_s
					unless agent.lastpop == call.origuniqueid
						crmpopurl = ServerConfig::CRMPOPURL.sub('_TENANTID_', tenantid).sub('_BRANDID_', brandid).sub('_CALLID_', call.origuniqueid).sub('_ITXT_', call.get('IVROPT').to_s)

						crmpopurl << "&case=#{call.get('CASEID')}"if call.get('CASEID')
						crmpopurl << "&ani=#{call.get('CALLERIDNUM')}"if call.get('CALLERIDNUM')

						Cpxlog.debug "Agent #{agent} picked up #{call}"

						agent.send_event('URL', crmpopurl)
						agent.lastpop = call.origuniqueid
					end
				else
					# TODO ?
					Cpxlog.warning "No URL pop for #{agent}, call kinda broken."
				end
			end
		else
			# TODO ?
			Cpxlog.warning "AgentPickup fired, but no agent?"
		end
	end
end

class QueueBreakoutServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid]
			# clear ringingto state, nobody is gonna get it
			if call.ringingto
			    begin
				  Agent[call.ringingto].state = Agent[call.ringingto].paused? ? Agent::RELEASED : Agent::IDLE if Agent[call.ringingto].state == Agent::RINGING
				rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
				  Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{Agent[call.ringingto].name}"
				  Server.instance.terminate_agent(Agent[call.ringingto])
				end
				Cpxlog.debug "Agent #{call.ringingto} is no longer ringing with #{call}, it broke out of queue"
				call.ringingto = nil
			end
		end
	end
end

class SystemShutdownServerEvent < ServerEvent
	def dispatch
		@eventserver.shutdown
		if @restart == 'True'
			Cpxlog.notice "CBX is restarting (up since #{@eventserver.startuptime})"
		else
			Cpxlog.notice "CBX is shutting down (up since #{@eventserver.startuptime})"
		end
	end
end

class QueueNoAnswerServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid]
			# clear ringingto state, nobody is gonna get it
			if call.ringingto
			    begin
				  Agent[call.ringingto].state = Agent[call.ringingto].paused? ? Agent::RELEASED : Agent::IDLE if Agent[call.ringingto].state == Agent::RINGING
				rescue Agent::InvalidStateError,Agent::InvalidStateChangeError => e
				  Cpxlog.warning "#{e.class.name}: #{e.message}, Terminating agent #{Agent[call.ringingto].name}"
				  Server.instance.terminate_agent(Agent[call.ringingto])
				end
				Cpxlog.debug "Agent #{call.ringingto} is no longer ringing with #{call}, it went back into queue after no answer"
				call.ringingto = nil
			end
		end
	end
end

=begin We don't really need this anymore, UnlinkedBy is more generic
class AgentCompleteServerEvent < ServerEvent
	def dispatch
		if call = Caller[@uniqueid]
			if md = /Agent\/([0-9]{4})$/.match(@membername) and agent = Agent[md[1].to_i]
				if @reason == 'agent'
					Cpxlog.info "Agent #{agent} hung up on #{call}"
				else
					Cpxlog.info "Caller #{call} hung up on #{agent}"
				end
			end
		end
	end
end
=end

=begin
class OriginateResponseServerEvent < ServerEvent
	def dispatch
		Cpxlog.notice "Where's the call with #{@actionid}?"
		if call = Caller.callers.detect{|x| x.get('ACTIONID') == @actionid}
			Cpxlog.notice "found #{call}"
			call.uniqueid = @uniqueid
		end
	end
end

class DialServerEvent < ServerEvent
	def dispatch
		if call = Caller[@srcuniqueid]
			Cpxlog.notice "#{call} dialing outbound on #{@destuniqueid} to #{call.get('OUTGOINGNUMBER')}"
		end
	end
end
=end
