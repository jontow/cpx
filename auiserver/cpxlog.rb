
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

# Logging class that mimics the Syslog API. Can log to stdout, syslog or
# remote syslog (and maybe someday a file).

module Cpxlog
	# severities
	LOG_EMERG = 0
	LOG_ALERT = 1
	LOG_CRIT = 2
	LOG_ERR = 3
	LOG_WARNING = 4
	LOG_NOTICE = 5
	LOG_INFO = 6
	LOG_DEBUG = 7

	SEVERITIES = {
		0 => 'EMERG',
		1 => 'ALERT',
		2 => 'CRITICAL',
		3 => 'ERROR',
		4 => 'WARNING',
		5 => 'NOTICE',
		6 => 'INFO',
		7 => 'DEBUG'
	}

	COLORS = {
		0 => "\e[1;5;31m",
		1 => "\e[0;5;31m",
		2 => "\e[1;31m",
		3 => "\e[0;31m",
		4 => "\e[1;33m",
		5 => "\e[1;32m",
		6 => "\e[0;32m",
		7 => "\e[0;36m"
	}

	class << self
		def open
			case ServerConfig::LOG_BACKEND
			when :syslog
				require 'syslog'
				Syslog.open('auiserver')
				@backend = Syslog
			when :remotesyslog
				require File.join(File.dirname(__FILE__), 'contrib/remotesyslog/remotesyslog')
				RemoteSyslog.open(ServerConf::REMOTE_SYSLOG, 'auiserver')
				@backend = RemoteSyslog
			when :file
				# TODO - implement this
				STDERR.puts 'Logging to a file is not currently supported'
			end

			if ServerConfig::LOG_CONSOLE
				@logconsole = true
				@consoleloglevel = 6
				@colorconsolelog = ServerConfig::LOG_COLOR
				if @colorconsolelog
					STDOUT.puts "\e[1;34m#{Time.now.strftime('%H:%M:%S')} \e[1;35m#{'SYSTEM'.center(8)}\e[0m Console logging is #{COLORS[@consoleloglevel]}#{SEVERITIES[@consoleloglevel]}\e[0m and above (change with SIGINFO)"
				else
					STDOUT.puts "#{Time.now.strftime('%H:%M:%S')} #{'SYSTEM'.center(8)} Console logging is #{SEVERITIES[@consoleloglevel]} and above (change with SIGINFO)"
				end
			end
		end

		def increment_log_level
			@consoleloglevel = (@consoleloglevel + 1) % 8
			if @colorconsolelog
				STDOUT.puts "\e[1;31m#{Time.now.strftime('%H:%M:%S')} \e[1;35m#{'SYSTEM'.center(8)}\e[0m Console logging is now #{COLORS[@consoleloglevel]}#{SEVERITIES[@consoleloglevel]}\e[0m and above"
			else
				STDOUT.puts "#{Time.now.strftime('%H:%M:%S')} #{'SYSTEM'.center(8)} Console logging is now #{SEVERITIES[@consoleloglevel]} and above"
			end
		end

		def format_with_caller(log)
				c = caller.detect{|x| ! x.include? __FILE__ }.split(':')[0..1].join(':').sub(/^\.\//, '')
				"#{c} #{log}"
		end

		def log(priority, message, *args)
			begin
			@backend.log(priority, format_with_caller(message), *args) if @backend
			log_to_console(priority, message, *args) if @logconsole
			rescue => e
			end
		end

		def log_to_console(priority, message, *args)
			return if @consoleloglevel < priority
			message = sprintf(message, *args) unless args.empty?
			if @colorconsolelog
				message.gsub!(/([1-9][0-9]+\.[0-9]+)/, "\e[0;36m\\1\e[0m")
				STDOUT.puts "\e[1;34m#{Time.now.strftime('%H:%M:%S')} #{COLORS[priority]}#{SEVERITIES[priority].center(8)}\e[0m #{message}"
			else
				STDOUT.puts "#{Time.now.strftime('%H:%M:%S')} #{SEVERITIES[priority].center(8)} #{message}"
			end
		end

		def emerg(*args)
			log(LOG_EMERG, *args)
		end

		alias emergency emerg

		def alert(*args)
			log(LOG_ALERT, *args)
		end

		def crit(*args)
			log(LOG_CRIT, *args)
		end

		alias critical crit

		def err(*args)
			log(LOG_ERR, *args)
		end

		alias error err

		def warning(*args)
			log(LOG_WARNING, *args)
		end

		alias warn warning

		def notice(*args)
			log(LOG_NOTICE, *args)
		end

		def info(*args)
			log(LOG_INFO, *args)
		end

		def debug(*args)
			log(LOG_DEBUG, *args)
		end
	end
end
