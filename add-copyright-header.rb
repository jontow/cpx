files = ARGV
header = File.read('header')

files.each do |file|
	contents = File.read(file)
	
	if (md = /^=begin license/.match(contents))
		puts "skipping already licensed file #{file}"
		next
	end

	fileheader = ''
	if x = contents.split("\n")[0][0] and x.chr == '#'
		fileheader << contents.split("\n")[0]
		contents = contents.split("\n", 2)[1]
	end

	
	File.open(file, 'w') do |f|
		f.puts fileheader unless fileheader.empty?	
		f.puts header
		f.puts contents
	end
end
