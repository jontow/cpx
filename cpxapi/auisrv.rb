
=begin license
 * Copyright (c) 2006-2008, Fused Solutions, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Fused Solutions, LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Fused Solutions, LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fused Solutions, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=end

require 'socket'
require 'thread'
require 'digest/md5'
require 'cgi'
require 'json'

require 'mysql'

$LOAD_PATH << File.dirname(__FILE__)

require 'constants/agentconstants'
require 'constants/cdrconstants'
require 'constants/protocol'

begin
	load 'conf/cpxapi.conf'
rescue LoadError
	STDERR.puts "Missing configuration file conf/cpxapi.conf"
	exit(-1)
end

module CpxXMLListing
	def to_xml(*args)
		doc = REXML::Document.new
		name = self[0].class.to_s.split('::')[-1]
		root = doc.add_element('CpxData')
		unless self.empty?
			queues = root.add_element(name+'s')
			self.each do |x|
				x.to_xml(queues)
			end
		end
		doc.to_s
	end
end

Thread.abort_on_exception = true
module CpxAPI

	class InvalidEvent < StandardError
	end

	class CpxAgent
		include AgentConstants

		def self.calculate_duration(i, skew, precision=2)
			seconds = (Time.now.to_i - (i + skew).abs).to_i
			if seconds > 60
				minutes = seconds / 60
				seconds %= 60
			end

			if minutes and minutes > 60
				hours = minutes / 60
				minutes %= 60
			end

			if hours and hours > 24
				days = hours / 24
				hours %= 24
			end
			#and so on, if we ever needed it...

			# Beware the ternary operator, for it is subtle and quick to error
			x = []
			x << "#{seconds} second#{seconds != 1 ? 's' : ''}"
			x << "#{minutes} minute#{minutes != 1 ? 's' : ''}" if minutes
			x << "#{hours} hour#{hours != 1 ? 's' : ''}" if hours
			x << "#{days} day#{days != 1 ? 's' : ''}" if days

			if precision > x.length
				x.reverse.join(' ')
			else
				x[precision*-1..-1].reverse.join(' ')
			end
		end

		attr_reader :id, :name
		attr_accessor :time, :state, :profile, :calltime, :wrapuptime, :idletime, :talkingto
		def initialize(id, name)
			@id = id
			@name = name
			@calltime = 0
			@wrapuptime = 0
			@idletime = 0
		end

		def state=(state)
			if state <= IDLE or state == IDLE or state == RELEASED
				@talkingto = nil
			end
			@state = state
		end

		def to_json(*a)
			{
				:json_class => self.class.name,
				:id => @id,
				:name => @name,
				:state => @state,
				:statename => STATENAMES[@state],
				:profile => @profile,
				:calltime => @calltime,
				:idletime => @idletime,
				:talkingto => @talkingto
			}.to_json(*a)
		end

		def to_xml(doc)
			a = doc.add_element('CpxAgent')
			att = {
				'id' => @id.to_s,
				'name' => @name,
				'state' => @state.to_s,
				'statename' => STATENAMES[@state],
				'profile' => @profile.to_s,
				'calltime' => @calltime.to_s,
				'idletime' => @idletime.to_s
			}

			a.add_attributes(att)

			if @talkingto
				c = a.add_element('TalkingTo')
				@talkingto.to_xml(c)
			end

		end
	end

	class CpxQueue
		attr_reader :name, :group, :members, :callers
		attr_accessor :calls, :abandoned, :completed, :avg_time, :max_time, :weight
		def initialize(name, group)
			@name = name
			@group = group
			@members = []
			@callers = []
			@weight = 0
		end

		def add_member(agent)
			@members << agent
		end

		def remove_member(agent)
			@members.delete(agent)
		end

		def add_caller(call, position)
			@callers.insert(position, call)
		end

		def remove_caller(call)
			@callers.delete(call)
		end

		def to_json(*a)
			{
				:json_class => self.class.name,
				:name => @name,
				:group => @group,
				:members => @members,
				:callers => @callers,
				:weight => @weight,
				:calls => @calls,
				:abandoned => @abandoned,
				:completed => @completed,
				:avg_time => @avg_time,
				:max_time => @max_time
			}.to_json(*a)
		end

		def to_xml(doc)
			q = doc.add_element('CpxQueue')
			q.add_attributes({ 
				'name' => @name,
				'group' => @group.to_s,
				'weight' => @weight.to_s,
				'calls' => @calls.to_s,
				'abandoned' => @abandoned.to_s,
				'completed' => @completed.to_s,
				'avg_time' => @avg_time.to_s,
				'max_time' => @max_time.to_s
			})
			
			unless @callers.empty?
				callers = q.add_element('Callers')
				@callers.each do |c|
					c.to_xml(callers)
				end
			end
	
			unless @members.empty?
				members = q.add_element('Members')
				@members.each do |m|
					m.to_xml(members)
				end
			end
		end
	end

	class CpxCaller
		attr_reader :uniqueid
		attr_accessor :enteredqueue, :type, :brandid, :callerid, :leftqueue, :queue, :abandoned
		def initialize(uniqueid)
			@uniqueid = uniqueid
		end

		def to_json(*a)
			{
				:json_class => self.class.name,
				:uniqueid => @uniqueid,
				:enteredqueue => @enteredqueue,
				:leftqueue => @leftqueue,
				:type => @type,
				:brandid => @brandid,
				:callerid => @callerid,
				:queue => @queue ? @queue.name : nil
			}.to_json(*a)
		end

		def to_xml(doc)
			call = doc.add_element('CpxCaller')
			call.add_attributes({
				'uniqueid' => @uniqueid.to_s,
				'enteredqueue' => @enteredqueue.to_s,
				'leftqueue' => @leftqueue.to_s,
				'type' => @type.to_s,
				'brandid' => @brandid.to_s,
				'callerid' => @callerid.to_s,
				'queue' => @queue ? @queue.name : ""
			})
		end
	end

	class CpxProfile
		attr_reader :id, :name
		def initialize(id, name)
			@id = id
			@name = name
		end

		def to_xml(doc)
			pro = doc.add_element('CpxProfile')
			att = {
				'id' => @id.to_s,
				'name' => @name
			}
			pro.add_attributes(att)
		end
	end

	#wrap this all in a nice class...
	class Main

		CALLTYPE_PRIORITIES = [:call, :voicemail, :email]

		class KickAssError < StandardError; end

		attr_reader :agents, :brandlist, :queuelist, :profiles, :queues, :queuegroups, :calls
		def initialize

			@counter = 0
			@outgoing = Queue.new
			@incoming = Queue.new

			@sent_events = {}
			@resent_events = {}

			@reply_callbacks = {}

			@agents = {}

			@brandlist = {}
			@queuelist = {}
			@profiles = []

			@queues = {}
			@queuegroups = {}

			@calls = {}

			@timeskew = 0
		end

		#wrapper for incrementing the counter
		def next_count
			@counter += 1
			@counter %= 65535 #make the counter wrap at (2**16)-1
		end

		#handle a login attempt
		def login

			begin
				@connection = TCPSocket.new(CpxAPIConf::HOST, CpxAPIConf::PORT)
			rescue Errno::ECONNREFUSED, Errno::EBADF, Errno::EINVAL, Errno::EPROTONOSUPPORT
				STDERR.puts 'no server listening'
				return
			rescue SocketError => e

				STDERR.puts "Socket Error: #{e.message}"
				return
			end

			if result = select([@connection], nil, nil, 1)
				line = @connection.gets
				if line =~ /^Agent Server/
					puts "Connected to agent server"
					@connection.puts "Protocol: #{AgentProtocol::MAJOR}.#{AgentProtocol::MINOR}"
					resp = @connection.gets
					code, message = resp.chomp.split(" ",2)
					case code.to_i
					when 0
						# NOOP
					when 1
						STDERR.puts message
					when 2
						STDERR.puts message
						return
					else
						STDERR.puts "Invalid response code in protocol handshake"
						return
					end
				else
					@connection.close
					STDERR.puts "Unexpected server response: #{line}"
					return
				end
			else
				@connection.close
				STDERR.puts "timed out waiting for server response"
				return
			end

			@connection.puts("GETSALT 1")
			line = @connection.gets
			event, counter, data = line.strip.split(' ', 3)
			begin
				salt = Integer(data)
			rescue ArgumentError
				STDERR.puts "Server returned an invalid salt: #{data}"
				return
			end

			saltedpass = Digest::MD5.hexdigest("#{salt}#{Digest::MD5.hexdigest(CpxAPIConf::PASSWORD)}")
			@connection.puts("LOGIN 2 #{CpxAPIConf::USERNAME}:#{saltedpass}")
			line = @connection.gets
			event, counter, data = line.strip.split(' ', 3)
			if event == 'ACK' and counter == '2'
				tier, profile, timestamp = data.split(' ')

				@timeskew = Time.now.to_i - timestamp.to_i
				bootstrap
				listen
			elsif event == 'ERR'
				@connection.close
				STDERR.puts "Received error when logging in #{data}"
				return
			else
				@connection.close
				STDERR.puts "Received unexpected event #{line}, disconnecting"
				return
			end
		end

		def bootstrap
			send('AGENTS', 'ALL')
			send('QUEUES', 'ALL') do |a, b, c|
				send('QUEUEMEMBERS', 'ALL')
				send('QUEUECALLERS', 'ALL')
				send('BRIDGEDCALLERS', 'ALL')
				send('QUEUENAMES') do |result, data, event|
					if result
						data[1..-2].split('),(').each do |e|
							queuename,commonname = e.split('|')
							@queuelist[queuename] = commonname
						end
					end
					send('QUEUEGROUPS') do |result, data, event|
						data.split(' ').map{|x| x.split(':', 2)}.each {|groupid, grouplabel| @queuegroups[groupid.to_i] = grouplabel }
						send('BRANDLIST') do |result, data, event|
							data[1..-2].split('),(').map{|x| x.split('|')}.sort_by{|x| [x[1].downcase]}.each do |brandid, brandlabel|
								@brandlist[brandid] = brandlabel
							end

							send('PROFILES') do |result, data, event|
								data.split(' ').map{|x| x.split(':', 2)}.each do |profileid, profilelabel|
									@profiles << CpxProfile.new(profileid, profilelabel)
								end
								bootstrap_old_calls
							end
						end
					end
				end
			end

		end

		def bootstrap_old_calls
			cutoff = (Time.now - 86400).to_i #(CpxAPIConf::TREND_INTERVAL * CpxAPIConf::TREND_SAMPLES)).to_i
			dbh = Mysql.real_connect(CpxAPIConf::MYSQL_CREDENTIALS[:host],
															 CpxAPIConf::MYSQL_CREDENTIALS[:username],
															 CpxAPIConf::MYSQL_CREDENTIALS[:password])
			dbh.select_db(CpxAPIConf::MYSQL_CREDENTIALS[:database])
			result = dbh.query("SELECT UniqueID, Data, Start FROM billing_transactions WHERE Transaction=#{CDRConstants::CDRINIT} AND End > #{cutoff}")
			while row = result.fetch_row
				next if @calls[row[0]]
				call = CpxCaller.new(row[0])
				call.type = row[1]

				res2 = dbh.query("SELECT Data, Start, End FROM billing_transactions WHERE UniqueID='#{row[0]}' AND Transaction=#{CDRConstants::INQUEUE}")
				if res2.num_rows == 1
					row2 = res2.fetch_row
					call.queue = @queues[row2[0]]
					call.enteredqueue = row2[1].to_i
					call.leftqueue = row2[2].to_i
				else
					call.enteredqueue = row[2].to_i
					call.leftqueue = row[2].to_i
				end
				res2.free

				res2 = dbh.query("SELECT Data FROM billing_transactions WHERE UniqueID='#{row[0]}' AND (Transaction=#{CDRConstants::ABANDONQUEUE} OR Transaction=#{CDRConstants::ABANDONIVR})")
				if res2.num_rows == 1
					call.abandoned = true
				end
				res2.free

				@calls[row[0]] = call
			end
			result.free
		end

		#are we already connected?
		# TODO - should the connection be killed after each login failure?
		def listening?
			return @listen || false
		end

		# send an event to the server
		def send(command, *data, &callback)
			count = next_count
			str = "#{command} #{count} #{data.join(' ')}"
			@sent_events[count] = {:time=>(Time.now.to_i)+300, :event=>str}
			@reply_callbacks[count] = callback
			@outgoing.enq str
		end

		class Callback
			attr_reader :event
			def initalize
				@called = false
			end

			def call(result, data, event)
				@called = true
				@event = event
			end

			def called?
				@called
			end
		end
		
		def send_blocking(command, *data)
			count = next_count
			str = "#{command} #{count} #{data.join(' ')}"
			@sent_events[count] = {:time=>(Time.now.to_i)+300, :event=>str}
			@outgoing.enq str
			callback = Callback.new
			@reply_callbacks[count] = callback
			loop do
				if callback.called?
					break
				else
					Thread.pass
				end
			end
			callback.event
		end

		# acknowledge an event
		def ack(counter, data)
			@outgoing.enq "ACK #{counter} #{data}"
		end

		# error in response to a bad event
		def err(counter, error)
			@outgoing.enq "ACK #{counter} #{error}"
		end

		#route an incoming event/reply/error
		#pulls items off the incoming queue
		def route_incoming(event)
			command, counter, data = event.split(' ', 3)

			if ['ACK', 'ERR'].include? command
				@sent_events.delete(counter.to_i)
				@resent_events.delete(counter.to_i)
				result = (command == 'ACK')
				if @reply_callbacks[counter.to_i]
					@reply_callbacks[counter.to_i].call(result, data, event)
					@reply_callbacks.delete(counter.to_i)# remove the callback
				end
			else
				begin
					#puts "[1;35m#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}[0;0m [1;32m#{command}[0;0m #{counter} #{data}"
					res = handle_event(command, counter, data)
					res ||= data
				rescue StandardError => e
					puts e.message
					puts e.backtrace
					err(counter, e.message)
				else
					ack(counter, res)
				end
			end
		end

		def handle_event(command, counter, data)
			case command
			when 'STATE'
				time, id, name, state, profile, calltime, wrapuptime, idletime = data.split(' ')
				return if name == CpxAPIConf::USERNAME

				# remove logged out agents
				if state.to_i < CpxAgent::IDLE
					if agent = @agents[id.to_i]
						@agents.delete(id.to_i)
						@queues.each do |x,q|
							q.remove_member(agent)
						end
					end
				elsif !(agent = @agents[id.to_i])
					agent = CpxAgent.new(id.to_i, name)
					@agents[id.to_i] = agent
				end

				agent.time = time.to_i
				agent.state = state.to_i
				agent.profile = profile.to_i
				agent.calltime = calltime.to_i
				agent.wrapuptime = wrapuptime.to_i
				agent.idletime = idletime.to_i

			when 'QUEUE'
				name, group, weight, calls, completed, abandoned, avg_time, max_time = data.split(' ')
				unless queue = @queues[name]
					queue = CpxQueue.new(name, group.to_i)
					@queues[name] = queue
				end

				queue.calls = calls.to_i
				queue.weight = weight.to_i
				queue.completed = completed.to_i
				queue.abandoned = abandoned.to_i
				queue.avg_time = avg_time.to_i
				queue.max_time = max_time.to_i

			when 'QUEUEMEMBER'
				id, name, statetime, state, queues = data.split(' ', 5)
				return if name == CpxAPIConf::USERNAME
				#if agent = @agents[id.to_i] #or state.to_i >= CpxAgent::IDLE
				agent = @agents[id.to_i]
				unless agent
					agent = CpxAgent.new(id.to_i, name)
					@agents[id.to_i] = agent
					agent.profile = 0
				end

				agent.state = state.to_i
				agent.time = statetime.to_i
				if queues.nil? or queues.empty?
					queues = []
				else
					queues = queues.split(':')
				end

				@queues.each do |x,q|
					if queues.include? q.name
						q.add_member(agent)
					else
						q.remove_member(agent)
					end
				end
				#end

			when 'QUEUECALLER'
				time, name, uniqueid, type, position, brandid, callerid = data.split(' ')
				if queue = @queues[name]

					unless call = @calls[uniqueid]
						call = CpxCaller.new(uniqueid)
					end

					cutoff = (Time.now - 86400).to_i
					# trim old calls out
					@calls = Hash[*(@calls.select{|id,c| c.enteredqueue > cutoff or !c.leftqueue}.flatten)]

					#p call, queue
					unless queue.callers.detect {|c| c.uniqueid == uniqueid}
						@calls[uniqueid] = call
						call.queue = queue
						call.enteredqueue = time.to_i
						call.type = type
						call.brandid = brandid
						call.callerid = callerid
						queue.calls += 1
						queue.add_caller(call, position.to_i)
					end
				else
					puts "Could not find queue #{queue}"
				end

			when 'QUEUECALLERREM'
				name, uniqueid, completed, abandoned, avg_queuetime, max_queuetime = data.split(' ')
				if queue = @queues[name]
					if call = @calls[uniqueid]
						#p call, queue
						call.leftqueue = Time.now.to_i
						queue.remove_caller(call)
						queue.calls -= 1
						queue.completed = completed.to_i
						if abandoned.to_i == queue.abandoned + 1
							call.abandoned = true
						end
						queue.abandoned = abandoned.to_i
						queue.avg_time = avg_queuetime.to_i
						queue.max_time = max_queuetime.to_i
					else
						puts "Could not find call #{uniqueid}"
					end
				else
					puts "Could not find queue #{queue}"
				end

			when 'BRIDGECALLER'
				agentid, uniqueid, brandid, callerid = data.split(' ')
				if agent = @agents[agentid.to_i] and call = @calls[uniqueid]
					agent.talkingto = call
				elsif agent and  agent.state == CpxAgent::OUTGOINGCALL
					call = CpxCaller.new(uniqueid)
					@calls[uniqueid] = call
					call.type = 'outgoing'
					call.brandid = brandid
					call.callerid = callerid
					# stub out these values so stuff works
					call.enteredqueue = Time.now.to_i
					call.leftqueue = Time.now.to_i

					agent.talkingto = call
				elsif agent
					call = CpxCaller.new(uniqueid)
					@calls[uniqueid] = call
					call.brandid = brandid
					call.callerid = callerid
					call.enteredqueue = Time.now.to_i
					call.leftqueue = Time.now.to_i

					send('CALLINFO', uniqueid) do |result, data, event|
						brandid, type, callerid, extra = data.split(' ', 4)
						call.type = type
					end

					agent.talkingto = call
				end

				#when 'UNBRIDGECALLER'
				#agentid, uniqueid = data.split(' ')
				#if agent = @agents[agentid.to_i]
				#end
				#

			else
				#puts "#{command} #{counter} #{data}"
			end

		end

		#listen on the connection
		def listen

			@listen = true

			catch :disconnect do
				loop do

					if result = select([@connection], nil, nil, 0.25)
						if socket = result[0][0]
							begin
								string = socket.gets
							rescue Errno::ECONNRESET
								throw :disconnect
							end
							if string
								route_incoming(string.chomp)
							else
								throw :disconnect
							end
						end
					end

					while !@outgoing.empty?
						string = @outgoing.shift
						begin
							@connection.puts string
						rescue Errno::EPIPE
							throw :disconnect
						end
					end

					# TODO - is this the best place for this? I'd prefer not to
					# have *another thread for this

					#check dead events and resend them and put them in the resent
					#events list
					check_dead_events(@sent_events) do |e|
						STDERR.puts "resending event \"#{e}\" because it's older than 10 seconds"
						command, counter, data = e.split(' ', 3)
						newcount = next_count
						str = "#{command} #{newcount} #{data}"
						@outgoing.enq str
						@resent_events[newcount] = {:time=>(Time.now.to_i)+600, :event=>str}
					end

					#check resent events for dead ones
					check_dead_events(@resent_events) do |e|
						#a resent event has failed to return an ack in 60 seconds.
						#this is very bad and indicates something shit itself
						#somewhere along the line.
						STDERR.puts "Event \"#{e}\" was resent and failed to return after 60 seconds"
						# TODO - freak out
					end
				end
			end

			STDERR.puts 'connection died!'
			sleep 5
			login

			#end

		end

		#abstraction for checking for dead events
		def check_dead_events(events, &resendcallback)
			if events.length > 0 #only bother to check if we've got events
				#select any expired events
				events.select{|k,v| v[:time] < Time.now.to_i}.each do |e|
					events.delete(e[0]) #delete them from this queue
					resendcallback.call(e[1][:event]) #execute the callback
				end
			end
		end
	end
end

