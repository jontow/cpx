
Merb::Router.prepare do |r|
	r.match(%r[^/(xml|json)/(.+)]).
		to(:controller => '[2]', :format => '[1]')

	r.match('/originate/::/:brand/:number/:target').
		to(:controller => 'originate', :action => 'to_[1]')
	r.match('/kick/:agent').
		to(:controller => 'kick')
	r.match('/endwrapup/:agent').
		to(:controller => 'endwrapup')
	r.match('/setstate/:agent/:state').
		to(:controller => 'setstate')
	r.match('/setprofile/:agent/:profile').
		to(:controller => 'setprofile')
	r.match('/notice/:agent/:message').
		to(:controller => 'notice')
	r.match('/sendurl/:agent/:url').
		to(:controller => 'sendurl')
end

