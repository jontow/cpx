require 'rubygems'

require 'thin'
require 'merb-core'
require 'json'
require 'rexml/document'

require 'auisrv'

$main = CpxAPI::Main.new
Thread.new do
	$main.login
end

require 'routes'
require 'controllers'

Merb::Logger.new(STDOUT)
#Merb.add_mime_type(:json, :to_json, %w[application/json text/x-json])
Merb.add_mime_type(:text, :to_s, %w[text/plain])
Merb.add_mime_type(:json, :to_json, %w[text/plain])
Merb.add_mime_type(:xml,  :to_xml,  %w[application/xml text/xml application/x-xml], :Encoding => "UTF-8")
#Merb.add_mime_type(:html, :to_html, %w[text/html application/xhtml+xml application/html])

Thin::Server.start('0.0.0.0', 3000) do
	use Rack::CommonLogger
	use Rack::ShowExceptions
	#map "/" do
		use Rack::Lint
		run Merb::Rack::Application.new
	#end
end
