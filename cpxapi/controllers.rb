
class AuthenticatedController < Merb::Controller
	clear_provides
	before :authenticate

	def authenticate
		user = basic_authentication.authenticate do |username, password|
			#User.authenticate(username, password)
			password == 'bdd'
		end

		if user
			@current_user = user
		else
			basic_authentication.request
		end
	end
end

class Queues < AuthenticatedController
	provides :xml, :json
	def index
		display $main.queues.values.extend(CpxXMLListing)
	end
end

class Agents < AuthenticatedController
	provides :xml, :json
	def index
		display $main.agents.values.extend(CpxXMLListing)
	end
end

class Profiles < AuthenticatedController
	provides :xml, :json
	def index
		display $main.profiles.extend(CpxXMLListing)
	end
end

class Calls < AuthenticatedController
	provides :xml, :json
	def index
		display $main.calls.values.extend(CpxXMLListing)
	end
end

########################################################################

class Originate < AuthenticatedController
	provides :text
	def to_queue
		display $main.send_blocking('ADIAL', @request.params[:brand], @request.params[:number],  'queue', @request.params[:target])
	end

	def to_agent
		display $main.send_blocking('ADIAL', @request.params[:brand], @request.params[:number],  'agent', @request.params[:target])
	end
end

class Kick < AuthenticatedController
	provides :text
	def index
		display $main.send_blocking('KICK', @request.params[:agent])
	end
end

class Notice < AuthenticatedController
	provides :text
	def index
		# TODO - unescape message parameter
		display $main.send_blocking('ABLAB', @request.params[:agent], @request.params[:message])
	end
end

class Sendurl < AuthenticatedController
	provides :text
	def index
		# TODO - this doesn't work right at all, due to routing/escaping issues. Fix it sometime.
		display $main.send_blocking('AURL', @request.params[:agent], @request.params[:url])
	end
end

class Endwrapup < AuthenticatedController
	provides :text
	def index
		#only_provides :text
		display $main.send_blocking('AENDWRAPUP', @request.params[:agent])
	end
end

class Setstate < AuthenticatedController
	provides :text
	def index
		display $main.send_blocking('ASTATE', @request.params[:agent], @request.params[:state])
	end
end

class Setprofile < AuthenticatedController
	provides :text
	def index
		display $main.send_blocking('APROFILE', @request.params[:agent], @request.params[:profile])
	end
end
